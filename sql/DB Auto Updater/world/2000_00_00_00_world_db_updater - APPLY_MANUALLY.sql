DROP TABLE IF EXISTS `updates`;
CREATE TABLE `updates` (
   `update` VARCHAR(255) NOT NULL COMMENT 'Filename of the update',
   `applied` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date when the update was applied.',
   PRIMARY KEY (`update`)
 ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='DB versioning information (Used by auto-updater)';
 