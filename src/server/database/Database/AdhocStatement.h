/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _ADHOCSTATEMENT_H
#define _ADHOCSTATEMENT_H

#include "SQLOperation.h"
#include "QueryResult.h"

#include <memory>

class BasicStatementTask : public SQLOperation
{
    typedef std::unique_ptr< QueryResultPromise > QueryResultPromisePtr;

public:
    BasicStatementTask( const char* sql, bool async = false );
    ~BasicStatementTask();

    bool                    Execute() override;
    QueryResultFuture       GetFuture() const;

private:
    const char*             m_sql;

    QueryResultPromisePtr   m_result;
};

#endif