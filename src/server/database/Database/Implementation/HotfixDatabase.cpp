/*
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "HotfixDatabase.h"

void HotfixDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_HOTFIXDATABASE_STATEMENTS);

    // BattlePetAbility.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_ABILITY, "SELECT Id, TypeId, IconId, RoundCooldown, VisualId, Flags, Name, Description FROM battle_pet_ability ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetAbilityEffect.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_ABILITY_EFFECT, "SELECT Id, AbilityTurnId, VisualId, TriggerAbility, EffectProperty, Unk1, Properties1, Properties2, Properties3, Properties4, Properties5, Properties6 FROM battle_pet_ability_effect ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetAbilityState.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_ABILITY_STATE, "SELECT Id, AbilityId, StateId, Value FROM battle_pet_ability_state ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetAbilityTurn.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_ABILITY_TURN, "SELECT Id, AbilityId, VisualId, Duration, HasProcType, ProcType FROM battle_pet_ability_turn ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetBreedQuality.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_BREED_QUALITY, "SELECT Id, Quality, Multiplier FROM battle_pet_breed_quality ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetBreedState.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_BREED_STATE, "SELECT Id, BreedId, StateId, Multiplier FROM battle_pet_breed_state ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetEffectProperties.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_EFFECT_PROPERTIES, "SELECT Id, Flags, PropertyName1, PropertyName2, PropertyName3, "
        "PropertyName4, PropertyName5, PropertyName6, IsAura1, IsAura2, IsAura3, IsAura4, IsAura5, "
        "IsAura6 FROM battle_pet_effect_properties ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetSpecies.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_SPECIES, "SELECT Id, NpcId, IconId, SpellId, FamilyId, Unk1, Flags, Description, Flavor FROM battle_pet_species ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetSpeciesState.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_SPECIES_STATE, "SELECT Id, SpeciesId, StateId, Modifier FROM battle_pet_species_state ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetSpeciesXAbility.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_SPECIES_XABILITY, "SELECT Id, SpeciesId, AbilityId, RequiredLevel, SlotId FROM battle_pet_species_xabilitity ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetState.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_STATE, "SELECT Id, Unk1, Name, Flags FROM battle_pet_state ORDER BY Id DESC", CONNECTION_SYNCH);

    // BattlePetVisual.db2
    PrepareStatement(HOTFIX_SEL_BATTLE_PET_VISUAL, "SELECT Id, Unk1, Unk2, Unk3, SlotId, Unk4, Unk5, Description FROM battle_pet_visual ORDER BY Id DESC", CONNECTION_SYNCH);

    // BroadcastText.db2
    PrepareStatement(HOTFIX_SEL_BROADCAST_TEXT, "SELECT Id, Language, MaleText, FemaleText, EmoteID1, EmoteID2, EmoteID3, "
        "EmoteDelay1, EmoteDelay2, EmoteDelay3, SoundID, EndEmoteId, Type FROM broadcast_text ORDER BY ID DESC", CONNECTION_SYNCH);

    // Creature.db2
    PrepareStatement(HOTFIX_SEL_CREATURE, "SELECT Id, Item1, Item2, Item3, MountId, DisplayId1, DisplayId2, DisplayId3, DisplayId4, "
        "DisplayId1Propability, DisplayId2Propability, DisplayId3Propability, DisplayId4Propability, Name, SubName, FemaleSubName, "
        "Rank, InhabitType FROM creature ORDER BY Id DESC", CONNECTION_SYNCH);

    // CreatureDifficulty.db2
    PrepareStatement(HOTFIX_SEL_CREATURE_DIFFICULTY, "SELECT Id, CreatureId, FactionId, Expansion, MinLevel, MaxLevel, Flags1, "
        "Flags2, Flags3, Flags4, Flags5 FROM creature_difficulty ORDER BY Id DESC", CONNECTION_SYNCH);

    // Curve.db2
    PrepareStatement(HOTFIX_SEL_CURVE, "SELECT Id, Type, Unk1 FROM curve ORDER BY Id DESC", CONNECTION_SYNCH);

    // CurvePoint.db2
    PrepareStatement(HOTFIX_SEL_CURVE_POINT, "SELECT Id, CurveId, OrderIndex, X, Y FROM curve_point ORDER BY Id DESC", CONNECTION_SYNCH);

    // DeviceBlacklist.db2
    PrepareStatement(HOTFIX_SEL_DEVICE_BLACKLIST, "SELECT Id, Unk1, Unk2 FROM device_blacklist ORDER BY Id DESC", CONNECTION_SYNCH);

    // DriverBlacklist.db2
    PrepareStatement(HOTFIX_SEL_DRIVER_BLACKLIST, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5, Unk6, Unk7 FROM driver_blacklist ORDER BY Id DESC", CONNECTION_SYNCH);

    // GameObjects.db2
    PrepareStatement(HOTFIX_SEL_GAMEOBJECTS, "SELECT Id, MapId, DisplayId, X, Y, Z, Rotation_X, Rotation_Y, Rotation_Z, Rotation_W, "
        "Size, TypeId, Data1, Data2, Data3, Data4, Name FROM gameobject ORDER BY Id DESC", CONNECTION_SYNCH);

    // Item.db2
    PrepareStatement(HOTFIX_SEL_ITEM, "SELECT Id, Class, SubClass, SoundOverrideSubclass, Material, DisplayId, InventoryType, Sheath "
        "FROM item ORDER BY Id DESC", CONNECTION_SYNCH);

    // ItemCurrencyCost.db2
    PrepareStatement(HOTFIX_SEL_ITEM_CURRENCY_COST, "SELECT Id, ItemID FROM item_currency_cost ORDER BY Id DESC", CONNECTION_SYNCH);

    // ItemExtendedCost.db2
    PrepareStatement(HOTFIX_SEL_ITEM_EXTENDED_COST, "SELECT Id, RequiredHonorPoints, RequiredArenaPoints, RequiredArenaSlot, "
        "RequiredItem1, RequiredItem2, RequiredItem3, RequiredItem4, RequiredItem5, "
        "RequiredItemCount1, RequiredItemCount2, RequiredItemCount3, RequiredItemCount4, RequiredItemCount5, "
        "RequiredPersonalArenaRating, ItemPurchaseGroup, "
        "RequiredCurrency1, RequiredCurrency2, RequiredCurrency3, RequiredCurrency4, RequiredCurrency5, "
        "RequiredCurrencyCount1, RequiredCurrencyCount2, RequiredCurrencyCount3, RequiredCurrencyCount4, RequiredCurrencyCount5, "
        "RequiredFactionId, RequiredFactionStanding, RequirementFlags, RequiredAchievement FROM item_extended_cost ORDER BY Id DESC", CONNECTION_SYNCH);

    // Item-sparse.db2
    PrepareStatement(HOTFIX_SEL_ITEM_SPARSE, "SELECT Id, Quality, Flags1, Flags2, Flags3, Unk1, Unk2, BuyCount, BuyPrice, SellPrice, InventoryType, "
        "AllowableClass, AllowableRace, ItemLevel, RequiredLevel, RequiredSkill, RequiredSkillRank, RequiredSpell, RequiredHonorRank, RequiredCityRank, "
        "RequiredReputationFaction, RequiredReputationRank, MaxCount, Stackable, ContainerSlots, "
        "ItemStatType1, ItemStatType2, ItemStatType3, ItemStatType4, ItemStatType5, ItemStatType6, ItemStatType7, ItemStatType8, ItemStatType9, ItemStatType10, "
        "ItemStatValue1, ItemStatValue2, ItemStatValue3, ItemStatValue4, ItemStatValue5, ItemStatValue6, ItemStatValue7, ItemStatValue8, ItemStatValue9, ItemStatValue10, "
        "ItemStatAllocation1, ItemStatAllocation2, ItemStatAllocation3, ItemStatAllocation4, ItemStatAllocation5, "
        "ItemStatAllocation6, ItemStatAllocation7, ItemStatAllocation8, ItemStatAllocation9, ItemStatAllocation10, "
        "ItemStatSocketCostMultiplier1, ItemStatSocketCostMultiplier2, ItemStatSocketCostMultiplier3, ItemStatSocketCostMultiplier4, ItemStatSocketCostMultiplier5, "
        "ItemStatSocketCostMultiplier6, ItemStatSocketCostMultiplier7, ItemStatSocketCostMultiplier8, ItemStatSocketCostMultiplier9, ItemStatSocketCostMultiplier10, "
        "ScalingStatDistribution, DamageType, Delay, RangedModRange, SpellId1, SpellId2, SpellId3, SpellId4, SpellId5, SpellTrigger1, SpellTrigger2, SpellTrigger3, "
        "SpellTrigger4, SpellTrigger5, SpellCharges1, SpellCharges2, SpellCharges3, SpellCharges4, SpellCharges5, SpellCooldown1, SpellCooldown2, SpellCooldown3, "
        "SpellCooldown4, SpellCooldown5, SpellCategory1, SpellCategory2, SpellCategory3, SpellCategory4, SpellCategory5, SpellCategoryCooldown1, SpellCategoryCooldown2, "
        "SpellCategoryCooldown3, SpellCategoryCooldown4, SpellCategoryCooldown5, Bonding, Name, Name2, Name3, Name4, Description, PageText, LanguageID, PageMaterial, "
        "StartQuest, LockID, Material, Sheath, RandomProperty, RandomSuffix, ItemSet, Area, Map, BagFamily, TotemCategory, "
        "SocketColor1, SocketColor2, SocketColor3, SocketBonus, GemProperties, ArmorDamageModifier, Duration, ItemLimitCategory, "
        "HolidayID, StatScalingFactor, CurrencySubstitutionID, CurrencySubstitutionCount FROM item_sparse ORDER BY Id DESC", CONNECTION_SYNCH);

    // ItemToBattlePet.db2
    PrepareStatement(HOTFIX_SEL_ITEM_BATTLE_PET, "SELECT Id, SpeciesId FROM item_battle_pet ORDER BY Id DESC", CONNECTION_SYNCH);

    // ItemToMountSpell.db2
    PrepareStatement(HOTFIX_SEL_ITEM_MOUNT_SPELL, "SELECT Id, MountSpellId FROM item_mount_spell ORDER BY Id DESC", CONNECTION_SYNCH);

    // ItemUpgrade.db2
    PrepareStatement(HOTFIX_SEL_ITEM_UPGRADE, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5 FROM item_upgrade ORDER BY Id DESC", CONNECTION_SYNCH);

    // KeyChain.db2
    PrepareStatement(HOTFIX_SEL_KEY_CHAIN, "SELECT Id, Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Key9, Key10, Key11, Key12, Key13, Key14, Key15, Key16, "
        "Key17, Key18, Key19, Key20, Key21, Key22, Key23, Key24, Key25, Key26, Key27, Key28, Key29, Key30, Key31, Key32 FROM key_chain ORDER BY Id DESC", CONNECTION_SYNCH);

    // Locale.db2
    PrepareStatement(HOTFIX_SEL_LOCALE, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5 FROM locales ORDER BY Id DESC", CONNECTION_SYNCH);

    // Location.db2
    PrepareStatement(HOTFIX_SEL_LOCATION, "SELECT Id, X, Z, Y, Rotation1, Rotation2, Rotation3 FROM location ORDER BY Id DESC", CONNECTION_SYNCH);

    // MapChallengeMode.db2
    PrepareStatement(HOTFIX_SEL_MAP_CHALLENGE_MODE, "SELECT Id, MapId, UnkString1, UnkString2, Unk1, BronzeMedalTimer, SilverMedalTimer, "
        "GoldMedalTimer, UnkString3, UnkString4 FROM map_challenge_mode ORDER BY Id DESC", CONNECTION_SYNCH);

    // MarketingPromotionsXLocale.db2
    PrepareStatement(HOTFIX_SEL_MARKETING_PROMOTIONS_XLOCALE, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5, Unk6, UrlName FROM marketing_promotions_xlocale ORDER BY Id DESC", CONNECTION_SYNCH);

    // Path.db2
    PrepareStatement(HOTFIX_SEL_PATH, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5, Unk6, Unk7 FROM path ORDER BY Id DESC", CONNECTION_SYNCH);

    // PathNode.db2
    PrepareStatement(HOTFIX_SEL_PATH_NODE, "SELECT Id, Unk1, Unk2, Unk3 FROM path_node ORDER BY Id DESC", CONNECTION_SYNCH);

    // PathNodeProperty.db2
    PrepareStatement(HOTFIX_SEL_PATH_NODE_PROPERTY, "SELECT Id, Unk1, Unk2, Unk3, Unk4 FROM path_node_property ORDER BY Id DESC", CONNECTION_SYNCH);

    // PathProperty.db2
    PrepareStatement(HOTFIX_SEL_PATH_PROPERTY, "SELECT Id, Unk1, Unk2, Unk3 FROM path_property ORDER BY Id DESC", CONNECTION_SYNCH);

    // QuestPackageItem.db2
    PrepareStatement(HOTFIX_SEL_QUEST_PACKAGE_ITEM, "SELECT Id, QuestPackageId, ItemID, ItemCount, FilterType FROM quest_package_item ORDER BY Id DESC", CONNECTION_SYNCH);

    // RulesetItemUpgrade.db2
    PrepareStatement(HOTFIX_SEL_RULESET_ITEM_UPGRADE, "SELECT Id, RulesetID, ItemUpgradeId, ItemEntry FROM ruleset_item_updgrade ORDER BY Id DESC", CONNECTION_SYNCH);

    // RulesetRaidLootUpgrade.db2
    PrepareStatement(HOTFIX_SEL_RULESET_RAID_LOOT_UPGRADE, "SELECT Id, RulesetID, RaidDifficulty, ItemUpgradeId, MapId FROM ruleset_raid_loot_updgrade ORDER BY Id DESC", CONNECTION_SYNCH);

    // SceneScript.db2
    PrepareStatement(HOTFIX_SEL_SCENE_SCRIPT, "SELECT Id, Name, Script, PrevSceneScriptId, NextSceneScriptId FROM scene_script ORDER BY Id DESC", CONNECTION_SYNCH);

    // SceneScriptPackage.db2
    PrepareStatement(HOTFIX_SEL_SCENE_SCRIPT_PACKAGE, "SELECT Id, Name FROM scene_script_package ORDER BY Id DESC", CONNECTION_SYNCH);

    // SceneScriptPackageMember.db2
    PrepareStatement(HOTFIX_SEL_SCENE_SCRIPT_PACKAGE_MEMBER, "SELECT Id, ScriptPackageId, ScriptId, OrderIndex, ParentScriptPackageId FROM scene_script_package_member ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellEffectCameraShakes.db2
    PrepareStatement(HOTFIX_SEL_SPELL_EFFECT_CAMERA_SHAKES, "SELECT Id, CameraShakes1, CameraShakes2, CameraShakes3 FROM spell_effect_camera_shakes ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellMissile.db2
    PrepareStatement(HOTFIX_SEL_SPELL_MISSILE, "SELECT Id, Flags, DefaultPitchMin, DefaultPitchMax, DefaultSpeedMin, DefaultSpeedMax, RandomizeFacingMin, RandomizeFacingMax, "
        "RandomizePitchMin, RandomizePitchMax, RandomizeSpeedMin, RandomizeSpeedMax, Gravity, MaxDuration, CollisionRadius FROM spell_missile ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellMissileMotion.db2
    PrepareStatement(HOTFIX_SEL_SPELL_MISSILE_MOTION, "SELECT Id, Name, ScriptBody, Flags, MissileCount FROM spell_missile_motion ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellReagents.db2
    PrepareStatement(HOTFIX_SEL_SPELL_REAGENTS, "SELECT ID, Reagent1, Reagent2, Reagent3, Reagent4, Reagent5, Reagent6, Reagent7, Reagent8, "
        "ReagentCount1, ReagentCount2, ReagentCount3, ReagentCount4, ReagentCount5, ReagentCount6, ReagentCount7, ReagentCount8, "
        "CurrencyID, CurrencyCount FROM spell_reagents ORDER BY ID DESC", CONNECTION_SYNCH);

    // SpellVisual.db2
    PrepareStatement(HOTFIX_SEL_SPELL_VISUAL, "SELECT Id, Unk1, PrecastKit, CastKit, ImpactKit, StateKit, StateDoneKit, ChannelKit, MissileDestinationAttachment, "
        "AnimEventSoundID, Flags, CasterImpactKit, TargetImpactKit, MissileAttachment, MissileTargetingKit, Unk2, InstantAreaKit, ImpactAreaKit, "
        "PersistentAreaKit, MissileCastOffsetX, MissileCastOffsetY, MissileCastOffsetZ, MissileImpactOffsetX, MissileImpactOffsetY, MissileImpactOffsetZ, "
        "Unk3, Unk4, Unk5, Unk6, Unk7 FROM spell_visual ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellVisualEffectName.db2
    PrepareStatement(HOTFIX_SEL_SPELL_VISUAL_EFFECT_NAME, "SELECT Id, FileName, AreaEffectSize, Scale, MinAllowedScale, MaxAllowedScale, Unk1, "
        "Unk2, Unk3 FROM spell_visual_effect_name ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellVisualKit.db2
    PrepareStatement(HOTFIX_SEL_SPELL_VISUAL_KIT, "SELECT Id, StartAnimId, AnimId, Unk1, AnimKitId, HeadEffect, ChestEffect, BaseEffect, LeftHandEffect, "
        "RightHandEffect, BreathEffect, LeftWeaponEffect, RightWeaponEffect, SpecialEffects1, SpecialEffects2, SpecialEffects3, WorldEffect, SoundId, "
        "CameraShakeId, Unk2, Unk3, Unk4, CharParamZero1, CharParamZero2, CharParamZero3, CharParamZero4, CharParamOne1, CharParamOne2, CharParamOne3, "
        "CharParamOne4, CharParamTwo1, CharParamTwo2, CharParamTwo3, CharParamTwo4, CharParamThree1, CharParamThree2, CharParamThree3, CharParamThree4, "
        "Flags FROM spell_visual_kit ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellVisualKitAreaModel.db2
    PrepareStatement(HOTFIX_SEL_SPELL_VISUAL_KIT_AREA_MODEL, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5, Unk6 FROM spell_visual_kit_area_model ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellVisualKitModelAttach.db2
    PrepareStatement(HOTFIX_SEL_SPELL_VISUAL_KIT_MODEL_ATTACH, "SELECT Id, ParentSpellVisualKitId, SpellVisualEffectNameId, AttachmentId, OffsetX, OffsetY, "
        "OffsetZ, Yaw, Pitch, Roll, Unk1, Unk2, Unk3, Unk4, Unk5 FROM spell_visual_kit_model_attach ORDER BY Id DESC", CONNECTION_SYNCH);

    // SpellVisualMissile.db2
    PrepareStatement(HOTFIX_SEL_SPELL_VISUAL_MISSILE, "SELECT Id, Unk1, Unk2, Unk3, Unk4, Unk5, Unk6, Unk7, Unk8, Flags, Unk10, "
        "Unk11, Unk12, Unk13, Unk14, Unk15, Unk16 FROM spell_visual_missile ORDER BY Id DESC", CONNECTION_SYNCH);

    // Vignette.db2
    PrepareStatement(HOTFIX_SEL_VIGNETTE, "SELECT Id, Name, IconId, Flags, Unk1, Unk2 FROM vignette ORDER BY Id DESC", CONNECTION_SYNCH);
}
