/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_OBJECT_H
#define SF_OBJECT_H

#include "Common.h"
#include "Position.h"
#include "GridReference.h"
#include "ObjectDefines.h"
#include "RefCounter.h"
#include "Map.h"
#include "UpdateFields.h"
#include "MovementInfo.h"
#include "ModelIgnoreFlags.h"

#define CONTACT_DISTANCE            0.5f
#define INTERACTION_DISTANCE        5.0f
#define ATTACK_DISTANCE             5.0f
#define INSPECT_DISTANCE            28.0f
#define TRADE_DISTANCE              11.11f

#define MAX_VISIBILITY_DISTANCE     SIZE_OF_GRIDS           // max distance for visible objects
#define SIGHT_RANGE_UNIT            50.0f
#define DEFAULT_VISIBILITY_DISTANCE 90.0f                   // default visible distance, 90 yards on continents
#define DEFAULT_VISIBILITY_INSTANCE 170.0f                  // default visible distance in instances, 170 yards
#define DEFAULT_VISIBILITY_BGARENAS 533.0f                  // default visible distance in BG/Arenas, roughly 533 yards

#define DEFAULT_PLAYER_BOUNDING_RADIUS      0.388999998569489f     // player size, also currently used (correctly?) for any non Unit world objects
#define DEFAULT_PLAYER_COMBAT_REACH         1.5f
#define MIN_MELEE_REACH                     2.0f
#define NOMINAL_MELEE_RANGE                 5.0f
#define MELEE_RANGE                         (NOMINAL_MELEE_RANGE - MIN_MELEE_REACH * 2) //center to center for players

#define DEFAULT_PHASE               169

enum TempSummonType
{
    TEMPSUMMON_TIMED_OR_DEAD_DESPAWN       = 1,             // despawns after a specified time OR when the creature disappears
    TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN     = 2,             // despawns after a specified time OR when the creature dies
    TEMPSUMMON_TIMED_DESPAWN               = 3,             // despawns after a specified time
    TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT = 4,             // despawns after a specified time after the creature is out of combat
    TEMPSUMMON_CORPSE_DESPAWN              = 5,             // despawns instantly after death
    TEMPSUMMON_CORPSE_TIMED_DESPAWN        = 6,             // despawns after a specified time after death
    TEMPSUMMON_DEAD_DESPAWN                = 7,             // despawns when the creature disappears
    TEMPSUMMON_MANUAL_DESPAWN              = 8              // despawns when UnSummon() is called
};

enum NotifyFlags
{
    NOTIFY_NONE                     = 0x00,
    NOTIFY_AI_RELOCATION            = 0x01,
    NOTIFY_VISIBILITY_CHANGED       = 0x02,
    NOTIFY_ALL                      = 0xFF
};

class Corpse;
class Creature;
class CreatureAI;
class DynamicObject;
class GameObject;
class InstanceScript;
class Player;
class Scenario;
class TempSummon;
class Transport;
class MotionTransport;
class Unit;
class UpdateData;
class ByteBuffer;
class WorldObject;
class WorldPacket;
class ZoneScript;

typedef std::unordered_map<Player*, UpdateData> UpdateDataMapType;

namespace UpdateMask
{
    typedef uint32 BlockType;

    enum DynamicFieldChangeType : uint8
    {
        VALUE_CHANGED           = 0x7F,
        VALUE_AND_SIZE_CHANGED  = 0x80
    };

    inline std::size_t GetBlockCount(std::size_t bitCount)
    {
        using BitsPerBlock = std::integral_constant<std::size_t, sizeof(BlockType) * 8>;
        return (bitCount + BitsPerBlock::value - 1) / BitsPerBlock::value;
    }

    inline std::size_t EncodeDynamicFieldChangeType(std::size_t blockCount, DynamicFieldChangeType changeType, uint8 updateType)
    {
        return blockCount | ((changeType & VALUE_AND_SIZE_CHANGED) * ((3 - updateType /*this part evaluates to 0 if update type is not VALUES*/) / 3));
    }

    template<typename T>
    inline void SetUpdateBit(T* data, std::size_t bitIndex)
    {
        static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value, "Type used for SetUpdateBit data arg is not an unsigned integer");
        using BitsPerBlock = std::integral_constant<std::size_t, sizeof(T) * 8>;
        data[bitIndex / BitsPerBlock::value] |= T(1) << (bitIndex % BitsPerBlock::value);
    }
}

class Object : public Tod::RefCounter
{
    public:
        virtual ~Object();

        bool IsInWorld() const { return m_inWorld; }

        virtual void AddToWorld();
        virtual void RemoveFromWorld();

        ObjectGuid GetGUID() const { return GetGuidValue(OBJECT_GUID); }
        PackedGuid const& GetPackGUID() const { return m_PackGUID; }
        uint32 GetEntry() const { return GetUInt32Value(OBJECT_ENTRY); }
        void SetEntry(uint32 entry) { SetUInt32Value(OBJECT_ENTRY, entry); }

        float GetObjectScale() const { return GetFloatValue(OBJECT_SCALE); }
        virtual void SetObjectScale(float scale) { SetFloatValue(OBJECT_SCALE, scale); }

        TypeID GetTypeId() const { return m_objectTypeId; }
        bool IsType(uint16 mask) const { return (mask & m_objectType); }

        virtual void BuildCreateUpdateBlockForPlayer(UpdateData* data, Player* target) const;
        void SendUpdateToPlayer(Player* player);

        void BuildValuesUpdateBlockForPlayer(UpdateData* data, Player* target) const;
        void BuildOutOfRangeUpdateBlock(UpdateData* data) const;

        virtual void DestroyForPlayer(Player* target, bool onDeath = false) const;

        int32 GetInt32Value(uint16 index) const;
        uint32 GetUInt32Value(uint16 index) const;
        uint64 GetUInt64Value(uint16 index) const;
        float GetFloatValue(uint16 index) const;
        uint8 GetByteValue(uint16 index, uint8 offset) const;
        uint16 GetUInt16Value(uint16 index, uint8 offset) const;
        ObjectGuid GetGuidValue(uint16 index) const;

        void SetInt32Value(uint16 index, int32 value);
        void SetUInt32Value(uint16 index, uint32 value);
        void UpdateUInt32Value(uint16 index, uint32 value);
        void SetUInt64Value(uint16 index, uint64 value);
        void SetFloatValue(uint16 index, float value);
        void SetByteValue(uint16 index, uint8 offset, uint8 value);
        void SetUInt16Value(uint16 index, uint8 offset, uint16 value);
        void SetInt16Value(uint16 index, uint8 offset, int16 value) { SetUInt16Value(index, offset, (uint16)value); }
        void SetGuidValue(uint16 index, ObjectGuid value);
        void SetStatFloatValue(uint16 index, float value);
        void SetStatInt32Value(uint16 index, int32 value);

        bool AddGuidValue(uint16 index, ObjectGuid value);
        bool RemoveGuidValue(uint16 index, ObjectGuid value);

        void ApplyModUInt32Value(uint16 index, int32 val, bool apply);
        void ApplyModInt32Value(uint16 index, int32 val, bool apply);
        void ApplyModUInt64Value(uint16 index, int32 val, bool apply);
        void ApplyModPositiveFloatValue(uint16 index, float val, bool apply);
        void ApplyModSignedFloatValue(uint16 index, float val, bool apply);

        void SetFlag(uint16 index, uint32 newFlag);
        void RemoveFlag(uint16 index, uint32 oldFlag);
        void ToggleFlag(uint16 index, uint32 flag);
        bool HasFlag(uint16 index, uint32 flag) const;
        void ApplyModFlag(uint16 index, uint32 flag, bool apply);

        void SetByteFlag(uint16 index, uint8 offset, uint8 newFlag);
        void RemoveByteFlag(uint16 index, uint8 offset, uint8 newFlag);
        void ToggleByteFlag(uint16 index, uint8 offset, uint8 flag);
        bool HasByteFlag(uint16 index, uint8 offset, uint8 flag) const;

        void SetFlag64(uint16 index, uint64 newFlag);
        void RemoveFlag64(uint16 index, uint64 oldFlag);
        void ToggleFlag64(uint16 index, uint64 flag);
        bool HasFlag64(uint16 index, uint64 flag) const;
        void ApplyModFlag64(uint16 index, uint64 flag, bool apply);

        std::vector<uint32> const& GetDynamicValues(uint16 index) const;
        uint32 GetDynamicValue(uint16 index, uint8 offset) const;
        void AddDynamicValue(uint16 index, uint32 value);
        void RemoveDynamicValue(uint16 index, uint32 value);
        void ClearDynamicValue(uint16 index);
        void SetDynamicValue(uint16 index, uint8 offset, uint32 value);

        void ClearUpdateMask(bool remove);

        uint16 GetValuesCount() const { return m_valuesCount; }

        virtual bool HasQuest(uint32 /* quest_id */) const { return false; }
        virtual bool HasInvolvedQuest(uint32 /* quest_id */) const { return false; }
        virtual void BuildUpdate(UpdateDataMapType&) { }
        void BuildFieldsUpdate(Player*, UpdateDataMapType &) const;

        void SetFieldNotifyFlag(uint16 flag) { _fieldNotifyFlags |= flag; }
        void RemoveFieldNotifyFlag(uint16 flag) { _fieldNotifyFlags &= uint16(~flag); }

        virtual bool IsPersonalLootEligible() const { return false; }

        // FG: some hacky helpers
        void ForceValuesUpdateAtIndex(uint32);

        Player* ToPlayer() { if (GetTypeId() == TYPEID_PLAYER) return reinterpret_cast<Player*>(this); else return NULL; }
        Player const* ToPlayer() const { if (GetTypeId() == TYPEID_PLAYER) return reinterpret_cast<Player const*>(this); else return NULL; }

        Creature* ToCreature() { if (GetTypeId() == TYPEID_UNIT) return reinterpret_cast<Creature*>(this); else return NULL; }
        Creature const* ToCreature() const { if (GetTypeId() == TYPEID_UNIT) return reinterpret_cast<Creature const*>(this); else return NULL; }

        Unit* ToUnit() { if (IsType(TYPEMASK_UNIT)) return reinterpret_cast<Unit*>(this); else return NULL; }
        Unit const* ToUnit() const { if (IsType(TYPEMASK_UNIT)) return reinterpret_cast<Unit const*>(this); else return NULL; }

        GameObject* ToGameObject() { if (GetTypeId() == TYPEID_GAMEOBJECT) return reinterpret_cast<GameObject*>(this); else return NULL; }
        GameObject const* ToGameObject() const { if (GetTypeId() == TYPEID_GAMEOBJECT) return reinterpret_cast<GameObject const*>(this); else return NULL; }

        Corpse* ToCorpse() { if (GetTypeId() == TYPEID_CORPSE) return reinterpret_cast<Corpse*>(this); else return NULL; }
        Corpse const* ToCorpse() const { if (GetTypeId() == TYPEID_CORPSE) return reinterpret_cast<Corpse const*>(this); else return NULL; }

        DynamicObject* ToDynObject() { if (GetTypeId() == TYPEID_DYNAMICOBJECT) return reinterpret_cast<DynamicObject*>(this); else return NULL; }
        DynamicObject const* ToDynObject() const { if (GetTypeId() == TYPEID_DYNAMICOBJECT) return reinterpret_cast<DynamicObject const*>(this); else return NULL; }

        AreaTrigger* ToAreaTrigger() { if (GetTypeId() == TYPEID_AREATRIGGER) return reinterpret_cast<AreaTrigger*>(this); else return NULL; }
        AreaTrigger const* ToAreaTrigger() const { if (GetTypeId() == TYPEID_AREATRIGGER) return reinterpret_cast<AreaTrigger const*>(this); else return NULL; }

    protected:
        Object();

        void _InitValues();

        void _Create(ObjectGuid const& guid);

        std::string _ConcatFields(uint16 startIndex, uint16 size) const;
        void _LoadIntoDataField(std::string const& data, uint32 startOffset, uint32 count);

        uint32 GetUpdateFieldData(Player const* target, uint32*& flags) const;
        uint32 GetDynamicUpdateFieldData(Player const* target, uint32*& flags) const;

        void BuildMovementUpdate(ByteBuffer* data, uint32 flags) const;
        virtual void BuildValuesUpdate(uint8 updatetype, ByteBuffer* data, Player* target) const;
        virtual void BuildDynamicValuesUpdate(uint8 updatetype, ByteBuffer* data, Player* target) const;

        uint16 m_objectType;

        TypeID m_objectTypeId;
        uint32 m_updateFlag;

        union
        {
            int32  *m_int32Values;
            uint32 *m_uint32Values;
            float  *m_floatValues;
        };

        std::vector<uint32>* _dynamicValues;

        std::vector<uint8> _changesMask;
        std::vector<UpdateMask::DynamicFieldChangeType> _dynamicChangesMask;
        std::vector<uint8>* _dynamicChangesArrayMask;

        uint16 m_valuesCount;
        uint16 _dynamicValuesCount;

        uint16 _fieldNotifyFlags;

        virtual void AddToObjectUpdate() = 0;
        virtual void RemoveFromObjectUpdate() = 0;
        void AddToObjectUpdateIfNeeded();

        bool m_objectUpdated;

    private:
        bool m_inWorld;

        PackedGuid m_PackGUID;

        // for output helpfull error messages from asserts
        bool PrintIndexError(uint32 index, bool set) const;
        Object(Object const& right) = delete;
        Object& operator=(Object const& right) = delete;
};

template<class T>
class GridObject
{
    public:
        virtual ~GridObject() { }

        bool IsInGrid() const { return _gridRef.isValid(); }
        void AddToGrid(GridRefManager<T>& m) { ASSERT(!IsInGrid()); _gridRef.link(&m, (T*)this); }
        void RemoveFromGrid() { ASSERT(IsInGrid()); _gridRef.unlink(); }
    private:
        GridReference<T> _gridRef;
};

template <class T_VALUES, class T_FLAGS, class FLAG_TYPE, uint8 ARRAY_SIZE>
class FlaggedValuesArray32
{
    public:
        FlaggedValuesArray32()
        {
            memset(&m_values, 0x00, sizeof(T_VALUES) * ARRAY_SIZE);
            m_flags = 0;
        }

        T_FLAGS  GetFlags() const { return m_flags; }
        bool     HasFlag(FLAG_TYPE flag) const { return m_flags & (T_FLAGS(1) << flag); }
        void     AddFlag(FLAG_TYPE flag) { m_flags |= (T_FLAGS(1) << flag); }
        void     DelFlag(FLAG_TYPE flag) { m_flags &= ~(T_FLAGS(1) << flag); }

        T_VALUES GetValue(FLAG_TYPE flag) const { return m_values[flag]; }
        void     SetValue(FLAG_TYPE flag, T_VALUES value) { m_values[flag] = value; }
        void     AddValue(FLAG_TYPE flag, T_VALUES value) { m_values[flag] += value; }

    private:
        T_VALUES m_values[ARRAY_SIZE];
        T_FLAGS m_flags;
};

enum MapObjectCellMoveState
{
    MAP_OBJECT_CELL_MOVE_NONE, //not in move list
    MAP_OBJECT_CELL_MOVE_ACTIVE, //in move list
    MAP_OBJECT_CELL_MOVE_INACTIVE, //in move list but should not move
};

class MapObject
{
        friend class Map; //map for moving creatures
        friend class ObjectGridLoader; //grid loader for loading creatures

    protected:
        MapObject() : _moveState(MAP_OBJECT_CELL_MOVE_NONE)
        {
            _newPosition.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
        }

    private:
        Cell _currentCell;
        Cell const& GetCurrentCell() const { return _currentCell; }
        void SetCurrentCell(Cell const& cell) { _currentCell = cell; }

        MapObjectCellMoveState _moveState;
        Position _newPosition;
        void SetNewCellPosition(float x, float y, float z, float o)
        {
            _moveState = MAP_OBJECT_CELL_MOVE_ACTIVE;
            _newPosition.Relocate(x, y, z, o);
        }
};

enum MovementAckTypes
{
    MOVEMENT_ACK_NONE                   = -1,
    MOVEMENT_ACK_VALID                  = 0,

    // do not change speed acks order
    MOVEMENT_ACK_SPEED_WALK             = 0,
    MOVEMENT_ACK_SPEED_RUN              = 1,
    MOVEMENT_ACK_SPEED_RUN_BACK         = 2,
    MOVEMENT_ACK_SPEED_SWIM             = 3,
    MOVEMENT_ACK_SPEED_SWIM_BACK        = 4,
    MOVEMENT_ACK_SPEED_TURN_RATE        = 5,
    MOVEMENT_ACK_SPEED_FLIGHT           = 6,
    MOVEMENT_ACK_SPEED_FLIGHT_BACK      = 7,
    MOVEMENT_ACK_SPEED_PITCH_RATE       = 8,

    MOVEMENT_ACK_COLLISION_HEIGHT       = 9,
    MOVEMENT_ACK_KNOCK_BACK             = 10,
    MOVEMENT_ACK_VEHICLE_REC            = 11,
    MOVEMENT_ACK_TELEPORT               = 12,

    MOVEMENT_ACK_DIFFERENT_BROADCAST    = 12,

    MOVEMENT_ACK_TRANSITION_SWIM_FLY    = 13,
    MOVEMENT_ACK_CAN_FLY                = 14,
    MOVEMENT_ACK_ROOT                   = 15,
    MOVEMENT_ACK_FALL                   = 16,
    MOVEMENT_ACK_GRAVITY                = 17,
    MOVEMENT_ACK_HOVER                  = 18,
    MOVEMENT_ACK_WATER_WALK             = 19,
    MOVEMENT_ACK_TURN_WHILE_FALLING     = 20,
    MOVEMENT_ACK_FORCE_MOVEMENT         = 21,   // NYI
    MOVEMENT_ACK_COLLISION              = 22,   // NYI

    MOVEMENT_ACK_MAX
};

class WorldObject : public Object, public WorldLocation
{
    protected:
        explicit WorldObject(bool isWorldObject); //note: here it means if it is in grid object list or world object list

    public:
        virtual ~WorldObject();

        virtual bool Update(uint32 /*time_diff*/) { return true; }
        virtual void RemoveFromWorld() override;

        void GetNearPoint2D(float &x, float &y, float distance, float absAngle) const;
        void GetNearPoint(float &x, float &y, float &z, float searcher_size, float distance2d, float absAngle) const;
        void GetClosePoint(float &x, float &y, float &z, float size, float distance2d = 0, float angle = 0) const;
        void MovePosition(Position &pos, float dist, float angle);
        Position GetNearPosition(float dist, float angle);
        void MovePositionToFirstCollision(Position &pos, float dist, float angle);
        Position GetFirstCollisionPosition(float dist, float angle);
        Position GetRandomNearPosition(float radius);
        void GetContactPoint(WorldObject const* obj, float &x, float &y, float &z, float distance2d = CONTACT_DISTANCE) const;

        virtual float GetCombatReach() const { return 0.0f; } // overridden (only) in Unit
        void UpdateGroundPositionZ(float x, float y, float &z) const;
        void UpdateAllowedPositionZ(float x, float y, float &z) const;

        void GetRandomPoint(Position const &srcPos, float distance, float &rand_x, float &rand_y, float &rand_z) const;
        Position GetRandomPoint(Position const &srcPos, float distance) const;

        virtual bool SetInPhase(uint32 id, bool update, bool apply);
        virtual bool SetInPhaseGroup(uint32 groupId, bool update, bool apply);
        void CopyPhaseFrom(WorldObject* obj, bool update = false);
        void UpdateAreaAndZonePhase();
        void ClearPhases(bool update = false);
        void RebuildTerrainSwaps();
        void RebuildWorldMapAreaSwaps();
        bool HasInPhaseList(uint32 phase);
        bool IsInPhase(uint32 phase) const { return _phases.find(phase) != _phases.end(); }
        bool IsInPhase(WorldObject const* obj) const;
        bool IsInTerrainSwap(uint32 terrainSwap) const { return _terrainSwaps.find(terrainSwap) != _terrainSwaps.end(); }
        std::set<uint32> const& GetPhases() const { return _phases; }
        std::set<uint32> const& GetTerrainSwaps() const { return _terrainSwaps; }
        std::set<uint32> const& GetWorldMapAreaSwaps() const { return _worldMapAreaSwaps; }
        int32 GetDBPhase() { return _dbPhase; }

        // if negative it is used as PhaseGroupId
        void SetDBPhase(int32 p) { _dbPhase = p; }

        uint32 GetZoneId() const;
        uint32 GetAreaId() const;
        void GetZoneAndAreaId(uint32& zoneid, uint32& areaid) const;
        bool IsOutdoors() const;

        InstanceScript* GetInstanceScript();

        std::string const& GetName() const { return m_name; }
        void SetName(std::string const& newname) { m_name=newname; }

        float GetDistance(WorldObject const* obj) const;
        float GetDistance(Position const &pos) const;
        float GetDistance(float x, float y, float z) const;
        float GetDistance2d(WorldObject const* obj) const;
        float GetDistance2d(float x, float y) const;
        float GetDistanceZ(WorldObject const* obj) const;

        bool IsSelfOrInSameMap(WorldObject const* obj) const;
        bool IsInMap(WorldObject const* obj) const;
        bool IsWithinDist3d(float x, float y, float z, float dist) const;
        bool IsWithinDist3d(Position const* pos, float dist) const;
        bool IsWithinDist2d(float x, float y, float dist) const;
        bool IsWithinDist2d(Position const* pos, float dist) const;
        // use only if you will sure about placing both object at same map
        bool IsWithinDist(WorldObject const* obj, float dist2compare, bool is3D = true) const;
        bool IsWithinDistInMap(WorldObject const* obj, float dist2compare, bool is3D = true, bool incOwnRadius = true, bool incTargetRadius = true) const;
        bool IsWithinLOS(float x, float y, float z, VMAP::ModelIgnoreFlags ignoreFlags = VMAP::ModelIgnoreFlags::Nothing) const;
        bool IsWithinLOSInMap(WorldObject const* obj, VMAP::ModelIgnoreFlags ignoreFlags = VMAP::ModelIgnoreFlags::Nothing) const;
        Position GetHitSpherePointFor(Position const& dest) const;
        void GetHitSpherePointFor(Position const& dest, float& x, float& y, float& z) const;
        bool GetDistanceOrder(WorldObject const* obj1, WorldObject const* obj2, bool is3D = true) const;
        bool IsInRange(WorldObject const* obj, float minRange, float maxRange, bool is3D = true) const;
        bool IsInRange2d(float x, float y, float minRange, float maxRange) const;
        bool IsInRange3d(float x, float y, float z, float minRange, float maxRange) const;
        bool isInFront(WorldObject const* target, float arc = float(M_PI)) const;
        bool isInBack(WorldObject const* target, float arc = float(M_PI)) const;

        bool IsInBetween(Position const& pos1, Position const& pos2, float size = 0) const;
        bool IsInBetween(WorldObject const* obj1, WorldObject const* obj2, float size = 0) const { return obj1 && obj2 && IsInBetween(obj1->GetPosition(), obj2->GetPosition(), size); }

        virtual void CleanupsBeforeDelete(bool finalCleanup = true);  // used in destructor or explicitly before mass creature delete to remove cross-references to already deleted units

        virtual void SendMessageToSet(WorldPacket* data, bool self);
        virtual void SendMessageToSetInRange(WorldPacket* data, float dist, bool self);
        virtual void SendMessageToSet(WorldPacket* data, Player const* skipped_rcvr);

        virtual uint8 GetLevelForTarget(WorldObject const* /*target*/) const { return 1; }

        void MonsterSay(const char* text, uint32 language, WorldObject const* target = nullptr);
        void MonsterYell(const char* text, uint32 language, WorldObject const* target = nullptr);
        void MonsterTextEmote(const char* text, WorldObject const* target = nullptr, bool IsBossEmote = false);
        void MonsterWhisper(const char* text, Player const* target = nullptr, bool IsBossWhisper = false);
        void MonsterSay(int32 textId, uint32 language, WorldObject const* target = nullptr);
        void MonsterYell(int32 textId, uint32 language, WorldObject const* target = nullptr);
        void MonsterTextEmote(int32 textId, WorldObject const* target = nullptr, bool IsBossEmote = false);
        void MonsterWhisper(int32 textId, Player const* target = nullptr, bool IsBossWhisper = false);

        void PlayDistanceSound(uint32 sound_id, Player* target = nullptr);
        void PlayDirectSound(uint32 sound_id, Player* target = nullptr);
        void PlayDirectMusic(uint32 musicId, Player* target = nullptr);

        virtual void SaveRespawnTime() { }
        void AddObjectToRemoveList();

        float GetGridActivationRange() const;
        float GetVisibilityRange() const;
        float GetSightRange(WorldObject const* target = NULL) const;
        bool CanSeeOrDetect(WorldObject const* obj, bool ignoreStealth = false, bool distanceCheck = false, bool checkAlert = false) const;

        FlaggedValuesArray32<int32, uint32, StealthType, TOTAL_STEALTH_TYPES> m_stealth;
        FlaggedValuesArray32<int32, uint32, StealthType, TOTAL_STEALTH_TYPES> m_stealthDetect;

        FlaggedValuesArray32<int32, uint64, InvisibilityType, TOTAL_INVISIBILITY_TYPES> m_invisibility;
        FlaggedValuesArray32<int32, uint64, InvisibilityType, TOTAL_INVISIBILITY_TYPES> m_invisibilityDetect;

        FlaggedValuesArray32<int32, uint32, ServerSideVisibilityType, TOTAL_SERVERSIDE_VISIBILITY_TYPES> m_serverSideVisibility;
        FlaggedValuesArray32<int32, uint32, ServerSideVisibilityType, TOTAL_SERVERSIDE_VISIBILITY_TYPES> m_serverSideVisibilityDetect;

        // Low Level Packets
        void SendPlaySound(uint32 Sound, bool OnlySelf);

        virtual void SetMap(Map* map);
        virtual void ResetMap();
        Map* GetMap() const { ASSERT(m_currMap); return m_currMap; }
        Map* FindMap() const { return m_currMap; }
        //used to check all object's GetMap() calls when object is not in world!

        //this function should be removed in nearest time...
        Map const* GetBaseMap() const;

        void SetZoneScript();
        ZoneScript* GetZoneScript() const { return m_zoneScript; }

        Scenario* GetScenario() const;

        TempSummon* SummonCreature(uint32 id, Position const& pos, TempSummonType spwtype = TEMPSUMMON_MANUAL_DESPAWN, uint32 despwtime = 0, uint32 vehId = 0) const;
        TempSummon* SummonCreature(uint32 id, float x, float y, float z, float ang = 0, TempSummonType spwtype = TEMPSUMMON_MANUAL_DESPAWN, uint32 despwtime = 0) const;
        GameObject* SummonGameObject(uint32 entry, Position const& pos, G3D::Quat const& rot, uint32 respawnTime /* s */);
        GameObject* SummonGameObject(uint32 entry, float x, float y, float z, float ang, G3D::Quat const& rot, uint32 respawnTime /* s */);
        Creature*   SummonTrigger(float x, float y, float z, float ang, uint32 dur, CreatureAI* (*GetAI)(Creature*) = NULL);
        void SummonCreatureGroup(uint8 group, std::list<TempSummon*>* list = NULL);

        Creature*   FindNearestCreature(uint32 entry, float range, bool alive = true) const;
        GameObject* FindNearestGameObject(uint32 entry, float range) const;
        GameObject* FindNearestGameObjectOfType(GameobjectTypes type, float range) const;
        Player* SelectNearestPlayer(float distance) const;

        void GetGameObjectListWithEntryInGrid(std::vector<GameObject*>& lList, uint32 uiEntry = 0, float fMaxSearchRange = 250.0f) const;
        void GetCreatureListWithEntryInGrid(std::vector<Creature*>& lList, uint32 uiEntry = 0, float fMaxSearchRange = 250.0f) const;
        void GetPlayerListInGrid(std::vector<Player*>& lList, float fMaxSearchRange) const;

        void DestroyForNearbyPlayers();
        virtual void UpdateObjectVisibility(bool forced = true);
        virtual void UpdateObjectVisibilityOnCreate()
        {
            UpdateObjectVisibility(true);
        }

        void BuildUpdate(UpdateDataMapType&) override;
        void AddToObjectUpdate() override;
        void RemoveFromObjectUpdate() override;

        //relocation and visibility system functions
        void AddToNotify(uint16 f) { m_notifyflags |= f;}
        bool IsNeedNotify(uint16 f) const { return m_notifyflags & f;}
        uint16 GetNotifyFlags() const { return m_notifyflags; }
        bool NotifyExecuted(uint16 f) const { return m_executed_notifies & f;}
        void SetNotified(uint16 f) { m_executed_notifies |= f;}
        void ResetAllNotifies() { m_notifyflags = 0; m_executed_notifies = 0; }

        bool IsActiveObject() const { return m_isActive; }
        void SetActive(bool isActiveObject);
        void SetWorldObject(bool apply);
        bool IsPermanentWorldObject() const { return m_isWorldObject; }
        bool IsWorldObject() const;

        uint32  LastUsedScriptID;

        // Transports
        Transport* GetTransport() const { return m_transport; }
        void SetTransport(Transport* t) { m_transport = t; }
        MotionTransport* GetMotionTransport() const;

        virtual float GetStationaryX() const { return GetPositionX(); }
        virtual float GetStationaryY() const { return GetPositionY(); }
        virtual float GetStationaryZ() const { return GetPositionZ(); }
        virtual float GetStationaryO() const { return GetOrientation(); }

        virtual uint16 GetAIAnimKitId() const       { return 0; }
        virtual uint16 GetMovementAnimKitId() const { return 0; }
        virtual uint16 GetMeleeAnimKitId() const    { return 0; }

        /*********************************************************/
        /***            MOVEMENT INFO SYSTEM                   ***/
        /*********************************************************/

        void SetMovementInfo(MovementInfo* info);

        ObjectGuid GetMovementGUID() const;
        Position GetMovementPosition() const;
        uint32 GetMovementTimer() const;

        float GetPitchLevel() const;
        float GetSplineElevation() const;

        uint32 GetAckIndex() const;

        void AddMovementFlag(uint32 f);
        void RemoveMovementFlag(uint32 f);
        bool HasMovementFlag(uint32 f) const;
        uint32 GetMovementFlags() const;
        void SetMovementFlags(uint32 f);

        void AddExtraMovementFlag(uint32 f);
        void RemoveExtraMovementFlag(uint32 f);
        bool HasExtraMovementFlag(uint32 f) const;
        uint32 GetExtraMovementFlags() const;
        void SetExtraMovementFlags(uint32 f);

        void SetTransportGUID(ObjectGuid guid);
        ObjectGuid GetTransportGUID() const;

        void SetTransportPosition(Position& pos);
        void SetTransportPosition(Position const pos);
        void SetTransportPosition(Position* pos);
        void SetTransportPosition(float x = 0.0f, float y = 0.0f, float z = 0.0f, float orient = 0.0f);
        Position GetTransportPosition() const;

        void SetTransportTimer(uint32 timer);
        uint32 GetTransportTimer() const;

        uint32 GetTransportPrevTimer() const;

        void SetTransportVehicleID(uint32 vehID);
        uint32 GetTransportVehicleID() const;

        void SetTransportSeatID(int32 seatID);
        int8 GetTransportSeatID() const;

        void SetFallTime(uint32 timer);
        uint32 GetFallTime() const;

        void SetFallInfo(float speedXY, float speedZ, float vcos, float vsin);

        float GetFallVerticalSpeed() const;
        float GetFallHorizontalSpeed() const;
        float GetFallSinAngle() const;
        float GetFallCosAngle() const;

        void InsertRemovedForcedMovement(uint32 RemovedForcedMovementID);
        std::vector<uint32>const& GetRemovedForcedMovements() const;

        void InsertForcedMovement(MovementForce& ForcedMovement);
        std::vector<MovementForce>const& GetForcedMovements() const;

        void ResetBaseMovementInfo();
        void ResetFall();
        void ResetTransport();

        MovementInfo m_movementInfo;
    protected:
        std::string m_name;
        bool m_isActive;
        const bool m_isWorldObject;
        ZoneScript* m_zoneScript;

        // transports
        Transport* m_transport;

        //these functions are used mostly for Relocate() and Corpse/Player specific stuff...
        //use them ONLY in LoadFromDB()/Create() funcs and nowhere else!
        //mapId/instanceId should be set in SetMap() function!
        void SetLocationMapId(uint32 _mapId) { m_mapId = _mapId; }
        void SetLocationInstanceId(uint32 _instanceId) { m_instanceId = _instanceId; }

        virtual bool IsNeverVisibleFor(WorldObject const* /*seer*/) const { return !IsInWorld(); }
        virtual bool IsAlwaysVisibleFor(WorldObject const* /*seer*/) const { return false; }
        virtual bool IsInvisibleDueToDespawn() const { return false; }
        //difference from IsAlwaysVisibleFor: 1. after distance check; 2. use owner or charmer as seer
        virtual bool IsAlwaysDetectableFor(WorldObject const* /*seer*/) const { return false; }

    private:
        Map* m_currMap;                                    //current object's Map location

        std::set<uint32> _phases;
        std::set<uint32> _terrainSwaps;
        std::set<uint32> _worldMapAreaSwaps;
        int32 _dbPhase;

        uint16 m_notifyflags;
        uint16 m_executed_notifies;
        virtual bool _IsWithinDist(WorldObject const* obj, float dist2compare, bool is3D, bool incOwnRadius = true, bool incTargetRadius = true) const;

        bool CanNeverSee(WorldObject const* obj) const;
        virtual bool CanAlwaysSee(WorldObject const* /*obj*/) const { return false; }
        bool CanDetect(WorldObject const* obj, bool ignoreStealth, bool checkAlert = false) const;
        bool CanDetectInvisibilityOf(WorldObject const* obj) const;
        bool CanDetectStealthOf(WorldObject const* obj, bool checkAlert = false) const;

        mutable Position _areaPosition;
        mutable uint32 _areaID;
        mutable uint32 _zoneID;
        mutable bool _isOutdoors;
};

namespace Trinity
{
    // Binary predicate to sort WorldObjects based on the distance to a reference WorldObject
    class ObjectDistanceOrderPred
    {
        public:
            ObjectDistanceOrderPred(WorldObject const* refObj, bool ascending = true) : _refObj(refObj), _ascending(ascending) { }

            bool operator()(WorldObject const* left, WorldObject const* right) const
            {
                return _refObj->GetDistanceOrder(left, right) == _ascending;
            }

        private:
            WorldObject const* _refObj;
            bool _ascending;
    };
}

#endif
