/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "DatabaseEnv.h"
#include "Log.h"
#include "WorldPacket.h"
#include "ObjectMgr.h"
#include "SpellMgr.h"
#include "Pet.h"
#include "Formulas.h"
#include "SpellAuras.h"
#include "SpellAuraEffects.h"
#include "SpellHistory.h"
#include "CreatureAI.h"
#include "Unit.h"
#include "Util.h"
#include "Group.h"
#include "WorldSession.h"
#include "Battleground.h"
#include "Transport.h"

#define PET_XP_FACTOR 0.05f

Pet::Pet(Player* owner, PetType type) :
    Guardian(NULL, owner, true), m_petOwner(owner), m_removed(false),
    m_petType(type), m_duration(0), m_loading(false), m_auraRaidUpdateMask(0), m_petSpecialization(0)
{
    ASSERT(m_owner->GetTypeId() == TYPEID_PLAYER);

    m_unitTypeMask |= UNIT_MASK_PET;
    if (type == HUNTER_PET)
        m_unitTypeMask |= UNIT_MASK_HUNTER_PET;

    if (!(m_unitTypeMask & UNIT_MASK_CONTROLABLE_GUARDIAN))
    {
        m_unitTypeMask |= UNIT_MASK_CONTROLABLE_GUARDIAN;
        InitCharmInfo();
    }

    m_name = "Pet";
    m_regenTimer = PET_FOCUS_REGEN_INTERVAL;

    m_stampeded = false;
}

Pet::~Pet()
{
}

void Pet::AddToWorld()
{
    ///- Register the pet for guid lookup
    if (!IsInWorld())
    {
        ///- Register the pet for guid lookup
        GetMap()->GetObjectsStore().Insert<Pet>(GetGUID(), this);
        Unit::AddToWorld();
        AIM_Initialize();
    }

    // Prevent stuck pets when zoning. Pets default to "follow" when added to world
    // so we'll reset flags and let the AI handle things
    if (GetCharmInfo() && GetCharmInfo()->HasCommandState(COMMAND_FOLLOW))
    {
        GetCharmInfo()->SetIsCommandAttack(false);
        GetCharmInfo()->SetIsCommandFollow(false);
        GetCharmInfo()->SetIsAtStay(false);
        GetCharmInfo()->SetIsFollowing(false);
        GetCharmInfo()->SetIsReturning(false);
    }
}

void Pet::RemoveFromWorld()
{
    ///- Remove the pet from the accessor
    if (IsInWorld())
    {
        ///- Don't call the function for Creature, normal mobs + totems go in a different storage
        Unit::RemoveFromWorld();
        GetMap()->GetObjectsStore().Remove<Pet>(GetGUID());
    }
}

bool Pet::LoadPetFromDB(Player* owner, uint32 petEntry, uint32 petnumber, bool current, PetSlot slotID, bool stampeded)
{
    m_loading = true;

    if (slotID == PET_SLOT_ACTUAL_PET_SLOT)
        slotID = owner->GetCurrentPetSlot();

    uint32 ownerid = owner->GetGUID().GetCounter();

    PreparedStatement* stmt = nullptr;

    if (petnumber)
    {
        // Known petnumber entry
        //        0     1      2       3       4     5       6        7     8       9        10        11       12       13           14          15          16
        // SELECT id, entry, owner, modelid, level, exp, Reactstate, slot, name, renamed, curhealth, curmana, abdata, savetime, CreatedBySpell, PetType, specialization
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PET_BY_ENTRY);
        stmt->setUInt32(0, ownerid);
        stmt->setUInt32(1, petnumber);
    }
    else if (current && slotID != PET_SLOT_SPECIAL_SLOT)
    {
        // Current pet (slot 0)
        //        0     1     2        3       4     5       6        7      8      9        10         11      12      13           14            15          16
        // SELECT id, entry, owner, modelid, level, exp, Reactstate, slot, name, renamed, curhealth, curmana, abdata, savetime, CreatedBySpell, PetType, specialization
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PET_BY_ENTRY_AND_SLOT);
        stmt->setUInt32(0, ownerid);
        stmt->setUInt32(1, slotID);
    }
    else if (petEntry)
    {
        // Known petEntry entry (unique for summoned pet, but non unique for hunter pet (only from current or not stabled pets)
        //        0     1     2        3       4     5       6        7      8      9        10         11      12      13           14            15          16
        // SELECT id, entry, owner, modelid, level, exp, Reactstate, slot, name, renamed, curhealth, curmana, abdata, savetime, CreatedBySpell, PetType, specialization
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PET_BY_ENTRY_AND_SLOT_2);
        stmt->setUInt32(0, ownerid);
        stmt->setUInt32(1, petEntry);
        stmt->setUInt32(2, PET_SLOT_HUNTER_FIRST);
        stmt->setUInt32(3, PET_SLOT_HUNTER_LAST);
        stmt->setUInt32(4, PET_SLOT_STABLE_LAST);
    }
    else
    {
        // Any current or other non-stabled pet (for hunter "call pet")
        //        0     1     2        3       4     5       6        7      8      9        10         11      12      13           14            15          16
        // SELECT id, entry, owner, modelid, level, exp, Reactstate, slot, name, renamed, curhealth, curmana, abdata, savetime, CreatedBySpell, PetType, specialization
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PET_BY_SLOT);
        stmt->setUInt32(0, ownerid);
        stmt->setUInt32(1, PET_SLOT_HUNTER_FIRST);
        stmt->setUInt32(2, PET_SLOT_HUNTER_LAST);
        stmt->setUInt32(3, slotID);
    }

    PreparedQueryResult result = CharacterDatabase.Query(stmt);

    if (!result)
    {
        m_loading = false;
        return false;
    }

    Field* fields = result->Fetch();

    // update for case of current pet "slot = 0"
    petEntry = fields[1].GetUInt32();
    if (!petEntry)
        return false;

    uint32 summonSpellId = fields[14].GetUInt32();
    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(summonSpellId);

    bool isTemporarySummon = spellInfo && spellInfo->GetDuration() > 0;

    // Check temporary summoned pets like mage water elemental.
    if (current && isTemporarySummon)
    {
        m_loading = false;
        return false;
    }

    PetType petType = PetType(fields[15].GetUInt8());
    if (petType == HUNTER_PET)
    {
        CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(petEntry);
        if (!creatureInfo || !creatureInfo->IsTameable(owner->CanTameExoticPets()))
            return false;
    }

    uint32 pet_number = fields[0].GetUInt32();

    if (current && owner->IsPetNeedBeTemporaryUnsummoned())
    {
        owner->SetTemporaryUnsummonedPetNumber(pet_number);
        return false;
    }

    Map* map = owner->GetMap();
    uint32 guid = map->GenerateLowGuid<HighGuid::Pet>();
    if (!Create(guid, map, petEntry))
        return false;

    CopyPhaseFrom(owner);

    SetPetType(petType);
    SetFaction(owner->GetFaction());
    SetUInt32Value(UNIT_CREATED_BY_SPELL, summonSpellId);

    CreatureTemplate const* cinfo = GetCreatureTemplate();
    if (IsCritter())
    {
        float px, py, pz;
        owner->GetClosePoint(px, py, pz, GetCombatReach(), PET_FOLLOW_DIST, GetFollowAngle());
        Relocate(px, py, pz, owner->GetOrientation());

        if (!IsPositionValid())
        {
            TC_LOG_ERROR("entities.pet", "Pet (guidlow %d, entry %d) not loaded. Suggested coordinates isn't valid (X: %f Y: %f)",
                GetGUID().GetCounter(), GetEntry(), GetPositionX(), GetPositionY());
            return false;
        }

        Transport* transport = GetTransportGUID().IsEmpty() ? owner->GetTransport() : nullptr;
        if (transport)
        {
            float x, y, z, o;
            GetPosition(x, y, z, o);
            transport->CalculatePassengerOffset(x, y, z, &o);
            SetTransportPosition(x, y, z, o);

            transport->AddPassenger(this);
        }

        map->AddToMap(this->ToCreature());
        return true;
    }

    m_charmInfo->SetPetNumber(pet_number, IsPermanentPetFor(owner));

    SetDisplayId(fields[3].GetUInt32());
    SetNativeDisplayId(fields[3].GetUInt32());
    uint32 petlevel = fields[4].GetUInt16();
    SetUInt64Value(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_NONE);
    SetName(fields[8].GetString());

    Powers powerType = POWER_MANA;
    switch (cinfo->unit_class)
    {
        case CLASS_WARRIOR:
            powerType = POWER_RAGE;
            break;
        case CLASS_ROGUE:
            powerType = POWER_ENERGY;
            break;
        default:
            break;
    }

    switch (GetPetType())
    {
        case SUMMON_PET:
            petlevel = owner->GetLevel();
            SetClass(CLASS_MAGE);
            SetUInt32Value(UNIT_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
            if (owner && owner->GetClass() == CLASS_WARLOCK)
            {
                SetClass(CLASS_ROGUE);
                SetPowerType(POWER_ENERGY); // Warlock's pets have energy
            }
            break;
        case HUNTER_PET:
            SetSheath(SHEATH_STATE_MELEE);
            SetClass(CLASS_WARRIOR);
            SetGender(GENDER_NONE);
            SetFieldPowerType(POWER_FOCUS);
            SetByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_RENAME_STATE, fields[9].GetBool() ? UNIT_CAN_BE_ABANDONED : UNIT_CAN_BE_RENAMED | UNIT_CAN_BE_ABANDONED);
            SetUInt32Value(UNIT_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
                                                            // this enables popup window (pet abandon, cancel)
            SetPowerType(POWER_FOCUS);
            break;
        default:
            if (!IsPetGhoul())
                TC_LOG_ERROR("entities.pet", "Pet have incorrect type (%u) for pet loading.", GetPetType());
            break;
    }

    SetUInt32Value(UNIT_PET_NAME_TIMESTAMP, uint32(time(NULL))); // cast can't be helped here
    SetCreatorGUID(owner->GetGUID());

    InitStatsForLevel(petlevel);
    SetUInt32Value(UNIT_PET_EXPERIENCE, fields[5].GetUInt32());

    SynchronizeLevelWithOwner();

    // Set pet's position after setting level, its size depends on it
    float px, py, pz;
    owner->GetClosePoint(px, py, pz, GetCombatReach(), PET_FOLLOW_DIST, GetFollowAngle());
    Relocate(px, py, pz, owner->GetOrientation());
    if (!IsPositionValid())
    {
        TC_LOG_ERROR("entities.pet", "Pet (guidlow %d, entry %d) not loaded. Suggested coordinates isn't valid (X: %f Y: %f)",
            GetGUID().GetCounter(), GetEntry(), GetPositionX(), GetPositionY());
        return false;
    }

    SetReactState(ReactStates(fields[6].GetUInt8()));
    SetCanModifyStats(true);

    if (GetPetType() == SUMMON_PET && !current && owner && owner->GetClass() != CLASS_WARLOCK)  //all (?) summon pets come with full health when called, but not when they are current
        SetPower(POWER_MANA, GetMaxPower(POWER_MANA));
    else
    {
        uint32 savedhealth = fields[10].GetUInt32();
        uint32 savedmana = fields[11].GetUInt32();
        if (!savedhealth && GetPetType() == HUNTER_PET)
            SetDeathState(JUST_DIED);
        else if (owner && owner->GetClass() != CLASS_WARLOCK)
        {
            SetHealth(savedhealth > GetMaxHealth() ? GetMaxHealth() : savedhealth);
            SetPower(POWER_MANA, savedmana > uint32(GetMaxPower(POWER_MANA)) ? GetMaxPower(POWER_MANA) : savedmana);
        }
        else
        {
            SetHealth(savedhealth > GetMaxHealth() ? GetMaxHealth() : savedhealth);
            SetMaxPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
            SetPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
        }
    }

    Transport* transport = GetTransportGUID().IsEmpty() ? owner->GetTransport() : nullptr;
    if (transport)
    {
        float x, y, z, o;
        GetPosition(x, y, z, o);
        transport->CalculatePassengerOffset(x, y, z, &o);
        SetTransportPosition(x, y, z, o);

        transport->AddPassenger(this);
    }

    owner->SetMinion(this, true, slotID == PET_SLOT_SPECIAL_SLOT ? PET_SLOT_OTHER_PET : slotID, stampeded);

    map->AddToMap(this->ToCreature());

    m_slot = fields[7].GetUInt8();

    uint32 timediff = uint32(time(NULL) - fields[13].GetUInt32());

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_AURA_EFFECT);
    stmt->setUInt32(0, m_charmInfo->GetPetNumber());
    PreparedQueryResult auraEffectResult = CharacterDatabase.Query(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_AURA);
    stmt->setUInt32(0, m_charmInfo->GetPetNumber());
    PreparedQueryResult auraResult = CharacterDatabase.Query(stmt);

    _LoadAuras(auraResult, auraEffectResult, timediff);

    if (Battleground* bg = owner->GetBattleground())
        if (bg->IsArena())
            RemoveArenaAuras();

    // load action bar, if data broken will fill later by default spells.
    if (!isTemporarySummon)
    {
        m_charmInfo->LoadPetActionBar(fields[12].GetString());

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SPELL);
        stmt->setUInt32(0, m_charmInfo->GetPetNumber());
        PreparedQueryResult spellResult = CharacterDatabase.Query(stmt);

        _LoadSpells(spellResult);

        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SPELL_COOLDOWN);
        stmt->setUInt32(0, m_charmInfo->GetPetNumber());
        PreparedQueryResult spellCooldownResult = CharacterDatabase.Query(stmt);

        stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_SPELL_CHARGES);
        stmt->setUInt32(0, m_charmInfo->GetPetNumber());
        PreparedQueryResult spellChargesResult = CharacterDatabase.Query(stmt);

        _LoadSpellCooldowns(spellCooldownResult, spellChargesResult);

        LearnPetPassives();
        InitLevelupSpellsForLevel();
        CastPetAuras(current);
    }

    TC_LOG_DEBUG("entities.pet", "New Pet has %s", GetGUID().ToString().c_str());

    uint16 specId = fields[16].GetUInt16();
    SetSpecialization(specId);

    // The SetSpecialization function will run these functions if the pet's spec is not 0
    if (!GetSpecialization())
    {
        CleanupActionBar();                                     // remove unknown spells from action bar after load
        owner->PetSpellInitialize();
    }

    owner->SetGroupUpdateFlag(GROUP_UPDATE_PET);

    if (GetPetType() == HUNTER_PET)
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_DECLINED_NAME);
        stmt->setUInt32(0, owner->GetGUID().GetCounter());
        stmt->setUInt32(1, GetCharmInfo()->GetPetNumber());
        PreparedQueryResult result = CharacterDatabase.Query(stmt);

        if (result)
        {
            Field* fields2 = result->Fetch();

            for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
                m_declinedNames[i] = fields2[i].GetString();
        }
    }

    // Set last used pet number (for use in BG's).
    if (owner->GetTypeId() == TYPEID_PLAYER && IsControlled() && !IsTemporarySummoned() && (GetPetType() == SUMMON_PET || GetPetType() == HUNTER_PET))
        owner->ToPlayer()->SetLastPetNumber(pet_number);

   m_loading = false;

    return true;
}

bool Pet::LoadPetFromDB(SQLQueryHolder* holder)
{
    m_loading = true;

    PreparedQueryResult result = holder->GetPreparedResult(PET_LOGIN_QUERY_LOAD_ACTUAL_PET_DATA);

    if (!result)
    {
        m_loading = false;
        return false;
    }

    Field* fields = result->Fetch();

    // update for case of current pet "slot = 0"
    uint32 petEntry = fields[1].GetUInt32();
    if (!petEntry)
        return false;

    uint32 summonSpellId = fields[14].GetUInt32();
    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(summonSpellId);

    bool isTemporarySummon = spellInfo && spellInfo->GetDuration() > 0;

    // Check temporary summoned pets like mage water elemental.
    if (isTemporarySummon)
    {
        m_loading = false;
        return false;
    }

    PetType petType = PetType(fields[15].GetUInt8());
    if (petType == HUNTER_PET)
    {
        CreatureTemplate const* creatureInfo = sObjectMgr->GetCreatureTemplate(petEntry);
        if (!creatureInfo || !creatureInfo->IsTameable(m_petOwner->CanTameExoticPets()))
            return false;
    }

    uint32 petNumber = fields[0].GetUInt32();

    if (m_petOwner->IsPetNeedBeTemporaryUnsummoned())
    {
        m_petOwner->SetTemporaryUnsummonedPetNumber(petNumber);
        return false;
    }

    Map* map = m_petOwner->GetMap();
    uint32 guid = map->GenerateLowGuid<HighGuid::Pet>();
    if (!Create(guid, map, petEntry))
        return false;

    CopyPhaseFrom(m_petOwner);

    SetPetType(petType);
    SetFaction(m_petOwner->GetFaction());
    SetUInt32Value(UNIT_CREATED_BY_SPELL, summonSpellId);

    CreatureTemplate const* cinfo = GetCreatureTemplate();
    if (IsCritter())
    {
        float px, py, pz;
        m_petOwner->GetClosePoint(px, py, pz, GetCombatReach(), PET_FOLLOW_DIST, GetFollowAngle());
        Relocate(px, py, pz, m_petOwner->GetOrientation());

        if (!IsPositionValid())
        {
            TC_LOG_ERROR("entities.pet", "Pet (guidlow %d, entry %d) not loaded. Suggested coordinates isn't valid (X: %f Y: %f)",
                GetGUID().GetCounter(), GetEntry(), GetPositionX(), GetPositionY());
            return false;
        }

        Transport* transport = GetTransportGUID().IsEmpty() ? m_petOwner->GetTransport() : nullptr;
        if (transport)
        {
            float x, y, z, o;
            GetPosition(x, y, z, o);
            transport->CalculatePassengerOffset(x, y, z, &o);
            SetTransportPosition(x, y, z, o);

            transport->AddPassenger(this);
        }

        map->AddToMap(this->ToCreature());
        return true;
    }

    m_charmInfo->SetPetNumber(petNumber, IsPermanentPetFor(m_petOwner));

    SetDisplayId(fields[3].GetUInt32());
    SetNativeDisplayId(fields[3].GetUInt32());
    uint32 petlevel = fields[4].GetUInt16();
    SetUInt64Value(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_NONE);
    SetName(fields[8].GetString());

    Powers powerType = POWER_MANA;
    switch (cinfo->unit_class)
    {
        case CLASS_WARRIOR:
            powerType = POWER_RAGE;
            break;
        case CLASS_ROGUE:
            powerType = POWER_ENERGY;
            break;
        default:
            break;
    }

    switch (GetPetType())
    {
        case SUMMON_PET:
            petlevel = m_petOwner->GetLevel();
            SetClass(CLASS_MAGE);
            SetUInt32Value(UNIT_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
            if (m_petOwner->GetClass() == CLASS_WARLOCK)
            {
                SetClass(CLASS_ROGUE);
                SetPowerType(POWER_ENERGY); // Warlock's pets have energy
            }
            break;
        case HUNTER_PET:
            SetSheath(SHEATH_STATE_MELEE);
            SetClass(CLASS_WARRIOR);
            SetGender(GENDER_NONE);
            SetFieldPowerType(POWER_FOCUS);
            SetByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_RENAME_STATE, fields[9].GetBool() ? UNIT_CAN_BE_ABANDONED : UNIT_CAN_BE_RENAMED | UNIT_CAN_BE_ABANDONED);
            SetUInt32Value(UNIT_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
                                                            // this enables popup window (pet abandon, cancel)
            SetPowerType(POWER_FOCUS);
            break;
        default:
            if (!IsPetGhoul())
                TC_LOG_ERROR("entities.pet", "Pet have incorrect type (%u) for pet loading.", GetPetType());
            break;
    }

    SetUInt32Value(UNIT_PET_NAME_TIMESTAMP, uint32(time(NULL))); // cast can't be helped here
    SetCreatorGUID(m_petOwner->GetGUID());

    InitStatsForLevel(petlevel);
    SetUInt32Value(UNIT_PET_EXPERIENCE, fields[5].GetUInt32());

    SynchronizeLevelWithOwner();

    // Set pet's position after setting level, its size depends on it
    float px, py, pz;
    m_petOwner->GetClosePoint(px, py, pz, GetCombatReach(), PET_FOLLOW_DIST, GetFollowAngle());
    Relocate(px, py, pz, m_petOwner->GetOrientation());
    if (!IsPositionValid())
    {
        TC_LOG_ERROR("entities.pet", "Pet (guidlow %d, entry %d) not loaded. Suggested coordinates isn't valid (X: %f Y: %f)",
            GetGUID().GetCounter(), GetEntry(), GetPositionX(), GetPositionY());
        return false;
    }

    SetReactState(ReactStates(fields[6].GetUInt8()));
    SetCanModifyStats(true);

    if (GetPetType() == SUMMON_PET && m_petOwner->GetClass() != CLASS_WARLOCK)  //all (?) summon pets come with full health when called, but not when they are current
        SetPower(POWER_MANA, GetMaxPower(POWER_MANA));
    else
    {
        uint32 savedhealth = fields[10].GetUInt32();
        uint32 savedmana = fields[11].GetUInt32();
        if (!savedhealth && GetPetType() == HUNTER_PET)
            SetDeathState(JUST_DIED);
        else if (m_petOwner->GetClass() != CLASS_WARLOCK)
        {
            SetHealth(savedhealth > GetMaxHealth() ? GetMaxHealth() : savedhealth);
            SetPower(POWER_MANA, savedmana > uint32(GetMaxPower(POWER_MANA)) ? GetMaxPower(POWER_MANA) : savedmana);
        }
        else
        {
            SetHealth(savedhealth > GetMaxHealth() ? GetMaxHealth() : savedhealth);
            SetMaxPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
            SetPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
        }
    }

    m_slot = fields[7].GetUInt8();

    Transport* transport = GetTransportGUID().IsEmpty() ? m_petOwner->GetTransport() : nullptr;
    if (transport)
    {
        float x, y, z, o;
        GetPosition(x, y, z, o);
        transport->CalculatePassengerOffset(x, y, z, &o);
        SetTransportPosition(x, y, z, o);

        transport->AddPassenger(this);
    }

    m_petOwner->SetMinion(this, true, PetSlot(m_slot));
    map->AddToMap(this->ToCreature());

    uint32 timediff = uint32(time(NULL) - fields[13].GetUInt32());

    _LoadAuras(holder->GetPreparedResult(PET_LOGIN_QUERY_LOAD_AURAS), holder->GetPreparedResult(PET_LOGIN_QUERY_LOAD_AURA_EFFECTS), timediff);

    if (Battleground* bg = m_petOwner->GetBattleground())
        if (bg->IsArena())
            RemoveArenaAuras();

    // load action bar, if data broken will fill later by default spells.
    if (!isTemporarySummon)
    {
        m_charmInfo->LoadPetActionBar(fields[12].GetString());

        _LoadSpells(holder->GetPreparedResult(PET_LOGIN_QUERY_LOAD_SPELLS));
        _LoadSpellCooldowns(holder->GetPreparedResult(PET_LOGIN_QUERY_LOAD_SPELL_COOLDOWNS), holder->GetPreparedResult(PET_LOGIN_QUERY_LOAD_SPELL_CHARGES));
        LearnPetPassives();
        InitLevelupSpellsForLevel();
        if (map->IsBattleArena())
            RemoveArenaAuras();

        CastPetAuras(true);
    }

    TC_LOG_DEBUG("entities.pet", "New Pet has %s", GetGUID().ToString().c_str());

    uint16 specId = fields[16].GetUInt16();
    SetSpecialization(specId);

    // The SetSpecialization function will run these functions if the pet's spec is not 0
    if (!GetSpecialization())
    {
        CleanupActionBar();                                     // remove unknown spells from action bar after load
        m_petOwner->PetSpellInitialize();
    }

    m_petOwner->SetGroupUpdateFlag(GROUP_UPDATE_PET);

    if (GetPetType() == HUNTER_PET)
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_PET_DECLINED_NAME);
        stmt->setUInt32(0, m_petOwner->GetGUID().GetCounter());
        stmt->setUInt32(1, GetCharmInfo()->GetPetNumber());
        PreparedQueryResult result = CharacterDatabase.Query(stmt);

        if (result)
        {
            Field* fields2 = result->Fetch();

            for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
                m_declinedNames[i] = fields2[i].GetString();
        }
    }

    // Set last used pet number (for use in BG's).
    if (IsControlled() && !IsTemporarySummoned() && (GetPetType() == SUMMON_PET || GetPetType() == HUNTER_PET))
        m_petOwner->ToPlayer()->SetLastPetNumber(petNumber);

    // must be after SetMinion (owner guid check)
    LoadMechanicTemplateImmunity();
    m_loading = false;

    return true;
}

void Pet::SavePetToDB(PetSlot slot, bool stampeded)
{
    if (!GetEntry())
        return;

    // save only fully controlled creature
    if (!IsControlled())
        return;

    // not save not player pets
    if (!GetOwnerGUID().IsPlayer())
        return;

    // not save stampeded pets
    if (stampeded)
        return;

    Player* owner = GetOwner();
    if (!owner)
        return;

    if (slot == PET_SLOT_ACTUAL_PET_SLOT)
        slot = owner->GetCurrentPetSlot();

    // not save pet as current if another pet temporary unsummoned
    if (slot == owner->GetCurrentPetSlot() && owner->GetTemporaryUnsummonedPetNumber() &&
        owner->GetTemporaryUnsummonedPetNumber() != m_charmInfo->GetPetNumber())
    {
        // pet will lost anyway at restore temporary unsummoned
        if (GetPetType() == HUNTER_PET)
            return;

        // for warlock case
        slot = PET_SLOT_OTHER_PET;
    }

    uint32 curhealth = GetHealth();
    uint32 curmana = GetPower(POWER_MANA);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    // save auras before possibly removing them
    _SaveAuras(trans);

    // stable and not in slot saves
    if (slot > PET_SLOT_HUNTER_LAST && GetPetType() == HUNTER_PET)
        RemoveAllAuras();

    _SaveSpells(trans);
    GetSpellHistory()->SaveToDB<Pet>(trans);
    CharacterDatabase.CommitTransaction(trans);

    // current/stable/not_in_slot
    if (slot >= PET_SLOT_HUNTER_FIRST)
    {
        uint32 ownerLowGUID = GetOwnerGUID().GetCounter();
        trans = CharacterDatabase.BeginTransaction();

        // remove current data
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_PET_BY_ID_AND_OWNER);
        stmt->setUInt32(0, ownerLowGUID);
        stmt->setUInt32(1, m_charmInfo->GetPetNumber());
        trans->Append(stmt);

        if (owner->GetLevel() < 10)
            SetReactState(REACT_DEFENSIVE);

        // save pet
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_PET);
        stmt->setUInt32(0, m_charmInfo->GetPetNumber());
        stmt->setUInt32(1, GetEntry());
        stmt->setUInt32(2, ownerLowGUID);
        stmt->setUInt32(3, GetNativeDisplayId());
        stmt->setUInt8(4, GetLevel());
        stmt->setUInt32(5, GetUInt32Value(UNIT_PET_EXPERIENCE));
        stmt->setUInt8(6, GetReactState());
        stmt->setInt16(7, slot);
        stmt->setString(8, m_name);
        stmt->setUInt8(9, HasByteFlag(UNIT_BYTES_2, 2, UNIT_CAN_BE_RENAMED) ? 0 : 1);
        stmt->setUInt32(10, curhealth);
        stmt->setUInt32(11, curmana);

        stmt->setString(12, GenerateActionBarData());

        stmt->setUInt32(13, time(NULL));
        stmt->setUInt32(14, GetUInt32Value(UNIT_CREATED_BY_SPELL));
        stmt->setUInt8(15, GetPetType());
        stmt->setUInt16(16, m_petSpecialization);
        trans->Append(stmt);

        CharacterDatabase.CommitTransaction(trans);
    }
    // delete
    else
    {
        if (owner->GetCurrentPetSlot() >= PET_SLOT_HUNTER_FIRST && owner->GetCurrentPetSlot() <= PET_SLOT_HUNTER_LAST)
            owner->SetPetSlotInUse(owner->GetCurrentPetSlot(), false);

        RemoveAllAuras();
        DeleteFromDB(m_charmInfo->GetPetNumber());
    }
}

void Pet::DeleteFromDB(uint32 guidlow)
{
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_PET_BY_ID);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_PET_DECLINEDNAME);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_AURA_EFFECTS);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_AURAS);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_SPELLS);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_SPELL_COOLDOWNS);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_SPELL_CHARGES);
    stmt->setUInt32(0, guidlow);
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);
}

void Pet::SetDeathState(DeathState s)                       // overwrite virtual Creature::setDeathState and Unit::setDeathState
{
    Creature::SetDeathState(s);
    if (GetDeathState() == CORPSE)
    {
        if (GetPetType() == HUNTER_PET)
        {
            // pet corpse non lootable and non skinnable
            SetUInt32Value(OBJECT_DYNAMIC_FLAGS, UNIT_DYNFLAG_NONE);
            RemoveFlag(UNIT_FLAGS, UNIT_FLAG_SKINNABLE);
            //SetFlag(UNIT_FLAGS, UNIT_FLAG_STUNNED);
        }
    }
    else if (GetDeathState() == ALIVE)
    {
        if (GetPetType() == HUNTER_PET)
        {
            CastPetAuras(true);

            if (Unit* owner = GetOwner())
                if (Player* player = owner->ToPlayer())
                    player->StopCastingCharm();
        }
        else
        {
            //RemoveFlag(UNIT_FLAGS, UNIT_FLAG_STUNNED);
            CastPetAuras(true);
        }
    }
}

bool Pet::Update(uint32 diff)
{
    if (m_removed)                                           // pet already removed, just wait in remove queue, no updates
        return false;

    if (m_loading)
        return false;

    switch (m_deathState)
    {
        case CORPSE:
        {
            if (GetPetType() != HUNTER_PET || m_corpseRemoveTime <= time(NULL))
            {
                Remove(PET_SLOT_ACTUAL_PET_SLOT, false, m_stampeded);   // hunters' pets never get removed because of death, NEVER!
                return true;
            }
            break;
        }
        case ALIVE:
        {
            // unsummon pet that lost owner
            Player* owner = GetOwner();
            if (!owner || (!IsWithinDistInMap(owner, GetMap()->GetVisibilityRange()) && !IsPossessed()) || (IsControlled() && !owner->GetPetGUID()))
            {
                Remove(PET_SLOT_ACTUAL_PET_SLOT, true, m_stampeded);
                return true;
            }

            if (IsControlled())
            {
                if (owner->GetPetGUID() != GetGUID() && !m_stampeded)
                {
                    TC_LOG_ERROR("entities.pet", "Pet %u is not pet of owner %s, removed", GetEntry(), GetOwner()->GetName().c_str());
                    Remove(GetPetType() == HUNTER_PET ? PET_SLOT_DELETED : PET_SLOT_ACTUAL_PET_SLOT, true, m_stampeded);
                    return true;
                }
            }

            if (m_duration > 0)
            {
                if (uint32(m_duration) > diff)
                    m_duration -= diff;
                else
                {
                    Remove(GetPetType() != SUMMON_PET ? PET_SLOT_DELETED : PET_SLOT_ACTUAL_PET_SLOT, false, m_stampeded);
                    return true;
                }
            }

            //regenerate focus for hunter pets or energy for deathknight's ghoul
            if (m_focusRegenTimer)
            {
                if (m_focusRegenTimer > diff)
                    m_focusRegenTimer -= diff;
                else
                {
                    switch (GetPowerType())
                    {
                        case POWER_FOCUS:
                            Regenerate(POWER_FOCUS);
                            m_focusRegenTimer += PET_FOCUS_REGEN_INTERVAL - diff;
                            if (!m_focusRegenTimer)
                                ++m_focusRegenTimer;

                            // Reset if large diff (lag) causes focus to get 'stuck'
                            if (m_focusRegenTimer > PET_FOCUS_REGEN_INTERVAL)
                                m_focusRegenTimer = PET_FOCUS_REGEN_INTERVAL;

                            break;
                        default:
                            m_focusRegenTimer = 0;
                            break;
                    }
                }
            }
            break;
        }
        default:
            break;
    }

    return Creature::Update(diff);
}

void Pet::Remove(PetSlot slot, bool returnreagent, bool stampeded)
{
    GetOwner()->RemovePet(this, slot, returnreagent, stampeded);
}

void Pet::GivePetXP(uint32 xp)
{
    if (GetPetType() != HUNTER_PET)
        return;

    if (xp < 1)
        return;

    if (!IsAlive())
        return;

    uint8 maxlevel = std::min((uint8)sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL), GetOwner()->GetLevel());
    uint8 petlevel = GetLevel();

    // If pet is detected to be at, or above(?) the players level, don't hand out XP
    if (petlevel >= maxlevel)
       return;

    uint32 nextLvlXP = GetUInt32Value(UNIT_PET_NEXT_LEVEL_EXPERIENCE);
    uint32 curXP = GetUInt32Value(UNIT_PET_EXPERIENCE);
    uint32 newXP = curXP + xp;

    // Check how much XP the pet should receive, and hand off have any left from previous levelups
    while (newXP >= nextLvlXP && petlevel < maxlevel)
    {
        // Subtract newXP from amount needed for nextlevel, and give pet the level
        newXP -= nextLvlXP;
        ++petlevel;

        GivePetLevel(petlevel);

        nextLvlXP = GetUInt32Value(UNIT_PET_NEXT_LEVEL_EXPERIENCE);
    }
    // Not affected by special conditions - give it new XP
    SetUInt32Value(UNIT_PET_EXPERIENCE, petlevel < maxlevel ? newXP : 0);
}

void Pet::GivePetLevel(uint8 level)
{
    if (!level || level == GetLevel())
        return;

    if (GetPetType() == HUNTER_PET)
    {
        SetUInt32Value(UNIT_PET_EXPERIENCE, 0);
        SetUInt32Value(UNIT_PET_NEXT_LEVEL_EXPERIENCE, uint32(sObjectMgr->GetXPForLevel(level) * PET_XP_FACTOR));
    }

    InitStatsForLevel(level);
    InitLevelupSpellsForLevel();
}

bool Pet::CreateBaseAtCreature(Creature* creature)
{
    ASSERT(creature);

    if (!CreateBaseAtTamed(creature->GetCreatureTemplate(), creature->GetMap()))
        return false;

    Relocate(creature->GetPositionX(), creature->GetPositionY(), creature->GetPositionZ(), creature->GetOrientation());

    if (!IsPositionValid())
    {
        TC_LOG_ERROR("entities.pet", "Pet (guidlow %d, entry %d) not created base at creature. Suggested coordinates isn't valid (X: %f Y: %f)",
            GetGUID().GetCounter(), GetEntry(), GetPositionX(), GetPositionY());
        return false;
    }

    CreatureTemplate const* cinfo = GetCreatureTemplate();
    if (!cinfo)
    {
        TC_LOG_ERROR("entities.pet", "CreateBaseAtCreature() failed, creatureInfo is missing!");
        return false;
    }

    SetDisplayId(creature->GetDisplayId());

    if (CreatureFamilyEntry const* cFamily = sCreatureFamilyStore.LookupEntry(cinfo->family))
        SetName(cFamily->Name);
    else
        SetName(creature->GetName());

    Transport* transport = GetTransportGUID().IsEmpty() ? creature->GetTransport() : nullptr;
    if (transport)
    {
        float x, y, z, o;
        GetPosition(x, y, z, o);
        transport->CalculatePassengerOffset(x, y, z, &o);
        SetTransportPosition(x, y, z, o);

        transport->AddPassenger(this);
    }

    return true;
}

bool Pet::CreateBaseAtCreatureInfo(CreatureTemplate const* cinfo, Unit* owner)
{
    if (!CreateBaseAtTamed(cinfo, owner->GetMap()))
        return false;

    if (CreatureFamilyEntry const* cFamily = sCreatureFamilyStore.LookupEntry(cinfo->family))
        SetName(cFamily->Name);

    Relocate(owner->GetPositionX(), owner->GetPositionY(), owner->GetPositionZ(), owner->GetOrientation());

    Transport* transport = GetTransportGUID().IsEmpty() ? owner->GetTransport() : nullptr;
    if (transport)
    {
        float x, y, z, o;
        GetPosition(x, y, z, o);
        transport->CalculatePassengerOffset(x, y, z, &o);
        SetTransportPosition(x, y, z, o);

        transport->AddPassenger(this);
    }

    return true;
}

bool Pet::CreateBaseAtTamed(CreatureTemplate const* cinfo, Map* map)
{
    TC_LOG_DEBUG("entities.pet", "Pet::CreateBaseForTamed");
    uint32 guid = map->GenerateLowGuid<HighGuid::Pet>();
    if (!Create(guid, map, cinfo->Entry))
        return false;

    SetPowerType(POWER_FOCUS);
    SetUInt32Value(UNIT_PET_NAME_TIMESTAMP, 0);
    SetUInt32Value(UNIT_PET_EXPERIENCE, 0);
    SetUInt32Value(UNIT_PET_NEXT_LEVEL_EXPERIENCE, uint32(sObjectMgr->GetXPForLevel(GetLevel()+1)*PET_XP_FACTOR));
    SetUInt64Value(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_NONE);

    if (cinfo->type == CREATURE_TYPE_BEAST)
    {
        SetClass(CLASS_WARRIOR);
        SetGender(GENDER_NONE);
        SetFieldPowerType(POWER_FOCUS);
        SetSheath(SHEATH_STATE_MELEE);
        SetByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_RENAME_STATE, UNIT_CAN_BE_RENAMED | UNIT_CAN_BE_ABANDONED);
    }

    return true;
}

/// @todo Move stat mods code to pet passive auras
bool Guardian::InitStatsForLevel(uint8 petlevel)
{
    CreatureTemplate const* cinfo = GetCreatureTemplate();
    ASSERT(cinfo);

    SetLevel(petlevel);

    //Determine pet type
    PetType petType = MAX_PET_TYPE;
    if (IsPet() && GetOwner()->GetTypeId() == TYPEID_PLAYER)
    {
        if ((m_owner->GetClass() == CLASS_WARLOCK) || (m_owner->GetClass() == CLASS_SHAMAN) ||
            (m_owner->GetClass() == CLASS_PRIEST) || (m_owner->GetClass() == CLASS_DEATH_KNIGHT))
        {
            petType = SUMMON_PET;
        }
        else if (GetOwner()->GetClass() == CLASS_HUNTER)
        {
            petType = HUNTER_PET;
            m_unitTypeMask |= UNIT_MASK_HUNTER_PET;
        }
        else
        {
            TC_LOG_ERROR("entities.pet", "Unknown type pet %u is summoned by player class %u", GetEntry(), GetOwner()->GetClass());
        }
    }

    uint32 creature_ID = (petType == HUNTER_PET) ? 1 : cinfo->Entry;

    SetMeleeDamageSchool(SpellSchools(cinfo->dmgschool));

    SetStatFlatModifier(UNIT_MOD_ARMOR, BASE_VALUE, float(petlevel * 50));

    SetBaseAttackTime(BASE_ATTACK, BASE_ATTACK_TIME);
    SetBaseAttackTime(OFF_ATTACK, BASE_ATTACK_TIME);
    SetBaseAttackTime(RANGED_ATTACK, BASE_ATTACK_TIME);

    SetFloatValue(UNIT_MOD_CASTING_SPEED, 1.0f);
    SetFloatValue(UNIT_MOD_SPELL_HASTE, 1.0f);

    //scale
    CreatureFamilyEntry const* cFamily = sCreatureFamilyStore.LookupEntry(cinfo->family);
    if (cFamily && cFamily->MinScale > 0.0f && petType == HUNTER_PET)
    {
        float scale;
        if (GetLevel() >= cFamily->MaxScaleLevel)
        {
            if (cinfo->type_flags & CREATURE_TYPE_FLAG_EXOTIC_PET)
                scale = 1.0f;
            else
                scale = cFamily->MaxScale;
        }
        else if (GetLevel() <= cFamily->MinScaleLevel)
            scale = cFamily->MinScale;
        else
            scale = cFamily->MinScale + float(GetLevel() - cFamily->MinScaleLevel) / cFamily->MaxScaleLevel * (cFamily->MaxScale - cFamily->MinScale);

        SetObjectScale(scale);
    }

    // Resistance
    // Hunters pet should not inherit resistances from creature_template, they have separate auras for that
    if (!IsHunterPet())
        for (uint8 i = SPELL_SCHOOL_HOLY; i < MAX_SPELL_SCHOOL; ++i)
            SetStatFlatModifier(UnitMods(UNIT_MOD_RESISTANCE_START + i), BASE_VALUE, float(cinfo->resistance[i]));

    //health, mana, armor and resistance
    PetLevelInfo const* pInfo = sObjectMgr->GetPetLevelInfo(creature_ID, petlevel);
    if (pInfo)                                      // exist in DB
    {
        if (creature_ID != 510)
            SetCreateHealth(pInfo->health);
        if (petType != HUNTER_PET && GetOwner() && GetOwner()->GetClass() != CLASS_WARLOCK && creature_ID != 510) // hunter's pets use focus and Warlock's pets use energy
            SetCreateMana(pInfo->mana);

        if (pInfo->armor > 0)
            SetStatFlatModifier(UNIT_MOD_ARMOR, BASE_VALUE, float(pInfo->armor));

        for (uint8 stat = 0; stat < MAX_STATS; ++stat)
            SetCreateStat(Stats(stat), float(pInfo->stats[stat]));
    }
    else                                            // not exist in DB, use some default fake data
    {
        // remove elite bonuses included in DB values
        CreatureBaseStats const* stats = sObjectMgr->GetCreatureBaseStats(petlevel, cinfo->unit_class);
        SetCreateHealth(stats->BaseHealth[cinfo->expansion]);
        SetCreateMana(stats->BaseMana);

        SetCreateStat(STAT_STRENGTH, 22);
        SetCreateStat(STAT_AGILITY, 22);
        SetCreateStat(STAT_STAMINA, 25);
        SetCreateStat(STAT_INTELLECT, 28);
        SetCreateStat(STAT_SPIRIT, 27);
    }

    SetBonusDamage(0);
    switch (petType)
    {
        case SUMMON_PET:
        {
            int32 fire = GetOwner()->GetUInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + SPELL_SCHOOL_FIRE);
            int32 shadow = GetOwner()->GetUInt32Value(PLAYER_MOD_DAMAGE_DONE_POS + SPELL_SCHOOL_SHADOW);
            int32 val = (fire > shadow) ? fire : shadow;
            if (val < 0)
                val = 0;

            SetBonusDamage(val);

            SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel - (petlevel / 4)));
            SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel + (petlevel / 4)));
            break;
        }
        case HUNTER_PET:
        {
            SetUInt32Value(UNIT_PET_NEXT_LEVEL_EXPERIENCE, uint32(sObjectMgr->GetXPForLevel(petlevel) * PET_XP_FACTOR));
            SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel - (petlevel / 4)));
            SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel + (petlevel / 4)));

            if (GetOwner()->ToPlayer())
                ApplyAttackTimePercentMod(BASE_ATTACK, GetOwner()->ToPlayer()->GetRatingBonusValue(CR_HASTE_RANGED), true);

            break;
        }
        default:
        {
            switch (GetEntry())
            {
                case PET_ENTRY_IMP:
                case PET_ENTRY_VOIDWALKER:
                case PET_ENTRY_SUCCUBUS:
                case PET_ENTRY_FELHUNTER:
                case PET_ENTRY_FELGUARD:
                {
                    if (GetPowerType() != POWER_ENERGY)
                        SetPowerType(POWER_ENERGY);

                    SetMaxPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
                    SetPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
                    break;
                }
                case PET_ENTRY_WATER_ELEMENTAL: // Summon Water Elemental Spell
                {
                    SetCreateHealth(m_owner->CountPctFromMaxHealth(50));
                    SetCreateMana(m_owner->GetMaxPower(POWER_MANA));
                    SetBonusDamage(int32(m_owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_FROST)));
                    break;
                }
                case PET_ENTRY_TREANT_GUARDIAN:
                case PET_ENTRY_TREANT_FERAL:
                case PET_ENTRY_TREANT_BALANCE:
                case PET_ENTRY_TREANT_RESTO:
                {
                    SetCreateHealth(m_owner->CountPctFromMaxHealth(10));
                    float bonusDmg = m_owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_NATURE) * 0.15f;
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel * 2.5f - (petlevel / 2) + bonusDmg));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel * 2.5f + (petlevel / 2) + bonusDmg));
                    break;
                }
                case PET_ENTRY_EARTH_ELEMENTAL: //earth elemental 36213
                {
                    if (!pInfo)
                        SetCreateHealth(100 + 120*petlevel);
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel - (petlevel / 4)));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel + (petlevel / 4)));
                    break;
                }
                case PET_ENTRY_FIRE_ELEMENTAL: //fire elemental
                {
                    if (!pInfo)
                    {
                        SetCreateHealth(40*petlevel);
                        SetCreateMana(28 + 10*petlevel);
                    }
                    SetBonusDamage(int32(GetOwner()->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_FIRE) * 0.5f));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel * 4 - petlevel));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel * 4 + petlevel));
                    break;
                }
                case PET_ENTRY_SHADOWFIEND: // Shadowfiend
                case PET_ENTRY_SHADOWFIEND_SHA:
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10*petlevel);
                        SetCreateHealth(28 + 30*petlevel);
                    }
                    int32 bonus_dmg = int32(GetOwner()->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_SHADOW) * 0.3f);
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float((petlevel * 4 - petlevel) + bonus_dmg));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float((petlevel * 4 + petlevel) + bonus_dmg));

                    break;
                }
                case PET_ENTRY_VENOMOUS_SNAKE: //Snake Trap - Venomous Snake
                {
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float((petlevel / 2) - 25));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float((petlevel / 2) - 18));
                    break;
                }
                case PET_ENTRY_VIPER: //Snake Trap - Viper
                {
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel / 2 - 10));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel / 2));
                    break;
                }
                case PET_ENTRY_FERAL_SPIRIT: // Feral Spirit
                {
                    if (!pInfo)
                        SetCreateHealth(30*petlevel);

                    // wolf attack speed is 1.5s
                    SetBaseAttackTime(BASE_ATTACK, cinfo->BaseAttackTime);

                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float((petlevel * 4 - petlevel)));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float((petlevel * 4 + petlevel)));

                    SetStatFlatModifier(UNIT_MOD_ARMOR, BASE_VALUE, float(GetOwner()->GetArmor()) * 0.35f);  // Bonus Armor (35% of player armor)
                    SetStatFlatModifier(UNIT_MOD_STAT_STAMINA, BASE_VALUE, float(GetOwner()->GetStat(STAT_STAMINA)) * 0.3f);  // Bonus Stamina (30% of player stamina)
                    if (!HasAura(58877))//prevent apply twice for the 2 wolves
                        AddAura(58877, this);//Spirit Hunt, passive, Spirit Wolves' attacks heal them and their master for 150% of damage done.
                    break;
                }
                case PET_ENTRY_MIRROR_IMAGE: // Mirror Image
                {
                    SetBonusDamage(int32(GetOwner()->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_FROST) * 0.33f));
                    SetDisplayId(GetOwner()->GetDisplayId());
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 30*petlevel);
                        SetCreateHealth(28 + 10*petlevel);
                    }
                    break;
                }
                case PET_ENTRY_GARGOYLE: // Ebon Gargoyle
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10*petlevel);
                        SetCreateHealth(28 + 30*petlevel);
                    }

                    // Convert Owner's haste into the Gargoyle spell haste
                    float ownerHaste = 1.0f  +  ((Player*)m_owner)->GetUInt32Value(PLAYER_COMBAT_RATINGS + CR_HASTE_MELEE) *
                                                ((Player*)m_owner)->GetRatingMultiplier(CR_HASTE_MELEE) / 100.0f;

                    float castAmount = GetFloatValue(UNIT_MOD_CASTING_SPEED);
                    ApplyPercentModFloatVar(castAmount, ownerHaste, false);
                    SetFloatValue(UNIT_MOD_CASTING_SPEED, castAmount);

                    // also make gargoyle benefit from haste auras, like unholy presence
                    int meleeHaste = ((Player*)m_owner)->GetTotalAuraModifier(SPELL_AURA_MOD_MELEE_HASTE);
                    ApplyCastTimePercentMod(meleeHaste, true);

                    SetBonusDamage(int32(m_owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.5f));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel - (petlevel / 4)));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel + (petlevel / 4)));
                    break;
                }
                case PET_ENTRY_BLOODWORM: // Bloodworms
                {
                    SetCreateHealth(m_owner->CountPctFromMaxHealth(6));
                    SetBonusDamage(int32(m_owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.006f));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel - 30 - (petlevel / 4)));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel - 30 + (petlevel / 4)));
                    break;
                }
                case PET_ENTRY_GUARDIAN_OF_ANCIENT_KINGS:
                {
                    if (Player* pOwner = m_owner->ToPlayer())
                    {
                        m_modMeleeHitChance = pOwner->GetFloatValue(PLAYER_UI_HIT_MODIFIER) + pOwner->GetRatingBonusValue(CR_HIT_MELEE);
                        m_baseSpellCritChance = pOwner->GetFloatValue(PLAYER_CRIT_PERCENTAGE) + pOwner->GetRatingBonusValue(CR_HIT_SPELL);
                    }

                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, 0.75f * m_owner->GetFloatValue(UNIT_MIN_DAMAGE));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, 0.75f * m_owner->GetFloatValue(UNIT_MAX_DAMAGE));
                    break;
                }
                case PET_ENTRY_HEALING_TIDE_TOTEM:
                {
                    SetCreateHealth(m_owner->CountPctFromMaxHealth(10));
                    break;
                }
                case PET_ENTRY_WILD_IMP:
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10 * petlevel);
                        SetCreateHealth(28 + 30 * petlevel);
                    }

                    SetBonusDamage(int32(m_owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_FIRE)));
                    break;
                }
                case PET_ENTRY_XUEN_THE_WHITE_TIGER:
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10 * petlevel);
                        SetCreateHealth(28 + 30 * petlevel);
                    }

                    SetBonusDamage(int32(m_owner->GetTotalAttackPowerValue(BASE_ATTACK)));
                    int32 bonus_dmg = (int32(m_owner->GetTotalAttackPowerValue(BASE_ATTACK) * 0.25f));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel * 4 - petlevel + bonus_dmg));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel * 4 + petlevel + bonus_dmg));
                    SetBaseAttackTime(BASE_ATTACK, 1 * IN_MILLISECONDS);
                    break;
                }
                case PET_ENTRY_MURDER_OF_CROWS:
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10 * petlevel);
                        SetCreateHealth(28 + 30 * petlevel);
                    }

                    int32 bonus_dmg = (int32(m_owner->GetTotalAttackPowerValue(RANGED_ATTACK) * 0.1f));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel * 4 - petlevel + bonus_dmg));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel * 4 + petlevel + bonus_dmg));

                    break;
                }
                case PET_ENTRY_DIRE_BEAST_DUNGEONS:
                case PET_ENTRY_DIRE_BEAST_VALLEY_OF_THE_FW:
                case PET_ENTRY_DIRE_BEAST_KALIMDOR:
                case PET_ENTRY_DIRE_BEAST_ESTERN_KINGDOMS:
                case PET_ENTRY_DIRE_BEAST_OUTLAND:
                case PET_ENTRY_DIRE_BEAST_NORTHREND:
                case PET_ENTRY_DIRE_BEAST_KRASARANG_WILDS:
                case PET_ENTRY_DIRE_BEAST_JADE_FOREST:
                case PET_ENTRY_DIRE_BEAST_VALE_OF_ETERNAL_BLOSSOMS:
                case PET_ENTRY_DIRE_BEAST_KUNLAI_SUMMIT:
                case PET_ENTRY_DIRE_BEAST_TOWNLONG_STEPPES:
                case PET_ENTRY_DIRE_BEAST_DREAD_WASTES:
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10 * petlevel);
                        SetCreateHealth(28 + 30 * petlevel);
                    }

                    int32 bonus_dmg = (int32(m_owner->GetTotalAttackPowerValue(RANGED_ATTACK) * 0.2f));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(petlevel * 4 - petlevel + bonus_dmg));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(petlevel * 4 + petlevel + bonus_dmg));
                    break;
                }
                case PET_ENTRY_MINDBENDER:
                case PET_ENTRY_MINDBENDER_SHA:
                {
                    if (!pInfo)
                    {
                        SetCreateMana(28 + 10 * petlevel);
                        SetCreateHealth(28 + 30 * petlevel);
                    }

                    int32 bonus_dmg = (int32(m_owner->SpellBaseDamageBonusDone(SPELL_SCHOOL_MASK_SHADOW)));
                    SetBaseWeaponDamage(BASE_ATTACK, MINDAMAGE, float(((petlevel * 4 - petlevel) + bonus_dmg) * 1.8f));
                    SetBaseWeaponDamage(BASE_ATTACK, MAXDAMAGE, float(((petlevel * 4 + petlevel) + bonus_dmg) * 1.8f));
                    break;
                }
                default:
                    break;
            }
            break;
        }
    }

    UpdateAllStats();

    SetFullHealth();

    switch (GetOwner()->GetClass())
    {
        case CLASS_WARLOCK:
            SetPower(POWER_ENERGY, GetCreatePowers(POWER_ENERGY));
            break;
        case CLASS_HUNTER:
            if (petType == HUNTER_PET)
                SetPower(POWER_FOCUS, GetCreatePowers(POWER_FOCUS));
            break;
        default:
            SetPower(POWER_MANA, GetMaxPower(POWER_MANA));
            break;
    }

    return true;
}

bool Pet::HaveInDiet(ItemTemplate const* item) const
{
    if (item->GetSubClass() != ITEM_SUBCLASS_FOOD_DRINK)
        return false;

    CreatureTemplate const* cInfo = GetCreatureTemplate();
    if (!cInfo)
        return false;

    CreatureFamilyEntry const* cFamily = sCreatureFamilyStore.LookupEntry(cInfo->family);
    if (!cFamily)
        return false;

    uint32 diet = cFamily->PetFoodMask;
    uint32 FoodMask = 1 << (item->FoodType-1);
    return diet & FoodMask;
}

uint32 Pet::GetCurrentFoodBenefitLevel(uint32 itemlevel) const
{
    /// -10 or greater food level
    if (GetLevel() <= itemlevel + 10)                       // possible to feed level 85 pet with ilevel 75 level food for full effect
        return 50;
    // -10 to -20
    else if (GetLevel() <= itemlevel + 20)
        return 25;
    // -20 to -30
    else if (GetLevel() <= itemlevel + 30)
        return 13;
    // -30 or more difference
    else
        return 0;                                           //food too low level
}

void Pet::_LoadSpellCooldowns(PreparedQueryResult cooldownsResult, PreparedQueryResult chargesResult)
{
    GetSpellHistory()->LoadFromDB<Pet>(cooldownsResult, chargesResult);
}

void Pet::_LoadSpells(PreparedQueryResult resultSpell)
{

    if (resultSpell)
    {
        do
        {
            Field* fields = resultSpell->Fetch();

            AddSpell(fields[0].GetUInt32(), ActiveStates(fields[1].GetUInt8()), PETSPELL_UNCHANGED);
        }
        while (resultSpell->NextRow());
    }
}

void Pet::_SaveSpells(SQLTransaction& trans)
{
    for (PetSpellMap::iterator itr = m_spells.begin(), next = m_spells.begin(); itr != m_spells.end(); itr = next)
    {
        ++next;

        // prevent saving family passives to DB
        if (itr->second.type == PETSPELL_FAMILY)
            continue;

        PreparedStatement* stmt;

        switch (itr->second.state)
        {
            case PETSPELL_REMOVED:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_SPELL_BY_SPELL);
                stmt->setUInt32(0, m_charmInfo->GetPetNumber());
                stmt->setUInt32(1, itr->first);
                trans->Append(stmt);

                m_spells.erase(itr);
                continue;
            case PETSPELL_CHANGED:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_SPELL_BY_SPELL);
                stmt->setUInt32(0, m_charmInfo->GetPetNumber());
                stmt->setUInt32(1, itr->first);
                trans->Append(stmt);

                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_PET_SPELL);
                stmt->setUInt32(0, m_charmInfo->GetPetNumber());
                stmt->setUInt32(1, itr->first);
                stmt->setUInt8(2, itr->second.active);
                trans->Append(stmt);

                break;
            case PETSPELL_NEW:
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_PET_SPELL);
                stmt->setUInt32(0, m_charmInfo->GetPetNumber());
                stmt->setUInt32(1, itr->first);
                stmt->setUInt8(2, itr->second.active);
                trans->Append(stmt);
                break;
            case PETSPELL_UNCHANGED:
                continue;
        }
        itr->second.state = PETSPELL_UNCHANGED;
    }
}

void Pet::_LoadAuras(PreparedQueryResult auraResult, PreparedQueryResult auraEffectResult, uint32 timediff)
{
    ObjectGuid casterGuid = ObjectGuid::Empty;
    std::map<AuraKey, AuraLoadEffectInfo> effectInfo;

    if (auraEffectResult)
    {
        do
        {
            Field* fields = auraEffectResult->Fetch();
            uint32 effectIndex = fields[3].GetUInt8();
            if (effectIndex < MAX_SPELL_EFFECTS)
            {
                casterGuid.Set(fields[0].GetUInt64());
                if (casterGuid.IsEmpty())
                    casterGuid = GetGUID();

                AuraKey key{ casterGuid, fields[1].GetUInt32(), fields[2].GetUInt32() };
                AuraLoadEffectInfo& info = effectInfo[key];
                info.Amounts[effectIndex] = fields[4].GetInt32();
                info.BaseAmounts[effectIndex] = fields[5].GetInt32();
            }
        } while (auraEffectResult->NextRow());
    }

    if (auraResult)
    {
        do
        {
            Field* fields = auraResult->Fetch();
            // NULL guid stored - pet is the caster of the spell - see Pet::_SaveAuras
            casterGuid.Set(fields[0].GetUInt64());
            if (casterGuid.IsEmpty())
                casterGuid = GetGUID();

            AuraKey key{ casterGuid, fields[1].GetUInt32(), fields[2].GetUInt32() };
            uint32 recalculateMask = fields[3].GetUInt32();
            uint8 stackCount = fields[4].GetUInt8();
            int32 maxDuration = fields[5].GetInt32();
            int32 remainTime = fields[6].GetInt32();
            uint8 remainCharges = fields[7].GetUInt8();

            SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(key.SpellId);
            if (!spellInfo)
            {
                TC_LOG_ERROR("entities.pet", "Unknown aura (spellid %u), ignore.", key.SpellId);
                continue;
            }

            // negative effects should continue counting down after logout
            if (remainTime != -1 && !spellInfo->IsPositive())
            {
                if (remainTime/IN_MILLISECONDS <= int32(timediff))
                    continue;

                remainTime -= timediff*IN_MILLISECONDS;
            }

            // prevent wrong values of remainCharges
            if (spellInfo->ProcCharges)
            {
                // we have no control over the order of applying auras and modifiers allow auras
                // to have more charges than value in SpellInfo
                if (remainCharges <= 0/* || remainCharges > spellproto->procCharges*/)
                    remainCharges = spellInfo->ProcCharges;
            }
            else
                remainCharges = 0;

            AuraLoadEffectInfo& info = effectInfo[key];
            if (Aura* aura = Aura::TryCreate(spellInfo, key.EffectMask, this, NULL, info.BaseAmounts.data(), NULL, casterGuid))
            {
                if (!aura->CanBeSaved())
                {
                    aura->Remove();
                    continue;
                }

                aura->SetLoadedState(maxDuration, remainTime, remainCharges, stackCount, recalculateMask, info.Amounts.data());
                aura->ApplyForTargets();
                TC_LOG_DEBUG("entities.pet", "Added aura spellid %u, effectmask %u", spellInfo->Id, key.EffectMask);
            }
        }
        while (auraResult->NextRow());
    }
}

void Pet::_SaveAuras(SQLTransaction& trans)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_AURA_EFFECTS);
    stmt->setUInt32(0, m_charmInfo->GetPetNumber());
    trans->Append(stmt);

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_PET_AURAS);
    stmt->setUInt32(0, m_charmInfo->GetPetNumber());
    trans->Append(stmt);

    uint8 index;
    for (AuraMap::const_iterator itr = m_ownedAuras.begin(); itr != m_ownedAuras.end(); ++itr)
    {
        // check if the aura has to be saved
        if (!itr->second->CanBeSaved() || IsPetAura(itr->second))
            continue;

        Aura* aura = itr->second;
        uint32 recalculateMask = 0;
        AuraKey key = aura->GenerateKey(recalculateMask);

        // don't save guid of caster in case we are caster of the spell - guid for pet is generated every pet load, so it won't match saved guid anyways
        if (key.Caster == GetGUID())
            key.Caster = ObjectGuid::Empty;

        index = 0;
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_PET_AURA);
        stmt->setUInt32(index++, m_charmInfo->GetPetNumber());
        stmt->setUInt64(index++, key.Caster.GetRawValue());
        stmt->setUInt32(index++, key.SpellId);
        stmt->setUInt32(index++, key.EffectMask);
        stmt->setUInt32(index++, recalculateMask);
        stmt->setUInt8(index++, aura->GetStackAmount());
        stmt->setInt32(index++, aura->GetMaxDuration());
        stmt->setInt32(index++, aura->GetDuration());
        stmt->setUInt8(index++, aura->GetCharges());
        trans->Append(stmt);

        for (AuraEffect const* effect : aura->GetAuraEffects())
        {
            if (effect)
            {
                index = 0;
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_PET_AURA_EFFECT);
                stmt->setUInt32(index++, m_charmInfo->GetPetNumber());
                stmt->setUInt64(index++, key.Caster.GetRawValue());
                stmt->setUInt32(index++, key.SpellId);
                stmt->setUInt32(index++, key.EffectMask);
                stmt->setUInt8(index++, effect->GetEffIndex());
                stmt->setInt32(index++, effect->GetAmount());
                stmt->setInt32(index++, effect->GetBaseAmount());
                trans->Append(stmt);
            }
        }
    }
}

bool Pet::AddSpell(uint32 spellId, ActiveStates active /*= ACT_DECIDE*/, PetSpellState state /*= PETSPELL_NEW*/, PetSpellType type /*= PETSPELL_NORMAL*/)
{
    SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(spellId);
    if (!spellInfo)
    {
        // do pet spell book cleanup
        if (state == PETSPELL_UNCHANGED)                    // spell load case
        {
            TC_LOG_ERROR("entities.pet", "Pet::addSpell: Non-existed in SpellStore spell #%u request, deleting for all pets in `pet_spell`.", spellId);

            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_INVALID_PET_SPELL);

            stmt->setUInt32(0, spellId);

            CharacterDatabase.Execute(stmt);
        }
        else
            TC_LOG_ERROR("entities.pet", "Pet::addSpell: Non-existed in SpellStore spell #%u request.", spellId);

        return false;
    }

    PetSpellMap::iterator itr = m_spells.find(spellId);
    if (itr != m_spells.end())
    {
        if (itr->second.state == PETSPELL_REMOVED)
        {
            m_spells.erase(itr);
            state = PETSPELL_CHANGED;
        }
        else if (state == PETSPELL_UNCHANGED && itr->second.state != PETSPELL_UNCHANGED)
        {
            // can be in case spell loading but learned at some previous spell loading
            itr->second.state = PETSPELL_UNCHANGED;

            if (active == ACT_ENABLED)
                ToggleAutocast(spellInfo, true);
            else if (active == ACT_DISABLED)
                ToggleAutocast(spellInfo, false);

            return false;
        }
        else
            return false;
    }

    PetSpell newspell;
    newspell.state = state;
    newspell.type = type;

    if (active == ACT_DECIDE)                               // active was not used before, so we save it's autocast/passive state here
    {
        if (spellInfo->IsAutocastable())
            newspell.active = ACT_DISABLED;
        else
            newspell.active = ACT_PASSIVE;
    }
    else
        newspell.active = active;

    if (spellInfo->IsRanked())
    {
        for (PetSpellMap::const_iterator itr2 = m_spells.begin(); itr2 != m_spells.end(); ++itr2)
        {
            if (itr2->second.state == PETSPELL_REMOVED)
                continue;

            SpellInfo const* oldRankSpellInfo = sSpellMgr->GetSpellInfo(itr2->first);

            if (!oldRankSpellInfo)
                continue;

            if (spellInfo->IsDifferentRankOf(oldRankSpellInfo))
            {
                // replace by new high rank
                if (spellInfo->IsHighRankOf(oldRankSpellInfo))
                {
                    newspell.active = itr2->second.active;

                    if (newspell.active == ACT_ENABLED)
                        ToggleAutocast(oldRankSpellInfo, false);

                    UnlearnSpell(itr2->first, false, false);
                    break;
                }
                // ignore new lesser rank
                else
                    return false;
            }
        }
    }

    m_spells[spellId] = newspell;

    if (spellInfo->IsPassive() && (!spellInfo->CasterAuraState || HasAuraState(AuraStateType(spellInfo->CasterAuraState))))
        CastSpell(this, spellId, true);
    else
        m_charmInfo->AddSpellToActionBar(spellInfo);

    if (newspell.active == ACT_ENABLED)
        ToggleAutocast(spellInfo, true);

    return true;
}

bool Pet::LearnSpell(uint32 spellId)
{
    if (!AddSpell(spellId))
        return false;

    if (!m_loading)
    {
        WorldPacket data(SMSG_PET_LEARNED_SPELLS, 3 + 4);

        data.WriteBits(1, 22);

        data.FlushBits();

        data << uint32(spellId);

        GetOwner()->SendDirectMessage(&data);

        GetOwner()->PetSpellInitialize();
    }

    return true;
}

void Pet::LearnSpells(std::vector<uint32> const& spellIds)
{
    if (!m_loading)
    {
        std::vector<uint32> spells;
        for (uint32 spell : spellIds)
        {
            if (!AddSpell(spell))
                continue;

            spells.push_back(spell);
        }

        WorldPacket data(SMSG_PET_LEARNED_SPELLS, 3 + spells.size() * (4));

        data.WriteBits(spells.size(), 22);

        data.FlushBits();

        for (uint32 SpellID : spells)
            data << uint32(SpellID);

        GetOwner()->SendDirectMessage(&data);

        GetOwner()->PetSpellInitialize();
    }
}

bool Pet::UnlearnSpell(uint32 spellId, bool learn_prev, bool clear_ab)
{
    if (RemoveSpell(spellId, learn_prev, clear_ab))
    {
        if (!m_loading)
        {
            WorldPacket data(SMSG_PET_UNLEARNED_SPELLS, 3 + 4);

            data.WriteBits(1, 22);

            data.FlushBits();

            data << uint32(spellId);

            GetOwner()->SendDirectMessage(&data);

            GetOwner()->PetSpellInitialize();
        }
        return true;
    }
    return false;
}

void Pet::UnlearnSpells(std::vector<uint32> const& spellIds, bool learn_prev, bool clear_ab)
{
    if (!m_loading)
    {
        std::vector<uint32> spells;
        for (uint32 spell : spellIds)
        {
            if (!RemoveSpell(spell, learn_prev, clear_ab))
                continue;

            spells.push_back(spell);
        }

        WorldPacket data(SMSG_PET_UNLEARNED_SPELLS, 3 + spells.size() * (4));

        data.WriteBits(spells.size(), 22);

        data.FlushBits();

        for (uint32 SpellID : spells)
            data << uint32(SpellID);

        GetOwner()->SendDirectMessage(&data);
    }
}

bool Pet::RemoveSpell(uint32 spell_id, bool learn_prev, bool clear_ab)
{
    PetSpellMap::iterator itr = m_spells.find(spell_id);
    if (itr == m_spells.end())
        return false;

    if (itr->second.state == PETSPELL_REMOVED)
        return false;

    if (itr->second.state == PETSPELL_NEW)
        m_spells.erase(itr);
    else
        itr->second.state = PETSPELL_REMOVED;

    RemoveAurasDueToSpell(spell_id);

    if (learn_prev)
    {
        if (uint32 prev_id = sSpellMgr->GetPrevSpellInChain(spell_id))
            LearnSpell(prev_id);
        else
            learn_prev = false;
    }

    // if remove last rank or non-ranked then update action bar at server and client if need
    if (m_charmInfo->RemoveSpellFromActionBar(spell_id) && !learn_prev && clear_ab)
    {
        if (!m_loading)
        {
            // need update action bar for last removed rank
            if (Unit* owner = GetOwner())
                if (owner->GetTypeId() == TYPEID_PLAYER)
                    owner->ToPlayer()->PetSpellInitialize();
        }
    }

    return true;
}

void Pet::InitLevelupSpellsForLevel()
{
    uint8 level = GetLevel();

    std::vector<uint32> learnSpells;
    std::vector<uint32> unlearnSpells;

    if (PetLevelupSpellSet const* levelupSpells = GetCreatureTemplate()->family ? sSpellMgr->GetPetLevelupSpellList(GetCreatureTemplate()->family) : NULL)
    {
        // PetLevelupSpellSet ordered by levels, process in reversed order
        for (PetLevelupSpellSet::const_reverse_iterator itr = levelupSpells->rbegin(); itr != levelupSpells->rend(); ++itr)
        {
            // will called first if level down
            if (itr->first > level)
                unlearnSpells.push_back(itr->second);                      // will learn prev rank if any
            // will called if level up
            else
                learnSpells.push_back(itr->second);                        // will unlearn prev rank if any
        }
    }

    int32 petSpellsId = GetCreatureTemplate()->PetSpellDataId ? -(int32)GetCreatureTemplate()->PetSpellDataId : GetEntry();

    // default spells (can be not learned if pet level (as owner level decrease result for example) less first possible in normal game)
    if (PetDefaultSpellsEntry const* defSpells = sSpellMgr->GetPetDefaultSpellsEntry(petSpellsId))
    {
        for (uint8 i = 0; i < MAX_CREATURE_SPELL_DATA_SLOT; ++i)
        {
            SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(defSpells->spellid[i]);
            if (!spellInfo)
                continue;

            // will called first if level down
            if (spellInfo->SpellLevel > level)
                unlearnSpells.push_back(spellInfo->Id);
            // will called if level up
            else
                learnSpells.push_back(spellInfo->Id);
        }
    }

    if (learnSpells.size() > 0)
        LearnSpells(learnSpells);

    if (unlearnSpells.size() > 0)
        UnlearnSpells(unlearnSpells, true);
}

void Pet::CleanupActionBar()
{
    for (uint8 i = 0; i < MAX_UNIT_ACTION_BAR_INDEX; ++i)
        if (UnitActionBarEntry const* ab = m_charmInfo->GetActionBarEntry(i))
            if (ab->GetAction() && ab->IsActionBarForSpell())
            {
                if (!HasSpell(ab->GetAction()))
                    m_charmInfo->SetActionBar(i, 0, ACT_PASSIVE);
                else if (ab->GetType() == ACT_ENABLED)
                {
                    if (SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(ab->GetAction()))
                        ToggleAutocast(spellInfo, true);
                }
            }
}

void Pet::InitPetCreateSpells()
{
    m_charmInfo->InitPetActionBar();
    m_spells.clear();

    LearnPetPassives();
    InitLevelupSpellsForLevel();

    CastPetAuras(false);
}

void Pet::ToggleAutocast(SpellInfo const* spellInfo, bool apply)
{
    if (!spellInfo->IsAutocastable())
        return;

    uint32 spellid = spellInfo->Id;

    PetSpellMap::iterator itr = m_spells.find(spellid);
    if (itr == m_spells.end())
        return;

    uint32 i;

    if (apply)
    {
        for (i = 0; i < m_autospells.size() && m_autospells[i] != spellid; ++i);    // just search

        if (i == m_autospells.size())
        {
            m_autospells.push_back(spellid);

            if (itr->second.active != ACT_ENABLED)
            {
                itr->second.active = ACT_ENABLED;
                if (itr->second.state != PETSPELL_NEW)
                    itr->second.state = PETSPELL_CHANGED;
            }
        }
    }
    else
    {
        AutoSpellList::iterator itr2 = m_autospells.begin();
        for (i = 0; i < m_autospells.size() && m_autospells[i] != spellid; ++i, ++itr2);    // just search

        if (i < m_autospells.size())
        {
            m_autospells.erase(itr2);
            if (itr->second.active != ACT_DISABLED)
            {
                itr->second.active = ACT_DISABLED;
                if (itr->second.state != PETSPELL_NEW)
                    itr->second.state = PETSPELL_CHANGED;
            }
        }
    }
}

bool Pet::IsPermanentPetFor(Player* owner) const
{
    switch (GetPetType())
    {
        case SUMMON_PET:
            switch (owner->GetClass())
            {
                case CLASS_WARLOCK:
                    return GetCreatureTemplate()->type == CREATURE_TYPE_DEMON;
                case CLASS_DEATH_KNIGHT:
                    return GetCreatureTemplate()->type == CREATURE_TYPE_UNDEAD;
                case CLASS_MAGE:
                    return GetCreatureTemplate()->type == CREATURE_TYPE_ELEMENTAL;
                default:
                    return false;
            }
        case HUNTER_PET:
            return true;
        default:
            return false;
    }
}

bool Pet::Create(uint32 guidlow, Map* map, uint32 Entry)
{
    ASSERT(map);
    SetMap(map);

    Object::_Create(ObjectGuid::Create<HighGuid::Pet>(Entry, guidlow));

    m_spawnId = guidlow;
    m_originalEntry = Entry;

    if (!InitEntry(Entry))
        return false;

    // Force regen flag for player pets, just like we do for players themselves
    SetFlag(UNIT_FLAGS2, UNIT_FLAG2_REGENERATE_POWER);
    SetSheath(SHEATH_STATE_MELEE);

    return true;
}

bool Pet::HasSpell(uint32 spell) const
{
    PetSpellMap::const_iterator itr = m_spells.find(spell);
    return itr != m_spells.end() && itr->second.state != PETSPELL_REMOVED;
}

// Get all passive spells in our skill line
void Pet::LearnPetPassives()
{
    CreatureTemplate const* cInfo = GetCreatureTemplate();
    if (!cInfo)
        return;

    CreatureFamilyEntry const* cFamily = sCreatureFamilyStore.LookupEntry(cInfo->family);
    if (!cFamily)
        return;

    PetFamilySpellsStore::const_iterator petStore = sDBCManager->GetPetFamilySpellsStore().find(cFamily->ID);
    if (petStore != sDBCManager->GetPetFamilySpellsStore().end())
    {
        // For general hunter pets skill 270
        // Passive 01~10, Passive 00 (20782, not used), Ferocious Inspiration (34457)
        // Scale 01~03 (34902~34904, bonus from owner, not used)
        for (PetFamilySpellsSet::const_iterator petSet = petStore->second.begin(); petSet != petStore->second.end(); ++petSet)
            AddSpell(*petSet, ACT_DECIDE, PETSPELL_NEW, PETSPELL_FAMILY);
    }
}

void Pet::CastPetAuras(bool current)
{
    Player* owner = GetOwner();
    if (!owner)
        return;

    if (!IsPermanentPetFor(owner->ToPlayer()))
        return;

    for (auto itr = owner->m_petAuras.begin(); itr != owner->m_petAuras.end();)
    {
        PetAura const* pa = *itr;
        ++itr;

        if (!current && pa->IsRemovedOnChangePet())
            owner->RemovePetAura(pa);
        else
            CastPetAura(pa);
    }
}

void Pet::CastPetAura(PetAura const* aura)
{
    uint32 auraId = aura->GetAura(GetEntry());
    if (!auraId)
        return;

    if (auraId == 35696)                                      // Demonic Knowledge
    {
        int32 basePoints = CalculatePct(aura->GetDamage(), GetStat(STAT_STAMINA) + GetStat(STAT_INTELLECT));
        CastCustomSpell(this, auraId, &basePoints, NULL, NULL, true);
    }
    else
        CastSpell(this, auraId, true);
}

bool Pet::IsPetAura(Aura const* aura)
{
    Player* owner = GetOwner();
    if (!owner)
        return false;

    // if the owner has that pet aura, return true
    for (auto itr = owner->m_petAuras.begin(); itr != owner->m_petAuras.end(); ++itr)
        if ((*itr)->GetAura(GetEntry()) == aura->GetId())
            return true;

    return false;
}

void Pet::SynchronizeLevelWithOwner()
{
    Player* owner = GetOwner();
    if (!owner)
        return;

    switch (GetPetType())
    {
        // always same level
        case SUMMON_PET:
        case HUNTER_PET:
            GivePetLevel(owner->GetLevel());
            break;
        default:
            break;
    }
}

Player* Pet::GetOwner() const
{
    return m_petOwner;
}

void Pet::SetDisplayId(uint32 modelId)
{
    Guardian::SetDisplayId(modelId);

    if (!IsControlled())
        return;

    if (Unit* owner = GetOwner())
        if (Player* player = owner->ToPlayer())
            if (player->GetGroup())
                player->SetGroupUpdateFlag(GROUP_UPDATE_FLAG_PET_MODEL_ID);
}

void Pet::LearnSpecializationSpells()
{
    std::vector<uint32> learnedSpells;

    if (std::vector<SpecializationSpellsEntry const*> const* specSpells = sDBCManager->GetSpecializationSpells(m_petSpecialization))
    {
        for (size_t j = 0; j < specSpells->size(); ++j)
        {
            SpecializationSpellsEntry const* specSpell = specSpells->at(j);
            SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(specSpell->SpellId);
            if (!spellInfo || spellInfo->SpellLevel > GetLevel())
                continue;

            learnedSpells.push_back(specSpell->SpellId);
        }
    }

    LearnSpells(learnedSpells);
}

void Pet::RemoveSpecializationSpells(bool clearActionBar)
{
    std::vector<uint32> unlearnedSpells;

    for (uint32 i = 0; i < MAX_SPECIALIZATIONS; ++i)
    {
        if (ChrSpecializationEntry const* specialization = sDBCManager->GetChrSpecializationByIndexArray(0, i))
        {
            if (std::vector<SpecializationSpellsEntry const*> const* specSpells = sDBCManager->GetSpecializationSpells(specialization->ID))
            {
                for (size_t j = 0; j < specSpells->size(); ++j)
                {
                    SpecializationSpellsEntry const* specSpell = specSpells->at(j);
                    unlearnedSpells.push_back(specSpell->SpellId);
                }
            }
        }
    }

    UnlearnSpells(unlearnedSpells, true, clearActionBar);
}

void Pet::SetSpecialization(uint16 spec)
{
    if (m_petSpecialization == spec)
        return;

    // remove all the old spec's specalization spells, set the new spec, then add the new spec's spells
    // clearActionBars is false because we'll be updating the pet actionbar later so we don't have to do it now
    RemoveSpecializationSpells(false);
    if (!sChrSpecializationStore.LookupEntry(spec))
    {
        m_petSpecialization = 0;
        return;
    }

    m_petSpecialization = spec;
    LearnSpecializationSpells();

    // resend SMSG_PET_SPELLS_MESSAGE to remove old specialization spells from the pet action bar
    CleanupActionBar();
    GetOwner()->PetSpellInitialize();

    WorldPacket data(SMSG_SET_PET_SPECIALIZATION, 4);

    data << uint16(m_petSpecialization);

    GetOwner()->GetSession()->SendPacket(&data);
}

std::string Pet::GenerateActionBarData() const
{
    std::ostringstream ss;

    for (uint32 i = ACTION_BAR_INDEX_START; i < ACTION_BAR_INDEX_END; ++i)
    {
        ss << uint32(m_charmInfo->GetActionBarEntry(i)->GetType()) << ' '
           << uint32(m_charmInfo->GetActionBarEntry(i)->GetAction()) << ' ';
    }

    return ss.str();
}
