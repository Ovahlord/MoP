/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITYCORE_PET_H
#define TRINITYCORE_PET_H

#include "PetDefines.h"
#include "TemporarySummon.h"
#include "WorldSession.h"

struct PetSpell
{
    ActiveStates active;
    PetSpellState state;
    PetSpellType type;
};

typedef std::unordered_map<uint32, PetSpell> PetSpellMap;
typedef std::vector<uint32> AutoSpellList;

class Player;
class PetAura;

class Pet : public Guardian
{
    public:
        explicit Pet(Player* owner, PetType type = MAX_PET_TYPE);
        virtual ~Pet();

        void AddToWorld() override;
        void RemoveFromWorld() override;

        void SetDisplayId(uint32 modelId) override;

        PetType GetPetType() const { return m_petType; }
        void SetPetType(PetType type) { m_petType = type; }
        bool IsControlled() const { return GetPetType() == SUMMON_PET || GetPetType() == HUNTER_PET; }
        bool IsTemporarySummoned() const { return m_duration > 0; }

        bool IsPermanentPetFor(Player* owner) const;        // pet have tab in character windows and set UNIT_PET_NUMBER

        bool Create(uint32 guidlow, Map* map, uint32 Entry);
        bool CreateBaseAtCreature(Creature* creature);
        bool CreateBaseAtCreatureInfo(CreatureTemplate const* cinfo, Unit* owner);
        bool CreateBaseAtTamed(CreatureTemplate const* cinfo, Map* map);
        bool LoadPetFromDB(Player* owner, uint32 petentry = 0, uint32 petnumber = 0, bool current = false, PetSlot slotID = PET_SLOT_SPECIAL_SLOT, bool stampeded = false);
        bool LoadPetFromDB(SQLQueryHolder* holder);
        bool IsLoading() const override { return m_loading; }
        void SavePetToDB(PetSlot slot, bool stampeded = false);
        void Remove(PetSlot slot, bool returnreagent = false, bool stampeded = false);
        static void DeleteFromDB(uint32 guidlow);

        void SetDeathState(DeathState s) override;                   // overwrite virtual Creature::setDeathState and Unit::setDeathState
        bool Update(uint32 diff) override;                           // overwrite virtual Creature::Update and Unit::Update

        uint8 GetPetAutoSpellSize() const override { return m_autospells.size(); }
        uint32 GetPetAutoSpellOnPos(uint8 pos) const override
        {
            if (pos >= m_autospells.size())
                return 0;
            else
                return m_autospells[pos];
        }

        void SetSlot(uint8 slot) { m_slot = slot; }
        uint8 GetSlot() { return m_slot; }

        void GivePetXP(uint32 xp);
        void GivePetLevel(uint8 level);
        void SynchronizeLevelWithOwner();
        bool HaveInDiet(ItemTemplate const* item) const;
        uint32 GetCurrentFoodBenefitLevel(uint32 itemlevel) const;
        void SetDuration(int32 dur) { m_duration = dur; }
        int32 GetDuration() const { return m_duration; }

        void ToggleAutocast(SpellInfo const* spellInfo, bool apply);

        bool HasSpell(uint32 spell) const override;

        void LearnPetPassives();
        void CastPetAuras(bool current);
        void CastPetAura(PetAura const* aura);
        bool IsPetAura(Aura const* aura);

        void _LoadSpellCooldowns(PreparedQueryResult cooldownsResult, PreparedQueryResult chargesResult);
        void _LoadAuras(PreparedQueryResult auraResult, PreparedQueryResult auraEffectResult, uint32 timediff);
        void _SaveAuras(SQLTransaction& trans);
        void _LoadSpells(PreparedQueryResult result);
        void _SaveSpells(SQLTransaction& trans);

        bool AddSpell(uint32 spellId, ActiveStates active = ACT_DECIDE, PetSpellState state = PETSPELL_NEW, PetSpellType type = PETSPELL_NORMAL);
        bool LearnSpell(uint32 spell_id);
        void LearnSpells(std::vector<uint32> const& spellIds);
        void InitLevelupSpellsForLevel();
        bool UnlearnSpell(uint32 spell_id, bool learn_prev, bool clear_ab = true);
        void UnlearnSpells(std::vector<uint32> const& spellIds, bool learn_prev, bool clear_ab = true);
        bool RemoveSpell(uint32 spell_id, bool learn_prev, bool clear_ab = true);
        void CleanupActionBar();
        std::string GenerateActionBarData() const;

        PetSpellMap     m_spells;
        AutoSpellList   m_autospells;

        void InitPetCreateSpells();

        uint16 GetSpecialization() const { return m_petSpecialization; }
        void SetSpecialization(uint16 spec);
        void LearnSpecializationSpells();
        void RemoveSpecializationSpells(bool clearActionBar);

        uint64 GetAuraUpdateMaskForRaid() const { return m_auraRaidUpdateMask; }
        void SetAuraUpdateMaskForRaid(uint8 slot) { m_auraRaidUpdateMask |= (uint64(1) << slot); }
        void ResetAuraUpdateMaskForRaid() { m_auraRaidUpdateMask = 0; }

        DeclinedNames const& GetDeclinedNames() const { return m_declinedNames; }

        bool    m_removed;                                  // prevent overwrite pet state in DB at next Pet::Update if pet already removed(saved)

        Player* GetOwner() const;

        bool IsStampeded() const { return m_stampeded; }
        void SetStampeded() { m_stampeded = true; }

    protected:
        Player* m_petOwner;
        PetType m_petType;
        int32   m_duration;                                 // time until unsummon (used mostly for summoned guardians and not used for controlled pets)
        uint64  m_auraRaidUpdateMask;
        bool    m_loading;
        uint32  m_focusRegenTimer;

        DeclinedNames m_declinedNames;

        uint16 m_petSpecialization;

        uint8   m_slot;

        bool m_stampeded;

    private:
        void SaveToDB(uint32, uint32) override                       // override of Creature::SaveToDB     - must not be called
        {
            ASSERT(false);
        }
        void DeleteFromDB() override                                 // override of Creature::DeleteFromDB - must not be called
        {
            ASSERT(false);
        }
};

struct PetStableData
{
    PetStableData() : PetNumber(0), PetEntry(0), PetSlot(PET_SLOT_OTHER_PET), PetLevel(0), PetStableState(PET_STABLE_NONE), PetDisplayID(0) { }

    uint32 PetNumber;
    uint32 PetEntry;
    uint8 PetSlot;
    uint32 PetLevel;
    uint32 PetDisplayID;
    uint8 PetStableState;
    std::string PetName;
};

#endif
