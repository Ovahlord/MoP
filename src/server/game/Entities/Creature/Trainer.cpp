/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Trainer.h"
#include "Creature.h"
#include "Player.h"
#include "SpellInfo.h"
#include "SpellMgr.h"

namespace Trainer
{

    Trainer::Trainer(uint32 id, Type type, std::string greeting, std::vector<Spell> spells) : _id(id), _type(type)
    {
        _greeting = std::move(greeting);
        _spells = std::move(spells);
    }

    void Trainer::SendSpells(Creature const* npc, Player* player) const
    {
        std::vector<Spell> trainerSpells;
        std::copy_if(std::begin(_spells), std::end(_spells), std::inserter(trainerSpells, std::end(trainerSpells)), [player](Spell const& trainerSpell)
        {
            return player->IsSpellFitByClassAndRace(trainerSpell.SpellId);
        });

        ObjectGuid guid = player->GetGUID();

        float reputationDiscount = player->GetReputationPriceDiscount(npc);

        WorldPacket data(SMSG_TRAINER_LIST, 1 + 8 + 4 + trainerSpells.size() * (1 + 4 + 4 + MAX_TRAINER_SPELL_ABILITY_REQS * (4) + 4 + 4 + 1) + _greeting.length() + 4 + 4);

        data.WriteGuidMask(guid, 4, 5);

        data.WriteBits(trainerSpells.size(), 19);
        data.WriteBits(_greeting.length(), 11);

        data.WriteGuidMask(guid, 6, 2, 7, 1, 3, 0);

        data.FlushBits();

        data.WriteGuidBytes(guid, 4);

        for (Spell const& trainerSpell : trainerSpells)
        {
            data << uint8(trainerSpell.ReqLevel);
            data << uint32(floor(trainerSpell.MoneyCost * reputationDiscount));
            data << uint32(trainerSpell.SpellId);

            // spells required (3 max)
            for (uint8 i = 0; i < MAX_TRAINER_SPELL_ABILITY_REQS; ++i)
                data << uint32(trainerSpell.ReqAbility[i]);

            data << uint32(trainerSpell.ReqSkillLine);
            data << uint32(trainerSpell.ReqSkillRank);
            data << uint8(GetSpellState(player, &trainerSpell));
        }

        data.WriteString(_greeting);

        data.WriteGuidBytes(guid, 6, 7, 1, 3);

        data << uint32(_id);

        data.WriteGuidBytes(guid, 5, 0, 2);

        data << uint32(_type);

        player->SendDirectMessage(&data);
    }

    void Trainer::TeachSpell(Creature* npc, Player* player, uint32 spellId) const
    {
        Spell const* trainerSpell = GetSpell(spellId);
        if (!trainerSpell || !CanTeachSpell(player, trainerSpell))
        {
            SendTeachFailure(npc, player, spellId, FailReason::Unavailable);
            return;
        }

        float reputationDiscount = player->GetReputationPriceDiscount(npc);
        int64 moneyCost = int64(trainerSpell->MoneyCost * reputationDiscount);
        if (!player->HasEnoughMoney(moneyCost))
        {
            SendTeachFailure(npc, player, spellId, FailReason::NotEnoughMoney);
            return;
        }

        player->ModifyMoney(-moneyCost);

        npc->SendPlaySpellVisualKit(179, 0, 0);     // 53 SpellCastDirected
        player->SendPlaySpellVisualKit(362, 1, 0);  // 113 EmoteSalute

        // learn explicitly or cast explicitly
        if (trainerSpell->IsCastable())
            player->CastSpell(player, trainerSpell->SpellId, true);
        else
            player->LearnSpell(trainerSpell->SpellId, false);
    }

    Spell const* Trainer::GetSpell(uint32 spellId) const
    {
        auto itr = std::find_if(_spells.begin(), _spells.end(), [spellId](Spell const& trainerSpell)
        {
            return trainerSpell.SpellId == spellId;
        });

        if (itr != _spells.end())
            return &(*itr);

        return nullptr;
    }

    bool Trainer::CanTeachSpell(Player const* player, Spell const* trainerSpell) const
    {
        SpellState state = GetSpellState(player, trainerSpell);
        if (state != SpellState::Available)
            return false;

        SpellInfo const* trainerSpellInfo = sSpellMgr->AssertSpellInfo(trainerSpell->SpellId);
        if (trainerSpellInfo->IsPrimaryProfessionFirstRank() && !player->GetFreePrimaryProfessionPoints())
            return false;

        return true;
    }

    SpellState Trainer::GetSpellState(Player const* player, Spell const* trainerSpell) const
    {
        if (player->HasSpell(trainerSpell->SpellId))
            return SpellState::Known;

        // check race/class requirement
        if (!player->IsSpellFitByClassAndRace(trainerSpell->SpellId))
            return SpellState::Unavailable;

        // check skill requirement
        if (trainerSpell->ReqSkillLine && player->GetBaseSkillValue(trainerSpell->ReqSkillLine) < trainerSpell->ReqSkillRank)
            return SpellState::Unavailable;

        for (int32 reqAbility : trainerSpell->ReqAbility)
            if (reqAbility && !player->HasSpell(reqAbility))
                return SpellState::Unavailable;

        // check level requirement
        if (player->GetLevel() < trainerSpell->ReqLevel)
            return SpellState::Unavailable;

        // check ranks
        if (uint32 previousRankSpellId = sSpellMgr->GetPrevSpellInChain(trainerSpell->LearnedSpellId))
            if (!player->HasSpell(previousRankSpellId))
                return SpellState::Unavailable;

        // check additional spell requirement
        SpellsRequiringSpellMapBounds requiredSpells = sSpellMgr->GetSpellsRequiredForSpellBounds(trainerSpell->SpellId);
        for (SpellsRequiringSpellMap::const_iterator itr = requiredSpells.first; itr != requiredSpells.second; ++itr)
            if (!player->HasSpell(itr->second))
                return SpellState::Unavailable;

        return SpellState::Available;
    }

    void Trainer::SendTeachFailure(Creature const* npc, Player* player, uint32 spellId, FailReason reason) const
    {
        ObjectGuid trainerGUID = npc->GetGUID();

        WorldPacket data(SMSG_TRAINER_BUY_FAILED, 1 + 8 + 4 + 4);

        data.WriteGuidMask(trainerGUID, 3, 0, 4, 7, 6, 1, 5, 2);

        data.WriteGuidBytes(trainerGUID, 1, 2, 0, 3, 4);

        data << uint32(reason);

        data.WriteGuidBytes(trainerGUID, 5, 6, 7);

        data << uint32(spellId);

        player->SendDirectMessage(&data);
    }
}
