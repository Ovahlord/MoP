/*
* Copyright (C) 2017 Project Aurora
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ReferAFriend.h"

RaFData::RaFData(uint32 accountId) : _accountID(accountId)
{
    _recruiter = 0;
    _recruitsCount = 0;
    _rewardsCount = 0;
    _invCount = 0;

    _recruiterAwarded = false;
    _loadedFromDB = false;
    _alreadySavedToDB = false;
}

RaFData::~RaFData()
{
}

void RaFData::LoadFromDB(PreparedQueryResult rafDataResult, PreparedQueryResult rafRecruitsResult)
{
    if (rafDataResult)
    {
        Field* fields = rafDataResult->Fetch();

        _recruiter          = fields[0].GetUInt32();
        _invCount           = fields[1].GetUInt32();
        _rewardsCount       = fields[2].GetUInt32();
        _recruiterAwarded   = fields[3].GetBool();

        _loadedFromDB = true;
    }

    if (rafRecruitsResult)
    {
        Field* fields = rafRecruitsResult->Fetch();

        _recruitsCount = uint32(fields[0].GetUInt64());
    }
}

void RaFData::SaveToDB()
{
    if (_alreadySavedToDB)
        return;

    PreparedStatement* stmt = nullptr;

    if (_loadedFromDB)
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_UPD_ACCOUNT_RAF_DATA);
        stmt->setUInt32(0, _recruiter);
        stmt->setUInt32(1, _invCount);
        stmt->setUInt32(2, _rewardsCount);
        stmt->setBool(3, _recruiterAwarded);
        stmt->setUInt32(4, _accountID);
    }
    else
    {
        if (!(_recruiter || _invCount || _rewardsCount || _recruiterAwarded))
            return;

        stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_RAF_DATA);
        stmt->setUInt32(0, _accountID);
        stmt->setUInt32(1, _recruiter);
        stmt->setUInt32(2, _invCount);
        stmt->setUInt32(3, _rewardsCount);
        stmt->setBool(4, _recruiterAwarded);
    }

    LoginDatabase.Execute(stmt);

    _alreadySavedToDB = true;
}

uint32 RaFData::GetRecruiter() const
{
    return _recruiter;
}

uint32 RaFData::GetRecruitsCount() const
{
    return _recruitsCount;
}

bool RaFData::IsARecruiter() const
{
    return _recruitsCount > 0;
}

void RaFData::IncreaseInvitationsCount()
{
    ++_invCount;

    _alreadySavedToDB = false;
}

void RaFData::ResetInvitationsCount()
{
    _invCount = 0;

    _alreadySavedToDB = false;
}

uint32 RaFData::GetInvitationsCount() const
{
    return _invCount;
}

void RaFData::IncreaseRewardsCount()
{
    ++_rewardsCount;

    _alreadySavedToDB = false;
}

void RaFData::DecreaseRewardsCount()
{
    --_rewardsCount;

    _alreadySavedToDB = false;
}

uint32 RaFData::GetRewardsCount() const
{
    return _rewardsCount;
}

void RaFData::SetRecruiterAwarded(bool award)
{
    _recruiterAwarded = award;

    _alreadySavedToDB = false;
}

bool RaFData::IsRecruiterAwarded() const
{
    return _recruiterAwarded;
}
