/*
* Copyright (C) 2017 Project Aurora
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RecruitAFriendRewardsStore_h__
#define RecruitAFriendRewardsStore_h__

#include "Singleton/Singleton.hpp"

#include "Common.h"

struct RaFReward
{
    RaFReward() : Id(0), ItemId(0), DisplayId(0) { }

    uint32 Id;
    uint32 ItemId;
    uint32 DisplayId;
    std::string Title;
    std::string Title2;
};

typedef std::unordered_map<uint32, RaFReward> RaFRewardsStore;

class RecruitAFriendRewardsStore
{
    public:
        void LoadRecruitAFriendRewards();

        RaFRewardsStore const& GetRaFRewards() const { return _rafRewards; }

        RaFReward const* GetRaFReward(uint32 productID) const;

    private:
        RaFRewardsStore _rafRewards;
};

#define sRecruitAFriendRewardsStore Tod::Singleton<RecruitAFriendRewardsStore>::GetSingleton()

#endif // RecruitAFriendRewardsStore_h__