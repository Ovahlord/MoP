/*
* Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BATTLEGROUNDDG_H
#define __BATTLEGROUNDDG_H

#include "Battleground.h"

#define BG_EVENT_START_BATTLE               9158 // Achievement: Newbs to Plowshares

enum BG_DG_ObjectTypes
{
    // Flags
    BG_DG_OBJECT_H_FLAG                     = 0,
    BG_DG_OBJECT_A_FLAG                     = 1,

    // Doors
    BG_DG_OBJECT_DOOR_H1                    = 2,
    BG_DG_OBJECT_DOOR_H2                    = 3,
    BG_DG_OBJECT_DOOR_A1                    = 4,
    BG_DG_OBJECT_DOOR_A2                    = 5,

    // Aura Banners
    BG_DG_OBJECT_AURA_ALLY                  = 6,
    BG_DG_OBJECT_AURA_HORDE                 = 7,
    BG_DG_OBJECT_AURA_CONTESTED             = 8,

    // Buffs
    BG_DG_OBJECT_REGENBUFF_PANDAREN_MINE    = 15,
    BG_DG_OBJECT_REGENBUFF_GOBLIN_MINE      = 16,
    BG_DG_OBJECT_BERSERKBUFF_1              = 17,
    BG_DG_OBJECT_BERSERKBUFF_2              = 18,

    BG_DG_OBJECT_MAX
};

enum BG_DG_Objectives
{
    DG_OBJECTIVE_CAPTURE_FLAG       = 42,
    DG_OBJECTIVE_RETURN_FLAG        = 44,
    DG_OBJECTIVE_ASSAULT_BASE       = 370,
    DG_OBJECTIVE_DEFEND_BASE        = 371
};

enum BG_DG_ObjectEntry
{
    BG_OBJECT_A_CART_DG_ENTRY          = 220164,
    BG_OBJECT_H_CART_DG_ENTRY          = 220166,
    BG_OBJECT_A_CART_GROUND_DG_ENTRY   = 220165,
    BG_OBJECT_H_CART_GROUND_DG_ENTRY   = 220174,

    BG_DG_OBJECTID_AURA_A              = 180100,
    BG_DG_OBJECTID_AURA_H              = 180101,
    BG_DG_OBJECTID_AURA_C              = 180102,

    BG_OBJECT_DOOR_H1_DG_ENTRY         = 220161,
    BG_OBJECT_DOOR_H2_DG_ENTRY         = 220366,
    BG_OBJECT_DOOR_A1_DG_ENTRY         = 220159,
    BG_OBJECT_DOOR_A2_DG_ENTRY         = 220160
};

enum BG_DG_CreatureEntry
{
    BG_CREATURE_NODE_BANNER_ENTRY       = 53194
};

enum BG_DG_Graveyard
{
    BG_DG_GRAVEYARD_ALLIANCE_SOUTH      = 4488,
    BG_DG_GRAVEYARD_ALLIANCE_NORTH      = 4546,
    BG_DG_GRAVEYARD_HORDE_SOUTH         = 4545,
    BG_DG_GRAVEYARD_HORDE_NORTH         = 4489,
};

enum BG_DG_Sounds
{
    BG_DG_SOUND_NODE_CLAIMED            = 8192,
    BG_DG_SOUND_NODE_CAPTURED_ALLIANCE  = 8173,
    BG_DG_SOUND_NODE_CAPTURED_HORDE     = 8213,
    BG_DG_SOUND_NODE_ASSAULTED_ALLIANCE = 8212,
    BG_DG_SOUND_NODE_ASSAULTED_HORDE    = 8174,
    BG_DG_SOUND_FLAGS_RESPAWNED         = 8232,
    BG_DG_SOUND_NEAR_VICTORY            = 8456
};

enum BG_DG_Defines
{
    BG_DG_WARNING_NEAR_VICTORY_SCORE    = 1400,
    BG_DG_MAX_TEAM_SCORE                = 1600,

    BG_DG_NotDGBGWeekendHonorTicks      = 130,
    BG_DG_DGBGWeekendHonorTicks         = 100,
    BG_DG_NotDGBGWeekendReputationTicks = 160,
    BG_DG_DGBGWeekendReputationTicks    = 120,

    BG_DG_CAPTURING_BANNER_TRIGGER      = 97372,

    BG_DG_SPELL_ALLIANCE_CART           = 140876,
    BG_DG_SPELL_HORDE_CART              = 141210,

    BG_DG_FLAG_RESPAWN_TIME             = 23000,
    BG_DG_FLAG_DROP_TIME                = 10000,
    BG_DG_FLAG_CAPTURING_TIME           = 60000,
    BG_DG_SOURCE_BY_REWARD_FLAG         = 200,
};

enum BG_DG_BattlegroundNodes
{
    BG_DG_NODE_GOBLIN           = 0,
    BG_DG_NODE_CENTER           = 1,
    BG_DG_NODE_PANDAREN         = 2,

    BG_DG_DYNAMIC_NODES_COUNT   = 3,

    BG_DG_SPIRIT_ALIANCE        = 3,
    BG_DG_SPIRIT_HORDE          = 4,

    BG_DG_ALL_NODES_COUNT       = 5,                       // All nodes (dynamic and static)

    BG_DG_CREATURES_MAX         = 8
};

const float BG_DG_NodePositions[BG_DG_DYNAMIC_NODES_COUNT][4] =
{
    { -397.773f,  574.368f, 110.97f,    0.9075713f},         // Goblin
    { -167.5035f, 499.059f, 92.83793f,  -2.600541f},         // Center
    { 68.3924f,   431.177f, 111.761f,   -2.303835f}          // Pandaren
};

const float BG_DG_BuffPositions[4][4] =
{
    { -429.53f, 580.727f,   110.9807f,  6.1835f },          // Goblin Healing Buff
    { 96.0529f, 426.092f,   111.1858f,  4.0595f },          // Pandaren Healing Buff
    { -93.555f, 375.340f,   135.5808f,  5.5167f },          // Berserker Buff
    { -239.43f, 624.611f,   135.6253f,  2.5623f }           // Berserker Buff
};

enum BG_DG_NodeStatus
{
    BG_DG_NODE_TYPE_NEUTRAL             = 0,
    BG_DG_NODE_TYPE_CONTESTED           = 1,
    BG_DG_NODE_STATUS_ALLY_CONTESTED    = 1,
    BG_DG_NODE_STATUS_HORDE_CONTESTED   = 2,
    BG_DG_NODE_TYPE_OCCUPIED            = 3,
    BG_DG_NODE_STATUS_ALLY_OCCUPIED     = 3,
    BG_DG_NODE_STATUS_HORDE_OCCUPIED    = 4
};

struct BG_DG_BannerTimer
{
    uint32 timer;
    uint8  type;
    uint8  teamIndex;
};

const uint32 BG_DG_TickIntervals[4] = { 0, 8000, 3000, 1000 };
const uint32 BG_DG_TickPoints[4]    = { 0, 10, 10, 30 };

//Light, Water, Mine, Ally, Horde
const uint32 BG_DG_GraveyardIds[BG_DG_ALL_NODES_COUNT] = { 4614, 4545, 4613, 4487, 4486 };

const float BG_DG_SpiritGuidePos[BG_DG_ALL_NODES_COUNT][4] =
{
    { -438.592f, 582.324f, 111.570f, 4.07f },     // Lighthouse
    { -222.046f, 806.070f, 137.452f, 4.94f },     // Waterworks
    { 118.779f,  418.938f, 112.863f, 2.70f },     // Mine
    { -274.985f, 171.789f, 136.185f, 0.67f },     // Alliance
    { -61.7795f, 831.194f, 135.378f, 4.15f },     // Horde
};

enum BG_DG_FlagState
{
    BG_DG_FLAG_STATE_ON_BASE      = 0,
    BG_DG_FLAG_STATE_WAIT_RESPAWN = 1,
    BG_DG_FLAG_STATE_ON_PLAYER    = 2,
    BG_DG_FLAG_STATE_ON_GROUND    = 3
};

enum BG_DG_Worldstates
{
    BG_DG_OP_RESOURCES_ALLY       = 7880,
    BG_DG_OP_RESOURCES_HORDE      = 7881,
    BG_DG_OP_OCCUPIED_BASES_ALLY  = 8230,
    BG_DG_OP_OCCUPIED_BASES_HORDE = 8231,
    BG_DG_OP_FLAG_A               = 7887,
    BG_DG_OP_FLAG_H               = 7904,
};

enum BG_DG_BannerAuras
{
    BG_DG_SPELL_CAPTURE_POINT_ALLY_CONTESTED    = 98543,
    BG_DG_SPELL_CAPTURE_POINT_ALLY_CONTROLLED   = 98519,
    BG_DG_SPELL_CAPTURE_POINT_HORDE_CONTESTED   = 98545,
    BG_DG_SPELL_CAPTURE_POINT_HORDE_CONTROLLED  = 98527,
    BG_DG_SPELL_CAPTURE_POINT_NEUTRAL           = 98554
};

const uint32 BG_DG_BANNER_AURAS[5] =
{
    BG_DG_SPELL_CAPTURE_POINT_NEUTRAL,
    BG_DG_SPELL_CAPTURE_POINT_ALLY_CONTESTED,
    BG_DG_SPELL_CAPTURE_POINT_HORDE_CONTESTED,
    BG_DG_SPELL_CAPTURE_POINT_ALLY_CONTROLLED,
    BG_DG_SPELL_CAPTURE_POINT_HORDE_CONTROLLED
};

const uint32 BG_DG_OP_NODEICONS[3][5] =
{
    { 7938, 7864, 7865, 7856, 7856 }, // Goblin
    { 7939, 7934, 7936, 7932, 7932 }, // Central
    { 7935, 7857, 7861, 7855, 7855 }  // Pandaren
};

struct BattlegroundDGScore : public BattlegroundScore
{
    BattlegroundDGScore() : CartsCaptures(0), CartsReturns(0), MinesAssaulted(0), MinesDefended(0) { };
    ~BattlegroundDGScore() { };

    uint32 CartsCaptures;
    uint32 CartsReturns;
    uint32 MinesAssaulted;
    uint32 MinesDefended;
};

class BattlegroundDG : public Battleground
{
    public:
        BattlegroundDG();
        ~BattlegroundDG();

        bool SetupBattleground() override;
        void Reset() override;

        void AddPlayer(Player* player) override;
        void RemovePlayer(Player* player, ObjectGuid guid, uint32 /*team*/) override;

        void StartingEventCloseDoors() override;
        void StartingEventOpenDoors() override;

        void EventPlayerDroppedFlag(Player* Source) override;
        void EventPlayerClickedOnFlag(Player* Source, WorldObject* obj) override;

        void EventPlayerCapturedFlag(Player* Source);

        void FillInitialWorldStates(WorldStateBuilder &builder) override;

        void HandleKillPlayer(Player* player, Player* killer) override;

        void UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor = true) override;

        WorldSafeLocsEntry const* GetClosestGraveYard(Player* player) override;
        
        void PostUpdateImpl(uint32 diff) override;

        void HandleAreaTrigger(Player* Source, uint32 Trigger, bool Entered) override;

        bool IsAllNodesControlledByTeam(uint32 team) const;
        bool IsTeamScores500Disadvantage(uint32 team) const { return m_TeamScores500Disadvantage[GetTeamIndexByTeamId(team)]; }

    private:
        uint8               m_Nodes[BG_DG_DYNAMIC_NODES_COUNT];
        uint8               m_prevNodes[BG_DG_DYNAMIC_NODES_COUNT];
        BG_DG_BannerTimer   m_BannerTimers[BG_DG_DYNAMIC_NODES_COUNT];
        uint32              m_NodeTimers[BG_DG_DYNAMIC_NODES_COUNT];
        uint32              m_lastTick[BG_TEAMS_COUNT];
        uint32              m_HonorScoreTics[BG_TEAMS_COUNT];
        uint32              m_ReputationScoreTics[BG_TEAMS_COUNT];
        bool                m_IsInformedNearVictory;
        uint32              m_HonorTics;
        uint32              m_ReputationTics;
        uint8               m_flagState[BG_TEAMS_COUNT];
        int32               m_flagsTimer[BG_TEAMS_COUNT];
        int32               m_flagsDropTimer[BG_TEAMS_COUNT];
        ObjectGuid          m_DroppedFlagGUID[BG_TEAMS_COUNT];
        uint32              m_ResourceInMineCart[BG_TEAMS_COUNT];
        int32               m_CheatersCheckTimer;
        bool                m_TeamScores500Disadvantage[BG_TEAMS_COUNT];

    private:
        void _CreateBanner(uint8 node, uint8 type, uint8 teamIndex, bool delay);
        void _DelBanner(uint8 node, uint8 type, uint8 teamIndex);
        void _SendNodeUpdate(uint8 node);
        void _NodeOccupied(uint8 node, Team team);
        void _NodeDeOccupied(uint8 node);

        int32 _GetNodeNameId(uint8 node);

        void RespawnFlag(uint32 Team, bool captured);
        void RespawnFlagAfterDrop(uint32 Team);

        ObjectGuid GetDroppedFlagGUID(uint32 TeamID)            { return m_DroppedFlagGUID[GetTeamIndexByTeamId(TeamID)]; }
        uint8 GetFlagState(uint32 team)                         { return m_flagState[GetTeamIndexByTeamId(team)]; }

        void SetDroppedFlagGUID(ObjectGuid guid, int32 TeamID) override
        {
            m_DroppedFlagGUID[GetTeamIndexByTeamId(TeamID)] = guid;
        }
};

#endif
