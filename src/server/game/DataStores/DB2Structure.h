/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_DB2STRUCTURE_H
#define TRINITY_DB2STRUCTURE_H

#include "Common.h"

#pragma pack(push, 1)

enum DB2Defines
{
    MAX_BATTLE_PET_EFFECT_PROPERTIES        = 6,

    MAX_ITEM_FLAGS                    = 3,
    MAX_ITEM_PROTO_SOCKETS                  = 3,
    MAX_ITEM_PROTO_STATS                    = 10,
    MAX_ITEM_PROTO_SPELLS                   = 5,

    MAX_BROADCAST_TEXT_EMOTES               = 3,

    MAX_CREATURE_ITEMS                      = 3,
    MAX_CREATURE_DISPLAYS                   = 4,
    MAX_CREATURE_FLAGS                      = 5,
    MAX_CREATURE_NAMES                      = 4,

    MAX_GAMEOBJECTS_DATA                    = 4,
    MAX_GAMEOBJECTS_NAMES                   = 4,

    MAX_ITEM_EXT_COST_ITEMS                 = 5,
    MAX_ITEM_EXT_COST_CURRENCIES            = 5,

    MAX_ITEM_NAMES                          = 4,

    KEYCHAIN_SIZE                           = 32,

    MAX_LOCATION_ROTATIONS                  = 3,

    MAX_SPELL_REAGENTS                      = 8,

    MAX_SPECIAL_EFFECTS                     = 3,

    MAX_CHAR_PARAMS                         = 4
};

struct BattlePetAbilityEntry
{
    uint32              Id;                                     // 0 - battle pet ability id
    int32               TypeId;                                 // 1 - battle pet type id (-1 for aura)
    uint32              IconId;                                 // 2 - icon id
    uint32              RoundCooldown;                          // 3 - cooldown in turns
    uint32              VisualId;                               // 4 - visual id (BattlePetVisual.db2)
    uint32              Flags;                                  // 5 - flags (see BattlePetAbilityFlags enum)
    LocalizedString*    Name;                                   // 6 - name text
    LocalizedString*    Description;                            // 7 - description text
};

struct BattlePetAbilityEffectEntry
{
    uint32      Id;                                             // 0
    uint32      AbilityTurnId;                                  // 1 - ability turn id (BattlePetAbilityTurn.db2)
    uint32      VisualId;                                       // 2 - visual id (BattlePetVisual.db2)
    uint32      TriggerAbility;                                 // 3 - parent ability
    uint32      EffectProperty;                                 // 4 - effect property id (BattlePetEffectProperties.db2)
    uint32      Unk1;                                           // 5 - effect property offset?
    int32       Properties[MAX_BATTLE_PET_EFFECT_PROPERTIES];   // 6 - values for effect property
};

struct BattlePetAbilityStateEntry
{
    uint32      Id;                                             // 0
    uint32      AbilityId;                                      // 1 - battle pet ability id (BattlePetAbility.db2)
    uint32      StateId;                                        // 2 - battle pet state id (BattlePetState.db2)
    int32       Value;                                          // 3 - value for state
};

struct BattlePetAbilityTurnEntry
{
    uint32      Id;                                             // 0
    uint32      AbilityId;                                      // 1 - parent ability id (BattlePetAbility.db2)
    uint32      VisualId;                                       // 2 - visual id (BattlePetVisual.db2)
    uint32      Duration;                                       // 3 - amount of turns the ability lasts
    uint32      HasProcType;                                    // 4 - if set to 1, next value is positive else -1
    int32       ProcType;
};

struct BattlePetBreedQualityEntry
{
    uint32      Id;                                             // 0
    uint32      Quality;                                        // 1 - quality id
    float       Multiplier;                                     // 2 - multiplier for breed id
};

struct BattlePetBreedStateEntry
{
    uint32      Id;                                             // 0
    uint32      BreedId;                                        // 1 - battle pet breed id
    uint32      StateId;                                        // 2 - battle pet state id (BattlePetState.db2)
    uint32      Modifier;                                       // 3 - value for state
};

struct BattlePetEffectPropertiesEntry
{
    uint32              Id;                                                 // 0
    uint32              Flags;                                              // 1 - flags
    LocalizedString*    PropertyName[MAX_BATTLE_PET_EFFECT_PROPERTIES];     // 2 - name
    uint32              IsAura[MAX_BATTLE_PET_EFFECT_PROPERTIES];           // 3 - set to 1 if an aura id
};

struct BattlePetSpeciesEntry
{
    uint32              Id;                                     // 0
    uint32              NpcId;                                  // 1 - npc id
    uint32              IconId;                                 // 2 - icon id
    uint32              SpellId;                                // 3 - summon spell id (Spell.dbc)
    uint32              FamilyId;                               // 4 - battle pet family id
    int32               Unk1;                                   // 5
    uint32              Flags;                                  // 6 - flags (see BattlePetSpeciesFlags enum)
    LocalizedString*    Description;                            // 7 - description text, contains method to obtain and cost
    LocalizedString*    Flavor;                                 // 8 - flavor text
};

struct BattlePetSpeciesStateEntry
{
    uint32      Id;                                             // 0
    uint32      SpeciesId;                                      // 1 - battle pet species id (BattlePetSpecies.db2)
    uint32      StateId;                                        // 2 - battle pet state id (BattlePetState.db2)
    int32       Modifier;                                       // 3 - modifier value for state
};

struct BattlePetSpeciesXAbilityEntry
{
    uint32      Id;                                             // 0
    uint32      SpeciesId;                                      // 1 - battle pet species id (BattlePetSpecies.db2)
    uint32      AbilityId;                                      // 2 - battle pet ability id (BattlePetAbility.db2)
    uint32      RequiredLevel;                                  // 3 - required level to use this ability
    uint32      SlotId;                                         // 4 - ability slot id (0-2)
};

struct BattlePetStateEntry
{
    uint32              Id;                                     // 0
    uint32              Unk1;                                   // 1 - only states 21 and 34 (linked states?)
    LocalizedString*    Name;                                   // 2 - name text
    uint32              Flags;                                  // 3 - flags
};

// @todo 5.4.8: needs more research
struct BattlePetVisualEntry
{
    uint32              Id;                                     // 0
    uint32              Unk1;                                   // 1
    uint32              Unk2;                                   // 2
    uint32              Unk3;                                   // 3
    uint32              SlotId;                                 // 4 - ? ability slot id (0-2)
    uint32              Unk4;                                   // 5
    uint32              Unk5;                                   // 6
    LocalizedString*    Description;                            // 7
};

struct BroadcastTextEntry
{
    uint32              Id;                                     // 0
    uint32              Language;                               // 1
    LocalizedString*    MaleText;                               // 2
    LocalizedString*    FemaleText;                             // 3
    uint32              Emote[MAX_BROADCAST_TEXT_EMOTES];       // 4-6
    uint32              EmoteDelay[MAX_BROADCAST_TEXT_EMOTES];  // 7-9
    uint32              SoundId;                                // 10
    uint32              EndEmoteId;                             // 11
    uint32              Type;                                   // 12
};

struct CreatureEntry
{
    uint32              Id;                                     // 0
    uint32              Item[MAX_CREATURE_ITEMS];               // 1-3
    uint32              MountId;                                // 4
    uint32              DisplayId[MAX_CREATURE_DISPLAYS];       // 5-8
    float               DisplayIdPropability[MAX_CREATURE_DISPLAYS]; // 9-12
    LocalizedString*    Name;                                   // 13
    LocalizedString*    SubName;                                // 14
    LocalizedString*    FemaleSubName;                          // 15
    uint32              Rank;                                   // 16
    uint32              InhabitType;                            // 17
};

struct CreatureDifficultyEntry
{
    uint32      Id;                                             // 0
    uint32      CreatureId;                                     // 1
    uint32      FactionId;                                      // 2
    uint32      Expansion;                                      // 3
    uint32      MinLevel;                                       // 4
    uint32      MaxLevel;                                       // 5
    uint32      Flags[MAX_CREATURE_FLAGS];                      // 6-10
};

struct CurveEntry
{
    uint32      Id;                                             // 0
    uint32      Type;                                           // 1
    uint32      Unk1;                                           // 2
};

struct CurvePointEntry
{
    uint32      Id;                                             // 0
    uint32      CurveID;                                        // 1
    uint32      OrderIndex;                                     // 2
    float       X;                                              // 3
    float       Y;                                              // 4
};

// @todo 5.4.8: needs more research
struct DeviceBlacklistEntry
{
    uint32      Id;                                             // 0
    uint32      Unk1;                                           // 1
    uint32      Unk2;                                           // 2
};

// @todo 5.4.8: needs more research
struct DriverBlacklistEntry
{
    uint32      Id;                                             // 0
    uint32      Unk1;                                           // 1
    uint32      Unk2;                                           // 2
    uint32      Unk3;                                           // 3
    uint32      Unk4;                                           // 4
    uint32      Unk5;                                           // 5
    uint32      Unk6;                                           // 6
    uint32      Unk7;                                           // 7
};

struct GameObjectEntry
{
    uint32              Id;                                     // 0
    uint32              MapId;                                  // 1
    uint32              DisplayId;                              // 2
    DBCPosition3D       Position;                               // 3-5
    float               Rotation_X;                             // 6
    float               Rotation_Y;                             // 7
    float               Rotation_Z;                             // 8
    float               Rotation_W;                             // 9
    float               Size;                                   // 10
    uint32              TypeId;                                 // 11
    uint32              Data[MAX_GAMEOBJECTS_DATA];             // 12-15
    LocalizedString*    Name;                                   // 16
};

struct ItemEntry
{
    uint32      Id;                                             // 0
    uint32      Class;                                          // 1
    uint32      SubClass;                                       // 2
    int32       SoundOverrideSubclass;                          // 3
    int32       Material;                                       // 4
    uint32      DisplayId;                                      // 5
    uint32      InventoryType;                                  // 6
    uint32      Sheath;                                         // 7
};

struct ItemCurrencyCostEntry
{
    uint32      Id;
    uint32      ItemId;
};

struct ItemExtendedCostEntry
{
    uint32      Id;                                             // 0 extended-cost entry id
    uint32      RequiredHonorPoints;                            // 1 required honor points
    uint32      RequiredArenaPoints;                            // 2 required arena points
    uint32      RequiredArenaSlot;                              // 3 arena slot restrictions (min slot value)
    uint32      RequiredItem[MAX_ITEM_EXT_COST_ITEMS];          // 4-8 required item id
    uint32      RequiredItemCount[MAX_ITEM_EXT_COST_ITEMS];     // 9-13 required count of 1st item
    uint32      RequiredPersonalArenaRating;                    // 14 required personal arena rating
    uint32      ItemPurchaseGroup;                              // 15
    uint32      RequiredCurrency[MAX_ITEM_EXT_COST_CURRENCIES]; // 16-20 required curency id
    uint32      RequiredCurrencyCount[MAX_ITEM_EXT_COST_CURRENCIES]; // 21-25 required curency count
    uint32      RequiredFactionId;
    uint32      RequiredFactionStanding;
    uint32      RequirementFlags;
    uint32      RequiredGuildLevel;
    uint32      RequiredAchievement;
};

struct ItemSparseEntry
{
    uint32              ID;                                     // 0
    uint32              Quality;                                // 1
    uint32              Flags[MAX_ITEM_FLAGS];                  // 2-4
    float               Unk_1;                                  // 5
    float               Unk_2;                                  // 6
    uint32              BuyCount;                               // 7
    uint32              BuyPrice;                               // 8
    uint32              SellPrice;                              // 9
    uint32              InventoryType;                          // 10
    int32               AllowableClass;                         // 11
    int32               AllowableRace;                          // 12
    uint32              ItemLevel;                              // 13
    int32               RequiredLevel;                          // 14
    uint32              RequiredSkill;                          // 15
    uint32              RequiredSkillRank;                      // 16
    uint32              RequiredSpell;                          // 17
    uint32              RequiredHonorRank;                      // 18
    uint32              RequiredCityRank;                       // 19
    uint32              RequiredReputationFaction;              // 20
    uint32              RequiredReputationRank;                 // 21
    uint32              MaxCount;                               // 22
    uint32              Stackable;                              // 23
    uint32              ContainerSlots;                         // 24
    int32               ItemStatType[MAX_ITEM_PROTO_STATS];     // 25 - 34
    int32               ItemStatValue[MAX_ITEM_PROTO_STATS];    // 35 - 44
    int32               ItemStatAllocation[MAX_ITEM_PROTO_STATS]; // 45 - 54
    float               ItemStatSocketCostMultiplier[MAX_ITEM_PROTO_STATS]; // 55 - 64
    uint32              ScalingStatDistribution;                // 65
    uint32              DamageType;                             // 66
    uint32              Delay;                                  // 67
    float               RangedModRange;                         // 68
    int32               SpellId[MAX_ITEM_PROTO_SPELLS];         // 69 - 73
    int32               SpellTrigger[MAX_ITEM_PROTO_SPELLS];    // 74 - 78
    int32               SpellCharges[MAX_ITEM_PROTO_SPELLS];    // 79 - 83
    int32               SpellCooldown[MAX_ITEM_PROTO_SPELLS];   // 84 - 88
    int32               SpellCategory[MAX_ITEM_PROTO_SPELLS];   // 89 - 93
    int32               SpellCategoryCooldown[MAX_ITEM_PROTO_SPELLS]; // 94 - 98
    uint32              Bonding;                                // 99
    LocalizedString*    Name[MAX_ITEM_NAMES];                   // 100-103
    LocalizedString*    Description;                            // 104
    uint32              PageText;                               // 105
    uint32              LanguageID;                             // 106
    uint32              PageMaterial;                           // 107
    uint32              StartQuest;                             // 108
    uint32              LockID;                                 // 109
    int32               Material;                               // 110
    uint32              Sheath;                                 // 111
    uint32              RandomProperty;                         // 112
    uint32              RandomSuffix;                           // 113
    uint32              ItemSet;                                // 114
    uint32              Area;                                   // 115
    uint32              Map;                                    // 116
    uint32              BagFamily;                              // 117
    uint32              TotemCategory;                          // 118
    uint32              SocketColor[MAX_ITEM_PROTO_SOCKETS];    // 119 - 121
    uint32              SocketContent[MAX_ITEM_PROTO_SOCKETS];  // 122 - 124
    int32               SocketBonus;                            // 125
    uint32              GemProperties;                          // 126
    float               ArmorDamageModifier;                    // 127
    uint32              Duration;                               // 128
    uint32              ItemLimitCategory;                      // 129
    uint32              HolidayId;                              // 130
    float               StatScalingFactor;                      // 131
    int32               CurrencySubstitutionId;                 // 132
    int32               CurrencySubstitutionCount;              // 133
};

struct ItemToBattlePetEntry
{
    uint32      Id;                                             // 0 - item id
    uint32      SpeciesId;                                      // 1 - battle pet species id (BattlePetSpecies.db2)
};

struct ItemToMountSpellEntry
{
    uint32      Id;                                             // 0
    uint32      MountSpellId;                                   // 1
};

struct ItemUpgradeEntry
{
    uint32     ID;                                              // 0
    uint32     UpgradeGroupId;                                  // 1 groupid from ItemUpgradePath.dbc
    uint32     IlvlStep;                                        // 2
    uint32     ItemLvl;                                         // 3 required ilvl
    uint32     CurrencyType;                                    // 4 CurrencyType cost
    uint32     CurrencyCost;                                    // 5 CurrencyCost amount
};

struct KeyChainEntry
{
    uint32      Id;
    uint8       Key[KEYCHAIN_SIZE];
};

// @todo 5.4.8: needs some research
struct LocaleEntry
{
    uint32      Id;                                             // 0
    uint32      Unk1;                                           // 1
    uint32      Unk2;                                           // 2
    uint32      Unk3;                                           // 3
    uint32      Unk4;                                           // 4
    uint32      Unk5;                                           // 5
};

struct LocationEntry
{
    uint32          Id;
    DBCPosition3D   Loc;
    float           Rotation[MAX_LOCATION_ROTATIONS];
};

// @todo 5.4.8: needs some research
struct MapChallengeModeEntry
{
    uint32              Id;
    uint32              MapId;
    LocalizedString*    UnkString1;
    LocalizedString*    UnkString2;
    uint32              Unk1;
    uint32              BronzeMedalTimer;
    uint32              SilverMedalTimer;
    uint32              GoldMedalTimer;
    LocalizedString*    UnkString3;
    LocalizedString*    UnkString4;
};

// @todo 5.4.8: needs some research
struct MarketingPromotionsXLocaleEntry
{
    uint32              Id;
    uint32              Unk1;
    int32               Unk2;
    uint32              Unk3;
    uint32              Unk4;
    uint32              Unk5;
    uint32              Unk6;
    LocalizedString*    UrlName;
};

// @todo 5.4.8: needs some research
struct PathEntry
{
    uint32      Id;
    uint32      Unk1;
    uint32      Unk2;
    uint32      Unk3;
    uint32      Unk4;
    uint32      Unk5;
    uint32      Unk6;
    uint32      Unk7;
};

// @todo 5.4.8: needs some research
struct PathNodeEntry
{
    uint32      Id;
    uint32      Unk1;
    uint32      Unk2;
    uint32      Unk3;
};

// @todo 5.4.8: needs some research
struct PathNodePropertyEntry
{
    uint32      Id;
    uint32      Unk1;
    uint32      Unk2;
    uint32      Unk3;
    uint32      Unk4;
};

// @todo 5.4.8: needs some research
struct PathPropertyEntry
{
    uint32      Id;
    uint32      Unk1;
    uint32      Unk2;
    uint32      Unk3;
};

struct QuestPackageItemEntry
{
    uint32      Id;
    uint32      QuestPackageId;
    uint32      ItemID;
    uint32      ItemCount;
    uint32      FilterType;
};

struct RulesetItemUpgradeEntry
{
    uint32      Id;
    uint32      RulesetID;
    uint32      ItemUpgradeId;
    uint32      ItemEntry;
};

struct RulesetRaidLootUpgradeEntry
{
    uint32      Id;
    uint32      RulesetID;
    uint32      RaidDifficulty;
    uint32      ItemUpgradeId;
    uint32      MapId;
};

struct SceneScriptEntry
{
    uint32              Id;
    LocalizedString*    Name;
    LocalizedString*    Script;
    uint32              PrevSceneScriptId;
    uint32              NextSceneScriptId;
};

struct SceneScriptPackageEntry
{
    uint32              Id;
    LocalizedString*    Name;
};

struct SceneScriptPackageMemberEntry
{
    uint32      Id;
    uint32      ScriptPackageId;
    uint32      ScriptId;
    uint32      OrderIndex;
    uint32      ParentScriptPackageId;
};

struct SpellEffectCameraShakesEntry
{
    uint32      Id;
    uint32      CameraShakes1;                                  // Id from CameraShakes.dbc
    uint32      CameraShakes2;                                  // Id from CameraShakes.dbc
    uint32      CameraShakes3;                                  // Id from CameraShakes.dbc
};

struct SpellMissileEntry
{
    uint32      Id;
    uint32      Flags;
    float       DefaultPitchMin;
    float       DefaultPitchMax;
    float       DefaultSpeedMin;
    float       DefaultSpeedMax;
    float       RandomizeFacingMin;
    float       RandomizeFacingMax;
    float       RandomizePitchMin;
    float       RandomizePitchMax;
    float       RandomizeSpeedMin;
    float       RandomizeSpeedMax;
    float       Gravity;
    float       MaxDuration;
    float       CollisionRadius;
};

struct SpellMissileMotionEntry
{
    uint32              Id;
    LocalizedString*    Name;
    LocalizedString*    ScriptBody;
    uint32              Flags;
    uint32              MissileCount;
};

struct SpellReagentsEntry
{
    uint32      Id;
    int32       Reagent[MAX_SPELL_REAGENTS];
    uint32      ReagentCount[MAX_SPELL_REAGENTS];
    uint32      CurrencyId;
    uint32      CurrencyCount;
};

// @todo 5.4.8: needs some research
struct SpellVisualEntry
{
    uint32          Id;
    uint32          Unk1;
    uint32          PrecastKit;                                 // Id from SpellVisualKit.db2
    uint32          CastKit;                                    // Id from SpellVisualKit.db2
    uint32          ImpactKit;                                  // Id from SpellVisualKit.db2
    uint32          StateKit;                                   // Id from SpellVisualKit.db2
    uint32          StateDoneKit;                               // Id from SpellVisualKit.db2
    uint32          ChannelKit;                                 // Id from SpellVisualKit.db2
    uint32          MissileDestinationAttachment;
    uint32          AnimEventSoundID;                           // Id from SoundEntries.dbc
    uint32          Flags;
    uint32          CasterImpactKit;                            // Id from SpellVisualKit.db2
    uint32          TargetImpactKit;                            // Id from SpellVisualKit.db2
    uint32          MissileAttachment;
    uint32          MissileTargetingKit;                        // Id from SpellVisualKit.db2
    uint32          Unk2;
    uint32          InstantAreaKit;                             // Id from SpellVisualKit.db2
    uint32          ImpactAreaKit;                              // Id from SpellVisualKit.db2
    uint32          PersistentAreaKit;                          // Id from SpellVisualKit.db2
    DBCPosition3D   MissileCastOffset;
    DBCPosition3D   MissileImpactOffset;
    uint32          Unk3;
    uint32          Unk4;
    uint32          Unk5;
    uint32          Unk6;
    uint32          Unk7;
};

// @todo 5.4.8: needs some research
struct SpellVisualEffectNameEntry
{
    uint32              Id;
    LocalizedString*    FileName;
    float               AreaEffectSize;
    float               Scale;
    float               MinAllowedScale;
    float               MaxAllowedScale;
    uint32              Unk1;
    float               Unk2;
    uint32              Unk3;
};

// @todo 5.4.8: needs some research
struct SpellVisualKitEntry
{
    uint32      Id;
    int32       StartAnimId;                                    // Id from Animation.dbc
    int32       AnimId;                                         // Id from Animation.dbc
    uint32      Unk1;
    uint32      AnimKitId;                                      // Id from Animation.dbc
    uint32      HeadEffect;                                     // Id from SpellVisualEffectName.db2
    uint32      ChestEffect;                                    // Id from SpellVisualEffectName.db2
    uint32      BaseEffect;                                     // Id from SpellVisualEffectName.db2
    uint32      LeftHandEffect;                                 // Id from SpellVisualEffectName.db2
    uint32      RightHandEffect;                                // Id from SpellVisualEffectName.db2
    uint32      BreathEffect;                                   // Id from SpellVisualEffectName.db2
    uint32      LeftWeaponEffect;                               // Id from SpellVisualEffectName.db2
    uint32      RightWeaponEffect;                              // Id from SpellVisualEffectName.db2
    uint32      SpecialEffects[MAX_SPECIAL_EFFECTS];            // Id from SpellVisualEffectName.db2
    uint32      WorldEffect;                                    // Id from SpellVisualEffectName.db2
    uint32      SoundId;                                        // Id from SoundEntries.dbc
    int32       CameraShakeId;                                  // Id from CameraShakes.dbc
    int32       Unk2;
    int32       Unk3;
    int32       Unk4;
    float       CharParamZero[MAX_CHAR_PARAMS];
    float       CharParamOne[MAX_CHAR_PARAMS];
    float       CharParamTwo[MAX_CHAR_PARAMS];
    float       CharParamThree[MAX_CHAR_PARAMS];
    uint32      Flags;
};

// @todo 5.4.8: needs some research
struct SpellVisualKitAreaModelEntry
{
    uint32      Id;
    uint32      Unk1;
    uint32      Unk2;
    uint32      Unk3;
    float       Unk4;
    float       Unk5;
    float       Unk6;
};

// @todo 5.4.8: needs some research
struct SpellVisualKitModelAttachEntry
{
    uint32          Id;
    uint32          ParentSpellVisualKitId;
    uint32          SpellVisualEffectNameId;
    int32           AttachmentId;
    DBCPosition3D   Offset;
    float           Yaw;
    float           Pitch;
    float           Roll;
    uint32          Unk1;
    int32           Unk2;
    int32           Unk3;
    int32           Unk4;
    uint32          Unk5;
};

// @todo 5.4.8: needs some research
struct SpellVisualMissileEntry
{
    uint32      Id;
    uint32      Unk1;
    uint32      Unk2;
    int32       Unk3;
    uint32      Unk4;
    int32       Unk5;
    int32       Unk6;
    uint32      Unk7;
    uint32      Unk8;
    uint32      Flags;
    uint32      Unk10;
    float       Unk11;
    float       Unk12;
    float       Unk13;
    float       Unk14;
    float       Unk15;
    float       Unk16;
};

// @todo 5.4.8: needs some research
struct VignetteEntry
{
    uint32              Id;
    LocalizedString*    Name;
    uint32              IconId;
    uint32              Flags;
    float               Unk1;
    float               Unk2;
};

#pragma pack(pop)

#endif
