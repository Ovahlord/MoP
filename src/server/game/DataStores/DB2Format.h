/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_DB2SFRM_H
#define TRINITY_DB2SFRM_H

char const BattlePetAbilityFormat[]             = "niiiiiss";
char const BattlePetAbilityEffectFormat[]       = "niiiiiiiiiii";
char const BattlePetAbilityStateFormat[]        = "niii";
char const BattlePetAbilityTurnFormat[]         = "niiiii";
char const BattlePetBreedQualityFormat[]        = "nif";
char const BattlePetBreedStateFormat[]          = "niii";
char const BattlePetEffectPropertiesFormat[]    = "nissssssiiiiii";
char const BattlePetSpeciesFormat[]             = "niiiiiiss";
char const BattlePetSpeciesStateFormat[]        = "niii";
char const BattlePetSpeciesXAbilityFormat[]     = "niiii";
char const BattlePetStateFormat[]               = "nisi";
char const BattlePetVisualFormat[]              = "niiiiiis";
char const BroadcastTextFormat[]                = "nissiiiiiiiii";
char const CreatureFormat[]                     = "niiiiiiiiffffsssii";
char const CreatureDifficultyFormat[]           = "niiiiiiiiii";
char const CurveFormat[]                        = "nii";
char const CurvePointFormat[]                   = "niiff";
char const DeviceBlacklistFormat[]              = "nii";
char const DriverBlacklistFormat[]              = "niiiiiii";
char const GameObjectsFormat[]                  = "niiffffffffiiiiis";
char const ItemFormat[]                         = "niiiiiii";
char const ItemCurrencyCostFormat[]             = "in";
char const ItemExtendedCostFormat[]             = "niiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
char const ItemSparseFormat[]                   = "niiiiffiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiffffffffffiiifiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiisssssiiiiiiiiiiiiiiiiiiiiiifiiifii";
char const ItemToBattlePetFormat[]              = "ni";
char const ItemToMountSpellFormat[]             = "ni";
char const ItemUpgradeFormat[]                  = "niiiii";
char const KeyChainFormat[]                     = "nbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
char const LocaleFormat[]                       = "niiiii";
char const LocationFormat[]                     = "nffffff";
char const MapChallengeModeFormat[]             = "nissiiiiss";
char const MarketingPromotionsXLocaleFormat[]   = "niiiiiis";
char const PathFormat[]                         = "niiiiiii";
char const PathNodeFormat[]                     = "niii";
char const PathNodePropertyFormat[]             = "niiii";
char const PathPropertyFormat[]                 = "niii";
char const QuestPackageItemFormat[]             = "niiii";
char const RulesetItemUpgradeFormat[]           = "niii";
char const RulesetRaidLootUpgradeFormat[]       = "niiii";
char const SceneScriptFormat[]                  = "nssii";
char const SceneScriptPackageFormat[]           = "ns";
char const SceneScriptPackageMemberFormat[]     = "niiii";
char const SpellEffectCameraShakesFormat[]      = "niii";
char const SpellMissileFormat[]                 = "nifffffffffffff";
char const SpellMissileMotionFormat[]           = "nssii";
char const SpellReagentsFormat[]                = "niiiiiiiiiiiiiiiiii";
char const SpellVisualFormat[]                  = "niiiiiiiiiiiiiiiiiiffffffiiiii";
char const SpellVisualEffectNameFormat[]        = "nsffffifi";
char const SpellVisualKitFormat[]               = "niiiiiiiiiiiiiiiiiiiiiffffffffffffffffi";
char const SpellVisualKitAreaModelFormat[]      = "niiifff";
char const SpellVisualKitModelAttachFormat[]    = "niiiffffffiiiii";
char const SpellVisualMissileFormat[]           = "niiiiiiiiiiffffff";
char const VignetteFormat[]                     = "nsiiff";

#endif
