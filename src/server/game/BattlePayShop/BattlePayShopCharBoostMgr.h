/*
 * Copyright (C) 2017-2017 Project Aurora
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PAY_SHOP_CHAR_BOOST_MGR_H
#define BATTLE_PAY_SHOP_CHAR_BOOST_MGR_H

#include "Singleton/Singleton.hpp"

#include "Common.h"

typedef std::unordered_map<uint8, uint32> EquipItemsMap;
typedef std::unordered_map<uint32, EquipItemsMap> EquipItemsStore;

typedef std::vector<std::pair<uint32, int32>> MiscItemsStore;

typedef std::unordered_set<uint32> ProfessionsSet;
typedef std::unordered_map<uint32, ProfessionsSet> ProfessionsStore;

typedef std::vector<uint32> SpellsStore;

class BattlePayShopCharBoostMgr
{
    BattlePayShopCharBoostMgr();
    ~BattlePayShopCharBoostMgr();

    friend class Tod::Singleton<BattlePayShopCharBoostMgr>;

public:
    void LoadFromDB();

    EquipItemsMap const* GetEquipItems(uint32 specialization) const;
    MiscItemsStore const& GetMiscItems() const;
    ProfessionsSet const* GetProfessions(uint32 classId) const;
    SpellsStore const& GetSpells() const;

private:
    void _LoadEquipItems();
    void _LoadMiscItems();
    void _LoadProfessions();
    void _LoadSpells();

    EquipItemsStore _equipItems;
    MiscItemsStore _miscItemsVector;
    ProfessionsStore _professionsMap;
    SpellsStore _spellsVector;
};

#define sBattlePayShopCharBoostMgr Tod::Singleton<BattlePayShopCharBoostMgr>::GetSingleton()

#endif
