/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu/>
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_MAP_UPDATER_H_INCLUDED
#define SF_MAP_UPDATER_H_INCLUDED

#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>

#include <unordered_map>
#include <memory>

class Map;

namespace Tod
{
    class MapUpdater
    {
    public:
        MapUpdater( uint8_t workersCount );
        ~MapUpdater();

        void Update( uint32_t diff );

    private:
        typedef std::unique_ptr< boost::asio::io_service::work > WorkPtr;
        typedef std::shared_ptr< std::promise< void > > PromisePtr;

        struct ThreadData
        {
            Map *      m_map;
            PromisePtr m_promise;
        };

        std::unordered_map< std::thread::id, ThreadData >   m_threadToMap;

        WorkPtr                     m_work;
        boost::asio::io_service     m_ioService;
        boost::thread_group         m_threadPool;
    };
}

#endif //_MAP_UPDATER_H_INCLUDED
