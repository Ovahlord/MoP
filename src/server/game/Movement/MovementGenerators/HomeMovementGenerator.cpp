/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "HomeMovementGenerator.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "WorldPacket.h"
#include "MoveSplineInit.h"
#include "MoveSpline.h"
#include "VehicleDefines.h"

void HomeMovementGenerator<Creature>::DoInitialize(Creature* owner)
{
    _setTargetLocation(owner);
}

void HomeMovementGenerator<Creature>::DoFinalize(Creature* owner)
{
    owner->ClearUnitState(UNIT_STATE_EVADE);
    if (arrived)
    {
        owner->LoadCreaturesAddon();
        owner->AI()->JustReachedHome();
        if (owner->IsRegeneratingHealth())
        {
            owner->SetFullHealth();
            owner->SetPower(POWER_MANA, owner->GetMaxPower(POWER_MANA));
        }
    }
}

void HomeMovementGenerator<Creature>::DoReset(Creature*) { }

void HomeMovementGenerator<Creature>::_setTargetLocation(Creature* owner)
{
    MoveSplineInit init(owner);

    // at apply we can select more nice return points base at current movegen
    if (owner->GetMotionMaster()->empty() || !owner->GetMotionMaster()->top()->GetResetPosition(owner, _x, _y, _z))
    {
        owner->GetHomePosition(_x, _y, _z, _o);
        init.SetFacing(_o);
    }

    init.MoveTo(_x, _y, _z);
    init.SetWalk(false);
    init.Launch();

    arrived = false;
    i_recalculateSpeed = false;

    owner->ClearUnitState(uint32(UNIT_STATE_ALL_STATE & ~(UNIT_STATE_EVADE | UNIT_STATE_IGNORE_PATHFINDING)));
}

bool HomeMovementGenerator<Creature>::DoUpdate(Creature* owner, const uint32 /*time_diff*/)
{
    arrived = owner->movespline->Finalized();

    if (arrived)
    {
        Vector3 dest = owner->movespline->FinalDestination();

        if (owner->movespline->onTransport)
            if (TransportBase* transport = owner->GetDirectTransport())
                transport->CalculatePassengerPosition(dest.x, dest.y, dest.z);

        if (owner->IsHovering())
            dest.z += owner->GetFloatValue(UNIT_HOVER_HEIGHT);

        bool arrivedDest = owner->GetPosition().IsInDist(dest.x, dest.y, dest.z, CONTACT_DISTANCE);
        if (arrivedDest)
            return false;

        _setTargetLocation(owner);
        return true;
    }

    if (i_recalculateSpeed)
        _setTargetLocation(owner);

    return !arrived;
}
