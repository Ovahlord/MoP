/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "MovementStructures.h"
#include "Player.h"

MovementStatusElements const PlayerMove[] = // 5.4.8 18414
{
    MSEHasPitch,                // 112
    MSEHasGuidByte2,            // 18
    MSEHasSpline,               // 148
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48
    MSEHasFallData,             // 140
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasFallDirection,        // 136
    MSEHasTransportData,        // 104
    MSEHasGuidByte4,            // 20
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasSplineElevation,      // 144
    MSEHasMovementFlags,        // 24
    MSEHasRemoteTimeValid,      // 172
    MSEMovementFlags,           // 24
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte1,            // 17
    MSEHasTimestamp,            // 32
    MSEMovementFlags2,          // 28
    MSEHasGuidByte5,            // 21
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte6,            // 22
    MSEPositionY,               // 40
    MSETransportGuidByte7,      // 63
    MSETransportPrevTime,       // 88 (22)
    MSETransportPositionX,      // 64
    MSETransportGuidByte5,      // 61
    MSETransportSeat,           // 80
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte3,      // 59
    MSETransportTime,           // 84 (21)
    MSETransportGuidByte4,      // 60
    MSETransportPositionZ,      // 72
    MSETransportGuidByte1,      // 57
    MSETransportPositionY,      // 68
    MSETransportOrientation,    // 76
    MSETransportGuidByte6,      // 62
    MSETransportVehicleId,      // 96
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEPositionZ,               // 44
    MSERemovedForces,           // 156
    MSETimestamp,               // 32
    MSEOrientation,             // 48
    MSEGuidByte3,               // 19
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEGuidByte0,               // 16
    MSEPitch,                   // 112
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEPositionX,               // 36
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEEnd
};

MovementStatusElements const MovementFallLand[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasFallData,             // 140
    MSEHasRemoteTimeValid,      // 172
    MSEHasSpline,               // 148
    MSEHasTimestamp,            // 32
    MSEHasGuidByte7,            // 23
    MSEHasHeightChangeFailed,   // 149
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte5,            // 21
    MSEHasPitch,                // 112
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags,        // 24
    MSEHasCounter,              // 168
    MSEHasGuidByte1,            // 17
    MSEHasTransportData,        // 104
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportVehicleId,   // 100
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEGuidByte4,               // 20
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte0,               // 16
    MSEGuidByte2,               // 18
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETransportGuidByte4,      // 60
    MSETransportPositionY,      // 68
    MSETransportOrientation,    // 76
    MSETransportPositionZ,      // 72
    MSETransportSeat,           // 80
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte6,      // 62
    MSETransportPrevTime,       // 88
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte5,      // 61
    MSETransportVehicleId,      // 96
    MSETransportTime,           // 84
    MSETransportPositionX,      // 64
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte0,      // 56
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEPitch,                   // 112
    MSEOrientation,             // 48
    MSEEnd
};

MovementStatusElements const MovementHeartBeat[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags,        // 24
    MSEHasSpline,               // 148
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte6,            // 22
    MSEHasPitch,                // 112
    MSEHasHeightChangeFailed,   // 149
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte4,            // 20
    MSEHasMovementFlags2,       // 28
    MSEHasOrientation,          // 48
    MSEHasTimestamp,            // 32
    MSEHasTransportData,        // 104
    MSEHasFallData,             // 140
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte0,            // 16
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportPrevTime,    // 92
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte6,               // 22
    MSEGuidByte1,               // 17
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte0,      // 56
    MSETransportVehicleId,      // 96
    MSETransportSeat,           // 80
    MSETransportGuidByte7,      // 63
    MSETransportPositionX,      // 64
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportPositionY,      // 68
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte5,      // 61
    MSETransportPositionZ,      // 72
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSECounter,                 // 168
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEEnd
};

MovementStatusElements const MovementJump[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte7,            // 23
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144
    MSEHasOrientation,          // 48
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportData,        // 104
    MSEHasSpline,               // 148
    MSERemovedForcesCount,      // 152
    MSEHasPitch,                // 112
    MSEHasMovementFlags,        // 24
    MSEHasTimestamp,            // 32
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasRemoteTimeValid,      // 172
    MSEHasFallData,             // 140
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte0,            // 16
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSEFallVerticalSpeed,       // 120
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte7,      // 63
    MSETransportSeat,           // 80
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte0,      // 56
    MSETransportPositionZ,      // 72
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportPositionY,      // 68
    MSETransportTime,           // 84
    MSETransportPositionX,      // 64
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte3,      // 59
    MSETransportVehicleId,      // 96
    MSETransportOrientation,    // 76
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MovementSetFacing[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasRemoteTimeValid,      // 172
    MSEHasPitch,                // 112
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48
    MSEHasTimestamp,            // 32
    MSEHasSpline,               // 148
    MSEHasCounter,              // 168
    MSEHasGuidByte4,            // 20
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte6,            // 22
    MSEHasFallData,             // 140
    MSEHasMovementFlags,        // 24
    MSEHasSplineElevation,      // 144
    MSEHasTransportData,        // 104
    MSEHasGuidByte7,            // 23
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte1,               // 17
    MSEGuidByte2,               // 18
    MSEGuidByte7,               // 23
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte2,      // 58
    MSETransportOrientation,    // 76
    MSETransportGuidByte7,      // 63
    MSETransportVehicleId,      // 96
    MSETransportGuidByte5,      // 61
    MSETransportTime,           // 84
    MSETransportPositionX,      // 64
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportSeat,           // 80
    MSETransportPositionY,      // 68
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte1,      // 57
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementSetPitch[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasMovementFlags,        // 24
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasGuidByte7,            // 23
    MSEHasTimestamp,            // 32
    MSEHasGuidByte0,            // 16
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte5,            // 21
    MSEHasCounter,              // 168
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasOrientation,          // 48
    MSEHasPitch,                // 112
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte1,            // 17
    MSEHasSpline,               // 148
    MSEHasFallData,             // 140
    MSEHasGuidByte3,            // 19
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEGuidByte2,               // 18
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSEGuidByte6,               // 22
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSESplineElevation,         // 144
    MSETransportPositionZ,      // 72
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte2,      // 58
    MSETransportVehicleId,      // 96
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportGuidByte4,      // 60
    MSETransportSeat,           // 80
    MSETransportOrientation,    // 76
    MSETransportPositionY,      // 68
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte7,      // 63
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementStartBackward[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasTimestamp,            // 32
    MSEHasOrientation,          // 48
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasFallData,             // 140
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte6,            // 22
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasGuidByte0,            // 16
    MSEHasMovementFlags,        // 24
    MSEHasPitch,                // 112
    MSEHasCounter,              // 168
    MSEHasMovementFlags2,       // 28
    MSEHasSpline,               // 148
    MSEHasGuidByte1,            // 17
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte5,               // 21
    MSEGuidByte2,               // 18
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSECounter,                 // 168
    MSETransportTime,           // 84
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte6,      // 62
    MSETransportSeat,           // 80
    MSETransportOrientation,    // 76
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportVehicleId,      // 96
    MSETransportGuidByte7,      // 63
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportGuidByte2,      // 58
    MSEOrientation,             // 48
    MSEFallTime,                // 116
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEEnd
};

MovementStatusElements const MovementStartForward[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasMovementFlags2,       // 28
    MSEHasHeightChangeFailed,   // 149
    MSEHasCounter,              // 168
    MSEHasSpline,               // 148
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48
    MSEHasFallData,             // 140
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte1,            // 17
    MSEHasTimestamp,            // 32
    MSEHasGuidByte7,            // 23
    MSEHasPitch,                // 112
    MSEHasTransportData,        // 104
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte3,            // 19
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte6,            // 22
    MSEHasRemoteTimeValid,      // 172
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSEGuidByte7,               // 23
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSEGuidByte3,               // 19
    MSEGuidByte2,               // 18
    MSEGuidByte4,               // 20
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte6,      // 62
    MSETransportPositionZ,      // 72
    MSETransportGuidByte4,      // 60
    MSETransportVehicleId,      // 96
    MSETransportSeat,           // 80
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSETransportPrevTime,       // 88
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte2,      // 58
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportTime,           // 84
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETimestamp,               // 32
    MSEPitch,                   // 112
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementStartStrafeLeft[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasGuidByte0,            // 16
    MSEHasTimestamp,            // 32
    MSEHasGuidByte3,            // 19
    MSEHasMovementFlags2,       // 28
    MSEHasPitch,                // 112
    MSEHasSpline,               // 148
    MSEHasGuidByte2,            // 18
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportData,        // 104
    MSEHasFallData,             // 140
    MSEHasGuidByte5,            // 21
    MSERemovedForcesCount,      // 152
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte4,            // 20
    MSEHasOrientation,          // 48
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte7,            // 23
    MSEHasCounter,              // 168
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte6,            // 22
    MSEHasMovementFlags,        // 24
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte4,   // 60
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEGuidByte0,               // 16
    MSEGuidByte2,               // 18
    MSERemovedForces,           // 156
    MSEGuidByte3,               // 19
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEGuidByte7,               // 23
    MSEGuidByte4,               // 20
    MSEGuidByte6,               // 22
    MSETransportGuidByte2,      // 58
    MSETransportPositionZ,      // 72
    MSETransportVehicleId,      // 96
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportPositionY,      // 68
    MSETransportGuidByte4,      // 60
    MSETransportTime,           // 84
    MSETransportSeat,           // 80
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte7,      // 63
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSEFallTime,                // 116
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSECounter,                 // 168
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSEEnd
};

MovementStatusElements const MovementStartStrafeRight[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasGuidByte0,            // 16
    MSEHasFallData,             // 140
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte3,            // 19
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportData,        // 104
    MSEHasCounter,              // 168
    MSEHasGuidByte1,            // 17
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte2,            // 18
    MSEHasPitch,                // 112
    MSEHasMovementFlags2,       // 28
    MSEHasOrientation,          // 48
    MSEHasSpline,               // 148
    MSEHasTimestamp,            // 32
    MSEHasFallDirection,        // 136
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportPrevTime,    // 92
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEGuidByte6,               // 22
    MSEGuidByte7,               // 23
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte5,               // 21
    MSEPitch,                   // 112
    MSETransportGuidByte1,      // 57
    MSETransportSeat,           // 80
    MSETransportGuidByte3,      // 59
    MSETransportPrevTime,       // 88
    MSETransportGuidByte7,      // 63
    MSETransportVehicleId,      // 96
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte0,      // 56
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSETransportPositionY,      // 68
    MSETransportPositionZ,      // 72
    MSETransportGuidByte4,      // 60
    MSETransportPositionX,      // 64
    MSETimestamp,               // 32
    MSEFallVerticalSpeed,       // 120
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSESplineElevation,         // 144
    MSEEnd
};

MovementStatusElements const MovementStartTurnLeft[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasOrientation,          // 48
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte5,            // 21
    MSEHasSpline,               // 148
    MSEHasTimestamp,            // 32
    MSEHasRemoteTimeValid,      // 172
    MSEHasHeightChangeFailed,   // 149
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte1,            // 17
    MSEHasMovementFlags2,       // 28
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasTransportData,        // 104
    MSEHasGuidByte7,            // 23
    MSEHasPitch,                // 112
    MSEHasSplineElevation,      // 144
    MSEHasFallData,             // 140
    MSEHasGuidByte6,            // 22
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte1,   // 57
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEGuidByte7,               // 23
    MSEGuidByte3,               // 19
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSEGuidByte2,               // 18
    MSEFallTime,                // 116
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEPitch,                   // 112
    MSETransportPositionY,      // 68
    MSETransportGuidByte3,      // 59
    MSETransportPositionX,      // 64
    MSETransportOrientation,    // 76
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte0,      // 56
    MSETransportVehicleId,      // 96
    MSETransportSeat,           // 80
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSEOrientation,             // 48
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MovementStartTurnRight[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEHasSpline,               // 148
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte0,            // 16
    MSEHasMovementFlags,        // 24
    MSEHasFallData,             // 140
    MSEHasPitch,                // 112
    MSEHasCounter,              // 168
    MSERemovedForcesCount,      // 152
    MSEHasSplineElevation,      // 144
    MSEHasMovementFlags2,       // 28
    MSEHasOrientation,          // 48
    MSEHasGuidByte2,            // 18
    MSEHasTimestamp,            // 32
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte3,            // 19
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportData,        // 104
    MSEHasGuidByte7,            // 23
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte1,   // 57
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEGuidByte7,               // 23
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEPitch,                   // 112
    MSETransportVehicleId,      // 96
    MSETransportGuidByte3,      // 59
    MSETransportPrevTime,       // 88
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte1,      // 57
    MSETransportPositionX,      // 64
    MSETransportSeat,           // 80
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte0,      // 56
    MSETransportPositionZ,      // 72
    MSETransportTime,           // 84
    MSETransportPositionY,      // 68
    MSETransportGuidByte6,      // 62
    MSETransportOrientation,    // 76
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementStop[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte2,            // 18
    MSEHasFallData,             // 140
    MSEHasGuidByte0,            // 16
    MSEHasRemoteTimeValid,      // 172
    MSEHasSpline,               // 148
    MSEHasCounter,              // 168
    MSEHasGuidByte1,            // 17
    MSERemovedForcesCount,      // 152
    MSEHasPitch,                // 112
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte6,            // 22
    MSEHasMovementFlags,        // 24
    MSEHasTimestamp,            // 32
    MSEHasMovementFlags2,       // 28
    MSEHasOrientation,          // 48
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte7,            // 23
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte6,   // 62
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEGuidByte0,               // 16
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEGuidByte6,               // 22
    MSEGuidByte1,               // 17
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEGuidByte5,               // 21
    MSEGuidByte7,               // 23
    MSEOrientation,             // 48
    MSEFallVerticalSpeed,       // 120
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSESplineElevation,         // 144
    MSETransportPositionX,      // 64
    MSETransportTime,           // 84
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportPositionY,      // 68
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte4,      // 60
    MSETransportVehicleId,      // 96
    MSETransportGuidByte0,      // 56
    MSETransportSeat,           // 80
    MSETransportPositionZ,      // 72
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MovementStopStrafe[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasFallData,             // 140
    MSEHasOrientation,          // 48
    MSEHasSplineElevation,      // 144
    MSEHasTimestamp,            // 32
    MSEHasMovementFlags,        // 24
    MSEHasCounter,              // 168
    MSEHasGuidByte6,            // 22
    MSEHasTransportData,        // 104
    MSEHasRemoteTimeValid,      // 172
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte4,            // 20
    MSEHasPitch,                // 112
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte0,            // 16
    MSEHasSpline,               // 148
    MSEHasGuidByte1,            // 17
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte0,               // 16
    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSETransportGuidByte0,      // 56
    MSETransportVehicleId,      // 96
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportPositionY,      // 68
    MSETransportPositionZ,      // 72
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportGuidByte3,      // 59
    MSETransportSeat,           // 80
    MSETransportPositionX,      // 64
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte5,      // 61
    MSETransportOrientation,    // 76
    MSEOrientation,             // 48
    MSESplineElevation,         // 144
    MSETimestamp,               // 32
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSEPitch,                   // 112
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementStopTurn[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEHasTransportData,        // 104
    MSERemovedForcesCount,      // 152
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte5,            // 21
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasRemoteTimeValid,      // 172
    MSEHasFallData,             // 140
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte1,            // 17
    MSEHasPitch,                // 112
    MSEHasGuidByte6,            // 22
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte2,            // 18
    MSEHasSpline,               // 148
    MSEHasMovementFlags2,       // 28
    MSEHasSplineElevation,      // 144
    MSEHasOrientation,          // 48
    MSEHasGuidByte7,            // 23
    MSEHasTimestamp,            // 32
    MSEMovementFlags2,          // 28
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte4,   // 60
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEGuidByte5,               // 21
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSETransportTime,           // 84
    MSETransportVehicleId,      // 96
    MSETransportSeat,           // 80
    MSETransportPositionY,      // 68
    MSETransportPositionX,      // 64
    MSETransportPrevTime,       // 88
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportGuidByte0,      // 56
    MSETransportPositionZ,      // 72
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte2,      // 58
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSECounter,                 // 168
    MSESplineElevation,         // 144
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementStartAscend[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasOrientation,          // 48
    MSEHasGuidByte3,            // 19
    MSEHasTransportData,        // 104
    MSEHasMovementFlags,        // 24
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte4,            // 20
    MSEHasTimestamp,            // 32
    MSEHasGuidByte7,            // 23
    MSEHasHeightChangeFailed,   // 149
    MSEHasPitch,                // 112
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags2,       // 28
    MSEHasSpline,               // 148
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte2,            // 18
    MSEHasCounter,              // 168
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte1,            // 17
    MSEHasSplineElevation,      // 144
    MSEHasFallData,             // 140
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte7,   // 63
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEGuidByte2,               // 18
    MSEGuidByte5,               // 21
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSETransportGuidByte3,      // 59
    MSETransportTime,           // 84
    MSETransportPositionY,      // 68
    MSETransportOrientation,    // 76
    MSETransportGuidByte6,      // 62
    MSETransportVehicleId,      // 96
    MSETransportPositionX,      // 64
    MSETransportGuidByte2,      // 58
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte7,      // 63
    MSETransportPositionZ,      // 72
    MSETransportSeat,           // 80
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte5,      // 61
    MSESplineElevation,         // 144
    MSEFallVerticalSpeed,       // 120
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEPitch,                   // 112
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementStartDescend[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasFallData,             // 140
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte4,            // 20
    MSEHasMovementFlags2,       // 28
    MSEHasPitch,                // 112
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte2,            // 18
    MSEHasSpline,               // 148
    MSEHasCounter,              // 168
    MSERemovedForcesCount,      // 152
    MSEHasTransportData,        // 104
    MSEHasOrientation,          // 48
    MSEHasGuidByte1,            // 17
    MSEHasHeightChangeFailed,   // 149
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144
    MSEHasTimestamp,            // 32
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSEGuidByte0,               // 16
    MSEGuidByte5,               // 21
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte7,      // 63
    MSETransportSeat,           // 80
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportPositionY,      // 68
    MSETransportVehicleId,      // 96
    MSETransportTime,           // 84
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportOrientation,    // 76
    MSETransportPositionZ,      // 72
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSEFallTime,                // 116
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEPitch,                   // 112
    MSECounter,                 // 168
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MovementStartSwim[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasSplineElevation,      // 144
    MSEHasTransportData,        // 104
    MSEHasGuidByte2,            // 18
    MSEHasRemoteTimeValid,      // 172
    MSEHasMovementFlags2,       // 28
    MSERemovedForcesCount,      // 152
    MSEHasPitch,                // 112
    MSEHasTimestamp,            // 32
    MSEHasGuidByte3,            // 19
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte1,            // 17
    MSEHasFallData,             // 140
    MSEHasCounter,              // 168
    MSEHasSpline,               // 148
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte4,            // 20
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte3,   // 59
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte7,               // 23
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSEGuidByte2,               // 18
    MSETransportGuidByte3,      // 59
    MSETransportPositionX,      // 64
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportSeat,           // 80
    MSETransportPrevTime,       // 88
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte2,      // 58
    MSETransportPositionZ,      // 72
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSETransportVehicleId,      // 96
    MSETransportGuidByte5,      // 61
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementStopSwim[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEHasPitch,                // 112
    MSEHasGuidByte6,            // 22
    MSEHasOrientation,          // 48
    MSEHasRemoteTimeValid,      // 172
    MSEHasHeightChangeFailed,   // 149
    MSEHasSpline,               // 148
    MSEHasCounter,              // 168
    MSEHasGuidByte4,            // 20
    MSEHasMovementFlags2,       // 28
    MSEHasTransportData,        // 104
    MSEHasFallData,             // 140
    MSEHasGuidByte1,            // 17
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte0,            // 16
    MSEHasTimestamp,            // 32
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte2,            // 18
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSEGuidByte4,               // 20
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEGuidByte2,               // 18
    MSETransportTime,           // 84
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte2,      // 58
    MSETransportPositionZ,      // 72
    MSETransportPositionX,      // 64
    MSETransportPositionY,      // 68
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte3,      // 59
    MSETransportSeat,           // 80
    MSETransportOrientation,    // 76
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportVehicleId,      // 96
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSESplineElevation,         // 144
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSEOrientation,             // 48
    MSEEnd
};

MovementStatusElements const MovementStopAscend[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasOrientation,          // 48
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte6,            // 22
    MSEHasMovementFlags2,       // 28
    MSEHasTimestamp,            // 32
    MSEHasCounter,              // 168
    MSEHasTransportData,        // 104
    MSEHasPitch,                // 112
    MSEHasSpline,               // 148
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte4,            // 20
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte5,            // 21
    MSERemovedForcesCount,      // 152
    MSEHasFallData,             // 140
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte1,            // 17
    MSEHasSplineElevation,      // 144
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte7,   // 63
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte2,               // 18
    MSETransportGuidByte5,      // 61
    MSETransportPositionY,      // 68
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte3,      // 59
    MSETransportPrevTime,       // 88
    MSETransportPositionX,      // 64
    MSETransportOrientation,    // 76
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte2,      // 58
    MSETransportPositionZ,      // 72
    MSETransportVehicleId,      // 96
    MSETransportTime,           // 84
    MSETransportSeat,           // 80
    MSETransportGuidByte6,      // 62
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEPitch,                   // 112
    MSECounter,                 // 168
    MSEOrientation,             // 48
    MSEEnd
};

MovementStatusElements const MovementStopPitch[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasFallData,             // 140
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte0,            // 16
    MSEHasTimestamp,            // 32
    MSEHasSpline,               // 148
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasOrientation,          // 48
    MSEHasMovementFlags,        // 24
    MSEHasPitch,                // 112
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags2,       // 28
    MSEHasRemoteTimeValid,      // 172
    MSEHasCounter,              // 168
    MSEHasTransportData,        // 104
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte1,            // 17
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETransportGuidByte1,      // 57
    MSETransportPositionX,      // 64
    MSETransportPositionZ,      // 72
    MSETransportPositionY,      // 68
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte0,      // 56
    MSETransportPrevTime,       // 88
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte3,      // 59
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportTime,           // 84
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementStartPitchDown[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte3,            // 19
    MSEHasOrientation,          // 48
    MSEHasGuidByte5,            // 21
    MSEHasRemoteTimeValid,      // 172
    MSEHasCounter,              // 168
    MSEHasSpline,               // 148
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte1,            // 17
    MSEHasSplineElevation,      // 144
    MSEHasFallData,             // 140
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportData,        // 104
    MSEHasTimestamp,            // 32
    MSEHasMovementFlags2,       // 28
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte0,            // 16
    MSEHasPitch,                // 112
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte0,   // 56
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSERemovedForces,           // 156
    MSEGuidByte7,               // 23
    MSEGuidByte2,               // 18
    MSEGuidByte1,               // 17
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETransportOrientation,    // 76
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportPositionZ,      // 72
    MSETransportGuidByte0,      // 56
    MSETransportTime,           // 84
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte1,      // 57
    MSETransportSeat,           // 80
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte5,      // 61
    MSETransportPositionX,      // 64
    MSETransportVehicleId,      // 96
    MSETransportPositionY,      // 68
    MSETransportGuidByte3,      // 59
    MSETransportPrevTime,       // 88
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementStartPitchUp[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasGuidByte0,            // 16
    MSEHasPitch,                // 112
    MSEHasGuidByte3,            // 19
    MSEHasSpline,               // 148
    MSEHasOrientation,          // 48
    MSEHasGuidByte5,            // 21
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte1,            // 17
    MSEHasFallData,             // 140
    MSEHasMovementFlags2,       // 28
    MSEHasTransportData,        // 104
    MSEHasCounter,              // 168
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte6,            // 22
    MSERemovedForcesCount,      // 152
    MSEHasTimestamp,            // 32
    MSEHasGuidByte4,            // 20
    MSEHasRemoteTimeValid,      // 172
    MSEHasSplineElevation,      // 144
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEGuidByte5,               // 21
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSETransportSeat,           // 80
    MSETransportGuidByte3,      // 59
    MSETransportVehicleId,      // 96
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportPositionX,      // 64
    MSETransportOrientation,    // 76
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportPositionZ,      // 72
    MSEPitch,                   // 112
    MSEOrientation,             // 48
    MSESplineElevation,         // 144
    MSEFallVerticalSpeed,       // 120
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MoveChngTransport[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasGuidByte1,            // 17
    MSEHasPitch,                // 112
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte4,            // 20
    MSEHasCounter,              // 168
    MSEHasGuidByte5,            // 21
    MSERemovedForcesCount,      // 152
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte6,            // 22
    MSEHasSpline,               // 148
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte0,            // 16
    MSEHasTransportData,        // 104
    MSEHasMovementFlags,        // 24
    MSEHasHeightChangeFailed,   // 149
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte3,            // 19
    MSEHasTimestamp,            // 32
    MSEHasFallData,             // 140
    MSEHasOrientation,          // 48
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte0,   // 56
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28

    MSEGuidByte5,               // 21
    MSEGuidByte2,               // 18
    MSERemovedForces,           // 156
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEFallVerticalSpeed,       // 120
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSETransportGuidByte6,      // 62
    MSETransportPositionZ,      // 72
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte4,      // 60
    MSETransportVehicleId,      // 96
    MSETransportGuidByte3,      // 59
    MSETransportTime,           // 84
    MSETransportGuidByte2,      // 58
    MSETransportPrevTime,       // 88
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportGuidByte1,      // 57
    MSETransportSeat,           // 80
    MSETransportOrientation,    // 76
    MSETransportGuidByte7,      // 63
    MSETransportPositionX,      // 64
    MSEOrientation,             // 48
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MoveSplineDone[] = // 5.4.8 18414
{
    MSEExtraElement,            // 176
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEHasGuidByte7,            // 23
    MSEHasMovementFlags2,       // 28
    MSEHasSplineElevation,      // 144 90h
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte3,            // 19
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte0,            // 16
    MSEHasPitch,                // 112 70h
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte1,            // 17
    MSEHasSpline,               // 148
    MSEHasGuidByte2,            // 18
    MSEHasCounter,              // 168
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte5,            // 21
    MSERemovedForcesCount,      // 152
    MSEHasOrientation,          // 48  30h
    MSEHasTimestamp,            // 32
    MSEHasFallData,             // 140
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte6,   // 62
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136

    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSEGuidByte7,               // 23
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSESplineElevation,         // 144 90h
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportPositionX,      // 64
    MSETransportGuidByte6,      // 62
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportTime,           // 84
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSECounter,                 // 168
    MSEOrientation,             // 48  30h
    MSETimestamp,               // 32
    MSEPitch,                   // 112 70h
    MSEEnd
};

MovementStatusElements const DismissControlledVehicle[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEHasGuidByte7,            // 23
    MSEHasFallData,             // 140
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144 90h
    MSEHasOrientation,          // 48  30h
    MSEHasMovementFlags,        // 24
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte6,            // 22
    MSEHasCounter,              // 168
    MSEHasGuidByte1,            // 17
    MSEHasTimestamp,            // 32
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte3,            // 19
    MSEHasSpline,               // 148
    MSEHasMovementFlags2,       // 28
    MSERemovedForcesCount,      // 152
    MSEHasPitch,                // 112 70h
    MSEHasGuidByte0,            // 16
    MSEMovementFlags2,          // 28
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportVehicleId,   // 100
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136

    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte4,               // 20
    MSEGuidByte0,               // 16
    MSEGuidByte7,               // 23
    MSEGuidByte5,               // 21
    MSEGuidByte2,               // 18
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte4,      // 60
    MSETransportPositionX,      // 64
    MSETransportGuidByte2,      // 58
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportGuidByte1,      // 57
    MSETransportOrientation,    // 76
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte5,      // 61
    MSETransportTime,           // 84
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportPositionY,      // 68
    MSEPitch,                   // 112 70h
    MSETimestamp,               // 32
    MSEOrientation,             // 48  30h
    MSESplineElevation,         // 144 90h
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementSetRunMode[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEHasPitch,                // 112
    MSEHasSplineElevation,      // 144
    MSERemovedForcesCount,      // 152
    MSEHasOrientation,          // 48
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte3,            // 19
    MSEHasCounter,              // 168
    MSEHasGuidByte6,            // 22
    MSEHasTimestamp,            // 32
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags2,       // 28
    MSEHasFallData,             // 140
    MSEHasGuidByte7,            // 23
    MSEHasSpline,               // 148
    MSEHasRemoteTimeValid,      // 172
    MSEHasHeightChangeFailed,   // 149
    MSEHasMovementFlags,        // 24
    MSEHasTransportData,        // 104
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEGuidByte5,               // 21
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSETransportPositionZ,      // 72
    MSETransportGuidByte3,      // 59
    MSETransportVehicleId,      // 96
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSETransportSeat,           // 80
    MSETransportPositionX,      // 64
    MSETransportTime,           // 84
    MSETransportPrevTime,       // 88
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte5,      // 61
    MSETransportPositionY,      // 68
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSEPitch,                   // 112
    MSEOrientation,             // 48
    MSESplineElevation,         // 144
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementSetWalkMode[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasSplineElevation,      // 144
    MSEHasRemoteTimeValid,      // 172
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte0,            // 16
    MSEHasFallData,             // 140
    MSEHasMovementFlags2,       // 28
    MSEHasPitch,                // 112
    MSEHasTimestamp,            // 32
    MSEHasSpline,               // 148
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte3,            // 19
    MSEHasCounter,              // 168
    MSEHasGuidByte6,            // 22
    MSEHasHeightChangeFailed,   // 149
    MSEHasOrientation,          // 48
    MSEHasTransportData,        // 104
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte4,               // 20
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte2,               // 18
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSETransportPositionX,      // 64
    MSETransportGuidByte6,      // 62
    MSETransportPositionZ,      // 72
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte4,      // 60
    MSETransportSeat,           // 80
    MSETransportGuidByte7,      // 63
    MSETransportPositionY,      // 68
    MSETransportGuidByte5,      // 61
    MSETransportTime,           // 84
    MSETransportGuidByte2,      // 58
    MSETransportOrientation,    // 76
    MSETransportGuidByte3,      // 59
    MSETransportPrevTime,       // 88
    MSETransportVehicleId,      // 96
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementSetFly[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasGuidByte5,            // 21
    MSEHasTransportData,        // 104
    MSEHasGuidByte3,            // 19
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags2,       // 28
    MSEHasHeightChangeFailed,   // 149
    MSEHasFallData,             // 140
    MSEHasGuidByte6,            // 22
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte7,            // 23
    MSEHasTimestamp,            // 32
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte2,            // 18
    MSEHasPitch,                // 112 70h
    MSEHasOrientation,          // 48  30h
    MSEHasGuidByte1,            // 17
    MSEHasSpline,               // 148
    MSEHasSplineElevation,      // 144 90h
    MSEHasCounter,              // 168
    MSEHasGuidByte4,            // 20
    MSEHasMovementFlags,        // 24
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte0,   // 56
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24

    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSEGuidByte5,               // 21
    MSEGuidByte2,               // 18
    MSEGuidByte4,               // 20
    MSEGuidByte0,               // 16
    MSEGuidByte7,               // 23
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportTime,           // 84
    MSETransportSeat,           // 80
    MSETransportPrevTime,       // 88
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte4,      // 60
    MSETransportPositionY,      // 68
    MSETransportGuidByte3,      // 59
    MSETransportPositionZ,      // 72
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSETransportPositionX,      // 64
    MSETransportOrientation,    // 76
    MSETransportVehicleId,      // 96
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSESplineElevation,         // 144 90h
    MSEPitch,                   // 112 70h
    MSEOrientation,             // 48  30h
    MSEEnd
};

MovementStatusElements const MovementSetCanTransitionBetweenSwimAndFlyAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasMovementFlags2,       // 28
    MSEHasPitch,                // 112
    MSERemovedForcesCount,      // 152
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte7,            // 23
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte1,            // 17
    MSEHasSpline,               // 148
    MSEHasGuidByte6,            // 22
    MSEHasMovementFlags,        // 24
    MSEHasCounter,              // 168
    MSEHasTimestamp,            // 32
    MSEHasOrientation,          // 48
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte5,            // 21
    MSEHasFallData,             // 140
    MSEHasTransportData,        // 104
    MSEHasGuidByte4,            // 20
    MSEHasSplineElevation,      // 144
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte2,   // 58
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24

    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte6,      // 62
    MSETransportPositionX,      // 64
    MSETransportVehicleId,      // 96
    MSETransportGuidByte1,      // 57
    MSETransportPositionY,      // 68
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte0,      // 56
    MSETransportOrientation,    // 76
    MSETransportGuidByte3,      // 59
    MSETransportTime,           // 84
    MSETransportGuidByte5,      // 61
    MSETransportSeat,           // 80
    MSETransportPositionZ,      // 72
    MSETransportPrevTime,       // 88
    MSETransportGuidByte2,      // 58
    MSEPitch,                   // 112
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementUpdateSwimBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte3,            // 27
    MSEHasGuidByte6,            // 30
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte1,            // 25
    MSEHasMovementFlags2,       // 36
    MSEHasOrientation,          // 56  38h
    MSERemovedForcesCount,      // 160
    MSEHasGuidByte7,            // 31
    MSEHasTimestamp,            // 40
    MSEHasRemoteTimeValid,      // 180
    MSEMovementFlags2,          // 36
    MSEHasFallData,             // 148
    MSEHasMovementFlags,        // 32
    MSEHasGuidByte5,            // 29
    MSEMovementFlags,           // 32
    MSEHasSplineElevation,      // 152 98h
    MSEHasCounter,              // 176
    MSEHasSpline,               // 156
    MSEHasTransportData,        // 112
    MSEHasPitch,                // 120 78h
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte1,   // 65
    MSEHasGuidByte2,            // 26
    MSEHasGuidByte4,            // 28
    MSEHasGuidByte0,            // 24
    MSEHasFallDirection,        // 144
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallCosAngle,            // 132 84h
    MSEFallSinAngle,            // 136 88h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallTime,                // 124
    MSEGuidByte0,               // 24
    MSETransportPrevTime,       // 96
    MSETransportGuidByte7,      // 71
    MSETransportGuidByte2,      // 66
    MSETransportPositionY,      // 76  4ch
    MSETransportGuidByte4,      // 68
    MSETransportSeat,           // 88
    MSETransportGuidByte3,      // 67
    MSETransportGuidByte0,      // 64
    MSETransportVehicleId,      // 104
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte6,      // 70
    MSETransportPositionX,      // 72  48h
    MSETransportPositionZ,      // 80  50h
    MSETransportOrientation,    // 84  54h
    MSETransportTime,           // 92
    MSEExtraElement,            // 16  10h
    MSEGuidByte1,               // 25
    MSEPositionX,               // 44  2ch
    MSEGuidByte4,               // 28
    MSEOrientation,             // 56  38h
    MSESplineElevation,         // 152 98h
    MSEGuidByte2,               // 26
    MSEGuidByte3,               // 27
    MSEPitch,                   // 120 78h
    MSEGuidByte7,               // 31
    MSEPositionZ,               // 52  34h
    MSERemovedForces,           // 164
    MSEPositionY,               // 48  30h
    MSECounter,                 // 176
    MSEGuidByte5,               // 29
    MSETimestamp,               // 40
    MSEGuidByte6,               // 30
    MSEEnd
};

MovementStatusElements const MovementUpdateSwimSpeed[] = // 5.4.8 18414
{
    MSEHasOrientation,          // 56  38h
    MSEHasGuidByte0,            // 24
    MSEHasSpline,               // 156
    MSEHasHeightChangeFailed,   // 157
    MSEHasPitch,                // 120 78h
    MSEHasGuidByte6,            // 30
    MSEHasRemoteTimeValid,      // 180
    MSEHasCounter,              // 176
    MSEHasFallData,             // 148
    MSEHasTimestamp,            // 40
    MSEHasMovementFlags,        // 32
    MSEHasSplineElevation,      // 152 98h
    MSEHasTransportData,        // 112
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte1,   // 65
    MSEMovementFlags,           // 32
    MSEHasFallDirection,        // 144
    MSEHasGuidByte4,            // 28
    MSEHasGuidByte2,            // 26
    MSEHasMovementFlags2,       // 36
    MSERemovedForcesCount,      // 160
    MSEHasGuidByte3,            // 27
    MSEMovementFlags2,          // 36
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte5,            // 29
    MSEHasGuidByte1,            // 25

    MSEFallSinAngle,            // 136 88h
    MSEFallCosAngle,            // 132 84h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallTime,                // 124
    MSEFallVerticalSpeed,       // 128 80h
    MSEExtraElement,            // 16  10h
    MSETransportPositionX,      // 72  48h
    MSETransportSeat,           // 88
    MSETransportGuidByte3,      // 67
    MSETransportPositionY,      // 76  4ch
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte2,      // 66
    MSETransportGuidByte0,      // 64
    MSETransportGuidByte1,      // 65
    MSETransportPositionZ,      // 80  50h
    MSETransportVehicleId,      // 104
    MSETransportTime,           // 92
    MSETransportGuidByte4,      // 68
    MSETransportOrientation,    // 84  54h
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte7,      // 71
    MSETransportPrevTime,       // 96
    MSEPositionX,               // 44  2ch
    MSECounter,                 // 176
    MSEGuidByte6,               // 30
    MSEGuidByte0,               // 24
    MSESplineElevation,         // 152 98h
    MSEGuidByte5,               // 29
    MSEOrientation,             // 56  38h
    MSEPitch,                   // 120 78h
    MSETimestamp,               // 40
    MSEPositionY,               // 48  30h
    MSEGuidByte1,               // 25
    MSERemovedForces,           // 164
    MSEGuidByte3,               // 27
    MSEPositionZ,               // 52  34h
    MSEGuidByte4,               // 28
    MSEGuidByte7,               // 31
    MSEGuidByte2,               // 26
    MSEEnd
};

MovementStatusElements const MovementUpdateTurnRate[] = // 5.4.8 18414
{
    MSEHasGuidByte4,            // 28
    MSEHasFallData,             // 148
    MSEHasMovementFlags,        // 32
    MSEHasSplineElevation,      // 152 98h
    MSEHasFallDirection,        // 144
    MSEHasRemoteTimeValid,      // 180
    MSEHasGuidByte5,            // 29
    MSEHasGuidByte6,            // 30
    MSEMovementFlags,           // 32
    MSEHasGuidByte3,            // 27
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte0,            // 24
    MSEHasGuidByte1,            // 25
    MSEHasSpline,               // 156
    MSEHasGuidByte7,            // 31
    MSEHasTimestamp,            // 40
    MSEHasGuidByte2,            // 26
    MSEHasCounter,              // 176
    MSEHasOrientation,          // 56  38h
    MSERemovedForcesCount,      // 160
    MSEHasPitch,                // 120 78h
    MSEHasMovementFlags2,       // 36
    MSEMovementFlags2,          // 36
    MSEHasTransportData,        // 112
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte4,   // 68
    MSETransportVehicleId,      // 104
    MSETransportPrevTime,       // 96
    MSETransportGuidByte3,      // 67
    MSETransportOrientation,    // 84  54h
    MSETransportGuidByte0,      // 64
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte6,      // 70
    MSETransportTime,           // 92
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte2,      // 66
    MSETransportPositionY,      // 76  4ch
    MSETransportPositionX,      // 72  48h
    MSETransportGuidByte7,      // 71
    MSETransportPositionZ,      // 80  50h
    MSETransportSeat,           // 88
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallSinAngle,            // 136 88h
    MSEFallCosAngle,            // 132 84h
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallTime,                // 124
    MSERemovedForces,           // 164
    MSEPositionY,               // 48  30h
    MSEPitch,                   // 120 78h
    MSEGuidByte7,               // 31
    MSECounter,                 // 176
    MSEGuidByte5,               // 29
    MSEGuidByte6,               // 30
    MSEGuidByte3,               // 27
    MSEPositionX,               // 44  2ch
    MSEGuidByte2,               // 26
    MSEGuidByte0,               // 24
    MSEOrientation,             // 56  38h
    MSESplineElevation,         // 152 98h
    MSEExtraElement,            // 16  10h
    MSETimestamp,               // 40
    MSEGuidByte4,               // 28
    MSEGuidByte1,               // 25
    MSEPositionZ,               // 52  34h
    MSEEnd
};

MovementStatusElements const MovementUpdateRunSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte0,            // 24
    MSEHasGuidByte3,            // 27
    MSEHasTimestamp,            // 40
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte4,            // 28
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte1,            // 25
    MSEHasPitch,                // 120 78h
    MSEHasGuidByte2,            // 26
    MSEHasGuidByte5,            // 29
    MSEHasCounter,              // 176
    MSEHasGuidByte6,            // 30
    MSEHasMovementFlags,        // 32
    MSERemovedForcesCount,      // 160
    MSEMovementFlags,           // 32
    MSEHasMovementFlags2,       // 36
    MSEMovementFlags2,          // 36
    MSEHasTransportData,        // 112
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte4,   // 68
    MSEHasSplineElevation,      // 152 98h
    MSEHasOrientation,          // 56  38h
    MSEHasFallData,             // 148
    MSEHasSpline,               // 156
    MSEHasFallDirection,        // 144
    MSEHasRemoteTimeValid,      // 180

    MSEFallVerticalSpeed,       // 128
    MSEFallSinAngle,            // 136
    MSEFallCosAngle,            // 132
    MSEFallHorizontalSpeed,     // 140
    MSEFallTime,                // 124
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte2,      // 66
    MSETransportSeat,           // 88
    MSETransportPositionZ,      // 80  50h
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte7,      // 71
    MSETransportGuidByte3,      // 67
    MSETransportPositionY,      // 76  4ch
    MSETransportPrevTime,       // 96
    MSETransportGuidByte6,      // 70
    MSETransportTime,           // 92
    MSETransportOrientation,    // 84  54h
    MSETransportVehicleId,      // 104
    MSETransportPositionX,      // 72  48h
    MSETransportGuidByte0,      // 64
    MSETransportGuidByte1,      // 65
    MSEPositionX,               // 44  2ch
    MSESplineElevation,         // 152 98h
    MSEGuidByte1,               // 25
    MSEGuidByte2,               // 26
    MSEGuidByte6,               // 30
    MSECounter,                 // 176
    MSEGuidByte0,               // 24
    MSETimestamp,               // 40
    MSEGuidByte3,               // 27
    MSEGuidByte5,               // 29
    MSERemovedForces,           // 164
    MSEPitch,                   // 120 78h
    MSEGuidByte7,               // 31
    MSEOrientation,             // 56  38h
    MSEPositionZ,               // 52  34h
    MSEPositionY,               // 48  30h
    MSEExtraElement,            // 16
    MSEGuidByte4,               // 28
    MSEEnd
};

MovementStatusElements const MovementUpdateFlightSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte3,            // 27
    MSEHasGuidByte2,            // 26
    MSEHasTransportData,        // 112
    MSEHasOrientation,          // 56  38h
    MSEHasSplineElevation,      // 152 98h
    MSEHasGuidByte6,            // 30
    MSEHasGuidByte7,            // 31
    MSEHasRemoteTimeValid,      // 180
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte0,   // 64
    MSEHasMovementFlags,        // 32
    MSERemovedForcesCount,      // 160
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte0,            // 24
    MSEHasCounter,              // 176
    MSEHasFallData,             // 148
    MSEHasGuidByte5,            // 29
    MSEHasGuidByte4,            // 28
    MSEHasMovementFlags2,       // 36
    MSEHasPitch,                // 120 78h
    MSEHasTimestamp,            // 40
    MSEHasFallDirection,        // 144
    MSEHasSpline,               // 156
    MSEMovementFlags,           // 32
    MSEMovementFlags2,          // 36
    MSEHasGuidByte1,            // 25

    MSEGuidByte5,               // 29
    MSEPositionZ,               // 52  34h
    MSEGuidByte7,               // 31
    MSETransportPositionX,      // 72  48h
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte0,      // 64
    MSETransportGuidByte2,      // 66
    MSETransportGuidByte5,      // 69
    MSETransportPrevTime,       // 96
    MSETransportGuidByte1,      // 65
    MSETransportPositionZ,      // 80  50h
    MSETransportVehicleId,      // 104
    MSETransportGuidByte7,      // 71
    MSETransportTime,           // 92
    MSETransportGuidByte3,      // 67
    MSETransportPositionY,      // 76  4ch
    MSETransportOrientation,    // 84  54h
    MSETransportSeat,           // 88
    MSEGuidByte0,               // 24
    MSEGuidByte2,               // 26
    MSEFallTime,                // 124
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallSinAngle,            // 136 88h
    MSEFallCosAngle,            // 132 84h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEPositionX,               // 44  2ch
    MSESplineElevation,         // 152 98h
    MSEGuidByte4,               // 28
    MSERemovedForces,           // 164
    MSEPositionY,               // 48  30h
    MSEGuidByte6,               // 30
    MSECounter,                 // 176
    MSETimestamp,               // 40
    MSEOrientation,             // 56  38h
    MSEGuidByte1,               // 25
    MSEExtraElement,            // 16  10h
    MSEPitch,                   // 120 78h
    MSEGuidByte3,               // 27
    MSEEnd
};

MovementStatusElements const MovementUpdateFlightBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte0,            // 24
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte2,            // 26
    MSEHasGuidByte1,            // 25
    MSEMovementFlags2,          // 36
    MSEHasRemoteTimeValid,      // 180
    MSEHasFallData,             // 148
    MSEHasCounter,              // 176
    MSERemovedForcesCount,      // 160
    MSEHasSplineElevation,      // 152 98h
    MSEHasTimestamp,            // 40
    MSEHasTransportData,        // 112
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte6,   // 70
    MSEHasMovementFlags,        // 32
    MSEHasSpline,               // 156
    MSEHasOrientation,          // 56  38h
    MSEHasFallDirection,        // 144
    MSEHasGuidByte5,            // 29
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte6,            // 30
    MSEHasPitch,                // 120 78h
    MSEHasMovementFlags2,       // 36
    MSEHasGuidByte3,            // 27
    MSEMovementFlags,           // 32
    MSEHasGuidByte4,            // 28
    MSETransportPositionZ,      // 80  50h
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte0,      // 64
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte3,      // 67
    MSETransportPrevTime,       // 96
    MSETransportGuidByte7,      // 71
    MSETransportOrientation,    // 84  54h
    MSETransportVehicleId,      // 104
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte2,      // 66
    MSETransportTime,           // 92
    MSETransportPositionX,      // 72  48h
    MSETransportSeat,           // 88
    MSETransportPositionY,      // 76  4ch
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallCosAngle,            // 132 84h
    MSEFallSinAngle,            // 136 88h
    MSEFallTime,                // 124
    MSEFallVerticalSpeed,       // 128 80h
    MSESplineElevation,         // 152 98h
    MSEGuidByte5,               // 29
    MSEPositionX,               // 44  2ch
    MSEGuidByte3,               // 27
    MSEGuidByte0,               // 24
    MSERemovedForces,           // 164
    MSETimestamp,               // 40
    MSEPitch,                   // 120 78h
    MSEGuidByte2,               // 26
    MSEGuidByte1,               // 25
    MSEGuidByte6,               // 30
    MSEExtraElement,            // 16  10h
    MSEOrientation,             // 56  38h
    MSECounter,                 // 176
    MSEPositionY,               // 48  30h
    MSEGuidByte7,               // 31
    MSEGuidByte4,               // 28
    MSEPositionZ,               // 52  34h
    MSEEnd
};

MovementStatusElements const MovementUpdateCollisionHeight[] = // 5.4.8 18414
{
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte3,            // 27
    MSEHasMovementFlags2,       // 36
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte4,            // 28
    MSEHasGuidByte2,            // 26
    MSEHasOrientation,          // 56  38h
    MSEHasGuidByte5,            // 29
    MSEHasSpline,               // 156
    MSEHasSplineElevation,      // 152 98h
    MSEHasMovementFlags,        // 32
    MSEHasTimestamp,            // 40
    MSEHasPitch,                // 120 78h
    MSEHasGuidByte6,            // 30
    MSEHasGuidByte1,            // 25
    MSEHasCounter,              // 176
    MSEMovementFlags,           // 32
    MSEHasRemoteTimeValid,      // 180
    MSERemovedForcesCount,      // 160
    MSEHasFallData,             // 148
    MSEHasFallDirection,        // 144
    MSEHasTransportData,        // 112
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte0,   // 64
    MSEHasGuidByte0,            // 24
    MSEMovementFlags2,          // 36
    MSETransportOrientation,    // 84  54h
    MSETransportGuidByte7,      // 71
    MSETransportPositionZ,      // 80  50h
    MSETransportSeat,           // 88
    MSETransportGuidByte3,      // 67
    MSETransportTime,           // 92
    MSETransportVehicleId,      // 104
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte2,      // 66
    MSETransportPositionY,      // 76  4ch
    MSETransportGuidByte0,      // 64
    MSETransportPrevTime,       // 96
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte5,      // 69
    MSETransportPositionX,      // 72  48h
    MSESplineElevation,         // 152 98h
    MSEPositionZ,               // 52  34h
    MSEPitch,                   // 120 78h
    MSEExtraElement,            // 16  10h
    MSEGuidByte5,               // 29
    MSEGuidByte7,               // 31
    MSEGuidByte4,               // 28
    MSEGuidByte3,               // 27
    MSECounter,                 // 176
    MSEPositionX,               // 44  2ch
    MSEFallCosAngle,            // 132 84h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallSinAngle,            // 136 88h
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallTime,                // 124
    MSEOrientation,             // 56  38h
    MSEGuidByte1,               // 25
    MSEPositionY,               // 48  30h
    MSERemovedForces,           // 164
    MSEGuidByte0,               // 24
    MSEExtraElement,            // 20  14h
    MSEGuidByte6,               // 30
    MSETimestamp,               // 40
    MSEGuidByte2,               // 26
    MSEEnd
};

MovementStatusElements const MovementForceRunSpeedChangeAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEExtraElement,            // 184
    MSEHasMovementFlags2,       // 28
    MSEHasCounter,              // 168
    MSEHasGuidByte6,            // 22
    MSEHasTimestamp,            // 32
    MSEHasFallData,             // 140
    MSEHasGuidByte5,            // 21
    MSEHasOrientation,          // 48  30h
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte1,            // 17
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte0,            // 16
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags,        // 24
    MSEHasPitch,                // 112 70h
    MSEHasSplineElevation,      // 144 90h
    MSEHasSpline,               // 148
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte5,   // 61
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28

    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte5,               // 21
    MSERemovedForces,           // 156
    MSECounter,                 // 168
    MSESplineElevation,         // 144 90h
    MSETransportGuidByte2,      // 58
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte3,      // 59
    MSETransportPositionZ,      // 72
    MSETransportPositionX,      // 64
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportPrevTime,       // 88
    MSETransportGuidByte5,      // 61
    MSETransportTime,           // 84
    MSETransportGuidByte4,      // 60
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEOrientation,             // 48  30h
    MSETimestamp,               // 32
    MSEPitch,                   // 112 70h
    MSEEnd
};

MovementStatusElements const MovementSetCollisionHeightAck[] = // 5.4.8 18414
{
    MSEExtraElement,            // 196
    MSEPositionZ,               // 52  34h
    MSEExtraElement,            // 16  10h
    MSEPositionY,               // 48  30h
    MSEMovementCounter,         // 184
    MSEPositionX,               // 44  2ch
    MSEHasRemoteTimeValid,      // 180
    MSERemovedForcesCount,      // 160
    MSEHasCounter,              // 176
    MSEExtraElement,            // 192
    MSEHasGuidByte5,            // 29
    MSEHasTimestamp,            // 40
    MSEHasSplineElevation,      // 152 98h
    MSEHasGuidByte4,            // 28
    MSEHasGuidByte6,            // 30
    MSEHasGuidByte3,            // 27
    MSEHasTransportData,        // 112
    MSEHasFallData,             // 148
    MSEHasPitch,                // 120 78h
    MSEHasOrientation,          // 56  38h
    MSEHasMovementFlags,        // 32
    MSEHasMovementFlags2,       // 36
    MSEHasGuidByte0,            // 24
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte1,            // 25
    MSEHasSpline,               // 156
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte2,            // 26
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte7,   // 71
    MSEMovementFlags,           // 32
    MSEHasFallDirection,        // 144
    MSEMovementFlags2,          // 36

    MSEGuidByte1,               // 25
    MSEGuidByte0,               // 24
    MSERemovedForces,           // 164
    MSEGuidByte5,               // 29
    MSEGuidByte4,               // 28
    MSEGuidByte3,               // 27
    MSEGuidByte6,               // 30
    MSEGuidByte7,               // 31
    MSEGuidByte2,               // 26
    MSETransportTime,           // 92
    MSETransportPositionY,      // 76  4ch
    MSETransportPositionX,      // 72  48h
    MSETransportGuidByte3,      // 67
    MSETransportSeat,           // 88
    MSETransportGuidByte5,      // 69
    MSETransportOrientation,    // 84  54h
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte0,      // 64
    MSETransportPrevTime,       // 96
    MSETransportVehicleId,      // 104
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte7,      // 71
    MSETransportGuidByte2,      // 66
    MSETransportPositionZ,      // 80  50h
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallSinAngle,            // 136 88h
    MSEFallCosAngle,            // 132 84h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallTime,                // 124
    MSEOrientation,             // 56  38h
    MSEPitch,                   // 120 78h
    MSESplineElevation,         // 152 98h
    MSETimestamp,               // 40
    MSECounter,                 // 176
    MSEEnd
};

MovementStatusElements const MovementForceFlightBackSpeedChangeAck[] = // 5.4.8 18414
{
    MSEExtraElement,            // 184
    MSEPositionZ,               // 44
    MSEMovementCounter,         // 176
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasTransportData,        // 104
    MSEHasMovementFlags,        // 24
    MSEHasPitch,                // 112
    MSEHasCounter,              // 168
    MSEHasSplineElevation,      // 144
    MSEHasHeightChangeFailed,   // 149
    MSEHasOrientation,          // 48
    MSEHasGuidByte6,            // 22
    MSEHasFallData,             // 140
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte2,            // 18
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte3,            // 19
    MSEHasSpline,               // 148
    MSEHasGuidByte1,            // 17
    MSEHasTimestamp,            // 32
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte7,            // 23
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSERemovedForces,           // 156
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEGuidByte2,               // 18
    MSETransportGuidByte7,      // 63
    MSETransportPositionX,      // 64
    MSETransportPositionY,      // 68
    MSETransportOrientation,    // 76
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte1,      // 57
    MSETransportSeat,           // 80
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportTime,           // 84
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte5,      // 61
    MSETransportVehicleId,      // 96
    MSETransportGuidByte2,      // 58
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEPitch,                   // 112
    MSECounter,                 // 168
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSEEnd
};

MovementStatusElements const MovementForceFlightSpeedChangeAck[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEMovementCounter,         // 176
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEExtraElement,            // 184
    MSEHasTimestamp,            // 32
    MSEHasCounter,              // 168
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasTransportData,        // 104
    MSEHasOrientation,          // 48
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte3,            // 19
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte2,            // 18
    MSEHasSplineElevation,      // 144
    MSEHasSpline,               // 148
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte1,            // 17
    MSEHasRemoteTimeValid,      // 172
    MSEHasFallData,             // 140
    MSEHasMovementFlags,        // 24
    MSEHasPitch,                // 112
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte6,   // 62
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136

    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSERemovedForces,           // 156
    MSEGuidByte6,               // 22
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSETransportOrientation,    // 76
    MSETransportTime,           // 84
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte7,      // 63
    MSETransportPositionY,      // 68
    MSETransportPositionZ,      // 72
    MSETransportVehicleId,      // 96
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte6,      // 62
    MSETransportSeat,           // 80
    MSETransportPositionX,      // 64
    MSETransportPrevTime,       // 88
    MSETransportGuidByte2,      // 58
    MSESplineElevation,         // 144
    MSEPitch,                   // 112
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETimestamp,               // 32
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementForcePitchRateSpeedChangeAck[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEExtraElement,            // 184
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEMovementCounter,         // 176
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte3,            // 19
    MSEHasPitch,                // 112
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte1,            // 17
    MSEHasFallData,             // 140
    MSEHasTimestamp,            // 32
    MSEHasCounter,              // 168
    MSEHasGuidByte2,            // 18
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte7,            // 23
    MSEHasOrientation,          // 48
    MSEHasGuidByte6,            // 22
    MSEHasHeightChangeFailed,   // 149
    MSEHasMovementFlags,        // 24
    MSEHasRemoteTimeValid,      // 172
    MSEHasSplineElevation,      // 144
    MSEHasTransportData,        // 104
    MSEHasSpline,               // 148
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte7,   // 63
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEGuidByte3,               // 19
    MSEGuidByte6,               // 22
    MSEGuidByte2,               // 18
    MSEGuidByte4,               // 20
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEGuidByte7,               // 23
    MSETransportVehicleId,      // 96
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte4,      // 60
    MSETransportPositionX,      // 64
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSETransportGuidByte3,      // 59
    MSETransportPositionY,      // 68
    MSETransportGuidByte0,      // 56
    MSETransportPrevTime,       // 88
    MSETransportSeat,           // 80
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte7,      // 63
    MSETransportPositionZ,      // 72
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEEnd
};

MovementStatusElements const MovementSetCanFlyAck[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEMovementCounter,         // 176
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasSpline,               // 148
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte3,            // 19
    MSEHasFallData,             // 140
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte0,            // 16
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte1,            // 17
    MSEHasRemoteTimeValid,      // 172
    MSEHasCounter,              // 168
    MSEHasSplineElevation,      // 144 90h
    MSEHasOrientation,          // 48  30h
    MSEHasGuidByte6,            // 22
    MSEHasTransportData,        // 104
    MSEHasTimestamp,            // 32
    MSEHasPitch,                // 112 70h
    MSEHasMovementFlags,        // 24
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte2,            // 18
    MSEMovementFlags2,          // 28
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136

    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSERemovedForces,           // 156
    MSEGuidByte3,               // 19
    MSEGuidByte2,               // 18
    MSEGuidByte7,               // 23
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSETransportPrevTime,       // 88
    MSETransportPositionX,      // 64
    MSETransportVehicleId,      // 96
    MSETransportOrientation,    // 76
    MSETransportGuidByte3,      // 59
    MSETransportSeat,           // 80
    MSETransportGuidByte5,      // 61
    MSETransportPositionZ,      // 72
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte4,      // 60
    MSETransportTime,           // 84
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSEOrientation,             // 48  30h
    MSETimestamp,               // 32
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSESplineElevation,         // 144 90h
    MSEPitch,                   // 112 70h
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementForceSwimBackSpeedChangeAck[] = // 5.4.8 18414
{
    MSEExtraElement,            // 184
    MSEPositionY,               // 40
    MSEMovementCounter,         // 176
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasGuidByte5,            // 21
    MSERemovedForcesCount,      // 152
    MSEHasTransportData,        // 104
    MSEHasCounter,              // 168
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte7,            // 23
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte4,            // 20
    MSEHasOrientation,          // 48
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte2,            // 18
    MSEHasMovementFlags2,       // 28
    MSEHasSpline,               // 148
    MSEHasGuidByte0,            // 16
    MSEHasTimestamp,            // 32
    MSEHasGuidByte3,            // 19
    MSEHasFallData,             // 140
    MSEHasRemoteTimeValid,      // 172
    MSEHasPitch,                // 112
    MSEHasGuidByte1,            // 17
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte4,   // 60
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSERemovedForces,           // 156
    MSEGuidByte5,               // 21
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte3,               // 19
    MSEGuidByte2,               // 18
    MSEGuidByte1,               // 17
    MSEGuidByte7,               // 23
    MSETransportGuidByte7,      // 63
    MSETransportPositionY,      // 68
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte0,      // 56
    MSETransportOrientation,    // 76
    MSETransportVehicleId,      // 96
    MSETransportPositionX,      // 64
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportSeat,           // 80
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSECounter,                 // 168
    MSESplineElevation,         // 144
    MSEFallVerticalSpeed,       // 120
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSETimestamp,               // 32
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementForceSwimSpeedChangeAck[] = // 5.4.8 18414
{
    MSEExtraElement,            // 184
    MSEPositionY,               // 40
    MSEMovementCounter,         // 176
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasGuidByte4,            // 20
    MSEHasHeightChangeFailed,   // 149
    MSEHasSplineElevation,      // 144 90h
    MSEHasGuidByte2,            // 18
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte3,            // 19
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte0,            // 16
    MSEHasPitch,                // 112 70h
    MSEHasCounter,              // 168
    MSEHasOrientation,          // 48  30h
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte1,            // 17
    MSEHasFallData,             // 140
    MSERemovedForcesCount,      // 152
    MSEHasTimestamp,            // 32
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte6,            // 22
    MSEHasTransportData,        // 104
    MSEHasSpline,               // 148
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEMovementFlags,           // 24

    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte7,               // 23
    MSEGuidByte2,               // 18
    MSETransportGuidByte7,      // 63
    MSETransportPrevTime,       // 88
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportGuidByte4,      // 60
    MSETransportPositionY,      // 68
    MSETransportPositionZ,      // 72
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte2,      // 58
    MSETransportOrientation,    // 76
    MSETransportTime,           // 84
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportPositionX,      // 64
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSETimestamp,               // 32
    MSESplineElevation,         // 144 90h
    MSECounter,                 // 168
    MSEPitch,                   // 112 70h
    MSEOrientation,             // 48  30h
    MSEEnd
};

MovementStatusElements const MovementForceTurnRateSpeedChangeAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEPositionZ,               // 44
    MSEExtraElement,            // 184
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasOrientation,          // 48
    MSEHasHeightChangeFailed,   // 149
    MSEHasPitch,                // 112
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte7,            // 23
    MSEHasSpline,               // 148
    MSEHasTimestamp,            // 32
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte2,            // 18
    MSEHasCounter,              // 168
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags2,       // 28
    MSEHasSplineElevation,      // 144
    MSEHasTransportData,        // 104
    MSEHasGuidByte6,            // 22
    MSEHasMovementFlags,        // 24
    MSERemovedForcesCount,      // 152
    MSEHasFallData,             // 140
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte4,            // 20
    MSEMovementFlags2,          // 28
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportPrevTime,    // 92
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte2,               // 18
    MSEGuidByte5,               // 21
    MSEGuidByte7,               // 23
    MSEGuidByte4,               // 20
    MSERemovedForces,           // 156
    MSEGuidByte6,               // 22
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte0,               // 16
    MSECounter,                 // 168
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSETransportGuidByte0,      // 56
    MSETransportPositionX,      // 64
    MSETransportOrientation,    // 76
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte4,      // 60
    MSETransportPositionY,      // 68
    MSETransportVehicleId,      // 96
    MSETransportGuidByte7,      // 63
    MSETransportPositionZ,      // 72
    MSETransportPrevTime,       // 88
    MSETransportGuidByte2,      // 58
    MSETransportSeat,           // 80
    MSEOrientation,             // 48
    MSEPitch,                   // 112
    MSESplineElevation,         // 144
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MovementForceWalkSpeedChangeAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEExtraElement,            // 184
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasSpline,               // 148
    MSEHasGuidByte0,            // 16
    MSEHasCounter,              // 168
    MSEHasOrientation,          // 48
    MSEHasSplineElevation,      // 144
    MSEHasPitch,                // 112
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte3,            // 19
    MSERemovedForcesCount,      // 152
    MSEHasFallData,             // 140
    MSEHasMovementFlags2,       // 28
    MSEHasTimestamp,            // 32
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte4,            // 20
    MSEHasRemoteTimeValid,      // 172
    MSEHasHeightChangeFailed,   // 149
    MSEHasTransportData,        // 104
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte6,            // 22
    MSEMovementFlags2,          // 28
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136

    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte7,               // 23
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSESplineElevation,         // 144
    MSETransportGuidByte5,      // 61
    MSETransportPositionX,      // 64
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportSeat,           // 80
    MSETransportPositionZ,      // 72
    MSETransportGuidByte1,      // 57
    MSETransportTime,           // 84
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSETransportPositionY,      // 68
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportVehicleId,      // 96
    MSETimestamp,               // 32
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementForceRunBackSpeedChangeAck[] = // 5.4.8 18414
{
    MSEExtraElement,            // 184
    MSEMovementCounter,         // 176
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte7,            // 23
    MSEHasSpline,               // 148
    MSEHasGuidByte1,            // 17
    MSERemovedForcesCount,      // 152
    MSEHasSplineElevation,      // 144
    MSEHasMovementFlags,        // 24
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte4,            // 20
    MSEHasRemoteTimeValid,      // 172
    MSEHasOrientation,          // 48
    MSEHasFallData,             // 140
    MSEHasPitch,                // 112
    MSEHasCounter,              // 168
    MSEHasTimestamp,            // 32
    MSEHasTransportData,        // 104
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte0,            // 16
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136

    MSEGuidByte4,               // 20
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte0,               // 16
    MSEGuidByte7,               // 23
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte6,               // 22
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportSeat,           // 80
    MSETransportGuidByte7,      // 63
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSETransportGuidByte0,      // 56
    MSETransportPositionX,      // 64
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte4,      // 60
    MSETransportPositionY,      // 68
    MSETransportGuidByte1,      // 57
    MSETransportVehicleId,      // 96
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEPitch,                   // 112
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEOrientation,             // 48
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MovementUpdateRunBackSpeed[] = // 5.4.8 18414
{
    MSEPositionZ,               // 52  34h
    MSEPositionY,               // 48  30h
    MSEPositionX,               // 44  2ch
    MSEExtraElement,            // 16  10h
    MSEHasMovementFlags2,       // 36
    MSEHasGuidByte2,            // 26
    MSEHasMovementFlags,        // 32
    MSEMovementFlags,           // 32
    MSEHasTransportData,        // 112
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte2,   // 66
    MSEHasTimestamp,            // 40
    MSEHasGuidByte3,            // 27
    MSEHasSpline,               // 156
    MSEHasGuidByte1,            // 25
    MSEHasGuidByte4,            // 28
    MSEHasGuidByte0,            // 24
    MSEHasOrientation,          // 56  38h
    MSEHasPitch,                // 120 78h
    MSERemovedForcesCount,      // 160
    MSEMovementFlags2,          // 36
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte7,            // 31
    MSEHasRemoteTimeValid,      // 180
    MSEHasSplineElevation,      // 152 98h
    MSEHasFallData,             // 148
    MSEHasFallDirection,        // 144
    MSEHasCounter,              // 176
    MSEHasGuidByte6,            // 30
    MSEHasGuidByte5,            // 29
    MSEGuidByte5,               // 29
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte6,      // 70
    MSETransportVehicleId,      // 104
    MSETransportGuidByte2,      // 66
    MSETransportPositionZ,      // 80  50h
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte7,      // 71
    MSETransportOrientation,    // 84  54h
    MSETransportPrevTime,       // 96
    MSETransportTime,           // 92
    MSETransportPositionY,      // 76  4ch
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte0,      // 64
    MSETransportSeat,           // 88
    MSETransportGuidByte3,      // 67
    MSETransportPositionX,      // 72  48h
    MSESplineElevation,         // 152 98h
    MSEGuidByte2,               // 26
    MSETimestamp,               // 40
    MSERemovedForces,           // 164
    MSEGuidByte1,               // 25
    MSEGuidByte3,               // 27
    MSEFallSinAngle,            // 136 88h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallCosAngle,            // 132 84h
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallTime,                // 124
    MSEGuidByte6,               // 30
    MSEPitch,                   // 120 78h
    MSEGuidByte4,               // 28
    MSEGuidByte7,               // 31
    MSECounter,                 // 176
    MSEGuidByte0,               // 24
    MSEOrientation,             // 56  38h
    MSEEnd
};

MovementStatusElements const MovementUpdatePitchRate[] = // 5.4.8 18414
{
    MSEHasGuidByte7,            // 23
    MSEHasMovementFlags,        // 24
    MSEHasSplineElevation,      // 144
    MSEHasGuidByte1,            // 17
    MSEHasSpline,               // 148
    MSEHasGuidByte0,            // 16
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte4,            // 20
    MSEMovementFlags2,          // 28
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte3,            // 19
    MSEHasOrientation,          // 48
    MSEHasGuidByte2,            // 18
    MSEHasRemoteTimeValid,      // 172
    MSEHasPitch,                // 112
    MSEHasFallData,             // 140
    MSEMovementFlags,           // 24
    MSEHasTransportData,        // 104
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportPrevTime,    // 92
    MSEHasFallDirection,        // 136
    MSEHasCounter,              // 168
    MSEHasTimestamp,            // 32
    MSEHasGuidByte5,            // 21
    MSETransportGuidByte3,      // 59
    MSETransportPositionZ,      // 72
    MSETransportVehicleId,      // 96
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportTime,           // 84
    MSETransportPositionY,      // 68
    MSETransportOrientation,    // 76
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte5,      // 61
    MSETransportSeat,           // 80
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte7,      // 63
    MSEGuidByte0,               // 16
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSEGuidByte7,               // 23
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEOrientation,             // 48
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSECounter,                 // 168
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSEGuidByte2,               // 18
    MSEExtraElement,            // 176
    MSEPositionZ,               // 44
    MSEGuidByte5,               // 21
    MSEPitch,                   // 112
    MSEEnd
};

MovementStatusElements const MovementUpdateWalkSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte4,            // 28
    MSEHasGuidByte0,            // 24
    MSEHasSplineElevation,      // 152 98h
    MSEHasCounter,              // 176
    MSEHasMovementFlags,        // 32
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte5,            // 29
    MSEHasMovementFlags2,       // 36
    MSEHasGuidByte6,            // 30
    MSEHasSpline,               // 156
    MSEHasGuidByte3,            // 27
    MSERemovedForcesCount,      // 160
    MSEHasPitch,                // 120 78h
    MSEHasRemoteTimeValid,      // 180
    MSEHasGuidByte1,            // 25
    MSEMovementFlags,           // 32
    MSEHasGuidByte2,            // 26
    MSEMovementFlags2,          // 36
    MSEHasFallData,             // 148
    MSEHasTimestamp,            // 40
    MSEHasTransportData,        // 112
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte6,   // 70
    MSEHasFallDirection,        // 144
    MSEHasOrientation,          // 56  38h
    MSEHasHeightChangeFailed,   // 157
    MSEPitch,                   // 120 78h
    MSEExtraElement,            // 16  10h
    MSETransportGuidByte3,      // 67
    MSETransportPrevTime,       // 96
    MSETransportPositionY,      // 76  4ch
    MSETransportGuidByte1,      // 65
    MSETransportPositionX,      // 72  48h
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte0,      // 64
    MSETransportTime,           // 92
    MSETransportSeat,           // 88
    MSETransportGuidByte2,      // 66
    MSETransportVehicleId,      // 104
    MSETransportOrientation,    // 84  54h
    MSETransportGuidByte7,      // 71
    MSETransportPositionZ,      // 80  50h
    MSEGuidByte2,               // 26
    MSESplineElevation,         // 152 98h
    MSEGuidByte1,               // 25
    MSETimestamp,               // 40
    MSEGuidByte4,               // 28
    MSEPositionY,               // 48  30h
    MSEFallTime,                // 124
    MSEFallVerticalSpeed,       // 128 80h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallCosAngle,            // 132 84h
    MSEFallSinAngle,            // 136 88h
    MSEGuidByte5,               // 29
    MSEPositionZ,               // 52  34h
    MSEGuidByte7,               // 31
    MSEPositionX,               // 44  2ch
    MSECounter,                 // 176
    MSERemovedForces,           // 164
    MSEGuidByte3,               // 27
    MSEGuidByte6,               // 30
    MSEOrientation,             // 56  38h
    MSEGuidByte0,               // 24
    MSEEnd
};

MovementStatusElements const ForceMoveRootAck[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEMovementCounter,         // 176
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEHasHeightChangeFailed,   // 149
    MSEHasTimestamp,            // 32
    MSEHasSplineElevation,      // 144 90h
    MSEHasSpline,               // 148
    MSEHasGuidByte3,            // 19
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte4,            // 20
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte6,            // 22
    MSEHasCounter,              // 168
    MSEHasPitch,                // 112 70h
    MSEHasFallData,             // 140
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte7,            // 23
    MSEHasTransportData,        // 104
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48  30h
    MSEHasGuidByte5,            // 21
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28

    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte5,               // 21
    MSEGuidByte7,               // 23
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEGuidByte6,               // 22
    MSERemovedForces,           // 156
    MSECounter,                 // 168
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte2,      // 58
    MSETransportPositionX,      // 64
    MSETransportGuidByte6,      // 62
    MSETransportPositionY,      // 68
    MSETransportPrevTime,       // 88
    MSETransportTime,           // 84
    MSETransportPositionZ,      // 72
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSETransportGuidByte1,      // 57
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportGuidByte4,      // 60
    MSEPitch,                   // 112 70h
    MSETimestamp,               // 32
    MSEOrientation,             // 48  30h
    MSESplineElevation,         // 144 90h
    MSEEnd
};

MovementStatusElements const ForceMoveUnrootAck[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEMovementCounter,         // 176
    MSEPositionZ,               // 44
    MSEHasGuidByte0,            // 16
    MSEHasPitch,                // 112 70h
    MSEHasFallData,             // 140
    MSEHasSplineElevation,      // 144 90h
    MSEHasSpline,               // 148
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte3,            // 19
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte5,            // 21
    MSEHasHeightChangeFailed,   // 149
    MSEHasTimestamp,            // 32
    MSEHasCounter,              // 168
    MSEHasMovementFlags,        // 24
    MSEHasTransportData,        // 104
    MSEHasGuidByte2,            // 18
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags2,       // 28
    MSEHasOrientation,          // 48  30h
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte3,   // 59
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24

    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte6,               // 22
    MSEGuidByte2,               // 18
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSECounter,                 // 168
    MSETransportSeat,           // 80
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte4,      // 60
    MSETransportTime,           // 84
    MSETransportGuidByte1,      // 57
    MSETransportPrevTime,       // 88
    MSETransportPositionZ,      // 72
    MSETransportVehicleId,      // 96
    MSETransportGuidByte5,      // 61
    MSETransportPositionY,      // 68
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETimestamp,               // 32
    MSESplineElevation,         // 144 90h
    MSEOrientation,             // 48  30h
    MSEPitch,                   // 112 70h
    MSEEnd
};

MovementStatusElements const MovementFallReset[] = // 5.4.8 18414
{
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEHasGuidByte7,            // 23
    MSEHasTransportData,        // 104
    MSEHasGuidByte6,            // 22
    MSEHasPitch,                // 112 70h
    MSEHasSplineElevation,      // 144 90h
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte2,            // 18
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte0,            // 16
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte1,            // 17
    MSEHasFallData,             // 140
    MSEHasTimestamp,            // 32
    MSEHasSpline,               // 148
    MSEHasHeightChangeFailed,   // 149
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte5,            // 21
    MSEHasRemoteTimeValid,      // 172
    MSEHasOrientation,          // 48  30h
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte5,   // 61
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24

    MSERemovedForces,           // 156
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte5,               // 21
    MSEGuidByte7,               // 23
    MSETransportGuidByte0,      // 56
    MSETransportPrevTime,       // 88
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSETransportPositionZ,      // 72
    MSETransportOrientation,    // 76
    MSETransportPositionX,      // 64
    MSETransportPositionY,      // 68
    MSETransportGuidByte3,      // 59
    MSETransportVehicleId,      // 96
    MSETransportTime,           // 84
    MSETransportGuidByte5,      // 61
    MSETransportSeat,           // 80
    MSEOrientation,             // 48  30h
    MSEFallTime,                // 116
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSESplineElevation,         // 144 90h
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSEPitch,                   // 112 70h
    MSEEnd
};

MovementStatusElements const MovementFallAck[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEMovementCounter,         // 176
    MSEHasSplineElevation,      // 144 90h
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte4,            // 20
    MSEHasSpline,               // 148
    MSEHasMovementFlags,        // 24
    MSEHasFallData,             // 140
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte7,            // 23
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte6,            // 22
    MSEHasTransportData,        // 104
    MSEHasGuidByte3,            // 19
    MSEHasHeightChangeFailed,   // 149
    MSEHasPitch,                // 112 70h
    MSEHasTimestamp,            // 32
    MSERemovedForcesCount,      // 152
    MSEHasCounter,              // 168
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48  30h
    MSEHasGuidByte2,            // 18
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportVehicleId,   // 100
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSEHasFallDirection,        // 136

    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte7,               // 23
    MSEGuidByte2,               // 18
    MSERemovedForces,           // 156
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSETransportPositionX,      // 64
    MSETransportPositionY,      // 68
    MSETransportVehicleId,      // 96
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte1,      // 57
    MSETransportTime,           // 84
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportPositionZ,      // 72
    MSETransportSeat,           // 80
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportGuidByte6,      // 62
    MSESplineElevation,         // 144 90h
    MSEPitch,                   // 112 70h
    MSEOrientation,             // 48  30h
    MSEEnd
};

MovementStatusElements const MovementGravityDisableAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEPositionX,               // 36
    MSEHasGuidByte0,            // 16
    MSEHasTransportData,        // 104
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte1,            // 17
    MSEHasSpline,               // 148
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte2,            // 18
    MSEHasOrientation,          // 48  30h
    MSEHasFallData,             // 140
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte3,            // 19
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte6,            // 22
    MSEHasSplineElevation,      // 144 90h
    MSEHasGuidByte4,            // 20
    MSEHasPitch,                // 112 70h
    MSERemovedForcesCount,      // 152
    MSEHasCounter,              // 168
    MSEHasMovementFlags,        // 24
    MSEHasTimestamp,            // 32
    MSEHasFallDirection,        // 136
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte7,   // 63
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28

    MSEGuidByte7,               // 23
    MSEGuidByte2,               // 18
    MSERemovedForces,           // 156
    MSEGuidByte6,               // 22
    MSEGuidByte3,               // 19
    MSEGuidByte0,               // 16
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSETransportGuidByte1,      // 57
    MSETransportPositionX,      // 64
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportSeat,           // 80
    MSETransportOrientation,    // 76
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte0,      // 56
    MSETransportPositionY,      // 68
    MSETransportPositionZ,      // 72
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportVehicleId,      // 96
    MSETransportGuidByte3,      // 59
    MSEPitch,                   // 112 70h
    MSEFallTime,                // 116
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEOrientation,             // 48  30h
    MSETimestamp,               // 32
    MSESplineElevation,         // 144 90h
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementGravityEnableAck[] = // 5.4.8 18414
{
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEMovementCounter,         // 176
    MSEHasOrientation,          // 48
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte3,            // 19
    MSEHasCounter,              // 168
    MSEHasMovementFlags2,       // 28
    MSEHasTransportData,        // 104
    MSEHasHeightChangeFailed,   // 149
    MSEHasMovementFlags,        // 24
    MSEHasTimestamp,            // 32
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasRemoteTimeValid,      // 172
    MSEHasSplineElevation,      // 144
    MSEHasPitch,                // 112
    MSERemovedForcesCount,      // 152
    MSEHasFallData,             // 140
    MSEHasGuidByte2,            // 18
    MSEHasSpline,               // 148
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24

    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte0,               // 16
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSEGuidByte5,               // 21
    MSETransportPrevTime,       // 88
    MSETransportGuidByte5,      // 61
    MSETransportPositionX,      // 64
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte6,      // 62
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte7,      // 63
    MSETransportSeat,           // 80
    MSETransportPositionY,      // 68
    MSETransportGuidByte3,      // 59
    MSETransportVehicleId,      // 96
    MSETransportPositionZ,      // 72
    MSEPitch,                   // 112
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSETimestamp,               // 32
    MSESplineElevation,         // 144
    MSECounter,                 // 168
    MSEOrientation,             // 48
    MSEEnd
};

MovementStatusElements const MovementHoverAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEPositionY,               // 40
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte2,            // 18
    MSEHasTransportData,        // 104
    MSEHasSplineElevation,      // 144 90h
    MSEHasCounter,              // 168
    MSEHasTimestamp,            // 32
    MSEHasPitch,                // 112 70h
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte3,            // 19
    MSEHasSpline,               // 148
    MSEHasGuidByte7,            // 23
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte0,            // 16
    MSEHasHeightChangeFailed,   // 149
    MSEHasRemoteTimeValid,      // 172
    MSEHasMovementFlags,        // 24
    MSEHasOrientation,          // 48  30h
    MSEHasFallData,             // 140
    MSEHasGuidByte1,            // 17
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasFallDirection,        // 136
    MSEMovementFlags,           // 24
    MSEMovementFlags2,          // 28
    MSERemovedForces,           // 156
    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte5,               // 21
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSEGuidByte0,               // 16
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte4,      // 60
    MSETransportPositionZ,      // 72
    MSETransportPositionX,      // 64
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte1,      // 57
    MSETransportTime,           // 84
    MSETransportOrientation,    // 76
    MSETransportPositionY,      // 68
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte6,      // 62
    MSETransportPrevTime,       // 88
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportGuidByte3,      // 59
    MSESplineElevation,         // 144 90h
    MSEPitch,                   // 112 70h
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSEOrientation,             // 48  30h
    MSEEnd
};

MovementStatusElements const MovementKnockBackAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEHasGuidByte5,            // 21
    MSEHasCounter,              // 168
    MSEHasTimestamp,            // 32
    MSEHasMovementFlags2,       // 28
    MSEHasRemoteTimeValid,      // 172
    MSEHasTransportData,        // 104
    MSEHasSplineElevation,      // 144 90h
    MSEHasGuidByte3,            // 19
    MSEHasPitch,                // 112 70h
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte1,            // 17
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte0,            // 16
    MSEHasOrientation,          // 48  30h
    MSEHasGuidByte2,            // 18
    MSEHasHeightChangeFailed,   // 149
    MSEHasFallData,             // 140
    MSEHasSpline,               // 148
    MSEMovementFlags,           // 24
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte7,   // 63
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28

    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSEGuidByte0,               // 16
    MSEGuidByte2,               // 18
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSERemovedForces,           // 156
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSETransportVehicleId,      // 96
    MSETransportPrevTime,       // 88
    MSETransportTime,           // 84
    MSETransportPositionX,      // 64
    MSETransportPositionY,      // 68
    MSETransportGuidByte7,      // 63
    MSETransportSeat,           // 80
    MSETransportGuidByte1,      // 57
    MSETransportPositionZ,      // 72
    MSETransportOrientation,    // 76
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte5,      // 61
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallCosAngle,            // 124
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSESplineElevation,         // 144 90h
    MSEPitch,                   // 112 70h
    MSETimestamp,               // 32
    MSEOrientation,             // 48  30h
    MSECounter,                 // 168
    MSEEnd
};

MovementStatusElements const MovementWaterWalkAck[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEMovementCounter,         // 176
    MSEPositionZ,               // 44
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte3,            // 19
    MSEHasFallData,             // 140
    MSEHasGuidByte2,            // 18
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte0,            // 16
    MSEHasPitch,                // 112 70h
    MSEHasHeightChangeFailed,   // 149
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte6,            // 22
    MSEHasTimestamp,            // 32
    MSEHasSpline,               // 148
    MSEHasSplineElevation,      // 144 90h
    MSEHasTransportData,        // 104
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte4,            // 20
    MSEHasCounter,              // 168
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte5,            // 21
    MSEHasOrientation,          // 48  30h
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte6,   // 62
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28

    MSEGuidByte7,               // 23
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte1,               // 17
    MSEGuidByte6,               // 22
    MSEGuidByte2,               // 18
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportTime,           // 84
    MSETransportPositionY,      // 68
    MSETransportGuidByte4,      // 60
    MSETransportPositionX,      // 64
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte3,      // 59
    MSETransportOrientation,    // 76
    MSETransportGuidByte2,      // 58
    MSETransportSeat,           // 80
    MSETransportVehicleId,      // 96
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte0,      // 56
    MSETransportPositionZ,      // 72
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSECounter,                 // 168
    MSEOrientation,             // 48  30h
    MSEPitch,                   // 112 70h
    MSETimestamp,               // 32
    MSESplineElevation,         // 144 90h
    MSEEnd
};

MovementStatusElements const MovementUpdateKnockBack[] = // 5.4.8 18414
{
    MSEHasGuidByte5,            // 21
    MSEHasSplineElevation,      // 144 90h
    MSEHasTimestamp,            // 32
    MSERemovedForcesCount,      // 152
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte0,            // 16
    MSEHasHeightChangeFailed,   // 149
    MSEHasOrientation,          // 48  30h
    MSEHasSpline,               // 148
    MSEHasTransportData,        // 104
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportVehicleId,   // 100
    MSEHasGuidByte3,            // 19
    MSEHasFallData,             // 140
    MSEHasCounter,              // 168
    MSEHasFallDirection,        // 136
    MSEHasGuidByte7,            // 23
    MSEHasRemoteTimeValid,      // 172
    MSEHasPitch,                // 112 70h
    MSEMovementFlags2,          // 28
    MSEHasMovementFlags,        // 24
    MSEMovementFlags,           // 24

    MSEGuidByte1,               // 17
    MSETransportGuidByte5,      // 61
    MSETransportVehicleId,      // 96
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte4,      // 60
    MSETransportPositionZ,      // 72
    MSETransportGuidByte7,      // 63
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte2,      // 58
    MSETransportPositionY,      // 68
    MSETransportGuidByte0,      // 56
    MSETransportSeat,           // 80
    MSETransportOrientation,    // 76
    MSETransportPositionX,      // 64
    MSETransportPrevTime,       // 88
    MSETransportTime,           // 84
    MSEGuidByte2,               // 18
    MSESplineElevation,         // 144 90h
    MSEFallSinAngle,            // 128
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSEPositionY,               // 40
    MSEOrientation,             // 48  30h
    MSERemovedForces,           // 156
    MSEGuidByte7,               // 23
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEPositionZ,               // 44
    MSECounter,                 // 168
    MSEGuidByte3,               // 19
    MSEGuidByte0,               // 16
    MSEPositionX,               // 36
    MSEPitch,                   // 112 70h
    MSEGuidByte5,               // 21
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const SplineMoveSetWalkSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte6,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEExtraElement,
    MSEEnd
};

MovementStatusElements const SplineMoveSetRunSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte2,
    MSEGuidByte4,
    MSEExtraElement,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEGuidByte7,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveSetRunBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte1,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte7,
    MSEExtraElement,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveSetSwimSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEExtraElement,
    MSEGuidByte5,
    MSEGuidByte0,
    MSEGuidByte2,
    MSEEnd
};

MovementStatusElements const SplineMoveSetSwimBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEGuidByte7,
    MSEGuidByte6,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEExtraElement,
    MSEEnd
};

MovementStatusElements const SplineMoveSetTurnRate[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEExtraElement,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const SplineMoveSetFlightSpeed[] = // 5.4.8 18414
{
    MSEExtraElement,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEGuidByte5,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const SplineMoveSetFlightBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte5,
    MSEHasGuidByte4,
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEGuidByte7,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEExtraElement,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte0,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const SplineMoveSetPitchRate[] = // 5.4.8 18414
{
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte5,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEGuidByte5,
    MSEExtraElement,
    MSEGuidByte4,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte6,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const MoveSetWalkSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEMovementCounter,
    MSEGuidByte4,
    MSEExtraElement,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const MoveSetRunSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEGuidByte1,
    MSEMovementCounter,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEGuidByte0,
    MSEExtraElement,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const MoveSetRunBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEMovementCounter,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte7,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEExtraElement,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const MoveSetSwimSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEMovementCounter,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEExtraElement,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEEnd
};

MovementStatusElements const MoveSetSwimBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEHasGuidByte7,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEMovementCounter,
    MSEExtraElement,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const MoveSetTurnRate[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEExtraElement,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEMovementCounter,
    MSEGuidByte1,
    MSEEnd
};

MovementStatusElements const MoveSetFlightSpeed[] = // 5.4.8 18414
{
    MSEExtraElement,
    MSEMovementCounter,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte1,
    MSEEnd
};

MovementStatusElements const MoveSetFlightBackSpeed[] = // 5.4.8 18414
{
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte2,
    MSEMovementCounter,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEExtraElement,
    MSEEnd
};

MovementStatusElements const MoveSetPitchRate[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte5,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEGuidByte4,
    MSEMovementCounter,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte1,
    MSEExtraElement,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveSetWalkMode[] = // 5.4.8 18414
{
    MSEHasGuidByte4,
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEGuidByte1,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const SplineMoveSetRunMode[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEGuidByte5,
    MSEGuidByte1,
    MSEGuidByte4,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEEnd
};

MovementStatusElements const SplineMoveGravityDisable[] = // 5.4.8 18414
{
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEHasGuidByte3,
    MSEGuidByte3,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const SplineMoveCollisionDisable[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEGuidByte7,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEGuidByte5,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const SplineMoveCollisionEnable[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte6,
    MSEHasGuidByte2,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEGuidByte4,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveGravityEnable[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEGuidByte6,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveSetHover[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte6,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEEnd
};

MovementStatusElements const SplineMoveUnsetHover[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte6,
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEGuidByte4,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveStartSwim[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEGuidByte4,
    MSEGuidByte0,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const SplineMoveStopSwim[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEGuidByte0,
    MSEGuidByte5,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEEnd
};

MovementStatusElements const SplineMoveSetFlying[] = // 5.4.8 18414
{
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEEnd
};

MovementStatusElements const SplineMoveUnsetFlying[] = // 5.4.8 18414
{
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const SplineMoveSetWaterWalk[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEGuidByte4,
    MSEGuidByte3,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveSetLandWalk[] = // 5.4.8 18414
{
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte3,
    MSEHasGuidByte4,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEGuidByte3,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const SplineMoveSetFeatherFall[] = // 5.4.8 18414
{
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const SplineMoveSetNormalFall[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEGuidByte7,
    MSEGuidByte5,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const SplineMoveRoot[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const SplineMoveUnroot[] = // 5.4.8 18414
{
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEGuidByte2,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const MoveSetCanTransitionBetweenSwimAndFly[] = // 5.4.8 18414
{
    MSEMovementCounter,
    MSEHasGuidByte4,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const MoveUnSetCanTransitionBetweenSwimAndFly[] = // 5.4.8 18414
{
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEGuidByte7,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEMovementCounter,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const MoveSetCanFly[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEGuidByte4,
    MSEGuidByte2,
    MSEMovementCounter,
    MSEGuidByte6,
    MSEGuidByte3,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const MoveUnsetCanFly[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte7,
    MSEMovementCounter,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const MoveSetHover[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEMovementCounter,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const MoveUnsetHover[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEMovementCounter,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const MoveWaterWalk[] = // 5.4.8 18414
{
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte5,
    MSEMovementCounter,
    MSEEnd
};

MovementStatusElements const MoveLandWalk[] = // 5.4.8 18414
{
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEGuidByte7,
    MSEGuidByte6,
    MSEGuidByte4,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte0,
    MSEGuidByte1,
    MSEMovementCounter,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const MoveFeatherFall[] = // 5.4.8 18414
{
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEMovementCounter,
    MSEGuidByte1,
    MSEGuidByte0,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const MoveNormalFall[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEMovementCounter,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const MoveRoot[] = // 5.4.8 18414
{
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte7,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte6,
    MSEGuidByte5,
    MSEMovementCounter,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEEnd
};

MovementStatusElements const MoveUnroot[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEHasGuidByte4,
    MSEHasGuidByte6,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEMovementCounter,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const ChangeSeatsOnControlledVehicle[] = // 5.4.8 18414
{
    MSEExtraElement,
    MSEPositionY,
    MSEPositionZ,
    MSEPositionX,
    MSEExtraElement,
    MSEExtraElement,
    MSEHasTimestamp,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEExtraElement,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEExtraElement,
    MSEExtraElement,
    MSEHasTransportData,
    MSEHasMovementFlags2,
    MSEExtraElement,
    MSEExtraElement,
    MSEHasGuidByte4,
    MSERemovedForcesCount,
    MSEExtraElement,
    MSEHasOrientation,
    MSEHasGuidByte5,
    MSEHasRemoteTimeValid,
    MSEHasSpline,
    MSEHasSplineElevation,
    MSEHasFallData,
    MSEHasMovementFlags,
    MSEHasCounter,
    MSEHasHeightChangeFailed,
    MSEHasGuidByte3,
    MSEHasPitch,
    MSEHasTransportGuidByte6,
    MSEHasTransportGuidByte0,
    MSEHasTransportGuidByte3,
    MSEHasTransportGuidByte7,
    MSEHasTransportPrevTime,
    MSEHasTransportVehicleId,
    MSEHasTransportGuidByte4,
    MSEHasTransportGuidByte1,
    MSEHasTransportGuidByte2,
    MSEHasTransportGuidByte5,
    MSEMovementFlags,
    MSEMovementFlags2,
    MSEHasFallDirection,
    MSEGuidByte6,
    MSEExtraElement,
    MSEGuidByte2,
    MSERemovedForces,
    MSEGuidByte5,
    MSEExtraElement,
    MSEExtraElement,
    MSEGuidByte3,
    MSEGuidByte7,
    MSEExtraElement,
    MSEExtraElement,
    MSEExtraElement,
    MSEGuidByte4,
    MSEGuidByte1,
    MSEExtraElement,
    MSEExtraElement,
    MSEGuidByte0,
    MSECounter,
    MSEFallVerticalSpeed,
    MSEFallSinAngle,
    MSEFallCosAngle,
    MSEFallHorizontalSpeed,
    MSEFallTime,
    MSETransportTime,
    MSETransportGuidByte6,
    MSETransportPositionZ,
    MSETransportGuidByte4,
    MSETransportGuidByte7,
    MSETransportOrientation,
    MSETransportGuidByte5,
    MSETransportSeat,
    MSETransportPositionX,
    MSETransportVehicleId,
    MSETransportGuidByte1,
    MSETransportGuidByte2,
    MSETransportGuidByte0,
    MSETransportPrevTime,
    MSETransportPositionY,
    MSETransportGuidByte3,
    MSESplineElevation,
    MSEOrientation,
    MSEPitch,
    MSETimestamp,
    MSEEnd
};

MovementStatusElements const AdjustSplineDurationMovement[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte0,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEGuidByte3,
    MSEExtraElement,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const MoveGravityDisable[] = // 5.4.8 18414
{
    MSEHasGuidByte6,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEMovementCounter,
    MSEGuidByte3,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEEnd
};

MovementStatusElements const MoveGravityEnable[] = // 5.4.8 18414
{
    MSEHasGuidByte3,
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEMovementCounter,
    MSEGuidByte5,
    MSEEnd
};

MovementStatusElements const MoveCollisionDisable[] =
{
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEHasGuidByte4,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEGuidByte4,
    MSEGuidByte3,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte7,
    MSEMovementCounter,
    MSEEnd
};

MovementStatusElements const MoveCollisionEnable[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte7,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEMovementCounter,
    MSEGuidByte3,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const MovementSetVehicleRecIdAck[] = // 5.4.8 18414
{
    MSEPositionY,               // 48  30h
    MSEMovementCounter,         // 184
    MSEPositionX,               // 44  2ch
    MSEPositionZ,               // 52  34h
    MSEExtraElement,            // 16  10h
    MSEHasGuidByte2,            // 26
    MSEHasMovementFlags2,       // 36
    MSEHasGuidByte3,            // 27
    MSERemovedForcesCount,      // 160
    MSEHasMovementFlags,        // 32
    MSEHasTimestamp,            // 40
    MSEHasFallData,             // 148
    MSEHasGuidByte5,            // 29
    MSEHasTransportData,        // 112
    MSEHasSpline,               // 156
    MSEHasGuidByte0,            // 24
    MSEHasGuidByte7,            // 31
    MSEHasGuidByte1,            // 25
    MSEHasCounter,              // 176
    MSEHasGuidByte4,            // 28
    MSEHasRemoteTimeValid,      // 180
    MSEHasGuidByte6,            // 30
    MSEHasSplineElevation,      // 152 98h
    MSEHasOrientation,          // 56  38h
    MSEHasHeightChangeFailed,   // 157
    MSEHasPitch,                // 120 78h
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte3,   // 67
    MSEHasTransportGuidByte2,   // 66
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte5,   // 69
    MSEMovementFlags,           // 32
    MSEMovementFlags2,          // 36
    MSEHasFallDirection,        // 144

    MSEGuidByte6,               // 30
    MSEGuidByte2,               // 26
    MSEGuidByte0,               // 24
    MSEGuidByte4,               // 28
    MSEGuidByte7,               // 31
    MSERemovedForces,           // 164
    MSEGuidByte1,               // 25
    MSEGuidByte3,               // 27
    MSEGuidByte5,               // 29
    MSETransportSeat,           // 88
    MSETransportPrevTime,       // 96
    MSETransportVehicleId,      // 104
    MSETransportGuidByte6,      // 70
    MSETransportGuidByte3,      // 67
    MSETransportPositionX,      // 72  48h
    MSETransportTime,           // 92
    MSETransportGuidByte4,      // 68
    MSETransportPositionZ,      // 80  50h
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte2,      // 66
    MSETransportGuidByte0,      // 64
    MSETransportGuidByte5,      // 69
    MSETransportGuidByte7,      // 71
    MSETransportOrientation,    // 84  54h
    MSETransportPositionY,      // 76  4ch
    MSEFallTime,                // 124
    MSEFallSinAngle,            // 136 88h
    MSEFallCosAngle,            // 132 84h
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallVerticalSpeed,       // 128 80h
    MSESplineElevation,         // 152 98h
    MSECounter,                 // 176
    MSEOrientation,             // 56  38h
    MSETimestamp,               // 40
    MSEPitch,                   // 120 78h
    MSEEnd
};

MovementStatusElements const MoveSetCanTurnWhileFalling[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte6,
    MSEHasGuidByte0,
    MSEHasGuidByte4,
    MSEHasGuidByte1,
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte3,
    MSEGuidByte6,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte3,
    MSEGuidByte0,
    MSEGuidByte4,
    MSEGuidByte7,
    MSEMovementCounter,
    MSEEnd
};

MovementStatusElements const MoveUnSetCanTurnWhileFalling[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte2,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte3,
    MSEGuidByte2,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEGuidByte4,
    MSEGuidByte5,
    MSEMovementCounter,
    MSEGuidByte1,
    MSEGuidByte6,
    MSEEnd
};

MovementStatusElements const MoveSetCanTurnWhileFallingAck[] = // 5.4.8 18414
{
    MSEPositionX,               // 36
    MSEPositionZ,               // 44
    MSEPositionY,               // 40
    MSEMovementCounter,         // 176
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte4,            // 20
    MSEHasFallData,             // 140
    MSEHasSpline,               // 148
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte5,            // 21
    MSEHasMovementFlags,        // 24
    MSEHasOrientation,          // 48
    MSEHasGuidByte6,            // 22
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte0,            // 16
    MSEHasCounter,              // 168
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte7,            // 23
    MSEHasTransportData,        // 104
    MSEHasRemoteTimeValid,      // 172
    MSEHasMovementFlags2,       // 28
    MSEHasTimestamp,            // 32
    MSEHasGuidByte2,            // 18
    MSEHasPitch,                // 112
    MSEHasSplineElevation,      // 144
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte1,   // 57
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSEGuidByte7,               // 23
    MSEGuidByte3,               // 19
    MSEGuidByte4,               // 20
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSETransportTime,           // 84
    MSETransportPositionY,      // 68
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSETransportGuidByte4,      // 60
    MSETransportPrevTime,       // 88
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte6,      // 62
    MSETransportGuidByte3,      // 59
    MSETransportVehicleId,      // 96
    MSETransportPositionZ,      // 72
    MSETransportPositionX,      // 64
    MSETransportSeat,           // 80
    MSEFallHorizontalSpeed,     // 132
    MSEFallCosAngle,            // 124
    MSEFallSinAngle,            // 128
    MSEFallVerticalSpeed,       // 120
    MSEFallTime,                // 116
    MSESplineElevation,         // 144
    MSEOrientation,             // 48
    MSECounter,                 // 168
    MSEPitch,                   // 112
    MSETimestamp,               // 32
    MSEEnd
};

MovementStatusElements const MoveApplyMovementForceAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 176
    MSEExtraElement,            // 196
    MSEExtraElement,            // 188
    MSEPositionX,               // 36
    MSEPositionY,               // 40
    MSEPositionZ,               // 44
    MSEExtraElement,            // 184
    MSEExtraElement,            // 192
    MSEHasGuidByte3,            // 19
    MSEHasSpline,               // 148
    MSEHasOrientation,          // 48  30h
    MSEHasTimestamp,            // 32
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte0,            // 16
    MSEHasMovementFlags2,       // 28
    MSEHasRemoteTimeValid,      // 172
    MSEHasCounter,              // 168
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte7,            // 23
    MSEHasSplineElevation,      // 144 90h
    MSERemovedForcesCount,      // 152
    MSEHasGuidByte6,            // 22
    MSEHasPitch,                // 112 70h
    MSEHasHeightChangeFailed,   // 149
    MSEHasFallData,             // 140
    MSEHasTransportData,        // 104
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte4,   // 60
    MSEMovementFlags2,          // 28
    MSEMovementFlags,           // 24
    MSEHasFallDirection,        // 136
    MSEGuidByte3,               // 19
    MSEGuidByte1,               // 17
    MSEGuidByte7,               // 23
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEGuidByte4,               // 20
    MSEGuidByte5,               // 21
    MSERemovedForces,           // 156
    MSEGuidByte2,               // 18
    MSETransportGuidByte0,      // 56
    MSETransportPositionX,      // 64
    MSETransportTime,           // 84
    MSETransportSeat,           // 80
    MSETransportGuidByte5,      // 61
    MSETransportPrevTime,       // 88
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte2,      // 58
    MSETransportGuidByte4,      // 60
    MSETransportGuidByte7,      // 63
    MSETransportOrientation,    // 76
    MSETransportPositionZ,      // 72
    MSETransportVehicleId,      // 96
    MSETransportPositionY,      // 68
    MSETransportGuidByte6,      // 62
    MSEPitch,                   // 112 70h
    MSEFallCosAngle,            // 124
    MSEFallHorizontalSpeed,     // 132
    MSEFallSinAngle,            // 128
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120
    MSESplineElevation,         // 144 90h
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSEOrientation,             // 48  30h
    MSEEnd
};

MovementStatusElements const MoveRemoveMovementForceAck[] = // 5.4.8 18414
{
    MSEMovementCounter,         // 184
    MSEPositionZ,               // 52  34h
    MSEExtraElement,            // 16
    MSEPositionX,               // 44  2ch
    MSEPositionY,               // 48  30h
    MSERemovedForcesCount,      // 160
    MSEHasMovementFlags,        // 32
    MSEHasSplineElevation,      // 152 98h
    MSEHasGuidByte5,            // 29
    MSEHasRemoteTimeValid,      // 180
    MSEHasGuidByte6,            // 30
    MSEHasPitch,                // 120 78h
    MSEHasGuidByte3,            // 27
    MSEHasGuidByte1,            // 25
    MSEHasTimestamp,            // 40
    MSEHasHeightChangeFailed,   // 157
    MSEHasGuidByte2,            // 26
    MSEHasFallData,             // 148
    MSEHasGuidByte7,            // 31
    MSEHasOrientation,          // 56  38h
    MSEHasTransportData,        // 112
    MSEHasGuidByte4,            // 28
    MSEHasMovementFlags2,       // 36
    MSEHasCounter,              // 176
    MSEHasSpline,               // 156
    MSEHasGuidByte0,            // 24
    MSETransportGuidByte3,      // 67
    MSEHasTransportGuidByte6,   // 70
    MSEHasTransportGuidByte1,   // 65
    MSEHasTransportGuidByte4,   // 68
    MSEHasTransportPrevTime,    // 100
    MSEHasTransportGuidByte7,   // 71
    MSEHasTransportGuidByte0,   // 64
    MSEHasTransportVehicleId,   // 108
    MSEHasTransportGuidByte5,   // 69
    MSEHasTransportGuidByte2,   // 66
    MSEHasFallDirection,        // 144
    MSEMovementFlags2,          // 36
    MSEMovementFlags,           // 32
    MSEGuidByte1,               // 25
    MSEGuidByte4,               // 28
    MSEGuidByte5,               // 29
    MSEGuidByte0,               // 24
    MSEGuidByte3,               // 27
    MSEGuidByte2,               // 26
    MSEGuidByte7,               // 31
    MSEGuidByte6,               // 30
    MSERemovedForces,           // 164
    MSEFallHorizontalSpeed,     // 140 8ch
    MSEFallSinAngle,            // 136 88h
    MSEFallCosAngle,            // 132 84h
    MSEFallTime,                // 124
    MSEFallVerticalSpeed,       // 128 80h
    MSETransportGuidByte0,      // 64
    MSETransportPositionY,      // 76  4ch
    MSETransportPrevTime,       // 96
    MSETransportOrientation,    // 84  54h
    MSETransportGuidByte7,      // 71
    MSETransportVehicleId,      // 104
    MSETransportGuidByte1,      // 65
    MSETransportGuidByte2,      // 66
    MSETransportPositionZ,      // 80  50h
    MSETransportTime,           // 92
    MSETransportGuidByte6,      // 70
    MSETransportPositionX,      // 72  48h
    MSETransportGuidByte4,      // 68
    MSETransportGuidByte5,      // 69
    MSEHasTransportGuidByte3,   // 67
    MSETransportSeat,           // 88
    MSEOrientation,             // 56  38h
    MSEPitch,                   // 120 78h
    MSETimestamp,               // 40
    MSESplineElevation,         // 152 98h
    MSECounter,                 // 176
    MSEEnd
};

MovementStatusElements const MoveApplyMovementForce[] = // 5.4.8 18414
{
    MSEHasGuidByte2,
    MSEHasGuidByte3,
    MSEExtraElement, // MSEExtra2Bits
    MSEHasGuidByte7,
    MSEHasGuidByte5,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEHasGuidByte6,
    MSEHasGuidByte4,
    MSEGuidByte6,
    MSEExtraElement, // MSEExtraFloat
    MSEGuidByte4,
    MSEExtraElement, // MSEExtraFloat
    MSEMovementCounter,
    MSEExtraElement, // MSEExtraInt32
    MSEGuidByte5,
    MSEExtraElement, // MSEExtraFloat
    MSEGuidByte0,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEExtraElement, // MSEExtraInt32
    MSEExtraElement, // MSEExtraFloat
    MSEEnd
};

MovementStatusElements const MoveRemoveMovementForce[] = // 5.4.8 18414
{
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte7,            // 23
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte3,            // 19
    MSEGuidByte4,               // 20
    MSEGuidByte7,               // 23
    MSEGuidByte0,               // 16
    MSEMovementCounter,         // 24
    MSEGuidByte1,               // 17
    MSEGuidByte3,               // 19
    MSEGuidByte5,               // 21
    MSEExtraElement,            // 28 MSEExtraInt32
    MSEGuidByte6,               // 22
    MSEGuidByte2,               // 18
    MSEEnd
};

MovementStatusElements const MoveUpdateApplyMovementForce[] = // 5.4.8 18414
{
    MSEHasMovementFlags,        // 24
    MSEHasGuidByte0,            // 16
    MSEHasMovementFlags2,       // 28
    MSEHasGuidByte7,            // 23
    MSEHasPitch,                // 112 70h
    MSEMovementFlags2,          // 28
    MSEHasTransportData,        // 104
    MSEHasFallData,             // 140
    MSEHasTransportPrevTime,    // 92
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte1,   // 57
    MSEHasGuidByte4,            // 20
    MSEHasCounter,              // 168
    MSEHasOrientation,          // 48  30h
    MSEMovementFlags,           // 24
    MSEHasRemoteTimeValid,      // 172
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte5,            // 21
    MSEHasTimestamp,            // 32
    MSEHasGuidByte3,            // 19
    MSEExtraElement,            // 8 MSEExtra2Bits
    MSEHasFallDirection,        // 136
    MSERemovedForcesCount,      // 152
    MSEHasHeightChangeFailed,   // 149
    MSEHasSplineElevation,      // 144 90h
    MSEHasGuidByte6,            // 22
    MSEHasSpline,               // 148
    MSEPitch,                   // 112 70h
    MSETransportOrientation,    // 76  4Ch
    MSETransportPositionY,      // 68  44h
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte2,      // 58
    MSETransportPositionX,      // 64  40h
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte4,      // 60
    MSETransportSeat,           // 80
    MSETransportGuidByte6,      // 62
    MSETransportVehicleId,      // 96
    MSETransportGuidByte7,      // 63
    MSETransportPrevTime,       // 88
    MSETransportTime,           // 84
    MSETransportGuidByte0,      // 56
    MSETransportPositionZ,      // 72  48h
    MSEGuidByte0,               // 16
    MSEGuidByte6,               // 22
    MSEFallCosAngle,            // 124 7Ch
    MSEFallHorizontalSpeed,     // 132 84h
    MSEFallSinAngle,            // 128 80h
    MSEFallTime,                // 116
    MSEFallVerticalSpeed,       // 120 78h
    MSEPositionY,               // 40  28h
    MSEExtraElement,            // 1Ch MSEExtraElement
    MSEGuidByte3,               // 19
    MSEExtraElement,            // 18h MSEExtraElement
    MSEGuidByte1,               // 17
    MSETimestamp,               // 32
    MSERemovedForces,           // 156
    MSEExtraElement,            // 24h MSEExtraElement
    MSEGuidByte5,               // 21
    MSEGuidByte4,               // 20
    MSEGuidByte2,               // 18
    MSEExtraElement,            // 16 MSEExtraInt32
    MSEPositionZ,               // 44  2Ch
    MSEExtraElement,            // 32 MSEExtraInt32
    MSEGuidByte7,               // 23
    MSESplineElevation,         // 144 90h
    MSEOrientation,             // 48  30h
    MSEPositionX,               // 36  24h
    MSECounter,                 // 168
    MSEExtraElement,            // 14h MSEExtraElement
    MSEEnd
};

MovementStatusElements const MoveUpdateRemoveMovementForce[] = // 5.4.8 18414
{
    MSEHasGuidByte2,            // 18
    MSEHasRemoteTimeValid,      // 172
    MSEHasCounter,              // 168
    MSEHasGuidByte3,            // 19
    MSEHasOrientation,          // 48  30h
    MSEHasTimestamp,            // 32
    MSEHasPitch,                // 112 70h
    MSEHasHeightChangeFailed,   // 149
    MSEHasGuidByte7,            // 23
    MSEHasTransportData,        // 104
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte1,            // 17
    MSEHasTransportGuidByte1,   // 57
    MSEHasTransportVehicleId,   // 100
    MSEHasTransportGuidByte4,   // 60
    MSEHasTransportGuidByte0,   // 56
    MSEHasTransportGuidByte7,   // 63
    MSEHasTransportGuidByte5,   // 61
    MSEHasTransportGuidByte2,   // 58
    MSEHasTransportGuidByte6,   // 62
    MSEHasTransportGuidByte3,   // 59
    MSEHasTransportPrevTime,    // 92
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte4,            // 20
    MSEHasFallData,             // 140
    MSEHasMovementFlags2,       // 28
    MSEHasMovementFlags,        // 24
    MSEHasSplineElevation,      // 144 90h
    MSEHasFallDirection,        // 136
    MSEMovementFlags2,          // 28
    MSEHasSpline,               // 148
    MSEHasGuidByte0,            // 16
    MSERemovedForcesCount,      // 152
    MSEMovementFlags,           // 24
    MSETransportGuidByte6,      // 62
    MSETransportOrientation,    // 76  4Ch
    MSETransportTime,           // 84
    MSETransportVehicleId,      // 96
    MSETransportGuidByte3,      // 59
    MSETransportGuidByte7,      // 63
    MSETransportPositionY,      // 68  44h
    MSETransportSeat,           // 80
    MSETransportPositionZ,      // 72  48h
    MSETransportPositionX,      // 64  40h
    MSETransportGuidByte1,      // 57
    MSETransportGuidByte0,      // 56
    MSETransportGuidByte5,      // 61
    MSETransportGuidByte2,      // 58
    MSETransportPrevTime,       // 88
    MSETransportGuidByte4,      // 60
    MSEFallHorizontalSpeed,     // 132 84h
    MSEFallCosAngle,            // 124 7Ch
    MSEFallSinAngle,            // 128 80h
    MSEFallVerticalSpeed,       // 120 78h
    MSEFallTime,                // 116
    MSEPositionX,               // 36  24h
    MSEGuidByte6,               // 22
    MSEGuidByte7,               // 23
    MSERemovedForces,           // 156
    MSEGuidByte0,               // 16
    MSEPitch,                   // 112 70h
    MSEPositionY,               // 40  28h
    MSEGuidByte4,               // 20
    MSEPositionZ,               // 44  2Ch
    MSEOrientation,             // 48  30h
    MSEExtraElement,            // 176 // MSEExtraInt32
    MSEGuidByte5,               // 21
    MSEGuidByte1,               // 17
    MSETimestamp,               // 32
    MSECounter,                 // 168
    MSEGuidByte2,               // 18
    MSEGuidByte3,               // 19
    MSESplineElevation,         // 144 90h
    MSEEnd
};

MovementStatusElements const MoveSkipTime[] = // 5.4.8 18414
{
    MSEHasGuidByte6,            // 22
    MSEHasGuidByte3,            // 19
    MSEHasGuidByte4,            // 20
    MSEHasGuidByte0,            // 16
    MSEHasGuidByte5,            // 21
    MSEHasGuidByte2,            // 18
    MSEHasGuidByte1,            // 17
    MSEHasGuidByte7,            // 23
    MSEGuidByte6,               // 22
    MSEGuidByte0,               // 16
    MSEGuidByte2,               // 18
    MSEGuidByte1,               // 17
    MSEGuidByte5,               // 21
    MSEGuidByte3,               // 19
    MSEMovementCounter,         // 24
    MSEGuidByte7,               // 23
    MSEGuidByte4,               // 20
    MSEEnd
};

MovementStatusElements const MoveSetVehicleRecID[] = // 5.4.8 18414
{
    MSEHasGuidByte0,
    MSEHasGuidByte6,
    MSEHasGuidByte1,
    MSEHasGuidByte3,
    MSEHasGuidByte7,
    MSEHasGuidByte4,
    MSEHasGuidByte5,
    MSEHasGuidByte2,
    MSEGuidByte6,
    MSEGuidByte7,
    MSEGuidByte0,
    MSEGuidByte3,
    MSEExtraElement,
    MSEMovementCounter,
    MSEGuidByte1,
    MSEGuidByte5,
    MSEGuidByte2,
    MSEGuidByte4,
    MSEEnd
};

MovementStatusElements const MoveUpdateVehicleRecID[] = // 5.4.8 18414
{
    MSEHasGuidByte5,
    MSEHasGuidByte7,
    MSEHasGuidByte2,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte0,
    MSEHasGuidByte3,
    MSEHasGuidByte6,
    MSEGuidByte5,
    MSEGuidByte7,
    MSEGuidByte4,
    MSEGuidByte6,
    MSEGuidByte2,
    MSEGuidByte1,
    MSEGuidByte3,
    MSEGuidByte0,
    MSEExtraElement,
    MSEEnd
};

MovementStatusElements const MoveKnockBack[] = // 5.4.8 18414
{
    MSEExtraElement,
    MSEExtraElement,
    MSEExtraElement,
    MSEMovementCounter,
    MSEExtraElement,
    MSEHasGuidByte2,
    MSEHasGuidByte0,
    MSEHasGuidByte7,
    MSEHasGuidByte1,
    MSEHasGuidByte4,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte3,
    MSEGuidByte6,
    MSEGuidByte0,
    MSEGuidByte7,
    MSEGuidByte5,
    MSEGuidByte4,
    MSEGuidByte3,
    MSEGuidByte1,
    MSEGuidByte2,
    MSEEnd
};

MovementStatusElements const MoveSetCollisionHeight[] = // 5.4.8 18414
{
    MSEHasGuidByte7,
    MSEHasGuidByte0,
    MSEHasGuidByte1,
    MSEExtraElement,
    MSEHasGuidByte3,
    MSEExtraElement,
    MSEHasGuidByte2,
    MSEHasGuidByte6,
    MSEHasGuidByte5,
    MSEHasGuidByte4,
    MSEExtraElement,
    MSEExtraElement,
    MSEGuidByte3,
    MSEGuidByte2,
    MSEGuidByte5,
    MSEGuidByte6,
    MSEMovementCounter,
    MSEExtraElement,
    MSEGuidByte7,
    MSEGuidByte1,
    MSEGuidByte4,
    MSEGuidByte0,
    MSEEnd
};

MovementStatusElements const MoveExtraCollisionHeight[] = // 5.4.8 18414
{
    MSEExtraExclusiveBit,
    MSEExtraBits2,
    MSEExtraFloat,
    MSEExtraExclusiveData,
    MSEExtraFloat
};

MovementStatusElements const MoveExtraUpdateCollisionHeight[] = // 5.4.8 18414
{
    MSEExtraFloat,
    MSEExtraFloat
};

MovementStatusElements const MoveExtraSpeed[] = // 5.4.8 18414
{
    MSEExtraFloat
};

MovementStatusElements const MoveExtraSetVehicleRecID[] = // 5.4.8 18414
{
    MSEExtraInt32
};

MovementStatusElements const MoveExtraKnockBack[] = // 5.4.8 18414
{
    MSEExtraFloat,
    MSEExtraFloat,
    MSEExtraFloat,
    MSEExtraFloat
};

void ExtraMovementStatusElement::ReadNextElement(ByteBuffer& packet)
{
    MovementStatusElements const element = _elements[_index++];

    switch (element)
    {
        case MSEHasGuidByte0:
        case MSEHasGuidByte1:
        case MSEHasGuidByte2:
        case MSEHasGuidByte3:
        case MSEHasGuidByte4:
        case MSEHasGuidByte5:
        case MSEHasGuidByte6:
        case MSEHasGuidByte7:
            packet.ReadGuidMask(_data.guid, (element - MSEHasGuidByte0));
            break;
        case MSEGuidByte0:
        case MSEGuidByte1:
        case MSEGuidByte2:
        case MSEGuidByte3:
        case MSEGuidByte4:
        case MSEGuidByte5:
        case MSEGuidByte6:
        case MSEGuidByte7:
            packet.ReadGuidBytes(_data.guid, (element - MSEGuidByte0));
            break;
        case MSEExtraFloat:
            if (_floatIndex < MAX_EXTRA_INDEX)
                packet >> _data.floatData[_floatIndex++];
            break;
        case MSEExtraInt8:
            packet >> _data.byteData;
            break;
        case MSEExtraInt32:
            if (_intIndex < MAX_EXTRA_INDEX)
                packet >> _data.intData[_intIndex++];
            break;
        case MSEExtraBits2:
            _data.bitsData = packet.ReadBits(2);
            break;
        default:
            ASSERT(PrintInvalidSequenceElement(element, __FUNCTION__));
            break;
    }
}

void ExtraMovementStatusElement::WriteNextElement(ByteBuffer& packet)
{
    MovementStatusElements const element = _elements[_index++];

    switch (element)
    {
        case MSEHasGuidByte0:
        case MSEHasGuidByte1:
        case MSEHasGuidByte2:
        case MSEHasGuidByte3:
        case MSEHasGuidByte4:
        case MSEHasGuidByte5:
        case MSEHasGuidByte6:
        case MSEHasGuidByte7:
            packet.WriteGuidMask(_data.guid, (element - MSEHasGuidByte0));
            break;
        case MSEGuidByte0:
        case MSEGuidByte1:
        case MSEGuidByte2:
        case MSEGuidByte3:
        case MSEGuidByte4:
        case MSEGuidByte5:
        case MSEGuidByte6:
        case MSEGuidByte7:
            packet.WriteGuidBytes(_data.guid, (element - MSEGuidByte0));
            break;
        case MSEExtraFloat:
            if (_floatIndex < MAX_EXTRA_INDEX)
                packet << float(_data.floatData[_floatIndex++]);
            break;
        case MSEExtraInt8:
            packet << int8(_data.byteData);
            break;
        case MSEExtraInt32:
            if (_intIndex < MAX_EXTRA_INDEX)
                packet << int32(_data.intData[_intIndex++]);
            break;
        case MSEExtraBits2:
            packet.WriteBits(_data.bitsData, 2);
            break;
        case MSEExtraExclusiveBit:
        {
            bool present = _data.exclusiveData != 0;
            packet.WriteBit(!present);
            break;
        }
        case MSEExtraExclusiveData:
            if (_data.exclusiveData != 0)
                packet << _data.exclusiveData;
            break;
        default:
            ASSERT(PrintInvalidSequenceElement(element, __FUNCTION__));
            break;
    }
}

bool PrintInvalidSequenceElement(MovementStatusElements const element, char const* function)
{
    TC_LOG_ERROR("entities.unit", "Incorrect sequence element %d detected at %s", element, function);
    return false;
}

MovementStatusElements const* GetMovementStatusElementsSequence(Opcodes opcode)
{
    switch (opcode)
    {
        case CMSG_MOVE_FALL_LAND:
            return MovementFallLand;
        case CMSG_MOVE_HEARTBEAT:
            return MovementHeartBeat;
        case CMSG_MOVE_JUMP:
            return MovementJump;
        case CMSG_MOVE_SET_FACING:
            return MovementSetFacing;
        case CMSG_MOVE_SET_PITCH:
            return MovementSetPitch;
        case CMSG_MOVE_START_ASCEND:
           return MovementStartAscend;
        case CMSG_MOVE_START_BACKWARD:
           return MovementStartBackward;
        case CMSG_MOVE_START_DESCEND:
            return MovementStartDescend;
        case CMSG_MOVE_START_FORWARD:
            return MovementStartForward;
        case CMSG_MOVE_START_PITCH_DOWN:
            return MovementStartPitchDown;
        case CMSG_MOVE_START_PITCH_UP:
            return MovementStartPitchUp;
        case CMSG_MOVE_START_STRAFE_LEFT:
            return MovementStartStrafeLeft;
        case CMSG_MOVE_START_STRAFE_RIGHT:
            return MovementStartStrafeRight;
        case CMSG_MOVE_START_SWIM:
            return MovementStartSwim;
        case CMSG_MOVE_START_TURN_LEFT:
            return MovementStartTurnLeft;
        case CMSG_MOVE_START_TURN_RIGHT:
            return MovementStartTurnRight;
        case CMSG_MOVE_STOP:
            return MovementStop;
        case CMSG_MOVE_STOP_ASCEND:
            return MovementStopAscend;
        case CMSG_MOVE_STOP_PITCH:
            return MovementStopPitch;
        case CMSG_MOVE_STOP_STRAFE:
            return MovementStopStrafe;
        case CMSG_MOVE_STOP_SWIM:
            return MovementStopSwim;
        case CMSG_MOVE_STOP_TURN:
            return MovementStopTurn;
        case SMSG_PLAYER_MOVE:
            return PlayerMove;
        case CMSG_MOVE_CHNG_TRANSPORT:
            return MoveChngTransport;
        case CMSG_MOVE_SPLINE_DONE:
            return MoveSplineDone;
        case CMSG_DISMISS_CONTROLLED_VEHICLE:
            return DismissControlledVehicle;
        case CMSG_FORCE_MOVE_ROOT_ACK:
            return ForceMoveRootAck;
        case CMSG_FORCE_MOVE_UNROOT_ACK:
            return ForceMoveUnrootAck;
        case CMSG_MOVE_FALL_RESET:
            return MovementFallReset;
        case CMSG_MOVE_FALL_ACK:
            return MovementFallAck;
        case CMSG_MOVE_FORCE_FLIGHT_BACK_SPEED_CHANGE_ACK:
            return MovementForceFlightBackSpeedChangeAck;
        case CMSG_MOVE_FORCE_FLIGHT_SPEED_CHANGE_ACK:
            return MovementForceFlightSpeedChangeAck;
        case CMSG_MOVE_FORCE_PITCH_RATE_CHANGE_ACK:
            return MovementForcePitchRateSpeedChangeAck;
        case CMSG_MOVE_FORCE_RUN_BACK_SPEED_CHANGE_ACK:
            return MovementForceRunBackSpeedChangeAck;
        case CMSG_MOVE_FORCE_RUN_SPEED_CHANGE_ACK:
            return MovementForceRunSpeedChangeAck;
        case CMSG_MOVE_FORCE_SWIM_BACK_SPEED_CHANGE_ACK:
            return MovementForceSwimBackSpeedChangeAck;
        case CMSG_MOVE_FORCE_SWIM_SPEED_CHANGE_ACK:
            return MovementForceSwimSpeedChangeAck;
        case CMSG_MOVE_FORCE_TURN_RATE_CHANGE_ACK:
            return MovementForceTurnRateSpeedChangeAck;
        case CMSG_MOVE_FORCE_WALK_SPEED_CHANGE_ACK:
            return MovementForceWalkSpeedChangeAck;
        case CMSG_MOVE_GRAVITY_DISABLE_ACK:
            return MovementGravityDisableAck;
        case CMSG_MOVE_GRAVITY_ENABLE_ACK:
            return MovementGravityEnableAck;
        case CMSG_MOVE_HOVER_ACK:
            return MovementHoverAck;
        case CMSG_MOVE_KNOCK_BACK_ACK:
            return MovementKnockBackAck;
        case CMSG_MOVE_SET_FLY:
            return MovementSetFly;
        case CMSG_MOVE_SET_CAN_FLY_ACK:
            return MovementSetCanFlyAck;
        case CMSG_MOVE_SET_CAN_TRANSITION_BETWEEN_SWIM_AND_FLY_ACK:
            return MovementSetCanTransitionBetweenSwimAndFlyAck;
        case CMSG_MOVE_SET_COLLISION_HEIGHT_ACK:
            return MovementSetCollisionHeightAck;
        case CMSG_MOVE_SET_VEHICLE_REC_ID_ACK:
            return MovementSetVehicleRecIdAck;
        case SMSG_MOVE_UPDATE_COLLISION_HEIGHT:
            return MovementUpdateCollisionHeight;
        case CMSG_MOVE_WATER_WALK_ACK:
            return MovementWaterWalkAck;
        case CMSG_MOVE_SET_RUN_MODE:
            return MovementSetRunMode;
        case CMSG_MOVE_SET_WALK_MODE:
            return MovementSetWalkMode;
        case SMSG_MOVE_UPDATE_FLIGHT_SPEED:
            return MovementUpdateFlightSpeed;
        case SMSG_MOVE_UPDATE_FLIGHT_BACK_SPEED:
            return MovementUpdateFlightBackSpeed;
        case SMSG_MOVE_UPDATE_RUN_SPEED:
            return MovementUpdateRunSpeed;
        case SMSG_MOVE_UPDATE_KNOCK_BACK:
            return MovementUpdateKnockBack;
        case SMSG_MOVE_UPDATE_RUN_BACK_SPEED:
            return MovementUpdateRunBackSpeed;
        case SMSG_MOVE_UPDATE_PITCH_RATE:
            return MovementUpdatePitchRate;
        case SMSG_MOVE_UPDATE_SWIM_BACK_SPEED:
            return MovementUpdateSwimBackSpeed;
        case SMSG_MOVE_UPDATE_SWIM_SPEED:
            return MovementUpdateSwimSpeed;
        case SMSG_MOVE_UPDATE_TURN_RATE:
            return MovementUpdateTurnRate;
        case SMSG_MOVE_UPDATE_WALK_SPEED:
            return MovementUpdateWalkSpeed;
        case SMSG_SPLINE_MOVE_SET_WALK_SPEED:
            return SplineMoveSetWalkSpeed;
        case SMSG_SPLINE_MOVE_SET_RUN_SPEED:
            return SplineMoveSetRunSpeed;
        case SMSG_SPLINE_MOVE_SET_RUN_BACK_SPEED:
            return SplineMoveSetRunBackSpeed;
        case SMSG_SPLINE_MOVE_SET_SWIM_SPEED:
            return SplineMoveSetSwimSpeed;
        case SMSG_SPLINE_MOVE_SET_SWIM_BACK_SPEED:
            return SplineMoveSetSwimBackSpeed;
        case SMSG_SPLINE_MOVE_SET_TURN_RATE:
            return SplineMoveSetTurnRate;
        case SMSG_SPLINE_MOVE_SET_FLIGHT_SPEED:
            return SplineMoveSetFlightSpeed;
        case SMSG_SPLINE_MOVE_SET_FLIGHT_BACK_SPEED:
            return SplineMoveSetFlightBackSpeed;
        case SMSG_SPLINE_MOVE_SET_PITCH_RATE:
            return SplineMoveSetPitchRate;
        case SMSG_MOVE_SET_WALK_SPEED:
            return MoveSetWalkSpeed;
        case SMSG_MOVE_SET_RUN_SPEED:
            return MoveSetRunSpeed;
        case SMSG_MOVE_SET_RUN_BACK_SPEED:
            return MoveSetRunBackSpeed;
        case SMSG_MOVE_SET_SWIM_SPEED:
            return MoveSetSwimSpeed;
        case SMSG_MOVE_SET_SWIM_BACK_SPEED:
            return MoveSetSwimBackSpeed;
        case SMSG_MOVE_SET_TURN_RATE:
            return MoveSetTurnRate;
        case SMSG_MOVE_SET_FLIGHT_SPEED:
           return MoveSetFlightSpeed;
        case SMSG_MOVE_SET_FLIGHT_BACK_SPEED:
            return MoveSetFlightBackSpeed;
        case SMSG_MOVE_SET_PITCH_RATE:
            return MoveSetPitchRate;
        case SMSG_SPLINE_MOVE_SET_WALK_MODE:
            return SplineMoveSetWalkMode;
        case SMSG_SPLINE_MOVE_SET_RUN_MODE:
            return SplineMoveSetRunMode;
        case SMSG_SPLINE_MOVE_COLLISION_DISABLE:
            return SplineMoveCollisionDisable;
        case SMSG_SPLINE_MOVE_COLLISION_ENABLE:
            return SplineMoveCollisionEnable;
        case SMSG_SPLINE_MOVE_GRAVITY_DISABLE:
            return SplineMoveGravityDisable;
        case SMSG_SPLINE_MOVE_GRAVITY_ENABLE:
            return SplineMoveGravityEnable;
        case SMSG_SPLINE_MOVE_SET_HOVER:
            return SplineMoveSetHover;
        case SMSG_SPLINE_MOVE_UNSET_HOVER:
            return SplineMoveUnsetHover;
        case SMSG_SPLINE_MOVE_START_SWIM:
            return SplineMoveStartSwim;
        case SMSG_SPLINE_MOVE_STOP_SWIM:
            return SplineMoveStopSwim;
        case SMSG_SPLINE_MOVE_SET_FLYING:
            return SplineMoveSetFlying;
        case SMSG_SPLINE_MOVE_UNSET_FLYING:
            return SplineMoveUnsetFlying;
        case SMSG_SPLINE_MOVE_SET_WATER_WALK:
            return SplineMoveSetWaterWalk;
        case SMSG_SPLINE_MOVE_SET_LAND_WALK:
            return SplineMoveSetLandWalk;
        case SMSG_SPLINE_MOVE_SET_FEATHER_FALL:
            return SplineMoveSetFeatherFall;
        case SMSG_SPLINE_MOVE_SET_NORMAL_FALL:
            return SplineMoveSetNormalFall;
        case SMSG_SPLINE_MOVE_ROOT:
            return SplineMoveRoot;
        case SMSG_SPLINE_MOVE_UNROOT:
            return SplineMoveUnroot;
        case SMSG_MOVE_SET_CAN_TRANSITION_BETWEEN_SWIM_AND_FLY:
            return MoveSetCanTransitionBetweenSwimAndFly;
        case SMSG_MOVE_UNSET_CAN_TRANSITION_BETWEEN_SWIM_AND_FLY:
            return MoveUnSetCanTransitionBetweenSwimAndFly;
        case SMSG_MOVE_SET_CAN_FLY:
            return MoveSetCanFly;
        case SMSG_MOVE_UNSET_CAN_FLY:
            return MoveUnsetCanFly;
        case SMSG_MOVE_SET_HOVER:
            return MoveSetHover;
        case SMSG_MOVE_UNSET_HOVER:
            return MoveUnsetHover;
        case SMSG_MOVE_WATER_WALK:
            return MoveWaterWalk;
        case SMSG_MOVE_LAND_WALK:
            return MoveLandWalk;
        case SMSG_MOVE_FEATHER_FALL:
            return MoveFeatherFall;
        case SMSG_MOVE_NORMAL_FALL:
            return MoveNormalFall;
        case SMSG_MOVE_ROOT:
            return MoveRoot;
        case SMSG_MOVE_UNROOT:
            return MoveUnroot;
        case SMSG_MOVE_GRAVITY_DISABLE:
            return MoveGravityDisable;
        case SMSG_MOVE_GRAVITY_ENABLE:
            return MoveGravityEnable;
        case SMSG_MOVE_COLLISION_DISABLE:
            return MoveCollisionDisable;
        case SMSG_MOVE_COLLISION_ENABLE:
            return MoveCollisionEnable;
        case CMSG_CHANGE_SEATS_ON_CONTROLLED_VEHICLE:
            return ChangeSeatsOnControlledVehicle;
        case SMSG_ADJUST_SPLINE_DURATION:
            return AdjustSplineDurationMovement;
        case CMSG_MOVE_SET_CAN_TURN_WHILE_FALLING_ACK:
            return MoveSetCanTurnWhileFallingAck;
        case SMSG_MOVE_SET_CAN_TURN_WHILE_FALLING:
            return MoveSetCanTurnWhileFalling;
        case SMSG_MOVE_UNSET_CAN_TURN_WHILE_FALLING:
            return MoveUnSetCanTurnWhileFalling;
        case CMSG_MOVE_APPLY_MOVEMENT_FORCE_ACK:
            return MoveApplyMovementForceAck;
        case CMSG_MOVE_REMOVE_MOVEMENT_FORCE_ACK:
            return MoveRemoveMovementForceAck;
        case SMSG_MOVE_APPLY_MOVEMENT_FORCE:
            return MoveApplyMovementForce;
        case SMSG_MOVE_REMOVE_MOVEMENT_FORCE:
            return MoveRemoveMovementForce;
        case SMSG_MOVE_UPDATE_APPLY_MOVEMENT_FORCE:
            return MoveUpdateApplyMovementForce;
        case SMSG_MOVE_UPDATE_REMOVE_MOVEMENT_FORCE:
            return MoveUpdateRemoveMovementForce;
        case SMSG_MOVE_SKIP_TIME:
            return MoveSkipTime;
        case SMSG_MOVE_SET_VEHICLE_REC_ID:
            return MoveSetVehicleRecID;
        case SMSG_SET_VEHICLE_REC_ID:
            return MoveUpdateVehicleRecID;
        case SMSG_MOVE_KNOCK_BACK:
            return MoveKnockBack;
        case SMSG_MOVE_SET_COLLISION_HEIGHT:
            return MoveSetCollisionHeight;
        default:
            break;
    }

    return NULL;
}

void GetExtraMovementStatusElements(Opcodes opcode, MoveStateChange const& stateChange, ExtraMovementStatusElement& extra)
{
    MovementStatusElements const* sequence = nullptr;
    ExtraMovementStatusElementsData data;

    switch (opcode)
    {
        case SMSG_MOVE_SET_COLLISION_HEIGHT:
        {
            sequence = MoveExtraCollisionHeight;

            data.floatData[0] = stateChange.CollisionHeight->Height;
            data.floatData[1] = stateChange.CollisionHeight->Scale;
            data.bitsData = stateChange.CollisionHeight->Reason;
            data.exclusiveData = stateChange.CollisionHeight->MountID;

            break;
        }
        case SMSG_MOVE_UPDATE_COLLISION_HEIGHT:
        {
            sequence = MoveExtraUpdateCollisionHeight;

            data.floatData[0] = stateChange.CollisionHeight->Height;
            data.floatData[1] = stateChange.CollisionHeight->Scale;

            break;
        }
        case SMSG_MOVE_SET_WALK_SPEED:
        case SMSG_MOVE_SET_RUN_SPEED:
        case SMSG_MOVE_SET_RUN_BACK_SPEED:
        case SMSG_MOVE_SET_SWIM_SPEED:
        case SMSG_MOVE_SET_SWIM_BACK_SPEED:
        case SMSG_MOVE_SET_TURN_RATE:
        case SMSG_MOVE_SET_FLIGHT_SPEED:
        case SMSG_MOVE_SET_FLIGHT_BACK_SPEED:
        case SMSG_MOVE_SET_PITCH_RATE:
        case SMSG_MOVE_UPDATE_WALK_SPEED:
        case SMSG_MOVE_UPDATE_RUN_SPEED:
        case SMSG_MOVE_UPDATE_RUN_BACK_SPEED:
        case SMSG_MOVE_UPDATE_SWIM_SPEED:
        case SMSG_MOVE_UPDATE_SWIM_BACK_SPEED:
        case SMSG_MOVE_UPDATE_TURN_RATE:
        case SMSG_MOVE_UPDATE_FLIGHT_SPEED:
        case SMSG_MOVE_UPDATE_FLIGHT_BACK_SPEED:
        case SMSG_MOVE_UPDATE_PITCH_RATE:
        case SMSG_SPLINE_MOVE_SET_WALK_SPEED:
        case SMSG_SPLINE_MOVE_SET_RUN_SPEED:
        case SMSG_SPLINE_MOVE_SET_RUN_BACK_SPEED:
        case SMSG_SPLINE_MOVE_SET_SWIM_SPEED:
        case SMSG_SPLINE_MOVE_SET_SWIM_BACK_SPEED:
        case SMSG_SPLINE_MOVE_SET_TURN_RATE:
        case SMSG_SPLINE_MOVE_SET_FLIGHT_SPEED:
        case SMSG_SPLINE_MOVE_SET_FLIGHT_BACK_SPEED:
        case SMSG_SPLINE_MOVE_SET_PITCH_RATE:
        {
            sequence = MoveExtraSpeed;

            data.floatData[0] = *stateChange.MoveSpeed;

            break;
        }
        case SMSG_MOVE_SET_VEHICLE_REC_ID:
        case SMSG_SET_VEHICLE_REC_ID:
        {
            sequence = MoveExtraSetVehicleRecID;

            data.intData[0] = *stateChange.VehicleRecID;

            break;
        }
        case SMSG_MOVE_KNOCK_BACK:
        {
            sequence = MoveExtraKnockBack;

            data.floatData[0] = stateChange.KnockBack->HorizontalSpeed;
            data.floatData[1] = stateChange.KnockBack->SinAngle;
            data.floatData[2] = stateChange.KnockBack->VerticalSpeed;
            data.floatData[3] = stateChange.KnockBack->CosAngle;

            break;
        }
        default:
            break;
    }

    if (!sequence)
        return;

    extra.SetElements(sequence);
    extra.SetData(data);
}
