/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "Log.h"
#include "Corpse.h"
#include "Creature.h"
#include "GameObject.h"
#include "Group.h"
#include "GuildMgr.h"
#include "LootMgr.h"
#include "ObjectAccessor.h"
#include "Object.h"
#include "Opcodes.h"
#include "Player.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Cell.h"
#include "CellImpl.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Scenario.h"

void WorldSession::HandleAutostoreLootItemOpcode(WorldPacket& recvData)
{
    uint32 lootCount = recvData.ReadBits(23);
    GuidVector guids(lootCount);

    for (uint32 i = 0; i < lootCount; i++)
        recvData.ReadGuidMask(guids[i], 2, 7, 0, 6, 5, 3, 1, 4);

    for (uint32 i = 0; i < lootCount; i++)
    {
        recvData.ReadGuidBytes(guids[i], 0, 4, 1, 7, 6, 5, 3, 2);

        uint8 lootSlot = 0;
        recvData >> lootSlot;

        Loot* loot = _player->GetLootObject(guids[i]);
        if (!loot)
        {
            TC_LOG_ERROR("network", "WorldSession::HandleAutostoreLootItemOpcode: Player %s tried to loot item from an object which is not being looted.", _player->GetName().c_str());
            continue;
        }

        Object* owner = loot->GetOwner();
        if (!owner)
        {
            TC_LOG_ERROR("network", "WorldSession::HandleAutostoreLootItemOpcode: Player %s tried to loot item from an object which don't have owner.", _player->GetName().c_str());
            continue;
        }

        switch (owner->GetTypeId())
        {
            case TYPEID_GAMEOBJECT:
            {
                GameObject* go = owner->ToGameObject();

                // not check distance for GO in case owned GO (fishing bobber case, for example) or Fishing hole GO
                if ((go->GetOwnerGUID() != _player->GetGUID() && go->GetGoType() != GAMEOBJECT_TYPE_FISHINGHOLE) && !_player->IsWithinDistInMap(go, INTERACTION_DISTANCE))
                {
                    _player->SendLootRelease(loot);
                    continue;
                }
                break;
            }
            case TYPEID_UNIT:
            {
                if (!loot->IsAoELoot)
                {
                    Creature* creature = owner->ToCreature();

                    bool lootAllowed = creature->IsAlive() == (_player->GetClass() == CLASS_ROGUE && loot->Type == LOOT_PICKPOCKETING);
                    if (!lootAllowed || !_player->IsWithinDistInMap(creature, INTERACTION_DISTANCE))
                    {
                        _player->SendLootError(loot, lootAllowed ? LOOT_ERROR_TOO_FAR : LOOT_ERROR_DIDNT_KILL);
                        continue;
                    }

                    if (!_player->HasInArc(static_cast<float>(M_PI), creature))
                    {
                        _player->SendLootError(loot, LOOT_ERROR_BAD_FACING);
                        continue;
                    }

                    if (!_player->IsStandState())
                    {
                        _player->SendLootError(loot, LOOT_ERROR_NOT_STANDING);
                        continue;
                    }

                    if (_player->HasUnitState(UNIT_STATE_STUNNED))
                    {
                        _player->SendLootError(loot, LOOT_ERROR_STUNNED);
                        continue;
                    }

                    if (_player->IsInDisallowedMountForm())
                    {
                        _player->SendLootError(loot, LOOT_ERROR_NOT_WHILE_SHAPESHIFTED);
                        continue;
                    }
                }

                break;
            }
            default:
                break;
        }

        _player->StoreLootItem(lootSlot, loot);

        // If player is removing the last LootItem, delete the empty container.
        if (loot->IsLooted() && owner->GetTypeId() == TYPEID_ITEM)
            _player->GetSession()->DoLootRelease(loot);
    }
}

void WorldSession::HandleLootMoneyOpcode(WorldPacket& /*recvData*/)
{
    PlayerLootObjects& lootObjects = _player->GetLootObjects();
    if (lootObjects.empty())
        return;

    for (auto & lootItr : lootObjects)
    {
        Loot* loot = lootItr.second;
        if (!loot)
        {
            TC_LOG_ERROR("network", "WorldSession::HandleLootMoneyOpcode: Player %s tried to loot item from an object which is not being looted.", _player->GetName().c_str());
            continue;
        }

        bool shareMoney = false;

        Object* owner = loot->GetOwner();
        if (!owner)
        {
            TC_LOG_ERROR("network", "WorldSession::HandleLootMoneyOpcode: Player %s tried to loot item from an object which don't have owner.", _player->GetName().c_str());
            continue;
        }

        switch (owner->GetTypeId())
        {
            case TYPEID_GAMEOBJECT:
            {
                GameObject* go = owner->ToGameObject();

                if (go->GetOwnerGUID() != _player->GetGUID() && go->GetGoType() != GAMEOBJECT_TYPE_FISHINGHOLE)
                {
                    if (!_player->IsWithinDistInMap(go, INTERACTION_DISTANCE))
                    {
                        _player->SendLootRelease(loot);
                        continue;
                    }

                    shareMoney = true;
                }

                break;
            }
            case TYPEID_CORPSE:
            {
                Corpse* bones = owner->ToCorpse();

                if (!_player->IsWithinDistInMap(bones, INTERACTION_DISTANCE))
                {
                    _player->SendLootRelease(loot);
                    continue;
                }

                break;
            }
            case TYPEID_ITEM:
            {
                Item* item = _player->GetItemByGuid(owner->GetGUID());

                if (!item)
                {
                    _player->SendLootRelease(loot);
                    continue;
                }

                break;
            }
            case TYPEID_UNIT:
            {
                Creature* creature = owner->ToCreature();

                if (!loot->IsAoELoot)
                {
                    bool lootAllowed = creature->IsAlive() == (_player->GetClass() == CLASS_ROGUE && loot->Type == LOOT_PICKPOCKETING);
                    if (!lootAllowed || !_player->IsWithinDistInMap(creature, INTERACTION_DISTANCE))
                    {
                        _player->SendLootError(loot, lootAllowed ? LOOT_ERROR_TOO_FAR : LOOT_ERROR_DIDNT_KILL);
                        continue;
                    }

                    if (!_player->HasInArc(static_cast<float>(M_PI), creature))
                    {
                        _player->SendLootError(loot, LOOT_ERROR_BAD_FACING);
                        continue;
                    }

                    if (!_player->IsStandState())
                    {
                        _player->SendLootError(loot, LOOT_ERROR_NOT_STANDING);
                        continue;
                    }

                    if (_player->HasUnitState(UNIT_STATE_STUNNED))
                    {
                        _player->SendLootError(loot, LOOT_ERROR_STUNNED);
                        continue;
                    }

                    if (_player->IsInDisallowedMountForm())
                    {
                        _player->SendLootError(loot, LOOT_ERROR_NOT_WHILE_SHAPESHIFTED);
                        continue;
                    }
                }

                if (!creature->IsAlive())
                    shareMoney = true;

                break;
            }
            default:
                continue;
        }

        loot->NotifyMoneyRemoved();
        if (shareMoney && _player->GetGroup())      //item, pickpocket and players can be looted only single player
        {
            Group* group = _player->GetGroup();

            std::vector<Player*> playersNear;
            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* member = itr->GetSource();
                if (!member)
                    continue;

                if (_player->IsAtGroupRewardDistance(member))
                    playersNear.push_back(member);
            }

            uint32 goldPerPlayer = uint32((loot->Gold) / (playersNear.size()));
            for (std::vector<Player*>::const_iterator i = playersNear.begin(); i != playersNear.end(); ++i)
            {
                (*i)->ModifyMoney(goldPerPlayer);
                (*i)->UpdateCriteria(CRITERIA_TYPE_LOOT_MONEY, goldPerPlayer);

                if (Guild* guild = sGuildMgr->GetGuildById((*i)->GetGuildId()))
                {
                    if (uint32 guildGold = CalculatePct(goldPerPlayer, (*i)->GetTotalAuraModifier(SPELL_AURA_DEPOSIT_BONUS_MONEY_IN_GUILD_BANK_ON_LOOT)))
                        guild->HandleMemberDepositMoney(this, guildGold, true);

                    guild->UpdateCriteria(CRITERIA_TYPE_LOOT_MONEY, (*i), goldPerPlayer);
                }

                WorldPacket data(SMSG_LOOT_MONEY_NOTIFY, 1 + 4);

                data.WriteBit(playersNear.empty()); // Controls the text displayed in chat. 0 is "Your share is..." and 1 is "You loot..."

                data << uint32(goldPerPlayer);

                (*i)->SendDirectMessage(&data);
            }
        }
        else
        {
            _player->ModifyMoney(loot->Gold);
            _player->UpdateCriteria(CRITERIA_TYPE_LOOT_MONEY, loot->Gold);
            if (Guild* guild = sGuildMgr->GetGuildById(_player->GetGuildId()))
            {
                if (uint32 guildGold = CalculatePct(loot->Gold, _player->GetTotalAuraModifier(SPELL_AURA_DEPOSIT_BONUS_MONEY_IN_GUILD_BANK_ON_LOOT)))
                    guild->HandleMemberDepositMoney(this, guildGold, true);

                guild->UpdateCriteria(CRITERIA_TYPE_LOOT_MONEY, _player, loot->Gold);
            }

            WorldPacket data(SMSG_LOOT_MONEY_NOTIFY, 1 + 4);

            data.WriteBit(true);   // "You loot..."

            data << uint32(loot->Gold);

            SendPacket(&data);
        }

        loot->Gold = 0;

        // Delete the money loot record from the DB
        if (!loot->ContainerID.IsEmpty())
            loot->DeleteLootMoneyFromContainerItemDB();

        // If player is removing the last LootItem, delete the empty container.
        if (loot->IsLooted() && owner->GetTypeId() == TYPEID_ITEM)
            _player->GetSession()->DoLootRelease(loot);
    }
}

void WorldSession::HandleLootOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 4, 5, 2, 7, 0, 1, 3, 6);
    recvData.ReadGuidBytes(guid, 3, 5, 0, 6, 4, 1, 7, 2);

    // Check possible cheat
    if (!_player->IsAlive() || !guid.IsCreatureOrVehicle())
        return;

    Creature const* mainCreature = ObjectAccessor::GetCreatureOrPetOrVehicle(*_player, guid);
    if (!mainCreature || !_player->IsWithinDistInMap(mainCreature, INTERACTION_DISTANCE))
        return;

    // AoE looting searcher
    float aoeDist = sWorld->getFloatConfig(CONFIG_MAX_AOE_LOOT_DISTANCE);
    std::vector<Creature*> lootCreatures;

    Trinity::AllLootableCreaturesInRange check(mainCreature, aoeDist);
    Trinity::CreatureListSearcher<Trinity::AllLootableCreaturesInRange> searcher(mainCreature, lootCreatures, check);
    Cell::VisitGridObjects(mainCreature, searcher, aoeDist);

    // Send number of AoE loot targets
    WorldPacket data(SMSG_LOOT_AOE_TARGETS, 4);
    data << uint32(lootCreatures.size());
    SendPacket(&data);

    _player->SendLoot(mainCreature->loot);

    if (!lootCreatures.empty())
    {
        // Send loot of AoE targets
        for (Creature const* creature : lootCreatures)
        {
            creature->loot->IsAoELoot = true;

            _player->SendLoot(creature->loot);

            // SMSG_LOOT_AOE_TARGETS_ACK is sent after every loot response
            WorldPacket data(SMSG_LOOT_AOE_TARGETS_ACK, 0);
            SendPacket(&data);
        }
    }

    // interrupt cast
    if (_player->IsNonMeleeSpellCasted(false))
        _player->InterruptNonMeleeSpells(false);
}

void WorldSession::HandleLootReleaseOpcode(WorldPacket& recvData)
{
    // cheaters can modify lguid to prevent correct apply loot release code and re-loot
    // use internal stored guid

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 4, 2, 3, 0, 5, 6, 1);
    recvData.ReadGuidBytes(guid, 0, 6, 4, 2, 5, 3, 7, 1);

    PlayerLootObjects& lootObjects = _player->GetLootObjects();
    if (lootObjects.empty())
        return;

    for (auto & loot : lootObjects)
        if (Object* owner = loot.second->GetOwner())
            if (owner->GetGUID() == guid)
            {
                DoLootRelease(loot.second);
                return;
            }
}

void WorldSession::DoLootRelease(Loot* loot)
{
    Player* player = GetPlayer();
    if (!player || !player->IsInWorld())
        return;

    //Player is not looking at loot list, he doesn't need to see updates on the loot list
    loot->RemoveLooter(player->GetGUID());
    player->RemoveLootObject(loot->GetGUID());

    if (player->GetLootObjects().empty())
    {
        player->SendLootReleaseAll();
        player->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_LOOTING);
    }

    Object* owner = loot->GetOwner();
    if (!owner)
    {
        TC_LOG_ERROR("network", "WorldSession::DoLootRelease: Player %s tried to loot item from an object which don't have owner.", player->GetName().c_str());
        return;
    }

    switch (owner->GetTypeId())
    {
        case TYPEID_GAMEOBJECT:
        {
            GameObject* go = owner->ToGameObject();

            // not check distance for GO in case owned GO (fishing bobber case, for example) or Fishing hole GO
            if ((go->GetOwnerGUID() != _player->GetGUID() && go->GetGoType() != GAMEOBJECT_TYPE_FISHINGHOLE) && !_player->IsWithinDistInMap(go, INTERACTION_DISTANCE))
                return;

            if (go->GetGoType() == GAMEOBJECT_TYPE_DOOR)
            {
                // locked doors are opened with spelleffect openlock, prevent remove its as looted
                go->UseDoorOrButton();
            }
            else if (loot->IsLooted() || go->GetGoType() == GAMEOBJECT_TYPE_FISHINGNODE)
            {
                if (go->GetGoType() == GAMEOBJECT_TYPE_FISHINGHOLE)
                {                                               // The fishing hole used once more
                    go->AddUse();                               // if the max usage is reached, will be despawned in next tick
                    if (go->GetUseCount() >= go->GetGOValue()->FishingHole.MaxOpens)
                        go->SetLootState(GO_JUST_DEACTIVATED);
                    else
                        go->SetLootState(GO_READY);
                }
                else
                    go->SetLootState(GO_JUST_DEACTIVATED);

                delete loot;
                go->loot = nullptr;
                loot = nullptr;
            }
            else
            {
                // not fully looted object
                go->SetLootState(GO_ACTIVATED, player);

                // if the round robin player release, reset it.
                if (player->GetGUID() == loot->RoundRobinPlayer)
                    loot->RoundRobinPlayer.Clear();
            }

            break;
        }
        case TYPEID_CORPSE:
        {
            Corpse* corpse = owner->ToCorpse();
            if (!_player->IsWithinDistInMap(corpse, INTERACTION_DISTANCE))
                return;

            if (loot->IsLooted())
                corpse->RemoveFlag(CORPSE_DYNAMIC_FLAGS, CORPSE_DYNFLAG_LOOTABLE);

            break;
        }
        case TYPEID_ITEM:
        {
            Item* pItem = player->GetItemByGuid(owner->GetGUID());
            if (!pItem)
                return;

            ItemTemplate const* proto = pItem->GetTemplate();

            // destroy only 5 items from stack in case prospecting and milling
            if (proto->GetFlags() & (ITEM_PROTO_FLAG_IS_PROSPECTABLE | ITEM_PROTO_FLAG_IS_MILLABLE))
            {
                delete loot;
                pItem->loot = nullptr;
                loot = nullptr;

                uint32 count = pItem->GetCount();

                // >=5 checked in spell code, but will work for cheating cases also with removing from another stacks.
                if (count > 5)
                    count = 5;
            
                player->DestroyItemCount(pItem, count, true);
            }
            else
            {
                // Only delete item if no loot or money (unlooted loot is saved to db) or if it isn't an openable item
                if (pItem->loot->IsLooted() || !(proto->GetFlags() & ITEM_PROTO_FLAG_HAS_LOOT))
                    player->DestroyItem(pItem->GetBagSlot(), pItem->GetSlot(), true);
            }

            break;
        }
        case TYPEID_UNIT:
        {
            Creature* creature = owner->ToCreature();

            bool lootAllowed = creature->IsAlive() == (player->GetClass() == CLASS_ROGUE && loot->Type == LOOT_PICKPOCKETING);
            if (!lootAllowed || !player->IsWithinDistInMap(creature, INTERACTION_DISTANCE))
                return;

            if (loot->IsLooted())
            {
                creature->RemoveFlag(OBJECT_DYNAMIC_FLAGS, UNIT_DYNFLAG_LOOTABLE);

                // skip pickpocketing loot for speed, skinning timer reduction is no-op in fact
                if (!creature->IsAlive())
                    creature->AllLootRemovedFromCorpse();
            }
            else
            {
                // if the round robin player release, reset it.
                if (player->GetGUID() == loot->RoundRobinPlayer)
                {
                    loot->RoundRobinPlayer.Clear();

                    if (Group* group = player->GetGroup())
                        group->SendLooter(creature, NULL);

                    // force update of dynamic flags, otherwise other group's players still not able to loot.
                    creature->ForceValuesUpdateAtIndex(OBJECT_DYNAMIC_FLAGS);
                }
            }

            break;
        }
        default:
            break;
    }
}

void WorldSession::HandleLootMasterGiveOpcode(WorldPacket& recvData)
{
    ObjectGuid targetGuid;

    recvData.ReadGuidMask(targetGuid, 0, 2, 4, 1, 7, 6);

    uint32 count = recvData.ReadBits(23);

    GuidVector guids(count);
    std::vector<uint8> slotIDs(count);

    for (uint32 i = 0; i < count; ++i)
        recvData.ReadGuidMask(guids[i], 6, 1, 7, 0, 3, 4, 5, 2);

    recvData.ReadGuidMask(targetGuid, 3, 5);

    for (uint32 i = 0; i < count; ++i)
    {
        recvData >> slotIDs[i];
        recvData.ReadGuidBytes(guids[i], 3, 0, 6, 7, 1, 4, 2, 5);
    }

    recvData.ReadGuidBytes(targetGuid, 4, 5, 2, 3, 6, 0, 7, 1);

    for (uint32 i = 0; i < count; ++i)
    {
        uint8 slotid = slotIDs[i];
        Loot* loot = _player->GetLootObject(guids[i]);
        if (!loot)
        {
            TC_LOG_ERROR("network", "WorldSession::HandleLootMasterGiveOpcode: Player %s tried to loot item from an object which is not being looted.", _player->GetName().c_str());
            continue;
        }

        Object* owner = loot->GetOwner();
        if (!owner)
        {
            TC_LOG_ERROR("network", "WorldSession::HandleLootMasterGiveOpcode: Player %s tried to loot item from an object which don't have owner.", _player->GetName().c_str());
            continue;
        }

        if (!_player->GetGroup() || _player->GetGroup()->GetMasterLooterGuid() != _player->GetGUID() || _player->GetGroup()->GetLootMethod() != MASTER_LOOT)
        {
            _player->SendLootError(loot, LOOT_ERROR_DIDNT_KILL);
            continue;
        }

        Player* target = ObjectAccessor::FindPlayer(targetGuid);
        if (!target)
        {
            _player->SendLootError(loot, LOOT_ERROR_PLAYER_NOT_FOUND);
            continue;
        }

        if (!_player->IsInRaidWith(target) || !_player->IsInMap(target))
        {
            _player->SendLootError(loot, LOOT_ERROR_MASTER_OTHER);
            TC_LOG_INFO("loot", "MasterLootItem: Player %s tried to give an item to ineligible player %s !", GetPlayer()->GetName().c_str(), target->GetName().c_str());
            continue;
        }

        if (slotid >= loot->Items.size() + loot->QuestItems.size())
        {
            TC_LOG_DEBUG("loot", "MasterLootItem: Player %s might be using a hack! (slot %d, size %lu)",
                GetPlayer()->GetName().c_str(), slotid, (unsigned long)loot->Items.size());
            continue;
        }

        LootItem& item = slotid >= loot->Items.size() ? loot->QuestItems[slotid - loot->Items.size()] : loot->Items[slotid];

        ItemPosCountVec dest;
        InventoryResult msg = target->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, item.ItemId, item.Count);
        if (item.FollowLootRules && !item.AllowedForPlayer(target))
            msg = EQUIP_ERR_CANT_EQUIP_EVER;
        if (msg != EQUIP_ERR_OK)
        {
            if (msg == EQUIP_ERR_ITEM_MAX_COUNT)
                _player->SendLootError(loot, LOOT_ERROR_MASTER_UNIQUE_ITEM);
            else if (msg == EQUIP_ERR_INV_FULL)
                _player->SendLootError(loot, LOOT_ERROR_MASTER_INV_FULL);
            else
                _player->SendLootError(loot, LOOT_ERROR_MASTER_OTHER);

            target->SendEquipError(msg, NULL, NULL, item.ItemId);
            continue;
        }

        // list of players allowed to receive this item in trade
        AllowedLooterSet looters = item.GetAllowedLooters();

        // not move item from loot to target inventory
        Item* newitem = target->StoreNewItem(dest, item.ItemId, true, item.RandomPropertyId, item.UpgradeId, looters);
        target->SendNewItem(newitem, uint32(item.Count), false, false, true);
        target->UpdateCriteria(CRITERIA_TYPE_LOOT_ITEM, item.ItemId, item.Count);
        if (Scenario* scenario = target->GetScenario())
            scenario->UpdateCriteria(CRITERIA_TYPE_LOOT_ITEM, target, item.ItemId, item.Count);

        target->UpdateCriteria(CRITERIA_TYPE_LOOT_TYPE, target, item.ItemId, item.Count, loot->Type);

        target->UpdateCriteria(CRITERIA_TYPE_LOOT_EPIC_ITEM, item.ItemId);
        if (Guild* guild = target->GetGuild())
            guild->UpdateCriteria(CRITERIA_TYPE_LOOT_EPIC_ITEM, target, item.ItemId);

        // mark as looted
        item.Count = 0;
        item.IsLooted = true;

        loot->NotifyItemRemoved(slotid);
        --loot->UnlootedCount;
    }
}

void WorldSession::HandleLootMasterAskForRoll(WorldPacket& recvData)
{
    ObjectGuid guid;
    uint8 slot = 0;

    recvData >> slot;

    recvData.ReadGuidMask(guid, 0, 2, 7, 1, 6, 3, 4, 5);
    recvData.ReadGuidBytes(guid, 7, 6, 4, 0, 2, 5, 3, 1);

    Loot* loot = nullptr;

    PlayerLootObjects& lootObjects = _player->GetLootObjects();
    if (lootObjects.empty())
        return;

    for (auto & lootObject : lootObjects)
        if (Object* owner = lootObject.second->GetOwner())
            if (owner->GetGUID() == guid)
            {
                loot = lootObject.second;
                break;
            }

    if (!loot)
    {
        TC_LOG_ERROR("network", "WorldSession::HandleLootMasterAskForRoll: Player %s tried to loot item from an object which is not being looted.", _player->GetName().c_str());
        return;
    }

    Object* owner = loot->GetOwner();
    if (!owner)
    {
        TC_LOG_ERROR("entities.player.cheat", "WorldSession::HandleLootMasterAskForRoll: Player %s tried to loot item from an object which don't have owner.", _player->GetName().c_str());
        return;
    }

    WorldObject* lootedObject = NULL;
    if (owner->GetTypeId() == TYPEID_UNIT)
        lootedObject = owner->ToCreature();
    else if (owner->GetTypeId() == TYPEID_GAMEOBJECT)
        lootedObject = owner->ToGameObject();

    if (!lootedObject)
    {
        TC_LOG_ERROR("network", "WorldSession::HandleLootMasterAskForRoll: Player %s tried to loot item from not proper object type.", _player->GetName().c_str());
        return;
    }

    if (!_player->GetGroup() || _player->GetGroup()->GetLooterGuid() != _player->GetGUID())
    {
        _player->SendLootRelease(loot);
        return;
    }

    if (slot >= loot->Items.size() + loot->QuestItems.size())
    {
        TC_LOG_ERROR("loot", "MasterLootItem: Player %s might be using a hack! (slot %d, size %lu)", GetPlayer()->GetName().c_str(), slot, (unsigned long)loot->Items.size());
        return;
    }

    LootItem& item = slot >= loot->Items.size() ? loot->QuestItems[slot - loot->Items.size()] : loot->Items[slot];

    if (item.AlreadyAskedForRoll)
        return;

    _player->GetGroup()->DoRollForAllMembers(lootedObject, slot, _player->GetMapId(), loot, item);
}
