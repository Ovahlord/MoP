/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "LFGMgr.h"
#include "ObjectMgr.h"
#include "Group.h"
#include "Player.h"
#include "Opcodes.h"
#include "WorldPacket.h"
#include "WorldSession.h"

void BuildPlayerLockDungeonBlock(ByteBuffer& data, LfgLockMap const& lock)
{
    for (LfgLockMap::const_iterator it = lock.begin(); it != lock.end(); ++it)
    {
        TC_LOG_TRACE("misc", "DungeonID: %u Lock status: %u Required itemLevel: %u Current itemLevel: %f",
            (it->first & 0x00FFFFFF), it->second.lockStatus, it->second.requiredItemLevel, it->second.currentItemLevel);

        data << uint32(it->first);                      // Dungeon entry (id + type)
        data << uint32(it->second.lockStatus);          // Lock status
        data << uint32(it->second.requiredItemLevel);   // Required itemLevel
        data << uint32(it->second.currentItemLevel);    // Current itemLevel
    }
}

void WorldSession::HandleLfgJoinOpcode(WorldPacket& recvData)
{
    uint32 roles;
    uint32 numDungeons;
    uint32 commentLen;
    bool QueueAsGroup;
    uint8 PartyIndex;

    recvData >> PartyIndex;

    for (int32 i = 0; i < 3; ++i)
        recvData.read_skip<uint32>(); // Needs

    recvData >> roles;

    numDungeons = recvData.ReadBits(22);
    commentLen = recvData.ReadBits(8);

    QueueAsGroup = recvData.ReadBit();

    if (!numDungeons)
    {
        TC_LOG_DEBUG("lfg", "CMSG_LFG_JOIN %s no dungeons selected", GetPlayerInfo().c_str());
        recvData.rfinish();
        return;
    }

    LfgDungeonSet newDungeons;
    for (uint32 i = 0; i < numDungeons; ++i)
    {
        uint32 dungeon;
        recvData >> dungeon;
        newDungeons.insert((dungeon & 0x00FFFFFF));        // remove the type from the dungeon entry
    }

    std::string comment = recvData.ReadString(commentLen);

    uint32 maxGroupSize = MAXGROUPSIZE;

    /*if (LFGDungeonEntry const* dungeon = sLFGDungeonStore.LookupEntry(*newDungeons.begin() & 0xFFFFFF))
    {
        uint8 type = dungeon->type;
        if (type == LFG_TYPE_SCENARIO)
            maxGroupSize = 3;
        else if (type == LFG_TYPE_RAID)
            maxGroupSize = 25;
    }*/

    if (!sLFGMgr->isOptionEnabled(LFG_OPTION_ENABLE_DUNGEON_FINDER | LFG_OPTION_ENABLE_RAID_BROWSER) ||
        (GetPlayer()->GetGroup() && GetPlayer()->GetGroup()->GetLeaderGUID() != GetPlayer()->GetGUID() &&
        (GetPlayer()->GetGroup()->GetMembersCount() == maxGroupSize || !GetPlayer()->GetGroup()->IsLFGGroup())))
    {
        recvData.rfinish();
        return;
    }

    TC_LOG_DEBUG("lfg", "CMSG_LFG_JOIN %s roles: %u, Dungeons: %u, Comment: %s", GetPlayerInfo().c_str(), roles, uint8(newDungeons.size()), comment.c_str());

    sLFGMgr->JoinLfg(GetPlayer(), uint8(roles), newDungeons, comment);
}

void WorldSession::HandleLfgLeaveOpcode(WorldPacket& recvData)
{
    // not needed packet
    recvData.rfinish();

    Group* group = GetPlayer()->GetGroup();
    ObjectGuid guid = GetPlayer()->GetGUID();
    ObjectGuid gguid = group ? group->GetGUID() : guid;

    TC_LOG_DEBUG("lfg", "CMSG_LFG_LEAVE %s in group: %u sent guid %s", GetPlayerInfo().c_str(), group ? 1 : 0, gguid.ToString().c_str());

    // Check cheating - only leader can leave the queue
    if (!group || group->GetLeaderGUID() == guid)
        sLFGMgr->LeaveLfg(gguid);
}

void WorldSession::HandleLfgProposalResultOpcode(WorldPacket& recvData)
{
    uint32 proposalID;
    bool accept;

    recvData >> proposalID;

    recvData.read_skip<uint32>(); // queueId
    recvData.read_skip<uint32>(); // 3 (unk)
    recvData.read_skip<uint32>(); // join time

    accept = recvData.ReadBit();

    // not needed packet
    recvData.rfinish();

    TC_LOG_DEBUG("lfg", "CMSG_LFG_PROPOSAL_RESULT %s proposal: %u accept: %u", GetPlayerInfo().c_str(), proposalID, accept ? 1 : 0);

    sLFGMgr->UpdateProposal(proposalID, GetPlayer()->GetGUID(), accept);
}

void WorldSession::HandleLfgSetRolesOpcode(WorldPacket& recvData)
{
    uint8 roles;
    recvData >> roles;                                     // Player Group Roles
    ObjectGuid guid = GetPlayer()->GetGUID();
    Group* group = GetPlayer()->GetGroup();
    if (!group)
    {
        TC_LOG_DEBUG("lfg", "CMSG_LFG_SET_ROLES %s Not in group", GetPlayerInfo().c_str());
        return;
    }
    ObjectGuid gguid = group->GetGUID();
    TC_LOG_DEBUG("lfg", "CMSG_LFG_SET_ROLES: Group %u, Player %s, Roles: %u",
        gguid.GetCounter(), GetPlayerInfo().c_str(), roles);
    sLFGMgr->UpdateRoleCheck(gguid, guid, roles);
}

void WorldSession::HandleLfgSetCommentOpcode(WorldPacket&  recvData)
{
    std::string comment;
    recvData >> comment;

    TC_LOG_DEBUG("lfg", "CMSG_LFG_SET_COMMENT %s comment: %s", GetPlayerInfo().c_str(), comment.c_str());

    sLFGMgr->SetComment(GetPlayer()->GetGUID(), comment);
}

void WorldSession::HandleLfgSetBootVoteOpcode(WorldPacket& recvData)
{
    bool agree;                                            // Agree to kick player
    agree = recvData.ReadBit();

    ObjectGuid guid = GetPlayer()->GetGUID();
    TC_LOG_DEBUG("lfg", "CMSG_LFG_SET_BOOT_VOTE %s agree: %u",
        GetPlayerInfo().c_str(), agree ? 1 : 0);
    sLFGMgr->UpdateBoot(guid, agree);
}

void WorldSession::HandleLfgTeleportOpcode(WorldPacket& recvData)
{
    bool out;
    out = recvData.ReadBit();

    TC_LOG_DEBUG("lfg", "CMSG_LFG_TELEPORT %s out: %u", GetPlayerInfo().c_str(), out ? 1 : 0);

    sLFGMgr->TeleportPlayer(GetPlayer(), out, true);
}

void WorldSession::HandleLfgGetLockInfoOpcode(WorldPacket& recvData)
{
    uint8 PartyIndex;
    recvData >> PartyIndex;

    bool ForPlayer;
    ForPlayer = recvData.ReadBit();

    TC_LOG_DEBUG("lfg", "CMSG_LFG_LOCK_INFO_REQUEST %s for %s", GetPlayerInfo().c_str(), (ForPlayer ? "player" : "party"));

    if (ForPlayer)
        SendLfgPlayerLockInfo();
    else
        SendLfgPartyLockInfo();
}

void WorldSession::SendLfgPlayerLockInfo()
{
    ObjectGuid guid = GetPlayer()->GetGUID();

    // Get Random dungeons that can be done at a certain level and expansion
    uint8 level = GetPlayer()->GetLevel();
    LfgDungeonSet const& randomDungeons = sLFGMgr->GetDungeonFinderDungeons(level, GetPlayer()->GetSession()->Expansion());

    // Get player locked Dungeons
    LfgLockMap const& lock = sLFGMgr->GetLockedDungeons(guid);

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PLAYER_INFO %s", GetPlayerInfo().c_str());

    WorldPacket data(SMSG_LFG_PLAYER_INFO);

    ByteBuffer dungeonData;
    ByteBuffer rewardData;

    bool HasGuid = false;

    data.WriteBits(lock.size(), 20);

    data.WriteBit(HasGuid);

    data.WriteBits(randomDungeons.size(), 17);

    for (LfgDungeonSet::const_iterator it = randomDungeons.begin(); it != randomDungeons.end(); ++it)
    {
        LfgReward const* reward = sLFGMgr->GetRandomDungeonReward(*it, level);

        Quest const* quest = NULL;
        Quest const* shortageQuest = NULL;

        bool firstRandom = false;

        uint32 bonusReputationValue = 0;

        if (reward)
        {
            quest = sObjectMgr->GetQuestTemplate(reward->firstQuest);
            if (quest)
            {
                firstRandom = GetPlayer()->CanRewardQuest(quest, false);
                if (!firstRandom)
                    quest = sObjectMgr->GetQuestTemplate(reward->otherQuest);
            }

            shortageQuest = sObjectMgr->GetQuestTemplate(reward->shortageFirstQuest);
            if (shortageQuest)
                if (!GetPlayer()->CanRewardQuest(shortageQuest, false))
                    shortageQuest = sObjectMgr->GetQuestTemplate(reward->shortageOtherQuest);

        }

        bool ShowCallToArms = shortageQuest != NULL;

        data.WriteBit(ShowCallToArms);
        data.WriteBit(firstRandom);

        data.WriteBits(quest ? quest->GetRewBonusCurrencyCount() : 0, 21);
        data.WriteBits(ShowCallToArms ? CALL_TO_ARMS_COUNT : 0, 19);
        data.WriteBits(quest ? quest->GetRewItemsCount() : 0, 20);

        // Call to Arms
        if (ShowCallToArms)
            for (uint32 i = 0; i < CALL_TO_ARMS_COUNT; ++i)
            {
                uint32 mask = PLAYER_ROLE_TANK;
                switch (i)
                {
                    case 1:
                        mask = PLAYER_ROLE_HEALER;
                        break;
                    case 2:
                        mask = PLAYER_ROLE_DAMAGE;
                        break;
                    default:
                        break;
                }

                data.WriteBits(shortageQuest ? shortageQuest->GetRewBonusCurrencyCount() : 0, 21);
                data.WriteBits(shortageQuest ? shortageQuest->GetRewItemsCount() : 0, 20);
                data.WriteBits(shortageQuest ? shortageQuest->GetRewCurrencyCount() : 0, 21);

                rewardData << uint32(0);        // RewardMoney (not sure)

                if (shortageQuest)
                    for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
                    {
                        int32 bonusCurrencyCount = shortageQuest->RewardBonusCurrencyCount[i];
                        if (bonusCurrencyCount)
                            if (uint32 bonusCurrencyId = shortageQuest->RewardCurrencyId[i])
                            {
                                CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(bonusCurrencyId);
                                if (!currency)
                                    continue;

                                int32 precision = currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION ? CURRENCY_PRECISION : 1;
                                bonusCurrencyCount *= precision;

                                rewardData << uint32(bonusCurrencyId);
                                rewardData << uint32(bonusCurrencyCount);
                            }
                    }

                if (shortageQuest)
                    for (uint32 i = 0; i < QUEST_REWARD_ITEM_COUNT; ++i)
                    {
                        if (uint32 itemId = shortageQuest->RewardItemId[i])
                        {
                            ItemTemplate const* item = sObjectMgr->GetItemTemplate(itemId);

                            rewardData << uint32(shortageQuest->RewardItemCount[i]);
                            rewardData << uint32(itemId);
                            rewardData << uint32(item ? item->GetDisplayId() : 0);
                        }
                    }

                rewardData << uint32(mask);

                if (shortageQuest)
                    for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
                    {
                        int32 currencyCount = shortageQuest->RewardCurrencyCount[i];
                        if (currencyCount)
                            if (uint32 currencyId = shortageQuest->RewardCurrencyId[i])
                            {
                                CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(currencyId);
                                if (!currency)
                                    continue;

                                int32 precision = currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION ? CURRENCY_PRECISION : 1;
                                currencyCount *= precision;

                                rewardData << uint32(currencyId);
                                rewardData << uint32(currencyCount);
                            }
                    }

                rewardData << uint32(0);    // RewardXP (not sure)
            }

        data.WriteBits(quest ? quest->GetRewCurrencyCount() : 0, 21);

        dungeonData << uint32(quest ? quest->XPValue(GetPlayer()->GetLevel()) : 0);

        dungeonData.FlushBits();
        dungeonData.append(rewardData);

        dungeonData << uint32(0);           // Slot (not sure)

        if (quest)
            for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
            {
                int32 bonusCurrencyCount = quest->RewardBonusCurrencyCount[i];
                if (bonusCurrencyCount)
                    if (uint32 bonusCurrencyId = quest->RewardCurrencyId[i])
                    {
                        CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(bonusCurrencyId);
                        if (!currency)
                            continue;

                        int32 precision = currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION ? CURRENCY_PRECISION : 1;
                        bonusCurrencyCount *= precision;

                        dungeonData << uint32(bonusCurrencyId);
                        dungeonData << uint32(bonusCurrencyCount);
                    }
            }

        dungeonData << uint32(0);                                   // CompletionQuantity (not sure)
        dungeonData << uint32(quest ? GetPlayer()->GetQuestMoneyReward(quest) : 0);

        if (quest)
            for (uint32 i = 0; i < QUEST_REWARD_ITEM_COUNT; ++i)
            {
                if (uint32 itemId = quest->RewardItemId[i])
                {
                    ItemTemplate const* item = sObjectMgr->GetItemTemplate(itemId);

                    dungeonData << uint32(quest->RewardItemCount[i]);
                    dungeonData << uint32(itemId);
                    dungeonData << uint32(item ? item->GetDisplayId() : 0);
                }
            }

        dungeonData << uint32(0); // CompletionLimit (not sure)
        dungeonData << uint32(0); // CompletionCurrencyID (not sure)
        dungeonData << uint32(0); // SpecificQuantity (not sure)
        dungeonData << uint32(0); // SpecificLimit (not sure)
        dungeonData << uint32(0); // OverallQuantity (not sure)
        dungeonData << uint32(*it);

        if (quest)
            for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
            {
                int32 currencyCount = quest->RewardCurrencyCount[i];
                if (currencyCount)
                    if (uint32 currencyId = quest->RewardCurrencyId[i])
                    {
                        CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(currencyId);
                        if (!currency)
                            continue;

                        int32 precision = currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION ? CURRENCY_PRECISION : 1;
                        currencyCount *= precision;

                        dungeonData << uint32(currencyId);
                        dungeonData << uint32(currencyCount);
                    }
            }

        dungeonData << uint32(0); // PurseWeeklyQuantity (not sure)
        dungeonData << uint32(0); // OverallLimit (not sure)
        dungeonData << uint32(0); // PurseWeeklyLimit (not sure)
        dungeonData << uint32(0); // PurseQuantity (not sure)
        dungeonData << uint32(0); // PurseLimit (not sure)
        dungeonData << uint32(0); // Quantity (not sure)
        dungeonData << uint32(0); // CompletedMask (not sure)
    }

    if (HasGuid)
    {
        data.WriteGuidMask(guid, 5, 1, 2, 7, 3, 0, 6, 4);
        data.WriteGuidBytes(guid, 7, 2, 3, 0, 4, 5, 6, 1);
    }

    data.FlushBits();
    data.append(dungeonData);

    BuildPlayerLockDungeonBlock(data, lock);

    SendPacket(&data);
}

void WorldSession::SendLfgPartyLockInfo()
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    TC_LOG_DEBUG("lfg", "CMSG_LFG_PARTY_LOCK_INFO_REQUEST %s", GetPlayerInfo().c_str());

    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    // Get the locked dungeons of the other party members
    LfgLockPartyMap lockMap;
    for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* plrg = itr->GetSource();
        if (!plrg)
            continue;

        ObjectGuid pguid = plrg->GetGUID();
        if (pguid == guid)
            continue;

        lockMap[pguid] = sLFGMgr->GetLockedDungeons(pguid);
    }

    uint32 size = 0;
    for (LfgLockPartyMap::const_iterator it = lockMap.begin(); it != lockMap.end(); ++it)
        size += 8 + 4 + uint32(it->second.size()) * (4 + 4 + 4 + 4);

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PARTY_INFO %s", GetPlayerInfo().c_str());

    WorldPacket data(SMSG_LFG_PARTY_INFO);

    ByteBuffer lockData;

    data.WriteBits(lockMap.size(), 22);

    for (LfgLockPartyMap::const_iterator it = lockMap.begin(); it != lockMap.end(); ++it)
    {
        ObjectGuid guid = it->first;

        bool hasGuid = false;

        data.WriteBit(hasGuid); // HasGuid

        if (hasGuid)
            data.WriteGuidMask(guid, 3, 6, 0, 5, 2, 7, 4, 1);

        data.WriteBits(it->second.size(), 20);

        BuildPlayerLockDungeonBlock(lockData, it->second);

        if (hasGuid)
            lockData.WriteGuidBytes(guid, 0, 3, 1, 4, 6, 2, 5, 7);
    }

    data.FlushBits();
    data.append(lockData);

    SendPacket(&data);
}

void WorldSession::HandleLfgGetStatus(WorldPacket& /*recvData*/)
{
    TC_LOG_DEBUG("lfg", "CMSG_LFG_GET_STATUS %s", GetPlayerInfo().c_str());

    if (!GetPlayer()->IsUsingLfg())
        return;

    ObjectGuid guid = GetPlayer()->GetGUID();
    LfgUpdateData updateData = sLFGMgr->GetLfgStatus(guid);

    if (GetPlayer()->GetGroup())
    {
        SendLfgUpdateStatus(updateData, true);
        updateData.dungeons.clear();
        SendLfgUpdateStatus(updateData, false);
    }
    else
    {
        SendLfgUpdateStatus(updateData, false);
        updateData.dungeons.clear();
        SendLfgUpdateStatus(updateData, true);
    }
}

void WorldSession::SendLfgUpdateStatus(LfgUpdateData const& updateData, bool party)
{
    bool NotifyUI = true;
    bool Joined = false;
    bool Queued = false;
    bool UnkBit = false;
    bool LFGJoined = false;

    ObjectGuid guid = _player->GetGUID();
    time_t JoinTime = sLFGMgr->GetQueueJoinTime(_player->GetGUID());
    uint32 queueId = sLFGMgr->GetQueueId(_player->GetGUID());
    uint32 roles = sLFGMgr->GetRoles(_player->GetGUID());

    switch (updateData.updateType)
    {
        case LFG_UPDATETYPE_JOIN_QUEUE_INITIAL:            // Joined queue outside the dungeon
            Joined = true;
            break;
        case LFG_UPDATETYPE_JOIN_QUEUE:
        case LFG_UPDATETYPE_ADDED_TO_QUEUE:                // Rolecheck Success
            Joined = true;
            Queued = true;
            LFGJoined = true;
            break;
        case LFG_UPDATETYPE_PROPOSAL_BEGIN:
            Joined = true;
            LFGJoined = true;
            break;
        case LFG_UPDATETYPE_UPDATE_STATUS:
            Joined = updateData.state != LFG_STATE_ROLECHECK && updateData.state != LFG_STATE_NONE;
            Queued = updateData.state == LFG_STATE_QUEUED;
            break;
        default:
            break;
    }

    TC_LOG_DEBUG("lfg", "SMSG_LFG_UPDATE_STATUS %s updatetype: %u, party %s", GetPlayerInfo().c_str(), updateData.updateType, party ? "true" : "false");

    WorldPacket data(SMSG_LFG_UPDATE_STATUS);

    data.WriteBits(updateData.comment.size(), 8);

    data.WriteBit(NotifyUI);
    data.WriteBit(Joined);

    data.WriteBits(updateData.dungeons.size(), 22);

    data.WriteGuidMask(guid, 2, 3, 1);

    data.WriteBit(Queued);

    data.WriteGuidMask(guid, 7, 6, 0);

    data.WriteBit(UnkBit);
    data.WriteBit(LFGJoined);

    ByteBuffer guidData;

    uint32 groupMembersCount = 0;
    if (party)
        if (Group* group = GetPlayer()->GetGroup())
            groupMembersCount = group->GetMembersCount();

    data.WriteBits(groupMembersCount, 24);

    data.WriteGuidMask(guid, 5);

    if (party)
        if (Group* group = GetPlayer()->GetGroup())
            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* member = itr->GetSource();
                if (!member)
                    continue;

                if (member->GetGUID() == _player->GetGUID())
                    continue;

                ObjectGuid memberGuid = member->GetGUID();

                data.WriteGuidMask(memberGuid, 7, 0, 4, 2, 5, 3, 1, 6);
                guidData.WriteGuidBytes(memberGuid, 7, 0, 1, 6, 4, 5, 2, 3);
            }

    data.WriteGuidMask(guid, 4);

    data.FlushBits();

    data.WriteGuidBytes(guid, 3);

    for (uint8 i = 0; i < 3; ++i)
        data << uint8(0); // unk (needed roles?)

    data.WriteGuidBytes(guid, 4);

    data.append(guidData);

    data.WriteGuidBytes(guid, 6);

    data << uint8(updateData.updateType);
    data << uint32(roles);
    data << uint32(queueId);

    data.WriteGuidBytes(guid, 5);

    data.WriteString(updateData.comment);

    data.WriteGuidBytes(guid, 2);

    LfgType type = LFG_TYPE_DUNGEON;
    for (LfgDungeonSet::const_iterator it = updateData.dungeons.begin(); it != updateData.dungeons.end(); ++it)
    {
        if (LFGDungeonEntry const* dungeon = sLFGDungeonStore.LookupEntry(*it & 0xFFFFFF))
            type = LfgType(dungeon->SubType);
        data << uint32(*it);
    }

    data.WriteGuidBytes(guid, 0, 1);

    data << uint32(JoinTime);
    data << uint8(type);
    data << uint32(3); // unk

    data.WriteGuidBytes(guid, 7);

    SendPacket(&data);
}

void WorldSession::SendLfgRoleChosen(ObjectGuid guid, uint8 roles)
{
    WorldPacket data(SMSG_LFG_ROLE_CHOSEN);

    data.WriteGuidMask(guid, 6, 2, 1, 7, 0);

    data.WriteBit(roles > 0);       // Ready

    data.WriteGuidMask(guid, 3, 5, 4);

    data.FlushBits();

    data.WriteGuidBytes(guid, 0, 3, 6);

    data << uint32(roles);          // Roles

    data.WriteGuidBytes(guid, 5, 1, 4, 2, 7);

    SendPacket(&data);
}

void WorldSession::SendLfgRoleCheckUpdate(LfgRoleCheck const& roleCheck)
{
    LfgDungeonSet dungeons;
    if (roleCheck.rDungeonId)
        dungeons.insert(roleCheck.rDungeonId);
    else
        dungeons = roleCheck.dungeons;

    TC_LOG_DEBUG("lfg", "SMSG_LFG_ROLE_CHECK_UPDATE %s", GetPlayerInfo().c_str());

    WorldPacket data(SMSG_LFG_ROLE_CHECK_UPDATE);

    ByteBuffer roleData;
    ObjectGuid Guid = roleCheck.leader;

    data << uint8(1);                                   // unk
    data << uint8(roleCheck.state);                     // Check result
    data.WriteBits(roleCheck.roles.size(), 21);         // Players in group

    if (!roleCheck.roles.empty())
    {
        for (LfgRolesMap::const_iterator it = roleCheck.roles.begin(); it != roleCheck.roles.end(); ++it)
        {
            ObjectGuid guid = it->first;
            Player* player = ObjectAccessor::FindConnectedPlayer(guid);
            uint8 level = player ? player->GetLevel() : 0;
            uint8 roles = it->second;

            data.WriteBit(roles > 0);                       // Ready

            data.WriteGuidMask(guid, 3, 0, 5, 2, 7, 1, 4, 6);

            roleData << uint8(level);                       // Level

            roleData.WriteGuidBytes(guid, 3, 6);

            roleData << uint32(roles);                      // Roles

            roleData.WriteGuidBytes(guid, 2, 4, 0, 1, 5, 7);
        }
    }

    data.WriteGuidMask(Guid, 3, 5);

    data.WriteBits(dungeons.size(), 22);

    data.WriteGuidMask(Guid, 0, 7, 6, 1, 4, 2);

    data.WriteBit(roleCheck.state == LFG_ROLECHECK_INITIALITING);

    data.FlushBits();

    data.WriteGuidBytes(Guid, 0);

    data.append(roleData);

    data.WriteGuidBytes(Guid, 1, 7, 6, 4, 3, 2, 5);

    if (!dungeons.empty())
        for (LfgDungeonSet::iterator it = dungeons.begin(); it != dungeons.end(); ++it)
            data << uint32(sLFGMgr->GetLFGDungeonEntry(*it)); // Dungeon

    SendPacket(&data);
}

void WorldSession::SendLfgJoinResult(LfgJoinResultData const& joinData)
{
    uint32 size = 0;
    ObjectGuid guid = GetPlayer()->GetGUID();
    uint32 queueId = sLFGMgr->GetQueueId(_player->GetGUID());
    for (LfgLockPartyMap::const_iterator it = joinData.lockmap.begin(); it != joinData.lockmap.end(); ++it)
        size += 8 + 4 + uint32(it->second.size()) * (4 + 4 + 4 + 4);

    TC_LOG_DEBUG("lfg", "SMSG_LFG_JOIN_RESULT %s checkResult: %u checkValue: %u", GetPlayerInfo().c_str(), joinData.result, joinData.state);

    WorldPacket data(SMSG_LFG_JOIN_RESULT);

    ByteBuffer lockData;

    data.WriteGuidMask(guid, 7, 6, 3, 0);

    data.WriteBits(joinData.lockmap.size(), 22);

    for (LfgLockPartyMap::const_iterator it = joinData.lockmap.begin(); it != joinData.lockmap.end(); ++it)
    {
        ObjectGuid playerGuid = it->first;

        data.WriteGuidMask(playerGuid, 3);

        data.WriteBits(it->second.size(), 20);

        data.WriteGuidMask(playerGuid, 6, 1, 4, 7, 2, 0, 5);

        lockData.WriteGuidBytes(playerGuid, 4);

        BuildPlayerLockDungeonBlock(lockData, it->second);

        lockData.WriteGuidBytes(playerGuid, 1, 0, 5, 7, 3, 6, 2);
    }

    data.WriteGuidMask(guid, 5, 1, 4, 2);

    data.FlushBits();

    data << uint8(joinData.result);

    data.append(lockData);

    data << uint8(joinData.state);

    data.WriteGuidBytes(guid, 2);

    data << uint32(time(NULL));         // Join date
    data << uint32(queueId);            // Queue Id
    data << uint32(3);                  // unk

    data.WriteGuidBytes(guid, 6, 4, 1, 0, 5, 7, 3);

    SendPacket(&data);
}

void WorldSession::SendLfgQueueStatus(LfgQueueStatusData const& queueData)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_QUEUE_STATUS %s dungeon: %u, waitTime: %d, "
        "avgWaitTime: %d, waitTimeTanks: %d, waitTimeHealer: %d, waitTimeDps: %d, "
        "queuedTime: %u, tanks: %u, healers: %u, dps: %u",
        GetPlayerInfo().c_str(), queueData.dungeonId, queueData.waitTime, queueData.waitTimeAvg,
        queueData.waitTimeTank, queueData.waitTimeHealer, queueData.waitTimeDps,
        queueData.queuedTime, queueData.tanks, queueData.healers, queueData.dps);

    WorldPacket data(SMSG_LFG_QUEUE_STATUS);

    ObjectGuid guid = _player->GetGUID();

    data.WriteGuidMask(guid, 4, 3, 5, 1, 2, 0, 6, 7);

    data << uint32(3); // unk

    data.WriteGuidBytes(guid, 0);

    data << uint32(queueData.queuedTime); // (not sure)

    data.WriteGuidBytes(guid, 4);

    data << int32(queueData.waitTime); // (not sure)

    // Tanks
    data << uint8(queueData.tanks);                        // Tanks needed
    data << int32(queueData.waitTimeTank);                 // Wait Tanks

    // Healers
    data << uint8(queueData.healers);                      // Healers needed
    data << int32(queueData.waitTimeHealer);               // Wait Healers

    // DPSs
    data << uint8(queueData.dps);                          // Dps needed
    data << int32(queueData.waitTimeDps);                  // Wait Dps

    data << uint32(queueData.JoinTime);
    data << uint32(queueData.queueId);

    data.WriteGuidBytes(guid, 1);

    data << int32(queueData.waitTimeAvg); // (not sure)

    data.WriteGuidBytes(guid, 7, 2);

    data << uint32(queueData.dungeonId);

    data.WriteGuidBytes(guid, 5, 3, 6);

    SendPacket(&data);
}

void WorldSession::SendLfgPlayerReward(LfgPlayerRewardData const& rewardData)
{
    if (!rewardData.rdungeonEntry || !rewardData.sdungeonEntry || !rewardData.quest)
        return;

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PLAYER_REWARD %s rdungeonEntry: %u, sdungeonEntry: %u, done: %u",
        GetPlayerInfo().c_str(), rewardData.rdungeonEntry, rewardData.sdungeonEntry, rewardData.done);

    Quest const* quest = rewardData.quest;
    if (!quest)
        return;

    std::vector<std::pair<uint8, bool>> Rewards;
    for (uint8 i = 0; i < QUEST_REWARD_ITEM_COUNT; ++i)
    {
        if (quest->RewardCurrencyId[i])
            Rewards.emplace_back(i, true);

        if (quest->RewardItemId[i])
            Rewards.emplace_back(i, false);
    }

    WorldPacket data(SMSG_LFG_PLAYER_REWARD);

    ByteBuffer rewardDatas;

    data << uint32(GetPlayer()->GetQuestMoneyReward(quest));
    data << uint32(rewardData.sdungeonEntry);       // Dungeon Finished (not sure)
    data << uint32(quest->XPValue(GetPlayer()->GetLevel()));
    data << uint32(rewardData.rdungeonEntry);       // Random Dungeon Finished (not sure)

    data.WriteBits(Rewards.size(), 20);

    for (auto & itr : Rewards)
    {
        uint8 slot = itr.first;
        bool IsCurrency = itr.second;

        uint32 rewardId = 0;
        uint32 rewardCount = 0;
        uint32 rewardBonusCount = 0;
        uint32 rewardDisplayId = 0;

        data.WriteBit(IsCurrency);

        if (IsCurrency)
        {
            uint32 currencyId = quest->RewardCurrencyId[slot];

            CurrencyTypesEntry const* currency = sCurrencyTypesStore.LookupEntry(currencyId);
            if (!currency)
                continue;

            int32 precision = currency->Flags & CURRENCY_ENTRY_FLAG_HIGH_PRECISION ? CURRENCY_PRECISION : 1;
            int32 currencyCount = quest->RewardCurrencyCount[slot] * precision;
            int32 bonusCurrencyCount = quest->RewardBonusCurrencyCount[slot] * precision;

            rewardId = currencyId;
            rewardCount = currencyCount;
            rewardBonusCount = bonusCurrencyCount;
            rewardDisplayId = 0;
        }
        else
        {
            uint32 itemId = quest->RewardItemId[slot];

            ItemTemplate const* item = sObjectMgr->GetItemTemplate(itemId);
            if (!item)
                continue;

            int32 itemCount = quest->RewardItemCount[slot];
            int32 itemDisplayId = item->GetDisplayId();

            rewardId = itemId;
            rewardCount = itemCount;
            rewardBonusCount = 0;
            rewardDisplayId = itemDisplayId;
        }

        rewardDatas << uint32(rewardId);
        rewardDatas << uint32(rewardBonusCount);
        rewardDatas << uint32(rewardDisplayId);
        rewardDatas << uint32(rewardCount);
    }

    data.FlushBits();
    data.append(rewardDatas);

    SendPacket(&data);
}

void WorldSession::SendLfgBootProposalUpdate(LfgPlayerBoot const& boot)
{
    ObjectGuid guid = boot.victim;
    LfgAnswer playerVote = boot.votes.find(GetPlayer()->GetGUID())->second;
    uint8 votesNum = 0;
    uint8 agreeNum = 0;
    uint32 secsleft = uint8((boot.cancelTime - time(NULL)) / 1000);
    for (LfgAnswerContainer::const_iterator it = boot.votes.begin(); it != boot.votes.end(); ++it)
    {
        if (it->second != LFG_ANSWER_PENDING)
        {
            ++votesNum;
            if (it->second == LFG_ANSWER_AGREE)
                ++agreeNum;
        }
    }
    TC_LOG_DEBUG("lfg", "SMSG_LFG_BOOT_PROPOSAL_UPDATE %s inProgress: %u - "
        "didVote: %u - agree: %u - victim: %s votes: %u - agrees: %u - left: %u - "
        "needed: %u - reason %s",
        GetPlayerInfo().c_str(), uint8(boot.inProgress), uint8(playerVote != LFG_ANSWER_PENDING),
        uint8(playerVote == LFG_ANSWER_AGREE), boot.victim.ToString().c_str(), votesNum, agreeNum,
        secsleft, LFG_GROUP_KICK_VOTES_NEEDED, boot.reason.c_str());

    WorldPacket data(SMSG_LFG_BOOT_PLAYER);

    data.WriteBit(0); // unk

    data.WriteGuidMask(guid, 3);

    data.WriteBit(0); // unk
    data.WriteBit(0); // unk
    data.WriteBit(0); // unk

    data.WriteGuidMask(guid, 6);

    data.WriteBits(boot.reason.size(), 8);

    data.WriteBit(0); // unk

    data.WriteGuidMask(guid, 1, 7, 5, 2, 0, 4);

    data.WriteGuidBytes(guid, 2, 4, 3, 6);

    data << uint32(0); // unk
    data << uint32(0); // unk

    data.WriteString(boot.reason);

    data.WriteGuidBytes(guid, 5, 0);

    data << uint32(0); // unk

    data.WriteGuidBytes(guid, 7);

    data << uint32(0); // unk

    data.WriteGuidBytes(guid, 1);

    /*data << uint8(boot.inProgress);                                 // Vote in progress
    data << uint8(agreeNum >= LFG_GROUP_KICK_VOTES_NEEDED);    // Did succeed
    data << uint8(playerVote != LFG_ANSWER_PENDING);           // Did Vote
    data << uint8(playerVote == LFG_ANSWER_AGREE);             // Agree
    data << uint32(votesNum);                                       // Total Votes
    data << uint32(agreeNum);                                       // Agree Count
    data << uint32(secsleft);                                       // Time Left
    data << uint32(LFG_GROUP_KICK_VOTES_NEEDED);               // Needed Votes*/

    SendPacket(&data);
}

void WorldSession::SendLfgUpdateProposal(LfgProposal const& proposal)
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    LfgProposalPlayerContainer::const_iterator itPlayer = proposal.players.find(guid);
    if (itPlayer == proposal.players.end())                  // Player MUST be in the proposal
        return;

    ObjectGuid playerGroupGUID = itPlayer->second.group;
    ObjectGuid proposalGroupGUID = proposal.group;
    bool silent = !proposal.isNew && playerGroupGUID == proposalGroupGUID;
    uint32 dungeonEntry = proposal.dungeonId;
    uint32 queueId = sLFGMgr->GetQueueId(_player->GetGUID());
    time_t JoinTime = sLFGMgr->GetQueueJoinTime(_player->GetGUID());

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PROPOSAL_UPDATE %s state: %u", GetPlayerInfo().c_str(), proposal.state);

    // show random dungeon if player selected random dungeon and it's not lfg group
    if (!silent)
    {
        LfgDungeonSet const& playerDungeons = sLFGMgr->GetSelectedDungeons(guid);
        if (!playerDungeons.empty() && playerDungeons.find(dungeonEntry) == playerDungeons.end())
            dungeonEntry = (*playerDungeons.begin());
    }

    dungeonEntry = sLFGMgr->GetLFGDungeonEntry(dungeonEntry);

    WorldPacket data(SMSG_LFG_PROPOSAL_UPDATE);

    ByteBuffer roleData;

    ObjectGuid dungeonGUID = ObjectGuid::Create<HighGuid::InstanceSave>(dungeonEntry);
    ObjectGuid playerGUID = guid;

    data.WriteGuidMask(dungeonGUID, 6, 0);

    data.WriteGuidMask(playerGUID, 1, 7, 5);

    data.WriteGuidMask(dungeonGUID, 5);

    data.WriteGuidMask(playerGUID, 4);

    data.WriteBit(0); // unk

    data.WriteGuidMask(dungeonGUID, 2);

    data.WriteGuidMask(playerGUID, 6);

    data.WriteGuidMask(dungeonGUID, 3, 7);

    data.WriteGuidMask(playerGUID, 3);

    data.WriteBits(proposal.players.size(), 21);
    for (LfgProposalPlayerContainer::const_iterator it = proposal.players.begin(); it != proposal.players.end(); ++it)
    {
        LfgProposalPlayer const& player = it->second;

        data.WriteBit(player.accept == LFG_ANSWER_AGREE);

        data.WriteBit(it->first == guid);

        if (!player.group)
        {
            data.WriteBit(0);
            data.WriteBit(0);
        }
        else
        {
            data.WriteBit(player.group == playerGroupGUID);     // Same group as the player
            data.WriteBit(player.group == proposal.group);      // Is group in dungeon
        }

        data.WriteBit(player.accept != LFG_ANSWER_PENDING);

        roleData << uint32(player.role);
    }

    data.WriteGuidMask(playerGUID, 2);

    data.WriteGuidMask(dungeonGUID, 4);

    data.WriteBit(0); // unk

    data.WriteGuidMask(playerGUID, 0);

    data.WriteGuidMask(dungeonGUID, 1);

    data.WriteGuidBytes(dungeonGUID, 1);

    data.WriteGuidBytes(playerGUID, 4);

    data.WriteGuidBytes(dungeonGUID, 4);

    data.WriteGuidBytes(playerGUID, 7, 2, 0);

    data << uint32(dungeonEntry);
    data << uint8(proposal.state);
    data << uint32(queueId);

    data.WriteGuidBytes(dungeonGUID, 6);

    data << uint32(proposal.id);

    data.WriteGuidBytes(playerGUID, 5, 3);

    data << uint32(JoinTime);

    data.WriteGuidBytes(dungeonGUID, 5);

    data.WriteGuidBytes(playerGUID, 6);

    data.FlushBits();
    data.append(roleData);

    data << uint32(proposal.encounters);

    data.WriteGuidBytes(dungeonGUID, 7);

    data.WriteGuidBytes(playerGUID, 1);

    data.WriteGuidBytes(dungeonGUID, 0, 2);

    data << uint32(3); // unk

    data.WriteGuidBytes(dungeonGUID, 3);

    SendPacket(&data);
}

void WorldSession::HandleSetLFGBonusReputationOpcode(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: Received CMSG_SET_LFG_BONUS_FACTION_ID");

    uint32 reputationId;

    recvData >> reputationId;

    GetPlayer()->SetLFGBonusReputation(reputationId);
}

void WorldSession::SendLfgLfrList(bool update)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_LFR_LIST %s update: %u", GetPlayerInfo().c_str(), update ? 1 : 0);

    WorldPacket data(SMSG_LFG_SEARCH_RESULT, 1);
    data << uint8(update);                                 // In Lfg Queue?
    SendPacket(&data);
}

void WorldSession::SendLfgDisabled()
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_DISABLED %s", GetPlayerInfo().c_str());

    WorldPacket data(SMSG_LFG_DISABLED, 0);
    SendPacket(&data);
}

void WorldSession::SendLfgOfferContinue(uint32 dungeonEntry)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_OFFER_CONTINUE %s dungeon entry: %u", GetPlayerInfo().c_str(), dungeonEntry);

    WorldPacket data(SMSG_LFG_OFFER_CONTINUE, 4);
    data << uint32(dungeonEntry);
    SendPacket(&data);
}

void WorldSession::SendLfgTeleportError(uint8 err)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_TELEPORT_DENIED %s reason: %u", GetPlayerInfo().c_str(), err);

    WorldPacket data(SMSG_LFG_TELEPORT_DENIED, 4);
    data << uint32(err);                                   // Error
    SendPacket(&data);
}

/*
void WorldSession::SendLfrUpdateListOpcode(uint32 dungeonEntry)
{
    TC_LOG_DEBUG("network", "SMSG_LFG_UPDATE_LIST %s dungeon entry: %u", GetPlayerInfo().c_str(), dungeonEntry);

    WorldPacket data(SMSG_LFG_UPDATE_LIST);
    SendPacket(&data);
}
*/

void WorldSession::HandleLFGGetJoinStatus(WorldPacket& /*recvData*/)
{
    // ToDO: finish.
    // Fires on player login, checks if player was in lfg group before he's been disconnected
    // Followed with SendLfgLfrList
}
