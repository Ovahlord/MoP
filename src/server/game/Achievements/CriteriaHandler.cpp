/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CriteriaHandler.h"
#include "Battleground.h"
#include "DBCStores.h"
#include "DB2Stores.h"
#include "DisableMgr.h"
#include "GameEventMgr.h"
#include "Group.h"
#include "Guild.h"
#include "InstanceScript.h"
#include "MapManager.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "RatedMgr.h"
#include "RatedInfo.h"
#include "ReputationMgr.h"
#include "ScriptMgr.h"
#include "SpellInfo.h"
#include "SpellMgr.h"
#include "BattlePet.h"
#include "Scenario.h"

CriteriaHandler::CriteriaHandler() { }

CriteriaHandler::~CriteriaHandler() { }

/**
 * this function will be called whenever the user might have done a criteria relevant action
 */
void CriteriaHandler::UpdateCriteria(CriteriaTypes type, uint64 miscValue1 /*= 0*/, uint64 miscValue2 /*= 0*/, uint64 miscValue3 /*= 0*/, Unit const* unit /*= nullptr*/, Player* referencePlayer /*= nullptr*/)
{
    if (type >= CRITERIA_TYPE_TOTAL)
    {
        TC_LOG_ERROR("criteria", "CriteriaHandler::UpdateCriteria: Wrong criteria type %u", type);
        return;
    }

    if (!referencePlayer)
    {
        TC_LOG_DEBUG("criteria", "CriteriaHandler::UpdateCriteria: Player is NULL! Cant update criteria");
        return;
    }

    // disable for gamemasters with GM-mode enabled
    if (referencePlayer->IsGameMaster())
    {
        TC_LOG_DEBUG("criteria", "CriteriaHandler::UpdateCriteria: [Player %s GM mode on] %s, %s (%u), " UI64FMTD ", " UI64FMTD ", " UI64FMTD,
            referencePlayer->GetName().c_str(), GetOwnerInfo().c_str(), CriteriaMgr::GetCriteriaTypeString(type), type, miscValue1, miscValue2, miscValue3);
        return;
    }

    TC_LOG_DEBUG("criteria", "CriteriaHandler::UpdateCriteria(%s, %u, " UI64FMTD ", " UI64FMTD ", " UI64FMTD ") %s",
        CriteriaMgr::GetCriteriaTypeString(type), type, miscValue1, miscValue2, miscValue3, GetOwnerInfo().c_str());

    CriteriaList const& criteriaList = GetCriteriaByType(type);
    for (Criteria const* criteria : criteriaList)
    {
        CriteriaTreeList const* trees = sCriteriaMgr->GetCriteriaTreesByCriteria(criteria->ID);
        if (!CanUpdateCriteria(criteria, trees, miscValue1, miscValue2, miscValue3, unit, referencePlayer))
            continue;

        switch (type)
        {
            // std. case: increment at 1
            case CRITERIA_TYPE_NUMBER_OF_TALENT_RESETS:
            case CRITERIA_TYPE_LOSE_DUEL:
            case CRITERIA_TYPE_CREATE_AUCTION:
            case CRITERIA_TYPE_WON_AUCTIONS:
            case CRITERIA_TYPE_ROLL_NEED:
            case CRITERIA_TYPE_ROLL_GREED:
            case CRITERIA_TYPE_QUEST_ABANDONED:
            case CRITERIA_TYPE_FLIGHT_PATHS_TAKEN:
            case CRITERIA_TYPE_ACCEPTED_SUMMONINGS:
            case CRITERIA_TYPE_LOOT_EPIC_ITEM:
            case CRITERIA_TYPE_RECEIVE_EPIC_ITEM:
            case CRITERIA_TYPE_DEATH:
            case CRITERIA_TYPE_COMPLETE_DAILY_QUEST:
            case CRITERIA_TYPE_DEATH_AT_MAP:
            case CRITERIA_TYPE_DEATH_IN_DUNGEON:
            case CRITERIA_TYPE_COMPLETE_RAID:
            case CRITERIA_TYPE_KILLED_BY_CREATURE:
            case CRITERIA_TYPE_KILLED_BY_PLAYER:
            case CRITERIA_TYPE_DEATHS_FROM:
            case CRITERIA_TYPE_BE_SPELL_TARGET:
            case CRITERIA_TYPE_BE_SPELL_TARGET2:
            case CRITERIA_TYPE_CAST_SPELL:
            case CRITERIA_TYPE_CAST_SPELL2:
            case CRITERIA_TYPE_WIN_RATED_ARENA:
            case CRITERIA_TYPE_USE_ITEM:
            case CRITERIA_TYPE_ROLL_NEED_ON_LOOT:
            case CRITERIA_TYPE_ROLL_GREED_ON_LOOT:
            case CRITERIA_TYPE_DO_EMOTE:
            case CRITERIA_TYPE_USE_GAMEOBJECT:
            case CRITERIA_TYPE_FISH_IN_GAMEOBJECT:
            case CRITERIA_TYPE_WIN_DUEL:
            case CRITERIA_TYPE_HK_CLASS:
            case CRITERIA_TYPE_HK_RACE:
            case CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE:
            case CRITERIA_TYPE_HONORABLE_KILL:
            case CRITERIA_TYPE_SPECIAL_PVP_KILL:
            case CRITERIA_TYPE_GET_KILLING_BLOWS:
            case CRITERIA_TYPE_HONORABLE_KILL_AT_AREA:
            case CRITERIA_TYPE_WIN_ARENA: // This also behaves like CRITERIA_TYPE_WIN_RATED_ARENA
            case CRITERIA_TYPE_ON_LOGIN:
            case CRITERIA_TYPE_OWN_BATTLE_PET_COUNT:
            case CRITERIA_TYPE_WIN_PET_BATTLE:
            case CRITERIA_TYPE_COMPLETE_ARCHAEOLOGY_PROJECTS:
            case CRITERIA_TYPE_WIN_BG:
            case CRITERIA_TYPE_COMPLETE_BATTLEGROUND:
            case CRITERIA_TYPE_PLAY_ARENA:
            case CRITERIA_TYPE_KILL_CREATURE_TYPE:
            case CRITERIA_TYPE_USE_LFD_TO_GROUP_WITH_PLAYERS:
            case CRITERIA_TYPE_LFG_COMPLETED:
            case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_INIT:
            case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_NO_INIT:
            case CRITERIA_TYPE_LFG_KICKED_BY_OTHERS:
            case CRITERIA_TYPE_LFG_ABANDONED:
            case CRITERIA_TYPE_CATCH_FROM_POOL:
            case CRITERIA_TYPE_BUY_GUILD_BANK_SLOTS:
            case CRITERIA_TYPE_WIN_RATED_BATTLEGROUND:
            case CRITERIA_TYPE_COMPLETE_QUESTS_GUILD:
            case CRITERIA_TYPE_HONORABLE_KILLS_GUILD:
            case CRITERIA_TYPE_KILL_CREATURE_TYPE_GUILD:
            case CRITERIA_TYPE_LFG_TANK_LEFT:
            case CRITERIA_TYPE_VISIT_BARBER_SHOP:
            case CRITERIA_TYPE_EQUIP_EPIC_ITEM:
            case CRITERIA_TYPE_EQUIP_ITEM:
            case CRITERIA_TYPE_RELEASE_SPIRIT:
            case CRITERIA_TYPE_OBTAIN_BATTLE_PET:
            case CRITERIA_TYPE_OWN_BATTLE_PET:
            case CRITERIA_TYPE_CAPTURE_BATTLE_PET:
            case CRITERIA_TYPE_COMPLETE_SCENARIO_COUNT:
            case CRITERIA_TYPE_COMPLETE_SCENARIO:
            case CRITERIA_TYPE_UPDATED_CRITERIA_ID:
                SetCriteriaProgress(criteria, 1, referencePlayer, PROGRESS_ACCUMULATE);
                break;
            // std case: increment at miscValue1
            case CRITERIA_TYPE_MONEY_FROM_VENDORS:
            case CRITERIA_TYPE_GOLD_SPENT_FOR_TALENTS:
            case CRITERIA_TYPE_MONEY_FROM_QUEST_REWARD:
            case CRITERIA_TYPE_GOLD_SPENT_FOR_TRAVELLING:
            case CRITERIA_TYPE_GOLD_SPENT_AT_BARBER:
            case CRITERIA_TYPE_GOLD_SPENT_FOR_MAIL:
            case CRITERIA_TYPE_LOOT_MONEY:
            case CRITERIA_TYPE_GOLD_EARNED_BY_AUCTIONS:
            case CRITERIA_TYPE_TOTAL_DAMAGE_RECEIVED:
            case CRITERIA_TYPE_TOTAL_HEALING_RECEIVED:
            case CRITERIA_TYPE_DAMAGE_DONE:
            case CRITERIA_TYPE_HEALING_DONE:
            case CRITERIA_TYPE_SPENT_GOLD_GUILD_REPAIRS:
            case CRITERIA_TYPE_REACH_BG_RATING:
                SetCriteriaProgress(criteria, miscValue1, referencePlayer, PROGRESS_ACCUMULATE);
                break;
            case CRITERIA_TYPE_KILL_CREATURE:
            case CRITERIA_TYPE_LOOT_TYPE:
            case CRITERIA_TYPE_LOOT_ITEM:
            case CRITERIA_TYPE_CURRENCY:
            case CRITERIA_TYPE_GUILD_CRAFT_ITEMS:
            case CRITERIA_TYPE_GUILD_COOK_RECIPE:
            case CRITERIA_TYPE_OWN_ITEM:
                SetCriteriaProgress(criteria, miscValue2, referencePlayer, PROGRESS_ACCUMULATE);
                break;
            // std case: high value at miscValue1
            case CRITERIA_TYPE_HIGHEST_AUCTION_BID:
            case CRITERIA_TYPE_HIGHEST_AUCTION_SOLD:
            case CRITERIA_TYPE_HIGHEST_HIT_DEALT:
            case CRITERIA_TYPE_HIGHEST_HIT_RECEIVED:
            case CRITERIA_TYPE_HIGHEST_HEAL_CAST:
            case CRITERIA_TYPE_HIGHEST_HEALING_RECEIVED:
                SetCriteriaProgress(criteria, miscValue1, referencePlayer, PROGRESS_HIGHEST);
                break;
            case CRITERIA_TYPE_REACH_LEVEL:
                SetCriteriaProgress(criteria, referencePlayer->GetLevel(), referencePlayer);
                break;
            case CRITERIA_TYPE_REACH_SKILL_LEVEL:
                if (uint32 skillvalue = referencePlayer->GetBaseSkillValue(criteria->Entry->Asset.SkillID))
                    SetCriteriaProgress(criteria, skillvalue, referencePlayer);
                break;
            case CRITERIA_TYPE_LEARN_SKILL_LEVEL:
                if (uint32 maxSkillvalue = referencePlayer->GetPureMaxSkillValue(criteria->Entry->Asset.SkillID))
                    SetCriteriaProgress(criteria, maxSkillvalue, referencePlayer);
                break;
            case CRITERIA_TYPE_COMPLETE_QUEST_COUNT:
                SetCriteriaProgress(criteria, referencePlayer->GetRewardedQuestCount(), referencePlayer);
                break;
            case CRITERIA_TYPE_COMPLETE_DAILY_QUEST_DAILY:
            {
                time_t nextDailyResetTime = sWorld->GetNextDailyQuestsResetTime();
                CriteriaProgress *progress = GetCriteriaProgress(criteria);

                if (!miscValue1) // Login case.
                {
                    // reset if player missed one day.
                    if (progress && progress->Date < (nextDailyResetTime - 2 * DAY))
                        SetCriteriaProgress(criteria, 0, referencePlayer, PROGRESS_SET);
                    continue;
                }

                if (progress && progress->DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
                    continue;

                ProgressType progressType;
                if (!progress)
                    // 1st time. Start count.
                    progressType = PROGRESS_SET;
                else if (progress->Date < (nextDailyResetTime - 2 * DAY))
                    // last progress is older than 2 days. Player missed 1 day => Restart count.
                    progressType = PROGRESS_SET;
                else if (progress->Date < (nextDailyResetTime - DAY))
                    // last progress is between 1 and 2 days. => 1st time of the day.
                    progressType = PROGRESS_ACCUMULATE;
                else
                    // last progress is within the day before the reset => Already counted today.
                    continue;

                SetCriteriaProgress(criteria, 1, referencePlayer, progressType);
                break;
            }
            case CRITERIA_TYPE_COMPLETE_QUESTS_IN_ZONE:
            {
                uint32 counter = 0;

                const RewardedQuestSet &rewQuests = referencePlayer->getRewardedQuests();
                for (RewardedQuestSet::const_iterator itr = rewQuests.begin(); itr != rewQuests.end(); ++itr)
                {
                    Quest const* quest = sObjectMgr->GetQuestTemplate(*itr);
                    if (quest && quest->GetZoneOrSort() >= 0 && uint32(quest->GetZoneOrSort()) == criteria->Entry->Asset.ZoneID)
                        ++counter;
                }
                SetCriteriaProgress(criteria, counter, referencePlayer);
                break;
            }
            case CRITERIA_TYPE_FALL_WITHOUT_DYING:
                // miscValue1 is the ingame fallheight*100 as stored in dbc
                SetCriteriaProgress(criteria, miscValue1, referencePlayer);
                break;
            case CRITERIA_TYPE_COMPLETE_QUEST:
            case CRITERIA_TYPE_LEARN_SPELL:
            case CRITERIA_TYPE_EXPLORE_AREA:
            case CRITERIA_TYPE_COMPLETE_ACHIEVEMENT:
            case CRITERIA_TYPE_OWN_PVP_HK_RANK:
            case CRITERIA_TYPE_BUY_GUILD_TABARD:
                SetCriteriaProgress(criteria, 1, referencePlayer);
                break;
            case CRITERIA_TYPE_BUY_BANK_SLOT:
                SetCriteriaProgress(criteria, referencePlayer->GetBankBagSlotCount(), referencePlayer);
                break;
            case CRITERIA_TYPE_GAIN_REPUTATION:
            {
                int32 reputation = referencePlayer->GetReputationMgr().GetReputation(criteria->Entry->Asset.FactionID);
                if (reputation > 0)
                    SetCriteriaProgress(criteria, reputation, referencePlayer);
                break;
            }
            case CRITERIA_TYPE_GAIN_EXALTED_REPUTATION:
                SetCriteriaProgress(criteria, referencePlayer->GetReputationMgr().GetExaltedFactionCount(), referencePlayer);
                break;
            case CRITERIA_TYPE_LEARN_SKILLLINE_SPELLS:
            case CRITERIA_TYPE_LEARN_SKILL_LINE:
            {
                uint32 spellCount = 0;
                for (PlayerSpellMap::const_iterator spellIter = referencePlayer->GetSpellMap().begin();
                    spellIter != referencePlayer->GetSpellMap().end();
                    ++spellIter)
                {
                    SkillLineAbilityMapBounds bounds = sSpellMgr->GetSkillLineAbilityMapBounds(spellIter->first);
                    for (SkillLineAbilityMap::const_iterator skillIter = bounds.first; skillIter != bounds.second; ++skillIter)
                    {
                        if (skillIter->second->SkillLine == criteria->Entry->Asset.SkillID)
                            spellCount++;
                    }
                }
                SetCriteriaProgress(criteria, spellCount, referencePlayer);
                break;
            }
            case CRITERIA_TYPE_GAIN_REVERED_REPUTATION:
                SetCriteriaProgress(criteria, referencePlayer->GetReputationMgr().GetReveredFactionCount(), referencePlayer);
                break;
            case CRITERIA_TYPE_GAIN_HONORED_REPUTATION:
                SetCriteriaProgress(criteria, referencePlayer->GetReputationMgr().GetHonoredFactionCount(), referencePlayer);
                break;
            case CRITERIA_TYPE_KNOWN_FACTIONS:
                SetCriteriaProgress(criteria, referencePlayer->GetReputationMgr().GetVisibleFactionCount(), referencePlayer);
                break;
            case CRITERIA_TYPE_EARN_HONORABLE_KILL:
                SetCriteriaProgress(criteria, referencePlayer->GetUInt32Value(PLAYER_LIFETIME_HONORABLE_KILLS), referencePlayer);
                break;
            case CRITERIA_TYPE_HIGHEST_GOLD_VALUE_OWNED:
                SetCriteriaProgress(criteria, referencePlayer->GetMoney(), referencePlayer, PROGRESS_HIGHEST);
                break;
            case CRITERIA_TYPE_EARN_ACHIEVEMENT_POINTS:
            case CRITERIA_TYPE_EARN_GUILD_ACHIEVEMENT_POINTS:
            case CRITERIA_TYPE_EARN_PET_BATTLE_ACHIEVEMENT_POINTS:
                if (!miscValue1)
                    continue;
                SetCriteriaProgress(criteria, miscValue1, referencePlayer, PROGRESS_ACCUMULATE);
                break;
            case CRITERIA_TYPE_REACH_GUILD_LEVEL:
                SetCriteriaProgress(criteria, miscValue1, referencePlayer);
                break;
            case CRITERIA_TYPE_LEVEL_BATTLE_PET:
                SetCriteriaProgress(criteria, miscValue2, referencePlayer);
                break;
            case CRITERIA_TYPE_HIGHEST_TEAM_RATING:
            case CRITERIA_TYPE_HIGHEST_PERSONAL_RATING:
                SetCriteriaProgress(criteria, miscValue1, referencePlayer, PROGRESS_HIGHEST);
                break;
            // FIXME: not triggered in code as result, need to implement
            case CRITERIA_TYPE_LFR_DUNGEONS_COMPLETED:
            case CRITERIA_TYPE_LFR_LEAVES:
            case CRITERIA_TYPE_LFR_VOTE_KICKS_INITIATED_BY_PLAYER:
            case CRITERIA_TYPE_LFR_VOTE_KICKS_NOT_INIT_BY_PLAYER:
            case CRITERIA_TYPE_BE_KICKED_FROM_LFR:
            case CRITERIA_TYPE_COUNT_OF_LFR_QUEUE_BOOSTS_BY_TANK:
            case CRITERIA_TYPE_CAPTURE_BATTLE_PET_CREDIT:
            case CRITERIA_TYPE_LEVEL_BATTLE_PET_CREDIT:
                break;                                   // Not implemented yet :(
        }

        for (CriteriaTree const* tree : *trees)
        {
            if (IsCompletedCriteriaTree(tree))
                CompletedCriteriaTree(tree, referencePlayer);

            AfterCriteriaTreeUpdate(tree, referencePlayer);
        }
    }
}

void CriteriaHandler::UpdateTimedCriteria(uint32 timeDiff)
{
    if (!_timeCriteriaTrees.empty())
    {
        for (auto itr = _timeCriteriaTrees.begin(); itr != _timeCriteriaTrees.end();)
        {
            // Time is up, remove timer and reset progress
            if (itr->second <= timeDiff)
            {
                CriteriaTree const* criteriaTree = sCriteriaMgr->GetCriteriaTree(itr->first);
                if (criteriaTree->Criteria)
                    RemoveCriteriaProgress(criteriaTree->Criteria);

                itr = _timeCriteriaTrees.erase(itr);
            }
            else
            {
                itr->second -= timeDiff;
                ++itr;
            }
        }
    }
}

void CriteriaHandler::StartCriteriaTimer(CriteriaTimedTypes type, uint32 entry, uint32 timeLost /* = 0 */)
{
    CriteriaList const& criteriaList = sCriteriaMgr->GetTimedCriteriaByType(type);
    for (Criteria const* criteria : criteriaList)
    {
        if (criteria->Entry->StartAsset != entry)
            continue;

        CriteriaTreeList const* trees = sCriteriaMgr->GetCriteriaTreesByCriteria(criteria->ID);
        bool canStart = false;
        for (CriteriaTree const* tree : *trees)
        {
            if (_timeCriteriaTrees.find(tree->ID) == _timeCriteriaTrees.end() && !IsCompletedCriteriaTree(tree))
            {
                // Start the timer
                if (criteria->Entry->StartTimer * IN_MILLISECONDS > timeLost)
                {
                    _timeCriteriaTrees[tree->ID] = criteria->Entry->StartTimer * IN_MILLISECONDS - timeLost;
                    canStart = true;
                }
            }
        }

        if (!canStart)
            continue;

        // and at client too
        SetCriteriaProgress(criteria, 0, nullptr, PROGRESS_SET);
    }
}

void CriteriaHandler::RemoveCriteriaTimer(CriteriaTimedTypes type, uint32 entry)
{
    CriteriaList const& criteriaList = sCriteriaMgr->GetTimedCriteriaByType(type);
    for (Criteria const* criteria : criteriaList)
    {
        if (criteria->Entry->StartAsset != entry)
            continue;

        CriteriaTreeList const* trees = sCriteriaMgr->GetCriteriaTreesByCriteria(criteria->ID);
        // Remove the timer from all trees
        for (CriteriaTree const* tree : *trees)
            _timeCriteriaTrees.erase(tree->ID);

        // remove progress
        RemoveCriteriaProgress(criteria);
    }
}

CriteriaProgress* CriteriaHandler::GetCriteriaProgress(Criteria const* entry)
{
    auto iter = _criteriaProgress.find(entry->ID);
    if (iter == _criteriaProgress.end())
        return nullptr;

    return &iter->second;
}

void CriteriaHandler::SetCriteriaProgress(Criteria const* criteria, uint64 changeValue, Player* referencePlayer, ProgressType progressType)
{
    // Don't allow to cheat - doing timed criteria without timer active
    CriteriaTreeList const* trees = nullptr;
    if (criteria->Entry->StartTimer)
    {
        trees = sCriteriaMgr->GetCriteriaTreesByCriteria(criteria->ID);
        if (!trees)
            return;

        bool hasTreeForTimed = false;
        for (CriteriaTree const* tree : *trees)
        {
            auto timedIter = _timeCriteriaTrees.find(tree->ID);
            if (timedIter != _timeCriteriaTrees.end())
            {
                hasTreeForTimed = true;
                break;
            }
        }

        if (!hasTreeForTimed)
            return;
    }

    TC_LOG_DEBUG("criteria", "CriteriaHandler::SetCriteriaProgress(%u, " UI64FMTD ") for %s", criteria->ID, changeValue, GetOwnerInfo().c_str());

    CriteriaProgress* progress = GetCriteriaProgress(criteria);
    if (!progress)
    {
        // not create record for 0 counter but allow it for timed criteria
        // we will need to send 0 progress to client to start the timer
        if (changeValue == 0 && !criteria->Entry->StartTimer)
            return;

        progress = &_criteriaProgress[criteria->ID];
        progress->Counter = changeValue;
        progress->DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NEW;
    }
    else
    {
        uint64 newValue = 0;
        switch (progressType)
        {
            case PROGRESS_SET:
                newValue = changeValue;
                break;
            case PROGRESS_ACCUMULATE:
            {
                // avoid overflow
                uint64 max_value = std::numeric_limits<uint64>::max();
                newValue = max_value - progress->Counter > changeValue ? progress->Counter + changeValue : max_value;
                break;
            }
            case PROGRESS_HIGHEST:
                newValue = progress->Counter < changeValue ? changeValue : progress->Counter;
                break;
            }

        // not update (not mark as changed) if counter will have same value
        if (progress->Counter == newValue && !criteria->Entry->StartTimer)
            return;

        progress->Counter = newValue;
        if (progress->LoadedFromDB)
            progress->DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_UPDATED;
    }

    progress->Date = time(NULL); // set the date to the latest update.

    uint32 timeElapsed = 0;

    if (criteria->Entry->StartTimer)
    {
        ASSERT(trees);

        for (CriteriaTree const* tree : *trees)
        {
            auto timedIter = _timeCriteriaTrees.find(tree->ID);
            if (timedIter != _timeCriteriaTrees.end())
            {
                // Client expects this in packet
                timeElapsed = criteria->Entry->StartTimer - (timedIter->second / IN_MILLISECONDS);

                // Remove the timer, we wont need it anymore
                if (IsCompletedCriteriaTree(tree))
                    _timeCriteriaTrees.erase(timedIter);
            }
        }
    }

    if (criteria->FlagsCu & CRITERIA_FLAG_CU_SHOW_CRITERIA_MEMBERS)
        progress->PlayerGUID = referencePlayer ? referencePlayer->GetGUID() : ObjectGuid::Empty;

    if (criteria->FlagsCu & CRITERIA_FLAG_CU_ACCOUNT)
        SendAccountCriteriaUpdate(criteria, progress, timeElapsed, true);
    else
        SendCriteriaUpdate(criteria, progress, timeElapsed, true);
}

void CriteriaHandler::RemoveCriteriaProgress(Criteria const* criteria)
{
    if (!criteria)
        return;

    auto criteriaProgress = _criteriaProgress.find(criteria->ID);
    if (criteriaProgress == _criteriaProgress.end())
        return;

    if (criteriaProgress->second.DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
        return;

    SendCriteriaProgressRemoved(criteria->ID);

    criteriaProgress->second.DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE;
}

bool CriteriaHandler::IsCompletedCriteriaTree(CriteriaTree const* tree)
{
    if (!CanCompleteCriteriaTree(tree))
        return false;

    uint64 requiredCount = tree->Entry->CriteriaCount;
    switch (tree->Entry->Operator)
    {
        case CRITERIA_TREE_OPERATOR_SINGLE:
            return tree->Criteria && IsCompletedCriteria(tree->Criteria, requiredCount);
        case CRITERIA_TREE_OPERATOR_SINGLE_NOT_COMPLETED:
            return !tree->Criteria || !IsCompletedCriteria(tree->Criteria, requiredCount);
        case CRITERIA_TREE_OPERATOR_ALL:
            for (CriteriaTree const* node : tree->Children)
                if (!IsCompletedCriteriaTree(node))
                    return false;
            return true;
        case CRITERIA_TREE_OPERAROR_SUM_CHILDREN:
        {
            uint64 progress = 0;
            CriteriaMgr::WalkCriteriaTree(tree, [this, &progress](CriteriaTree const* criteriaTree)
            {
                if (criteriaTree->Criteria)
                    if (CriteriaProgress const* criteriaProgress = GetCriteriaProgress(criteriaTree->Criteria))
                        progress += criteriaProgress->Counter;
            });
            return progress >= requiredCount;
        }
        case CRITERIA_TREE_OPERATOR_MAX_CHILD:
        {
            uint64 progress = 0;
            CriteriaMgr::WalkCriteriaTree(tree, [this, &progress](CriteriaTree const* criteriaTree)
            {
                if (criteriaTree->Criteria)
                    if (CriteriaProgress const* criteriaProgress = GetCriteriaProgress(criteriaTree->Criteria))
                        if (criteriaProgress->Counter > progress)
                            progress = criteriaProgress->Counter;
            });
            return progress >= requiredCount;
        }
        case CRITERIA_TREE_OPERATOR_COUNT_DIRECT_CHILDREN:
        {
            uint64 progress = 0;
            for (CriteriaTree const* node : tree->Children)
                if (node->Criteria)
                    if (CriteriaProgress const* criteriaProgress = GetCriteriaProgress(node->Criteria))
                        if (criteriaProgress->Counter >= 1)
                            if (++progress >= requiredCount)
                                return true;

            return false;
        }
        case CRITERIA_TREE_OPERATOR_ANY:
        {
            uint64 progress = 0;
            for (CriteriaTree const* node : tree->Children)
                if (IsCompletedCriteriaTree(node))
                    if (++progress >= requiredCount)
                        return true;

            return false;
        }
        default:
            break;
    }

    return false;
}

bool CriteriaHandler::CanUpdateCriteriaTree(Criteria const* criteria, CriteriaTree const* tree, Player* referencePlayer) const
{
    if ((tree->Entry->Flags & CRITERIA_TREE_FLAG_HORDE_ONLY && referencePlayer->GetTeam() != HORDE) ||
        (tree->Entry->Flags & CRITERIA_TREE_FLAG_ALLIANCE_ONLY && referencePlayer->GetTeam() != ALLIANCE))
    {
        TC_LOG_TRACE("criteria", "CriteriaHandler::CanUpdateCriteriaTree: (Id: %u Type %s CriteriaTree %u) Wrong faction",
            criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type), tree->Entry->ID);
        return false;
    }

    return true;
}

bool CriteriaHandler::CanCompleteCriteriaTree(CriteriaTree const* /*tree*/)
{
    return true;
}

bool CriteriaHandler::IsCompletedCriteria(Criteria const* criteria, uint64 requiredAmount)
{
    CriteriaProgress const* progress = GetCriteriaProgress(criteria);
    if (!progress)
        return false;

    if (progress->DBStatus == ACHIEVEMENT_OR_CRITERIA_DB_STATUS_REMOVE)
        return false;

    switch (CriteriaTypes(criteria->Entry->Type))
    {
        case CRITERIA_TYPE_WIN_BG:
        case CRITERIA_TYPE_KILL_CREATURE:
        case CRITERIA_TYPE_COMPLETE_ARCHAEOLOGY_PROJECTS:
        case CRITERIA_TYPE_REACH_LEVEL:
        case CRITERIA_TYPE_REACH_GUILD_LEVEL:
        case CRITERIA_TYPE_REACH_SKILL_LEVEL:
        case CRITERIA_TYPE_COMPLETE_QUEST_COUNT:
        case CRITERIA_TYPE_COMPLETE_DAILY_QUEST_DAILY:
        case CRITERIA_TYPE_COMPLETE_QUESTS_IN_ZONE:
        case CRITERIA_TYPE_DAMAGE_DONE:
        case CRITERIA_TYPE_HEALING_DONE:
        case CRITERIA_TYPE_COMPLETE_DAILY_QUEST:
        case CRITERIA_TYPE_FALL_WITHOUT_DYING:
        case CRITERIA_TYPE_BE_SPELL_TARGET2:
        case CRITERIA_TYPE_CAST_SPELL2:
        case CRITERIA_TYPE_HONORABLE_KILL_AT_AREA:
        case CRITERIA_TYPE_WIN_RATED_ARENA:
        case CRITERIA_TYPE_BUY_BANK_SLOT:
        case CRITERIA_TYPE_GAIN_REPUTATION:
        case CRITERIA_TYPE_GAIN_EXALTED_REPUTATION:
        case CRITERIA_TYPE_VISIT_BARBER_SHOP:
        case CRITERIA_TYPE_EQUIP_EPIC_ITEM:
        case CRITERIA_TYPE_ROLL_NEED_ON_LOOT:
        case CRITERIA_TYPE_ROLL_GREED_ON_LOOT:
        case CRITERIA_TYPE_HK_CLASS:
        case CRITERIA_TYPE_HK_RACE:
        case CRITERIA_TYPE_DO_EMOTE:
        case CRITERIA_TYPE_EQUIP_ITEM:
        case CRITERIA_TYPE_MONEY_FROM_QUEST_REWARD:
        case CRITERIA_TYPE_LOOT_MONEY:
        case CRITERIA_TYPE_USE_GAMEOBJECT:
        case CRITERIA_TYPE_SPECIAL_PVP_KILL:
        case CRITERIA_TYPE_FISH_IN_GAMEOBJECT:
        case CRITERIA_TYPE_LEARN_SKILLLINE_SPELLS:
        case CRITERIA_TYPE_WIN_DUEL:
        case CRITERIA_TYPE_LOOT_TYPE:
        case CRITERIA_TYPE_LEARN_SKILL_LINE:
        case CRITERIA_TYPE_USE_LFD_TO_GROUP_WITH_PLAYERS:
        case CRITERIA_TYPE_GET_KILLING_BLOWS:
        case CRITERIA_TYPE_CURRENCY:
        case CRITERIA_TYPE_OWN_BATTLE_PET_COUNT:
        case CRITERIA_TYPE_WIN_PET_BATTLE:
        case CRITERIA_TYPE_EARN_ACHIEVEMENT_POINTS:
        case CRITERIA_TYPE_EARN_GUILD_ACHIEVEMENT_POINTS:
        case CRITERIA_TYPE_EARN_PET_BATTLE_ACHIEVEMENT_POINTS:
        case CRITERIA_TYPE_GUILD_COOK_RECIPE:
        case CRITERIA_TYPE_SPENT_GOLD_GUILD_REPAIRS:
        case CRITERIA_TYPE_GUILD_CRAFT_ITEMS:
        case CRITERIA_TYPE_CATCH_FROM_POOL:
        case CRITERIA_TYPE_BUY_GUILD_BANK_SLOTS:
        case CRITERIA_TYPE_WIN_RATED_BATTLEGROUND:
        case CRITERIA_TYPE_REACH_BG_RATING:
        case CRITERIA_TYPE_COMPLETE_QUESTS_GUILD:
        case CRITERIA_TYPE_HONORABLE_KILLS_GUILD:
        case CRITERIA_TYPE_KILL_CREATURE_TYPE_GUILD:
        case CRITERIA_TYPE_LEVEL_BATTLE_PET:
        case CRITERIA_TYPE_WIN_ARENA:
        case CRITERIA_TYPE_EARN_HONORABLE_KILL:
        case CRITERIA_TYPE_COMPLETE_BATTLEGROUND:
        case CRITERIA_TYPE_DEATH:
        case CRITERIA_TYPE_BE_SPELL_TARGET:
        case CRITERIA_TYPE_CAST_SPELL:
        case CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE:
        case CRITERIA_TYPE_HONORABLE_KILL:
        case CRITERIA_TYPE_OWN_ITEM:
        case CRITERIA_TYPE_HIGHEST_PERSONAL_RATING:
        case CRITERIA_TYPE_USE_ITEM:
        case CRITERIA_TYPE_LOOT_ITEM:
        case CRITERIA_TYPE_LOOT_EPIC_ITEM:
        case CRITERIA_TYPE_OBTAIN_BATTLE_PET:
        case CRITERIA_TYPE_OWN_BATTLE_PET:
        case CRITERIA_TYPE_CAPTURE_BATTLE_PET:
        case CRITERIA_TYPE_COMPLETE_SCENARIO_COUNT:
        case CRITERIA_TYPE_COMPLETE_SCENARIO:
        case CRITERIA_TYPE_UPDATED_CRITERIA_ID:
            return progress->Counter >= requiredAmount;
        case CRITERIA_TYPE_COMPLETE_ACHIEVEMENT:
        case CRITERIA_TYPE_LEARN_SPELL:
        case CRITERIA_TYPE_EXPLORE_AREA:
        case CRITERIA_TYPE_OWN_PVP_HK_RANK:
        case CRITERIA_TYPE_BUY_GUILD_TABARD:
        case CRITERIA_TYPE_COMPLETE_QUEST:
            return progress->Counter >= 1;
        case CRITERIA_TYPE_LEARN_SKILL_LEVEL:
            return progress->Counter >= (requiredAmount * 75);
        case CRITERIA_TYPE_ON_LOGIN:
            return true;
        // handle all statistic-only criteria here
        case CRITERIA_TYPE_DEATH_AT_MAP:
        case CRITERIA_TYPE_DEATH_IN_DUNGEON:
        case CRITERIA_TYPE_COMPLETE_RAID:
        case CRITERIA_TYPE_KILLED_BY_CREATURE:
        case CRITERIA_TYPE_KILLED_BY_PLAYER:
        case CRITERIA_TYPE_DEATHS_FROM:
        case CRITERIA_TYPE_HIGHEST_TEAM_RATING:
        case CRITERIA_TYPE_MONEY_FROM_VENDORS:
        case CRITERIA_TYPE_GOLD_SPENT_FOR_TALENTS:
        case CRITERIA_TYPE_NUMBER_OF_TALENT_RESETS:
        case CRITERIA_TYPE_GOLD_SPENT_AT_BARBER:
        case CRITERIA_TYPE_GOLD_SPENT_FOR_MAIL:
        case CRITERIA_TYPE_LOSE_DUEL:
        case CRITERIA_TYPE_GOLD_EARNED_BY_AUCTIONS:
        case CRITERIA_TYPE_CREATE_AUCTION:
        case CRITERIA_TYPE_HIGHEST_AUCTION_BID:
        case CRITERIA_TYPE_HIGHEST_AUCTION_SOLD:
        case CRITERIA_TYPE_HIGHEST_GOLD_VALUE_OWNED:
        case CRITERIA_TYPE_WON_AUCTIONS:
        case CRITERIA_TYPE_GAIN_REVERED_REPUTATION:
        case CRITERIA_TYPE_GAIN_HONORED_REPUTATION:
        case CRITERIA_TYPE_KNOWN_FACTIONS:
        case CRITERIA_TYPE_RECEIVE_EPIC_ITEM:
        case CRITERIA_TYPE_ROLL_NEED:
        case CRITERIA_TYPE_ROLL_GREED:
        case CRITERIA_TYPE_QUEST_ABANDONED:
        case CRITERIA_TYPE_FLIGHT_PATHS_TAKEN:
        case CRITERIA_TYPE_ACCEPTED_SUMMONINGS:
        case CRITERIA_TYPE_PLAY_ARENA:
        case CRITERIA_TYPE_GOLD_SPENT_FOR_TRAVELLING:
        case CRITERIA_TYPE_HIGHEST_HIT_DEALT:
        case CRITERIA_TYPE_HIGHEST_HIT_RECEIVED:
        case CRITERIA_TYPE_TOTAL_DAMAGE_RECEIVED:
        case CRITERIA_TYPE_HIGHEST_HEAL_CAST:
        case CRITERIA_TYPE_TOTAL_HEALING_RECEIVED:
        case CRITERIA_TYPE_HIGHEST_HEALING_RECEIVED:
        case CRITERIA_TYPE_LFG_COMPLETED:
        case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_INIT:
        case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_NO_INIT:
        case CRITERIA_TYPE_LFG_KICKED_BY_OTHERS:
        case CRITERIA_TYPE_LFG_ABANDONED:
        case CRITERIA_TYPE_LFG_TANK_LEFT:
        case CRITERIA_TYPE_KILL_CREATURE_TYPE:
        case CRITERIA_TYPE_RELEASE_SPIRIT:
        default:
            break;
    }

    return false;
}

bool CriteriaHandler::CanUpdateCriteria(Criteria const* criteria, CriteriaTreeList const* trees, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer)
{
    if (DisableMgr::IsDisabledFor(DISABLE_TYPE_CRITERIA, criteria->ID, nullptr))
    {
        TC_LOG_TRACE("criteria", "CriteriaHandler::CanUpdateCriteria: (Id: %u Type %s) Disabled", criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type));
        return false;
    }

    bool treeRequirementPassed = false;
    for (CriteriaTree const* tree : *trees)
    {
        if (!CanUpdateCriteriaTree(criteria, tree, referencePlayer))
            continue;

        treeRequirementPassed = true;
        break;
    }

    if (!treeRequirementPassed)
        return false;

    if (!RequirementsSatisfied(criteria, miscValue1, miscValue2, miscValue3, unit, referencePlayer))
    {
        TC_LOG_TRACE("criteria", "CriteriaHandler::CanUpdateCriteria: (Id: %u Type %s) Requirements not satisfied", criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type));
        return false;
    }

    if (criteria->Modifier && !AdditionalRequirementsSatisfied(criteria->Modifier, miscValue1, miscValue2, miscValue3, unit, referencePlayer))
    {
        TC_LOG_TRACE("criteria", "CriteriaHandler::CanUpdateCriteria: (Id: %u Type %s) Requirements have not been satisfied", criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type));
        return false;
    }

    if (!ConditionsSatisfied(criteria->Entry, referencePlayer))
    {
        TC_LOG_TRACE("criteria", "CriteriaHandler::CanUpdateCriteria: (Id: %u Type %s) Conditions have not been satisfied", criteria->ID, CriteriaMgr::GetCriteriaTypeString(criteria->Entry->Type));
        return false;
    }

    return true;
}

bool CriteriaHandler::ConditionsSatisfied(CriteriaEntry const* criteria, Player* referencePlayer) const
{
    if (criteria->EligibilityWorldStateID)
    {
        uint32 PlayerWorldStateValue = referencePlayer->GetWorldStateForPlayer(criteria->EligibilityWorldStateID);
        if (PlayerWorldStateValue != criteria->EligibilityWorldStateValue)
            return false;
    }

    if (criteria->FailEvent)
    {
        switch (criteria->FailEvent)
        {
        case CRITERIA_CONDITION_BG_MAP:
            if (!referencePlayer->InBattleground())
                return false;
            break;
        case CRITERIA_CONDITION_HAS_AURA:
            if (!referencePlayer->HasAura(criteria->FailAsset))
                return false;
            break;
        case CRITERIA_CONDITION_NOT_IN_GROUP:
            if (referencePlayer->GetGroup())
                return false;
            break;
        default:
            break;
        }
    }

    return true;
}

bool CriteriaHandler::RequirementsSatisfied(Criteria const* criteria, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) const
{
    switch (CriteriaTypes(criteria->Entry->Type))
    {
        case CRITERIA_TYPE_COMPLETE_DAILY_QUEST:
        case CRITERIA_TYPE_DEATH:
        case CRITERIA_TYPE_KILLED_BY_PLAYER:
        case CRITERIA_TYPE_FALL_WITHOUT_DYING:
        case CRITERIA_TYPE_HONORABLE_KILL:
        case CRITERIA_TYPE_WIN_RATED_ARENA:
        case CRITERIA_TYPE_VISIT_BARBER_SHOP:
        case CRITERIA_TYPE_GET_KILLING_BLOWS:
        case CRITERIA_TYPE_MONEY_FROM_VENDORS:
        case CRITERIA_TYPE_GOLD_SPENT_FOR_TALENTS:
        case CRITERIA_TYPE_NUMBER_OF_TALENT_RESETS:
        case CRITERIA_TYPE_MONEY_FROM_QUEST_REWARD:
        case CRITERIA_TYPE_GOLD_SPENT_FOR_TRAVELLING:
        case CRITERIA_TYPE_GOLD_SPENT_AT_BARBER:
        case CRITERIA_TYPE_GOLD_SPENT_FOR_MAIL:
        case CRITERIA_TYPE_LOOT_MONEY:
        case CRITERIA_TYPE_SPECIAL_PVP_KILL:
        case CRITERIA_TYPE_WIN_DUEL:
        case CRITERIA_TYPE_LOSE_DUEL:
        case CRITERIA_TYPE_KILL_CREATURE_TYPE:
        case CRITERIA_TYPE_GUILD_COOK_RECIPE:
        case CRITERIA_TYPE_GOLD_EARNED_BY_AUCTIONS:
        case CRITERIA_TYPE_CREATE_AUCTION:
        case CRITERIA_TYPE_HIGHEST_AUCTION_BID:
        case CRITERIA_TYPE_WON_AUCTIONS:
        case CRITERIA_TYPE_HIGHEST_AUCTION_SOLD:
        case CRITERIA_TYPE_ROLL_GREED:
        case CRITERIA_TYPE_ROLL_NEED:
        case CRITERIA_TYPE_RELEASE_SPIRIT:
        case CRITERIA_TYPE_HIGHEST_HIT_DEALT:
        case CRITERIA_TYPE_HIGHEST_HIT_RECEIVED:
        case CRITERIA_TYPE_TOTAL_DAMAGE_RECEIVED:
        case CRITERIA_TYPE_TOTAL_HEALING_RECEIVED:
        case CRITERIA_TYPE_HIGHEST_HEAL_CAST:
        case CRITERIA_TYPE_HIGHEST_HEALING_RECEIVED:
        case CRITERIA_TYPE_QUEST_ABANDONED:
        case CRITERIA_TYPE_FLIGHT_PATHS_TAKEN:
        case CRITERIA_TYPE_ACCEPTED_SUMMONINGS:
        case CRITERIA_TYPE_LFG_COMPLETED:
        case CRITERIA_TYPE_USE_LFD_TO_GROUP_WITH_PLAYERS:
        case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_INIT:
        case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_NO_INIT:
        case CRITERIA_TYPE_LFG_KICKED_BY_OTHERS:
        case CRITERIA_TYPE_LFG_ABANDONED:
        case CRITERIA_TYPE_SPENT_GOLD_GUILD_REPAIRS:
        case CRITERIA_TYPE_REACH_GUILD_LEVEL:
        case CRITERIA_TYPE_GUILD_CRAFT_ITEMS:
        case CRITERIA_TYPE_CATCH_FROM_POOL:
        case CRITERIA_TYPE_BUY_GUILD_BANK_SLOTS:
        case CRITERIA_TYPE_WIN_RATED_BATTLEGROUND:
        case CRITERIA_TYPE_REACH_BG_RATING:
        case CRITERIA_TYPE_BUY_GUILD_TABARD:
        case CRITERIA_TYPE_COMPLETE_QUESTS_GUILD:
        case CRITERIA_TYPE_HONORABLE_KILLS_GUILD:
        case CRITERIA_TYPE_KILL_CREATURE_TYPE_GUILD:
        case CRITERIA_TYPE_LFG_TANK_LEFT:
        case CRITERIA_TYPE_COMPLETE_SCENARIO_COUNT:
        case CRITERIA_TYPE_OWN_BATTLE_PET:
        case CRITERIA_TYPE_OWN_BATTLE_PET_COUNT:
        case CRITERIA_TYPE_CAPTURE_BATTLE_PET:
        case CRITERIA_TYPE_WIN_PET_BATTLE:
            if (!miscValue1)
                return false;
            break;
        case CRITERIA_TYPE_COMPLETE_ARCHAEOLOGY_PROJECTS:
            if (!miscValue2)
                return false;
            break;
        case CRITERIA_TYPE_BUY_BANK_SLOT:
        case CRITERIA_TYPE_COMPLETE_DAILY_QUEST_DAILY:
        case CRITERIA_TYPE_COMPLETE_QUEST_COUNT:
        case CRITERIA_TYPE_EARN_ACHIEVEMENT_POINTS:
        case CRITERIA_TYPE_EARN_GUILD_ACHIEVEMENT_POINTS:
        case CRITERIA_TYPE_EARN_PET_BATTLE_ACHIEVEMENT_POINTS:
        case CRITERIA_TYPE_GAIN_EXALTED_REPUTATION:
        case CRITERIA_TYPE_GAIN_HONORED_REPUTATION:
        case CRITERIA_TYPE_GAIN_REVERED_REPUTATION:
        case CRITERIA_TYPE_HIGHEST_GOLD_VALUE_OWNED:
        case CRITERIA_TYPE_KNOWN_FACTIONS:
        case CRITERIA_TYPE_REACH_LEVEL:
        case CRITERIA_TYPE_ON_LOGIN:
        case CRITERIA_TYPE_EARN_HONORABLE_KILL:
            break;
        case CRITERIA_TYPE_COMPLETE_ACHIEVEMENT:
            if (!RequiredAchievementSatisfied(criteria->Entry->Asset.AchievementID))
                return false;
            break;
        case CRITERIA_TYPE_WIN_BG:
            if (!miscValue1 || criteria->Entry->Asset.MapID != referencePlayer->GetMapId())
                return false;
            break;
        case CRITERIA_TYPE_KILL_CREATURE:
        case CRITERIA_TYPE_KILLED_BY_CREATURE:
            if (!miscValue1 || criteria->Entry->Asset.CreatureID != miscValue1)
                return false;
            break;
        case CRITERIA_TYPE_REACH_SKILL_LEVEL:
        case CRITERIA_TYPE_LEARN_SKILL_LEVEL:
            // update at loading or specific skill update
            if (miscValue1 && miscValue1 != criteria->Entry->Asset.SkillID)
                return false;
            break;
        case CRITERIA_TYPE_COMPLETE_QUESTS_IN_ZONE:
            if (miscValue1 && miscValue1 != criteria->Entry->Asset.ZoneID)
                return false;
            break;
        case CRITERIA_TYPE_COMPLETE_BATTLEGROUND:
        case CRITERIA_TYPE_DEATH_AT_MAP:
            if (!miscValue1 || referencePlayer->GetMapId() != criteria->Entry->Asset.MapID)
                return false;
            break;
        case CRITERIA_TYPE_DEATH_IN_DUNGEON:
        case CRITERIA_TYPE_COMPLETE_RAID:
        {
            if (!miscValue1)
                return false;

            Map const* map = referencePlayer->IsInWorld() ? referencePlayer->GetMap() : sMapMgr->FindMap(referencePlayer->GetMapId(), referencePlayer->GetInstanceId());
            if (!map || !map->IsDungeon())
                return false;

            //FIXME: work only for instances where max == min for players
            if (map->ToInstanceMap()->GetMaxPlayers() != criteria->Entry->Asset.GroupSize)
                return false;
            break;
        }
        case CRITERIA_TYPE_DEATHS_FROM:
            if (!miscValue1 || miscValue2 != criteria->Entry->Asset.DamageType)
                return false;
            break;
        case CRITERIA_TYPE_COMPLETE_QUEST:
        {
            // if miscValues != 0, it contains the questID.
            if (miscValue1)
            {
                if (miscValue1 != criteria->Entry->Asset.QuestID)
                    return false;
            }
            else
            {
                // login case.
                if (!referencePlayer->GetQuestRewardStatus(criteria->Entry->Asset.QuestID))
                    return false;
            }
            break;
        }
        case CRITERIA_TYPE_BE_SPELL_TARGET:
        case CRITERIA_TYPE_BE_SPELL_TARGET2:
        case CRITERIA_TYPE_CAST_SPELL:
        case CRITERIA_TYPE_CAST_SPELL2:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.SpellID)
                return false;
            break;
        case CRITERIA_TYPE_LEARN_SPELL:
            if (miscValue1 && miscValue1 != criteria->Entry->Asset.SpellID)
                return false;

            if (!referencePlayer->HasSpell(criteria->Entry->Asset.SpellID))
                return false;
            break;
        case CRITERIA_TYPE_LOOT_TYPE:
        {
            // miscValue1 = itemId - miscValue2 = count of item loot
            // miscValue3 = loot_type (note: 0 = LOOT_CORPSE and then it ignored)
            if (!miscValue1 || !miscValue2 || !miscValue3 || miscValue3 != criteria->Entry->Asset.LootType)
                return false;

            ItemTemplate const* proto = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!proto)
                return false;

            break;
        }
        case CRITERIA_TYPE_OWN_ITEM:
            if (!miscValue1 || criteria->Entry->Asset.ItemID != miscValue1)
                return false;
            break;
        case CRITERIA_TYPE_USE_ITEM:
        case CRITERIA_TYPE_LOOT_ITEM:
        case CRITERIA_TYPE_EQUIP_ITEM:
            if (!miscValue1 || criteria->Entry->Asset.ItemID != miscValue1)
                return false;
            break;
        case CRITERIA_TYPE_EXPLORE_AREA:
        {
            WorldMapOverlayEntry const* worldOverlayEntry = sWorldMapOverlayStore.LookupEntry(criteria->Entry->Asset.WorldMapOverlayID);
            if (!worldOverlayEntry)
                break;

            bool matchFound = false;
            for (int j = 0; j < MAX_WORLD_MAP_OVERLAY_AREA_IDX; ++j)
            {
                AreaTableEntry const* area = sAreaStore.LookupEntry(worldOverlayEntry->AreaID[j]);
                if (!area)
                    break;

                if (area->AreaBit < 0)
                    continue;

                uint16 playerIndexOffset = uint16(uint32(area->AreaBit) / 32);
                if (playerIndexOffset >= PLAYER_EXPLORED_ZONES_SIZE)
                    continue;

                uint32 mask = 1 << (uint32(area->AreaBit) % 32);
                if (referencePlayer->GetUInt32Value(PLAYER_EXPLORED_ZONES + playerIndexOffset) & mask)
                {
                    matchFound = true;
                    break;
                }
            }

            if (!matchFound)
                return false;
            break;
        }
        case CRITERIA_TYPE_OWN_PVP_HK_RANK:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.PvPHKRank)
                return false;
            break;
        case CRITERIA_TYPE_GAIN_REPUTATION:
            if (miscValue1 && miscValue1 != criteria->Entry->Asset.FactionID)
                return false;
            break;
        case CRITERIA_TYPE_EQUIP_EPIC_ITEM:
            if (!miscValue1 || miscValue2 != criteria->Entry->Asset.ItemSlot)
                return false;
            break;
        case CRITERIA_TYPE_ROLL_NEED_ON_LOOT:
        case CRITERIA_TYPE_ROLL_GREED_ON_LOOT:
        {
            // miscValue1 = itemid miscValue2 = diced value
            if (!miscValue1 || miscValue2 != criteria->Entry->Asset.RollValue)
                return false;

            ItemTemplate const* proto = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!proto)
                return false;
            break;
        }
        case CRITERIA_TYPE_DO_EMOTE:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.EmoteID)
                return false;
            break;
        case CRITERIA_TYPE_DAMAGE_DONE:
        case CRITERIA_TYPE_HEALING_DONE:
            if (!miscValue1)
                return false;

            if (criteria->Entry->FailEvent == CRITERIA_CONDITION_BG_MAP)
            {
                if (!referencePlayer->InBattleground())
                    return false;

                // map specific case (BG in fact) expected player targeted damage/heal
                if (!unit || unit->GetTypeId() != TYPEID_PLAYER)
                    return false;
            }
            break;
        case CRITERIA_TYPE_USE_GAMEOBJECT:
        case CRITERIA_TYPE_FISH_IN_GAMEOBJECT:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.GameObjectID)
                return false;
            break;
        case CRITERIA_TYPE_LEARN_SKILLLINE_SPELLS:
        case CRITERIA_TYPE_LEARN_SKILL_LINE:
            if (miscValue1 && miscValue1 != criteria->Entry->Asset.SkillID)
                return false;
            break;
        case CRITERIA_TYPE_LOOT_EPIC_ITEM:
        case CRITERIA_TYPE_RECEIVE_EPIC_ITEM:
        {
            if (!miscValue1)
                return false;

            ItemTemplate const* proto = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!proto || proto->GetQuality() < ITEM_QUALITY_EPIC)
                return false;
            break;
        }
        case CRITERIA_TYPE_HK_CLASS:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.ClassID)
                return false;
            break;
        case CRITERIA_TYPE_HK_RACE:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.RaceID)
                return false;
            break;
        case CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.ObjectiveId)
                return false;
            break;
        case CRITERIA_TYPE_HONORABLE_KILL_AT_AREA:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.AreaID)
                return false;
            break;
        case CRITERIA_TYPE_CURRENCY:
            if (!miscValue1 || !miscValue2 || int64(miscValue2) < 0 ||
                miscValue1 != criteria->Entry->Asset.CurrencyID)
                return false;
            break;
        case CRITERIA_TYPE_WIN_ARENA:
        case CRITERIA_TYPE_PLAY_ARENA:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.MapID)
                return false;
            break;
        case CRITERIA_TYPE_HIGHEST_TEAM_RATING:
        case CRITERIA_TYPE_HIGHEST_PERSONAL_RATING:
            if (!miscValue2 || miscValue2 != criteria->Entry->Asset.TeamType)
                return false;
            break;
        case CRITERIA_TYPE_OBTAIN_BATTLE_PET:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.BattlePetID)
                return false;
            break;
        case CRITERIA_TYPE_LEVEL_BATTLE_PET:
            if (!miscValue1 || !miscValue2 || miscValue2 != criteria->Entry->Asset.BattlePetLevel)
                return false;
            break;
        case CRITERIA_TYPE_COMPLETE_SCENARIO:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.ScenarioID)
                return false;
            break;
        case CRITERIA_TYPE_UPDATED_CRITERIA_ID:
            if (!miscValue1 || miscValue1 != criteria->Entry->Asset.CriteriaID)
                return false;
            break;
        default:
            break;
    }
    return true;
}

bool CriteriaHandler::AdditionalRequirementsSatisfied(ModifierTreeNode const* tree, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) const
{
    switch (tree->Entry->Operator)
    {
        case MODIFIER_TREE_OPERATOR_SINGLE:
            return tree->Entry && AdditionalRequirementSatisfied(tree->Entry, miscValue1, miscValue2, miscValue3, unit, referencePlayer);
        case MODIFIER_TREE_OPERATOR_SINGLE_NOT_COMPLETED:
            return !tree->Entry || !AdditionalRequirementSatisfied(tree->Entry, miscValue1, miscValue2, miscValue3, unit, referencePlayer);
        case MODIFIER_TREE_OPERATOR_ALL:
            for (ModifierTreeNode const* childrenTree : tree->Children)
                if (!AdditionalRequirementsSatisfied(childrenTree, miscValue1, miscValue2, miscValue3, unit, referencePlayer))
                    return false;
            return true;
        case MODIFIER_TREE_OPERATOR_ANY:
        {
            uint64 progress = 0;
            for (ModifierTreeNode const* childrenTree : tree->Children)
                if (AdditionalRequirementsSatisfied(childrenTree, miscValue1, miscValue2, miscValue3, unit, referencePlayer))
                    if (++progress >= tree->Entry->Amount)
                        return true;

            return false;
        }
        default:
            break;
    }

    return false;
}

bool CriteriaHandler::AdditionalRequirementSatisfied(ModifierTreeEntry const* modifier, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) const
{
    uint32 reqType = modifier->Type;
    if (!reqType)
        return true;

    uint32 reqValue = modifier->Asset[0];
    uint32 reqAddValue = modifier->Asset[1];

    switch (CriteriaAdditionalCondition(reqType))
    {
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_DRUNK_VALUE: // 1
            if (!referencePlayer || referencePlayer->GetDrunkValue() < reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_ITEM_LEVEL: // 3
        {
            ItemTemplate const* const item = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!item || item->GetItemLevel() < reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_CREATURE_ENTRY: // 4
            if (!unit || unit->GetEntry() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_MUST_BE_PLAYER: // 5
            if (!unit || unit->GetTypeId() != TYPEID_PLAYER)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_MUST_BE_DEAD: // 6
            if (!unit || unit->IsAlive())
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_MUST_BE_ENEMY: // 7
            if (!unit || !referencePlayer->IsHostileTo(unit))
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_HAS_AURA: // 8
            if (!referencePlayer->HasAura(reqValue))
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_HAS_AURA: // 10
            if (!unit || !unit->HasAura(reqValue))
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_HAS_AURA_TYPE: // 11
            if (!unit || !unit->HasAuraType(AuraType(reqValue)))
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_ITEM_QUALITY_MIN: // 14
        {
            ItemTemplate const* const item = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!item || item->GetQuality() < reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_ITEM_QUALITY_EQUALS: // 15
        {
            ItemTemplate const* const item = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!item || item->GetQuality() != reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_MUST_BE_ALIVE: // 16
            if (!referencePlayer || !referencePlayer->IsAlive())
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_AREA_OR_ZONE: // 17
        {
            uint32 zoneId, areaId;
            referencePlayer->GetZoneAndAreaId(zoneId, areaId);
            if (zoneId != reqValue && areaId != reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_AREA_OR_ZONE: // 18
        {
            if (!unit)
                return false;
            uint32 zoneId, areaId;
            unit->GetZoneAndAreaId(zoneId, areaId);
            if (zoneId != reqValue && areaId != reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_HAS_ITEM: // 19
            if (!referencePlayer->HasItemCount(reqValue))
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_MAP_DIFFIULTY_OLD: // 20
        {
            DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(referencePlayer->GetMap()->GetDifficultyID());
            if (!difficultyEntry)
                return false;
            if (difficultyEntry->OldEnumValue != reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_CREATURE_YIELDS_XP: // 21
            if (!unit || !referencePlayer->IsHonorOrXPTarget(unit))
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_RATED_TYPE: // 24
        {
            Battleground* bg = referencePlayer->GetBattleground();
            if (!bg || !bg->IsRated() || bg->GetRatedType() != RatedType(reqValue))
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_RACE: // 25
            if (referencePlayer->GetRace() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_CLASS: // 26
            if (referencePlayer->GetClass() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_RACE: // 27
            if (!unit || unit->GetTypeId() != TYPEID_PLAYER || unit->GetRace() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_CLASS: // 28
            if (!unit || unit->GetTypeId() != TYPEID_PLAYER || unit->GetClass() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_MAX_GROUP_MEMBERS: // 29
            if (referencePlayer->GetGroup() && referencePlayer->GetGroup()->GetMembersCount() >= reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_CREATURE_TYPE: // 30
        {
            if (!unit)
                return false;

            Creature const* const creature = unit->ToCreature();
            if (!creature || creature->GetCreatureType() != reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_MAP: // 32
            if (referencePlayer->GetMapId() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_LEVEL: // 34
        {
            PetBattle* battle = sPetBattleSystem->GetPlayerPetBattle(referencePlayer->GetGUID());
            if (!battle)
                return false;

            PetBattleTeam* team = battle->GetTeam(referencePlayer->GetGUID());
            if (!team)
                return false;

            for (BattlePet* battlePet : team->BattlePets)
                if (battlePet->GetLevel() < reqValue)
                    return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_COMPLETE_QUEST_NOT_IN_GROUP: // 35
            if (referencePlayer->GetGroup())
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_MIN_PERSONAL_RATING: // 37
        {
            RatedInfo* rInfo = sRatedMgr->GetRatedInfo(referencePlayer->GetGUID());
            if (!rInfo)
                return false;

            uint32 maxPersonalRating = 0;
            for (uint8 i = 0; i < MAX_ARENA_SLOT; ++i)
            {
                RatedType ratedType = RatedInfo::GetRatedTypeBySlot(i);
                StatsBySlot const* stats = rInfo->GetStatsBySlot(ratedType);
                uint32 personalRating = stats->PersonalRating;

                if (maxPersonalRating < personalRating)
                    maxPersonalRating = personalRating;
            }

            if (maxPersonalRating <= reqValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_TITLE_BIT_INDEX: // 38
        {
            uint32 fieldIndexOffset = reqValue / 32;
            uint32 flag = 1 << (reqValue % 32);
            if (!referencePlayer->HasFlag(PLAYER_KNOWN_TITLES + fieldIndexOffset, flag))
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_SOURCE_LEVEL: // 39
            if (referencePlayer->GetLevel() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_LEVEL: // 40
            if (!unit || unit->GetLevel() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_ZONE: // 41
            if (!unit || unit->GetZoneId() != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_HEALTH_PERCENT_BELOW: // 46
            if (!unit || unit->GetHealthPct() >= reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_MIN_ACHIEVEMENT_POINTS: // 56
            if (miscValue1 < reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_REQUIRES_LFG_GROUP: // 58
        {
            Group* group = referencePlayer->GetGroup();
            if (!group)
                return false;

            if (!group->IsLFGGroup())
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_LAST_MAN_STANDING: // 60
        {
            // done in Battleground::EndBattleground
            return true;
        }
        case CRITERIA_ADDITIONAL_CONDITION_REQUIRES_GUILD_GROUP: // 61
        {
            Group* group = referencePlayer->GetGroup();
            if (!group)
                return false;

            if (!group->IsGuildGroupForPlayer(referencePlayer))
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_GUILD_REPUTATION: // 62
        {
            if (uint32(referencePlayer->GetReputationMgr().GetReputation(GUILD_REP)) < reqValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_RATED_BATTLEGROUND: // 63
        {
            Battleground* bg = referencePlayer->GetBattleground();
            if (!bg)
                return false;

            if (!bg->IsRatedBattleground())
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_PROJECT_RARITY: // 65
            if (miscValue1 != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_PROJECT_RACE: // 66
            if (!miscValue2 || miscValue2 != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_ACTIVE_WORLD_STATE: // 67
            if (referencePlayer->GetWorldStateForPlayer(reqValue) == 0)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_MAP_DIFFICULTY: // 68
        {
            if (uint32(referencePlayer->GetMap()->GetDifficultyID()) != reqValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_TARGET_EQUAL_OR_HIGHER_LEVEL: // 70
        {
            if (!unit)
                return false;

            if (uint32(unit->GetLevel()) < reqValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_SCENARIO_ID: // 74
            if (miscValue1 != reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_FAMILY: // 78
        {
            // CRITERIA_TYPE_CAPTURE_BATTLE_PET
            // CRITERIA_TYPE_LEVEL_BATTLE_PET
            if (miscValue1)
            {
                BattlePetSpeciesEntry const* species = sBattlePetSpeciesStore.LookupEntry(miscValue1);
                if (!species)
                    return false;

                if (species->FamilyId != reqValue)
                    return false;
            }
            // CRITERIA_TYPE_WIN_PET_BATTLE
            else
            {
                if (!unit)
                    return false;

                PetBattle* battle = sPetBattleSystem->GetPlayerPetBattle(referencePlayer->GetGUID());
                if (!battle)
                    return false;

                PetBattleTeam* team = battle->GetTeam(unit->GetGUID());
                if (!team)
                    return false;

                bool FoundFamily = false;
                for (BattlePet* battlePet : team->BattlePets)
                    if (battlePet->GetFamily() == reqValue)
                    {
                        FoundFamily = true;
                        break;
                    }

                if (!FoundFamily)
                    return false;
            }

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_HEALTH_PCT: // 79
            if (!miscValue2 || miscValue2 >= reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_WORLD_BOSS_RAID_GROUP: // 80
        {
            Group* group = referencePlayer->GetGroup();
            if (!group)
                return false;

            uint32 guildID = referencePlayer->GetGuildId();
            if (!guildID)
                return false;

            uint32 sameGuildMembersCount = 0;
            for (GroupReference const* itr = group->GetFirstMember(); itr != nullptr; itr = itr->next())
                if (Player* member = itr->GetSource())
                    if (member->GetGuildId() == guildID)
                        ++sameGuildMembersCount;

            if (sameGuildMembersCount < reqValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_NPC_ENTRY: // 81
        {
            if (!unit)
                return false;

            // CRITERIA_TYPE_CAPTURE_BATTLE_PET
            if (miscValue1)
            {
                BattlePetSpeciesEntry const* species = sBattlePetSpeciesStore.LookupEntry(miscValue1);
                if (!species)
                    return false;

                if (species->NpcId != reqValue)
                    return false;
            }
            // CRITERIA_TYPE_WIN_PET_BATTLE
            else
            {
                if (!unit)
                    return false;

                PetBattle* battle = sPetBattleSystem->GetPlayerPetBattle(referencePlayer->GetGUID());
                if (!battle)
                    return false;

                PetBattleTeam* team = battle->GetTeam(unit->GetGUID());
                if (!team)
                    return false;

                bool FoundNpc = false;
                for (BattlePet* battlePet : team->BattlePets)
                    if (battlePet->GetNpc() == reqValue)
                    {
                        FoundNpc = true;
                        break;
                    }

                if (!FoundNpc)
                    return false;
            }

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_QUALITY: // 89
            if (!miscValue3 || miscValue3 >= reqValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_BATTLE_TYPE: // 90
        {
            PetBattle* battle = sPetBattleSystem->GetPlayerPetBattle(referencePlayer->GetGUID());
            if (!battle)
                return false;

            if (battle->GetType() != reqValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_BATTLE_PET_SPECIES: // 91
        {
            // CRITERIA_TYPE_OWN_BATTLE_PET
            // CRITERIA_TYPE_CAPTURE_BATTLE_PET
            if (miscValue1)
            {
                if (miscValue1 != reqValue)
                    return false;
            }
            // CRITERIA_TYPE_WIN_PET_BATTLE
            else
            {
                if (!unit)
                    return false;

                PetBattle* battle = sPetBattleSystem->GetPlayerPetBattle(referencePlayer->GetGUID());
                if (!battle)
                    return false;

                PetBattleTeam* team = battle->GetTeam(unit->GetGUID());
                if (!team)
                    return false;

                bool FoundSpecies = false;
                for (BattlePet* battlePet : team->BattlePets)
                    if (battlePet->GetSpecies() == reqValue)
                    {
                        FoundSpecies = true;
                        break;
                    }

                if (!FoundSpecies)
                    return false;
            }

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_REPUTATION_RANK_POINTS: // 95
        {
            ReputationRank rank = ReputationMgr::ReputationToRank(reqAddValue);
            if (rank < referencePlayer->GetReputationRank(reqValue))
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_ITEM_CLASS_AND_SUBCLASS: // 96
        {
            ItemTemplate const* const item = sObjectMgr->GetItemTemplate(uint32(miscValue1));
            if (!item)
                return false;

            if (item->GetClass() != reqValue && item->GetSubClass() != reqAddValue)
                return false;

            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_CURRENCY_COUNT: // 121
            if (referencePlayer->GetCurrency(reqValue, false) < reqAddValue)
                return false;
            break;
        case CRITERIA_ADDITIONAL_CONDITION_MAP_TYPE: //122
        {
            MapEntry const* mapEntry =  referencePlayer->GetMap()->GetEntry();
            if (!mapEntry || mapEntry->InstanceType != reqValue)
                return false;
            break;
        }
        case CRITERIA_ADDITIONAL_CONDITION_ARENA_SEASON: // 125
        {
            uint32 CurrentArenaSeason = sWorld->getBoolConfig(CONFIG_ARENA_SEASON_IN_PROGRESS) ? sWorld->getIntConfig(CONFIG_ARENA_SEASON_ID) : 0;
            if (!CurrentArenaSeason || CurrentArenaSeason != reqValue)
                return false;
            break;
        }
        default:
            return false;
    }

    return true;
}

char const* CriteriaMgr::GetCriteriaTypeString(uint32 type)
{
    return GetCriteriaTypeString(CriteriaTypes(type));
}

char const* CriteriaMgr::GetCriteriaTypeString(CriteriaTypes type)
{
    switch (type)
    {
        case CRITERIA_TYPE_KILL_CREATURE:
            return "KILL_CREATURE";
        case CRITERIA_TYPE_WIN_BG:
            return "TYPE_WIN_BG";
        case CRITERIA_TYPE_COMPLETE_ARCHAEOLOGY_PROJECTS:
            return "COMPLETE_RESEARCH";
        case CRITERIA_TYPE_REACH_LEVEL:
            return "REACH_LEVEL";
        case CRITERIA_TYPE_REACH_SKILL_LEVEL:
            return "REACH_SKILL_LEVEL";
        case CRITERIA_TYPE_COMPLETE_ACHIEVEMENT:
            return "COMPLETE_ACHIEVEMENT";
        case CRITERIA_TYPE_COMPLETE_QUEST_COUNT:
            return "COMPLETE_QUEST_COUNT";
        case CRITERIA_TYPE_COMPLETE_DAILY_QUEST_DAILY:
            return "COMPLETE_DAILY_QUEST_DAILY";
        case CRITERIA_TYPE_COMPLETE_QUESTS_IN_ZONE:
            return "COMPLETE_QUESTS_IN_ZONE";
        case CRITERIA_TYPE_CURRENCY:
            return "CURRENCY";
        case CRITERIA_TYPE_DAMAGE_DONE:
            return "DAMAGE_DONE";
        case CRITERIA_TYPE_COMPLETE_DAILY_QUEST:
            return "COMPLETE_DAILY_QUEST";
        case CRITERIA_TYPE_COMPLETE_BATTLEGROUND:
            return "COMPLETE_BATTLEGROUND";
        case CRITERIA_TYPE_DEATH_AT_MAP:
            return "DEATH_AT_MAP";
        case CRITERIA_TYPE_DEATH:
            return "DEATH";
        case CRITERIA_TYPE_DEATH_IN_DUNGEON:
            return "DEATH_IN_DUNGEON";
        case CRITERIA_TYPE_COMPLETE_RAID:
            return "COMPLETE_RAID";
        case CRITERIA_TYPE_KILLED_BY_CREATURE:
            return "KILLED_BY_CREATURE";
        case CRITERIA_TYPE_UPDATED_CRITERIA_ID:
            return "UPDATED_CRITERIA_ID";
        case CRITERIA_TYPE_KILLED_BY_PLAYER:
            return "KILLED_BY_PLAYER";
        case CRITERIA_TYPE_FALL_WITHOUT_DYING:
            return "FALL_WITHOUT_DYING";
        case CRITERIA_TYPE_DEATHS_FROM:
            return "DEATHS_FROM";
        case CRITERIA_TYPE_COMPLETE_QUEST:
            return "COMPLETE_QUEST";
        case CRITERIA_TYPE_BE_SPELL_TARGET:
            return "BE_SPELL_TARGET";
        case CRITERIA_TYPE_CAST_SPELL:
            return "CAST_SPELL";
        case CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE:
            return "BG_OBJECTIVE_CAPTURE";
        case CRITERIA_TYPE_HONORABLE_KILL_AT_AREA:
            return "HONORABLE_KILL_AT_AREA";
        case CRITERIA_TYPE_WIN_ARENA:
            return "WIN_ARENA";
        case CRITERIA_TYPE_PLAY_ARENA:
            return "PLAY_ARENA";
        case CRITERIA_TYPE_LEARN_SPELL:
            return "LEARN_SPELL";
        case CRITERIA_TYPE_HONORABLE_KILL:
            return "HONORABLE_KILL";
        case CRITERIA_TYPE_OWN_ITEM:
            return "OWN_ITEM";
        case CRITERIA_TYPE_WIN_RATED_ARENA:
            return "WIN_RATED_ARENA";
        case CRITERIA_TYPE_HIGHEST_TEAM_RATING:
            return "HIGHEST_TEAM_RATING";
        case CRITERIA_TYPE_HIGHEST_PERSONAL_RATING:
            return "HIGHEST_PERSONAL_RATING";
        case CRITERIA_TYPE_LEARN_SKILL_LEVEL:
            return "LEARN_SKILL_LEVEL";
        case CRITERIA_TYPE_USE_ITEM:
            return "USE_ITEM";
        case CRITERIA_TYPE_LOOT_ITEM:
            return "LOOT_ITEM";
        case CRITERIA_TYPE_EXPLORE_AREA:
            return "EXPLORE_AREA";
        case CRITERIA_TYPE_OWN_PVP_HK_RANK:
            return "OWN_PVP_HK_RANK";
        case CRITERIA_TYPE_BUY_BANK_SLOT:
            return "BUY_BANK_SLOT";
        case CRITERIA_TYPE_GAIN_REPUTATION:
            return "GAIN_REPUTATION";
        case CRITERIA_TYPE_GAIN_EXALTED_REPUTATION:
            return "GAIN_EXALTED_REPUTATION";
        case CRITERIA_TYPE_VISIT_BARBER_SHOP:
            return "VISIT_BARBER_SHOP";
        case CRITERIA_TYPE_EQUIP_EPIC_ITEM:
            return "EQUIP_EPIC_ITEM";
        case CRITERIA_TYPE_ROLL_NEED_ON_LOOT:
            return "ROLL_NEED_ON_LOOT";
        case CRITERIA_TYPE_ROLL_GREED_ON_LOOT:
            return "GREED_ON_LOOT";
        case CRITERIA_TYPE_HK_CLASS:
            return "HK_CLASS";
        case CRITERIA_TYPE_HK_RACE:
            return "HK_RACE";
        case CRITERIA_TYPE_DO_EMOTE:
            return "DO_EMOTE";
        case CRITERIA_TYPE_HEALING_DONE:
            return "HEALING_DONE";
        case CRITERIA_TYPE_GET_KILLING_BLOWS:
            return "GET_KILLING_BLOWS";
        case CRITERIA_TYPE_EQUIP_ITEM:
            return "EQUIP_ITEM";
        case CRITERIA_TYPE_MONEY_FROM_VENDORS:
            return "MONEY_FROM_VENDORS";
        case CRITERIA_TYPE_GOLD_SPENT_FOR_TALENTS:
            return "GOLD_SPENT_FOR_TALENTS";
        case CRITERIA_TYPE_NUMBER_OF_TALENT_RESETS:
            return "NUMBER_OF_TALENT_RESETS";
        case CRITERIA_TYPE_MONEY_FROM_QUEST_REWARD:
            return "MONEY_FROM_QUEST_REWARD";
        case CRITERIA_TYPE_GOLD_SPENT_FOR_TRAVELLING:
            return "GOLD_SPENT_FOR_TRAVELLING";
        case CRITERIA_TYPE_GOLD_SPENT_AT_BARBER:
            return "GOLD_SPENT_AT_BARBER";
        case CRITERIA_TYPE_GOLD_SPENT_FOR_MAIL:
            return "GOLD_SPENT_FOR_MAIL";
        case CRITERIA_TYPE_LOOT_MONEY:
            return "LOOT_MONEY";
        case CRITERIA_TYPE_USE_GAMEOBJECT:
            return "USE_GAMEOBJECT";
        case CRITERIA_TYPE_BE_SPELL_TARGET2:
            return "BE_SPELL_TARGET2";
        case CRITERIA_TYPE_SPECIAL_PVP_KILL:
            return "SPECIAL_PVP_KILL";
        case CRITERIA_TYPE_FISH_IN_GAMEOBJECT:
            return "FISH_IN_GAMEOBJECT";
        case CRITERIA_TYPE_ON_LOGIN:
            return "ON_LOGIN";
        case CRITERIA_TYPE_LEARN_SKILLLINE_SPELLS:
            return "LEARN_SKILLLINE_SPELLS";
        case CRITERIA_TYPE_WIN_DUEL:
            return "WIN_DUEL";
        case CRITERIA_TYPE_LOSE_DUEL:
            return "LOSE_DUEL";
        case CRITERIA_TYPE_KILL_CREATURE_TYPE:
            return "KILL_CREATURE_TYPE";
        case CRITERIA_TYPE_GUILD_COOK_RECIPE:
            return "GUILD_COOK_RECIPE";
        case CRITERIA_TYPE_GOLD_EARNED_BY_AUCTIONS:
            return "GOLD_EARNED_BY_AUCTIONS";
        case CRITERIA_TYPE_CREATE_AUCTION:
            return "CREATE_AUCTION";
        case CRITERIA_TYPE_HIGHEST_AUCTION_BID:
            return "HIGHEST_AUCTION_BID";
        case CRITERIA_TYPE_WON_AUCTIONS:
            return "WON_AUCTIONS";
        case CRITERIA_TYPE_HIGHEST_AUCTION_SOLD:
            return "HIGHEST_AUCTION_SOLD";
        case CRITERIA_TYPE_HIGHEST_GOLD_VALUE_OWNED:
            return "HIGHEST_GOLD_VALUE_OWNED";
        case CRITERIA_TYPE_GAIN_REVERED_REPUTATION:
            return "GAIN_REVERED_REPUTATION";
        case CRITERIA_TYPE_GAIN_HONORED_REPUTATION:
            return "GAIN_HONORED_REPUTATION";
        case CRITERIA_TYPE_KNOWN_FACTIONS:
            return "KNOWN_FACTIONS";
        case CRITERIA_TYPE_LOOT_EPIC_ITEM:
            return "LOOT_EPIC_ITEM";
        case CRITERIA_TYPE_RECEIVE_EPIC_ITEM:
            return "RECEIVE_EPIC_ITEM";
        case CRITERIA_TYPE_SEND_EVENT_SCENARIO:
            return "SEND_EVENT_SCENARIO";
        case CRITERIA_TYPE_ROLL_NEED:
            return "ROLL_NEED";
        case CRITERIA_TYPE_ROLL_GREED:
            return "ROLL_GREED";
        case CRITERIA_TYPE_RELEASE_SPIRIT:
            return "RELEASE_SPIRIT";
        case CRITERIA_TYPE_OBTAIN_BATTLE_PET:
            return "OBTAIN_BATTLE_PET";
        case CRITERIA_TYPE_HIGHEST_HIT_DEALT:
            return "HIT_DEALT";
        case CRITERIA_TYPE_HIGHEST_HIT_RECEIVED:
            return "HIT_RECEIVED";
        case CRITERIA_TYPE_TOTAL_DAMAGE_RECEIVED:
            return "TOTAL_DAMAGE_RECEIVED";
        case CRITERIA_TYPE_HIGHEST_HEAL_CAST:
            return "HIGHEST_HEAL_CAST";
        case CRITERIA_TYPE_TOTAL_HEALING_RECEIVED:
            return "TOTAL_HEALING_RECEIVED";
        case CRITERIA_TYPE_HIGHEST_HEALING_RECEIVED:
            return "HIGHEST_HEALING_RECEIVED";
        case CRITERIA_TYPE_QUEST_ABANDONED:
            return "QUEST_ABANDONED";
        case CRITERIA_TYPE_FLIGHT_PATHS_TAKEN:
            return "FLIGHT_PATHS_TAKEN";
        case CRITERIA_TYPE_LOOT_TYPE:
            return "LOOT_TYPE";
        case CRITERIA_TYPE_CAST_SPELL2:
            return "CAST_SPELL2";
        case CRITERIA_TYPE_LEARN_SKILL_LINE:
            return "LEARN_SKILL_LINE";
        case CRITERIA_TYPE_EARN_HONORABLE_KILL:
            return "EARN_HONORABLE_KILL";
        case CRITERIA_TYPE_ACCEPTED_SUMMONINGS:
            return "ACCEPTED_SUMMONINGS";
        case CRITERIA_TYPE_EARN_ACHIEVEMENT_POINTS:
            return "EARN_ACHIEVEMENT_POINTS";
        case CRITERIA_TYPE_LFG_COMPLETED:
            return "LFG_COMPLETED";
        case CRITERIA_TYPE_EARN_PET_BATTLE_ACHIEVEMENT_POINTS:
            return "EARN_PET_BATTLE_ACHIEVEMENT_POINTS";
        case CRITERIA_TYPE_USE_LFD_TO_GROUP_WITH_PLAYERS:
            return "USE_LFD_TO_GROUP_WITH_PLAYERS";
        case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_INIT:
            return "LFG_KICKED_OTHERS_WITH_INIT";
        case CRITERIA_TYPE_LFG_KICKED_OTHERS_WITH_NO_INIT:
            return "LFG_KICKED_OTHERS_WITH_NO_INIT";
        case CRITERIA_TYPE_LFG_KICKED_BY_OTHERS:
            return "LFG_KICKED_BY_OTHERS";
        case CRITERIA_TYPE_LFG_ABANDONED:
            return "LFG_ABANDONED";
        case CRITERIA_TYPE_SPENT_GOLD_GUILD_REPAIRS:
            return "SPENT_GOLD_GUILD_REPAIRS";
        case CRITERIA_TYPE_REACH_GUILD_LEVEL:
            return "REACH_GUILD_LEVEL";
        case CRITERIA_TYPE_GUILD_CRAFT_ITEMS:
            return "GUILD_CRAFT_ITEMS";
        case CRITERIA_TYPE_CATCH_FROM_POOL:
            return "CATCH_FROM_POOL";
        case CRITERIA_TYPE_BUY_GUILD_BANK_SLOTS:
            return "BUY_GUILD_BANK_SLOTS";
        case CRITERIA_TYPE_EARN_GUILD_ACHIEVEMENT_POINTS:
            return "EARN_GUILD_ACHIEVEMENT_POINTS";
        case CRITERIA_TYPE_WIN_RATED_BATTLEGROUND:
            return "WIN_RATED_BATTLEGROUND";
        case CRITERIA_TYPE_REACH_BG_RATING:
            return "REACH_BG_RATING";
        case CRITERIA_TYPE_BUY_GUILD_TABARD:
            return "BUY_GUILD_TABARD";
        case CRITERIA_TYPE_COMPLETE_QUESTS_GUILD:
            return "COMPLETE_QUESTS_GUILD";
        case CRITERIA_TYPE_HONORABLE_KILLS_GUILD:
            return "HONORABLE_KILLS_GUILD";
        case CRITERIA_TYPE_KILL_CREATURE_TYPE_GUILD:
            return "KILL_CREATURE_TYPE_GUILD";
        case CRITERIA_TYPE_LFG_TANK_LEFT:
            return "LFG_TANK_LEFT";
        case CRITERIA_TYPE_COMPLETE_GUILD_CHALLENGE_TYPE:
            return "GUILD_CHALLENGE_TYPE";
        case CRITERIA_TYPE_COMPLETE_GUILD_CHALLENGE:
            return "GUILD_CHALLENGE";
        case CRITERIA_TYPE_LFR_DUNGEONS_COMPLETED:
            return "LFR_DUNGEONS_COMPLETED";
        case CRITERIA_TYPE_LFR_LEAVES:
            return "LFR_LEAVES";
        case CRITERIA_TYPE_LFR_VOTE_KICKS_INITIATED_BY_PLAYER:
            return "LFR_VOTE_KICKS_INITIATED_BY_PLAYER";
        case CRITERIA_TYPE_LFR_VOTE_KICKS_NOT_INIT_BY_PLAYER:
            return "LFR_VOTE_KICKS_NOT_INIT_BY_PLAYER";
        case CRITERIA_TYPE_BE_KICKED_FROM_LFR:
            return "BE_KICKED_FROM_LFR";
        case CRITERIA_TYPE_COUNT_OF_LFR_QUEUE_BOOSTS_BY_TANK:
            return "COUNT_OF_LFR_QUEUE_BOOSTS_BY_TANK";
        case CRITERIA_TYPE_COMPLETE_SCENARIO_COUNT:
            return "COMPLETE_SCENARIO_COUNT";
        case CRITERIA_TYPE_COMPLETE_SCENARIO:
            return "COMPLETE_SCENARIO";
        case CRITERIA_TYPE_REACH_AREATRIGGER_WITH_ACTIONSET:
            return "REACH_AREATRIGGER_WITH_ACTIONSET";
        case CRITERIA_TYPE_OWN_BATTLE_PET:
            return "OWN_BATTLE_PET";
        case CRITERIA_TYPE_OWN_BATTLE_PET_COUNT:
            return "OWN_BATTLE_PET_COUNT";
        case CRITERIA_TYPE_CAPTURE_BATTLE_PET:
            return "CAPTURE_BATTLE_PET";
        case CRITERIA_TYPE_WIN_PET_BATTLE:
            return "WIN_PET_BATTLE";
        case CRITERIA_TYPE_LEVEL_BATTLE_PET:
            return "LEVEL_BATTLE_PET";
        case CRITERIA_TYPE_CAPTURE_BATTLE_PET_CREDIT:
            return "CAPTURE_BATTLE_PET_CREDIT";
        case CRITERIA_TYPE_LEVEL_BATTLE_PET_CREDIT:
            return "LEVEL_BATTLE_PET_CREDIT";
    }
    return "MISSING_TYPE";
}

//==========================================================
CriteriaMgr::~CriteriaMgr()
{
    for (auto itr = _criteriaTrees.begin(); itr != _criteriaTrees.end(); ++itr)
        delete itr->second;

    for (auto itr = _criteria.begin(); itr != _criteria.end(); ++itr)
        delete itr->second;

    for (auto itr = _criteriaModifiers.begin(); itr != _criteriaModifiers.end(); ++itr)
        delete itr->second;
}

void CriteriaMgr::LoadCriteriaModifiersTree()
{
    uint32 oldMSTime = getMSTime();

    if (sModifierTreeStore.GetNumRows() == 0)
    {
        TC_LOG_ERROR("server.loading", ">> Loaded 0 criteria modifiers.");
        return;
    }

    // Load modifier tree nodes
    for (uint32 i = 0; i < sModifierTreeStore.GetNumRows(); ++i)
    {
        ModifierTreeEntry const* tree = sModifierTreeStore.LookupEntry(i);
        if (!tree)
            continue;

        ModifierTreeNode* node = new ModifierTreeNode();
        node->Entry = tree;
        _criteriaModifiers[node->Entry->ID] = node;
    }

    // Build tree
    for (auto itr = _criteriaModifiers.begin(); itr != _criteriaModifiers.end(); ++itr)
    {
        if (!itr->second->Entry->Parent)
            continue;

        auto parent = _criteriaModifiers.find(itr->second->Entry->Parent);
        if (parent != _criteriaModifiers.end())
            parent->second->Children.push_back(itr->second);
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u criteria modifiers in %u ms", uint32(_criteriaModifiers.size()), GetMSTimeDiffToNow(oldMSTime));
}

template<typename T>
T GetEntry(std::unordered_map<uint32, T> const& map, CriteriaTreeEntry const* tree)
{
    CriteriaTreeEntry const* cur = tree;
    auto itr = map.find(tree->ID);
    while (itr == map.end())
    {
        if (!cur->ParentID)
            break;

        cur = sCriteriaTreeStore.LookupEntry(cur->ParentID);
        if (!cur)
            break;

        itr = map.find(cur->ID);
    }

    if (itr == map.end())
        return nullptr;

    return itr->second;
};

void CriteriaMgr::LoadCriteriaList()
{
    uint32 oldMSTime = getMSTime();

    if (sCriteriaTreeStore.GetNumRows() == 0)
    {
        TC_LOG_ERROR("server.loading", ">> Loaded 0 criteria.");
        return;
    }

    std::unordered_map<uint32 /*criteriaTreeID*/, AchievementEntry const*> achievementCriteriaTreeIds;
    for (AchievementEntry const* achievement : sAchievementStore)
        if (achievement->CriteriaTree)
            achievementCriteriaTreeIds[achievement->CriteriaTree] = achievement;

    std::unordered_map<uint32 /*criteriaTreeID*/, ScenarioStepEntry const*> scenarioCriteriaTreeIds;
    for (ScenarioStepEntry const* scenarioStep : sScenarioStepStore)
        if (scenarioStep->CriteriaTreeID)
            scenarioCriteriaTreeIds[scenarioStep->CriteriaTreeID] = scenarioStep;

    // Load criteria tree nodes
    for (CriteriaTreeEntry const* tree : sCriteriaTreeStore)
    {
        // Find linked achievement
        AchievementEntry const* achievement = GetEntry(achievementCriteriaTreeIds, tree);
        ScenarioStepEntry const* scenarioStep = GetEntry(scenarioCriteriaTreeIds, tree);
        if (!achievement && !scenarioStep)
            continue;

        CriteriaTree* criteriaTree = new CriteriaTree();
        criteriaTree->ID = tree->ID;
        criteriaTree->Achievement = achievement;
        criteriaTree->ScenarioStep = scenarioStep;
        criteriaTree->Entry = tree;

        _criteriaTrees[criteriaTree->Entry->ID] = criteriaTree;
    }

    // Build tree
    for (auto itr = _criteriaTrees.begin(); itr != _criteriaTrees.end(); ++itr)
    {
        if (!itr->second->Entry->ParentID)
            continue;

        auto parent = _criteriaTrees.find(itr->second->Entry->ParentID);
        if (parent != _criteriaTrees.end())
        {
            parent->second->Children.push_back(itr->second);
            while (parent != _criteriaTrees.end())
            {
                auto cur = parent;
                parent = _criteriaTrees.find(parent->second->Entry->ParentID);
                if (parent == _criteriaTrees.end())
                    if (sCriteriaStore.LookupEntry(itr->second->Entry->CriteriaID))
                        _criteriaTreeByCriteria[itr->second->Entry->CriteriaID].push_back(cur->second);
            }
        }
        else if (sCriteriaStore.LookupEntry(itr->second->Entry->CriteriaID))
            _criteriaTreeByCriteria[itr->second->Entry->CriteriaID].push_back(itr->second);
    }

    // Load criteria
    uint32 criterias = 0;
    uint32 guildCriterias = 0;
    uint32 scenarioCriterias = 0;
    for (CriteriaEntry const* criteriaEntry : sCriteriaStore)
    {
        auto treeItr = _criteriaTreeByCriteria.find(criteriaEntry->ID);
        if (treeItr == _criteriaTreeByCriteria.end())
            continue;

        Criteria* criteria = new Criteria();
        criteria->ID = criteriaEntry->ID;
        criteria->Entry = criteriaEntry;
        auto mod = _criteriaModifiers.find(criteriaEntry->ModifierTreeId);
        if (mod != _criteriaModifiers.end())
            criteria->Modifier = mod->second;

        _criteria[criteria->ID] = criteria;

        for (CriteriaTree const* tree : treeItr->second)
        {
            if (AchievementEntry const* achievement = tree->Achievement)
            {
                if (achievement->Flags & ACHIEVEMENT_FLAG_GUILD)
                    criteria->FlagsCu |= CRITERIA_FLAG_CU_GUILD;
                else if (achievement->Flags & ACHIEVEMENT_FLAG_ACCOUNT)
                    criteria->FlagsCu |= CRITERIA_FLAG_CU_ACCOUNT;
                else if (achievement->Flags & ACHIEVEMENT_FLAG_SHOW_CRITERIA_MEMBERS)
                    criteria->FlagsCu |= CRITERIA_FLAG_CU_SHOW_CRITERIA_MEMBERS;
                else
                    criteria->FlagsCu |= CRITERIA_FLAG_CU_PLAYER;
            }
            else if (tree->ScenarioStep)
                criteria->FlagsCu |= (CRITERIA_FLAG_CU_SCENARIO | CRITERIA_FLAG_CU_SHOW_CRITERIA_MEMBERS);
        }

        if (criteria->FlagsCu & (CRITERIA_FLAG_CU_PLAYER | CRITERIA_FLAG_CU_ACCOUNT))
        {
            ++criterias;
            _criteriasByType[criteriaEntry->Type].push_back(criteria);
        }

        if (criteria->FlagsCu & CRITERIA_FLAG_CU_GUILD)
        {
            ++guildCriterias;
            _guildCriteriasByType[criteriaEntry->Type].push_back(criteria);
        }

        if (criteria->FlagsCu & CRITERIA_FLAG_CU_SCENARIO)
        {
            ++scenarioCriterias;
            _scenarioCriteriasByType[criteriaEntry->Type].push_back(criteria);
        }

        if (criteriaEntry->StartEvent)
            _criteriasByTimedType[criteriaEntry->StartEvent].push_back(criteria);

        if (criteriaEntry->FailEvent)
            _criteriasByFailEvent[criteriaEntry->FailEvent].push_back(criteria);
    }

    for (auto& p : _criteriaTrees)
        const_cast<CriteriaTree*>(p.second)->Criteria = GetCriteria(p.second->Entry->CriteriaID);

    TC_LOG_INFO("server.loading", ">> Loaded %u criteria, %u guild criteria and %u scenario criteria in %u ms.", criterias, guildCriterias, scenarioCriterias, GetMSTimeDiffToNow(oldMSTime));
}

CriteriaTree const* CriteriaMgr::GetCriteriaTree(uint32 criteriaTreeId) const
{
    auto itr = _criteriaTrees.find(criteriaTreeId);
    if (itr == _criteriaTrees.end())
        return nullptr;

    return itr->second;
}

Criteria const* CriteriaMgr::GetCriteria(uint32 criteriaId) const
{
    auto itr = _criteria.find(criteriaId);
    if (itr == _criteria.end())
        return nullptr;

    return itr->second;
}
