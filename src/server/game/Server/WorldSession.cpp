/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 /** \file
     \ingroup u2w
 */

#include "WorldSocket.h"                                    // must be first to make ACE happy with ACE includes in it
#include <zlib.h>
#include "Config.hpp"
#include "Common.h"
#include "DatabaseEnv.h"
#include "AccountMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Player.h"
#include "Vehicle.h"
#include "ObjectMgr.h"
#include "GuildMgr.h"
#include "Group.h"
#include "Guild.h"
#include "World.h"
#include "ObjectAccessor.h"
#include "BattlegroundMgr.h"
#include "OutdoorPvPMgr.h"
#include "MapManager.h"
#include "SocialMgr.h"
#include "zlib.h"
#include "ScriptMgr.h"
#include "Transport.h"
#include "WardenWin.h"
#include "WardenMac.h"
#include "BattlePet.h"
#include "BattlePetQueue.h"
#include "Pet.h"
#include "QueryCallback.h"
#include "BattlePayShop.h"
#include "ReferAFriend.h"
#include "GameTime.h"

namespace
{
    std::string const DefaultPlayerName = "<none>";
} // namespace

bool MapSessionFilter::Process( WorldPacket* packet )
{
    OpcodeHandler const* opHandle = clientOpcodeTable[ packet->GetOpcode() ];

    //let's check if our opcode can be really processed in Map::Update()
    if ( opHandle->ProcessingPlace == PROCESS_INPLACE )
        return true;

    //we do not process thread-unsafe packets
    if ( opHandle->ProcessingPlace == PROCESS_THREADUNSAFE )
        return false;

    Player* player = m_pSession->GetPlayer();
    if ( !player )
        return false;

    //in Map::Update() we do not process packets where player is not in world!
    return player->IsInWorld();
}

//we should process ALL packets when player is not in world/logged in
//OR packet handler is not thread-safe!
bool WorldSessionFilter::Process( WorldPacket* packet )
{
    OpcodeHandler const* opHandle = clientOpcodeTable[ packet->GetOpcode() ];
    //check if packet handler is supposed to be safe
    if ( opHandle->ProcessingPlace == PROCESS_INPLACE )
        return true;

    //thread-unsafe packets should be processed in World::UpdateSessions()
    if ( opHandle->ProcessingPlace == PROCESS_THREADUNSAFE )
        return true;

    //no player attached? -> our client! ^^
    Player* player = m_pSession->GetPlayer();
    if ( !player )
        return true;

    //lets process all packets for non-in-the-world player
    return ( player->IsInWorld() == false );
}

/// WorldSession constructor
WorldSession::WorldSession( uint32 id, WorldSocket* sock, AccountTypes sec, uint8 expansion, time_t mute_time, LocaleConstant locale) :
    m_muteTime( mute_time ),
    m_timeOutTime( 0 ),
    AntiDOS( this ),
    m_GUIDLow( 0 ),
    _player( nullptr ),
    m_Socket( sock ),
    _security( sec ),
    _accountId( id ),
    m_expansion( expansion ),
    _warden( nullptr ),
    _logoutTime( 0 ),
    m_inQueue( false ),
    m_playerLoading( false ),
    m_playerLogout( false ),
    m_playerRecentlyLogout( false ),
    m_playerSave( false ),
    m_sessionDbcLocale( sWorld->GetAvailableDbcLocale( locale ) ),
    m_sessionDbLocaleIndex( locale ),
    m_latency( 0 ),
    m_clientTimeDelay( 0 ),
    m_TutorialsChanged( TUTORIALS_FLAG_NONE ),
    _filterAddonMessages( false ),
    _RBACData( nullptr ),
    expireTime( 60000 ), // 1 min after socket loss, session is deleted
    forceExit( false ),
    m_currentBankerGUID(),
    m_guildRosterTimer( 0 ),
    _battlePayShop( nullptr ),
    _rafData( nullptr )
{
    memset( m_Tutorials, 0, sizeof( m_Tutorials ) );

    if ( sock )
    {
        m_Address = sock->GetRemoteAddress();
        sock->AddReference();
        ResetTimeOutTime(false);
        LoginDatabase.PExecute( "UPDATE account SET online = 1 WHERE id = %u;", GetAccountId() );     // One-time query
    }
}

/// WorldSession destructor
WorldSession::~WorldSession()
{
    ///- unload player if not unloaded
    if ( _player )
        LogoutPlayer( true );

    /// - If have unclosed socket, close it
    if ( m_Socket )
    {
        m_Socket->CloseSocket();
        m_Socket->RemoveReference();
        m_Socket = NULL;
    }

    delete _warden;
    delete _RBACData;

    // first save, then delete
    if ( _battlePayShop )
    {
        _battlePayShop->SaveToDB();
        delete _battlePayShop;
    }

    // first save, then delete
    if (_rafData)
    {
        _rafData->SaveToDB();
        delete _rafData;
    }

    ///- empty incoming packet queue
    WorldPacket* packet = NULL;
    while ( _recvQueue.next( packet ) )
        delete packet;

    LoginDatabase.PExecute( "UPDATE account SET online = 0 WHERE id = %u;", GetAccountId() );     // One-time query
}

std::string const & WorldSession::GetPlayerName() const
{
    return _player != NULL ? _player->GetName() : DefaultPlayerName;
}

std::string WorldSession::GetPlayerInfo() const
{
    std::ostringstream ss;

    ss << "[Player: " << GetPlayerName()
        << " (Guid: " << ( _player != NULL ? _player->GetGUID().ToString() : "" )
        << ", Account: " << GetAccountId() << ")]";

    return ss.str();
}

/// Get player guid if available. Use for logging purposes only
uint32 WorldSession::GetGUIDLow() const
{
    return GetPlayer() ? GetPlayer()->GetGUID().GetCounter() : 0;
}

/// Send a packet to the client
void WorldSession::SendPacket( WorldPacket const* packet, bool forced /*= false*/ )
{
    if ( !m_Socket )
        return;

    if ( packet->GetOpcode() == NULL_OPCODE )
    {
        TC_LOG_ERROR( "network.opcode", "Prevented sending of NULL_OPCODE to %s", GetPlayerInfo().c_str() );
        return;
    }
    else if ( packet->GetOpcode() == UNKNOWN_OPCODE )
    {
        TC_LOG_ERROR( "network.opcode", "Prevented sending of UNKNOWN_OPCODE to %s", GetPlayerInfo().c_str() );
        return;
    }

    if ( !forced )
    {
        OpcodeHandler const* handler = serverOpcodeTable[ packet->GetOpcode() ];
        if ( !handler || handler->Status == STATUS_UNHANDLED )
        {
            TC_LOG_ERROR( "network.opcode", "Prevented sending disabled opcode %s to %s", GetOpcodeNameForLogging( packet->GetOpcode(), true ).c_str(), GetPlayerInfo().c_str() );
            return;
        }
    }

#ifdef TRINITY_DEBUG
    // Code for network use statistic
    static uint64 sendPacketCount = 0;
    static uint64 sendPacketBytes = 0;

    static time_t firstTime = time( NULL );
    static time_t lastTime = firstTime;                     // next 60 secs start time

    static uint64 sendLastPacketCount = 0;
    static uint64 sendLastPacketBytes = 0;

    time_t cur_time = time( NULL );

    if ( ( cur_time - lastTime ) < 60 )
    {
        sendPacketCount += 1;
        sendPacketBytes += packet->size();

        sendLastPacketCount += 1;
        sendLastPacketBytes += packet->size();
    }
    else
    {
        uint64 minTime = uint64( cur_time - lastTime );
        uint64 fullTime = uint64( lastTime - firstTime );
        TC_LOG_DEBUG( "misc", "Send all time packets count: " UI64FMTD " bytes: " UI64FMTD " avr.count/sec: %f avr.bytes/sec: %f time: %u", sendPacketCount, sendPacketBytes, float( sendPacketCount ) / fullTime, float( sendPacketBytes ) / fullTime, uint32( fullTime ) );
        TC_LOG_DEBUG( "misc", "Send last min packets count: " UI64FMTD " bytes: " UI64FMTD " avr.count/sec: %f avr.bytes/sec: %f", sendLastPacketCount, sendLastPacketBytes, float( sendLastPacketCount ) / minTime, float( sendLastPacketBytes ) / minTime );

        lastTime = cur_time;
        sendLastPacketCount = 1;
        sendLastPacketBytes = packet->wpos();               // wpos is real written size
    }
#endif                                                      // !TRINITY_DEBUG

    if ( m_Socket->SendPacket( *packet ) == -1 )
        m_Socket->CloseSocket();
}

/// Add an incoming packet to the queue
void WorldSession::QueuePacket( WorldPacket* new_packet )
{
    _recvQueue.add( new_packet );
}

/// Logging helper for unexpected opcodes
void WorldSession::LogUnexpectedOpcode( WorldPacket* packet, const char* status, const char *reason )
{
    TC_LOG_ERROR( "network.opcode", "Received unexpected opcode %s Status: %s Reason: %s from %s",
                  GetOpcodeNameForLogging( packet->GetOpcode(), false ).c_str(), status, reason, GetPlayerInfo().c_str() );
}

/// Logging helper for unexpected opcodes
void WorldSession::LogUnprocessedTail( WorldPacket* packet )
{
    if ( !sLog->ShouldLog( "network.opcode", LOG_LEVEL_TRACE ) || packet->rpos() >= packet->wpos() )
        return;

    TC_LOG_TRACE( "network.opcode", "Unprocessed tail data (read stop at %u from %u) Opcode %s from %s",
                  uint32( packet->rpos() ), uint32( packet->wpos() ), GetOpcodeNameForLogging( packet->GetOpcode(), false ).c_str(), GetPlayerInfo().c_str() );
    packet->print_storage();
}

/// Update the WorldSession (triggered by World update)
bool WorldSession::Update( uint32 diff, PacketFilter& updater )
{
    /// Update Timeout timer.
    UpdateTimeOutTime( diff );

    ///- Before we process anything:
    /// If necessary, kick the player because the client didn't send anything for too long
    /// (or they've been idling in character select)
    if ( IsConnectionIdle() )
        m_Socket->CloseSocket();

    ///- Retrieve packets from the receive queue and call the appropriate handlers
    /// not process packets if socket already closed
    WorldPacket* packet = NULL;
    WorldPacket* movementPacket = NULL;
    //! Delete packet after processing by default
    bool deletePacket = true;
    std::vector<WorldPacket*> requeuePackets;
    uint32 processedPackets = 0;
    time_t currentTime = time( NULL );

    while ( m_Socket && !m_Socket->IsClosed() && _recvQueue.next( packet, updater ) )
    {
        OpcodeHandler const* opHandle = clientOpcodeTable[ packet->GetOpcode() ];
        try
        {
            switch ( opHandle->Status )
            {
                case STATUS_LOGGEDIN:
                    if ( !_player )
                    {
                        // skip STATUS_LOGGEDIN opcode unexpected errors if player logout sometime ago - this can be network lag delayed packets
                        //! If player didn't log out a while ago, it means packets are being sent while the server does not recognize
                        //! the client to be in world yet. We will re-add the packets to the bottom of the queue and process them later.
                        if ( !m_playerRecentlyLogout )
                        {
                            requeuePackets.push_back(packet);
                            deletePacket = false;
                            TC_LOG_DEBUG("network", "Re-enqueueing packet with opcode %s with with status STATUS_LOGGEDIN. "
                                "Player is currently not in world yet.", GetOpcodeNameForLogging(packet->GetOpcode(), false).c_str());
                        }
                    }
                    else if ( _player->IsInWorld() && AntiDOS.EvaluateOpcode( *packet, currentTime ) )
                    {
                        sScriptMgr->OnPacketReceive( m_Socket, WorldPacket( *packet ) );

                        // groupped movement packets
                        if ( packet->GetOpcode() > CMSG_GROUPPED_MOVEMENT_PACKET)
                        {
                            if ( movementPacket )
                                delete movementPacket;
                            movementPacket = new WorldPacket( packet->GetOpcode(), packet->size() );
                            movementPacket->append(* ( (ByteBuffer* )packet ) );
                        }
                        else
                        {
                            if ( movementPacket )
                            {
                                HandleMovementOpcodes( *movementPacket );
                                delete movementPacket;
                                movementPacket = NULL;
                            }

                            ( this->*opHandle->Handler )( *packet );
                        }

                        LogUnprocessedTail( packet );
                    }
                    // lag can cause STATUS_LOGGEDIN opcodes to arrive after the player started a transfer
                    break;
                case STATUS_LOGGEDIN_OR_RECENTLY_LOGGOUT:
                    if ( !_player && !m_playerRecentlyLogout && !m_playerLogout ) // There's a short delay between _player = null and m_playerRecentlyLogout = true during logout
                        LogUnexpectedOpcode( packet, "STATUS_LOGGEDIN_OR_RECENTLY_LOGGOUT",
                                             "the player has not logged in yet and not recently logout" );
                    else if ( AntiDOS.EvaluateOpcode( *packet, currentTime ) )
                    {
                        if ( movementPacket )
                        {
                            if ( _player && _player->IsInWorld() )
                                HandleMovementOpcodes( *movementPacket );
                            delete movementPacket;
                            movementPacket = NULL;
                        }

                        // not expected _player or must checked in packet hanlder
                        sScriptMgr->OnPacketReceive( m_Socket, WorldPacket( *packet ) );
                        ( this->*opHandle->Handler )( *packet );
                        LogUnprocessedTail( packet );
                    }
                    break;
                case STATUS_TRANSFER:
                    if ( !_player )
                        LogUnexpectedOpcode( packet, "STATUS_TRANSFER", "the player has not logged in yet" );
                    else if ( _player->IsInWorld() )
                        LogUnexpectedOpcode( packet, "STATUS_TRANSFER", "the player is still in world" );
                    else if ( AntiDOS.EvaluateOpcode( *packet, currentTime ) )
                    {
                        if (movementPacket)
                        {
                            delete movementPacket;
                            movementPacket = NULL;
                        }

                        sScriptMgr->OnPacketReceive( m_Socket, WorldPacket( *packet ) );
                        ( this->*opHandle->Handler )( *packet );
                        LogUnprocessedTail( packet );
                    }
                    break;
                case STATUS_AUTHED:
                    // prevent cheating with skip queue wait
                    if ( m_inQueue )
                    {
                        LogUnexpectedOpcode( packet, "STATUS_AUTHED", "the player not pass queue yet" );
                        break;
                    }

                    // some auth opcodes can be recieved before STATUS_LOGGEDIN_OR_RECENTLY_LOGGOUT opcodes
                    // however when we recieve CMSG_CHAR_ENUM we are surely no longer during the logout process.
                    if ( packet->GetOpcode() == CMSG_CHAR_ENUM )
                        m_playerRecentlyLogout = false;

                    if ( AntiDOS.EvaluateOpcode( *packet, currentTime ) )
                    {
                        sScriptMgr->OnPacketReceive( m_Socket, WorldPacket( *packet ) );
                        ( this->*opHandle->Handler )( *packet );
                        LogUnprocessedTail( packet );
                    }
                    break;
                case STATUS_NEVER:
                    TC_LOG_ERROR( "network.opcode", "Received not allowed opcode %s from %s", GetOpcodeNameForLogging( packet->GetOpcode(), false ).c_str(),
                                  GetPlayerInfo().c_str() );
                    break;
                case STATUS_UNHANDLED:
                    TC_LOG_ERROR( "network.opcode", "Received not handled opcode %s from %s", GetOpcodeNameForLogging( packet->GetOpcode(), false ).c_str(),
                                  GetPlayerInfo().c_str() );
                    break;
            }
        }
        catch ( ByteBufferException const& )
        {
            TC_LOG_ERROR( "network", "WorldSession::Update ByteBufferException occured while parsing a packet (opcode: %s) from client %s, accountid=%i. Skipped packet.",
                          GetOpcodeNameForLogging( packet->GetOpcode(), false ).c_str(), GetRemoteAddress().c_str(), GetAccountId() );
            packet->hexlike();
        }

        if ( deletePacket )
            delete packet;

        deletePacket = true;

#define MAX_PROCESSED_PACKETS_IN_SAME_WORLDSESSION_UPDATE 100
        processedPackets++;

        //process only a max amout of packets in 1 Update() call.
        //Any leftover will be processed in next update
        if ( processedPackets > MAX_PROCESSED_PACKETS_IN_SAME_WORLDSESSION_UPDATE )
            break;
    }

    if ( movementPacket )
    {
        if ( _player && _player->IsInWorld() )
            HandleMovementOpcodes( *movementPacket );
        delete movementPacket;
        movementPacket = NULL;
    }

    _recvQueue.readd(requeuePackets.begin(), requeuePackets.end());

    if (m_Socket && !m_Socket->IsClosed())
    {
        if (_warden)
            _warden->Update();

        ProcessQueryCallbacks();
    }

    //check if we are safe to proceed with logout
    //logout procedure should happen only in World::UpdateSessions() method!!!
    if ( updater.ProcessLogout() )
    {
        time_t currTime = time( NULL );
        ///- If necessary, log the player out
        if ( ShouldLogOut( currTime ) && !m_playerLoading )
            LogoutPlayer( true );

        if ( m_Socket && GetPlayer() && _warden )
            _warden->Update();

        ///- Cleanup socket pointer if need
        if ( m_Socket && m_Socket->IsClosed() )
        {
            expireTime -= expireTime > diff ? diff : expireTime;
            if ( expireTime < diff || forceExit || !GetPlayer() )
            {
                m_Socket->RemoveReference();
                m_Socket = NULL;
            }
        }

        if ( !m_Socket )
            return false;                                       //Will remove this session from the world session map
    }

    return true;
}

/// %Log the player out
void WorldSession::LogoutPlayer( bool save )
{
    // finish pending transfers before starting the logout
    while ( _player && _player->IsBeingTeleportedFar() )
        HandleMoveWorldportAckOpcode();

    m_playerLogout = true;
    m_playerSave = save;

    if ( _player )
    {
        PlayerLootObjects& lootObjects = _player->GetLootObjects();
        if ( !lootObjects.empty() )
            for ( auto& lootItr : lootObjects )
                if ( Loot* loot = lootItr.second )
                    DoLootRelease( loot );

        ///- If the player just died before logging out, make him appear as a ghost
        if ( _player->GetDeathTimer() )
        {
            _player->GetHostileRefManager().deleteReferences();
            _player->BuildPlayerRepop();
            _player->RepopAtGraveyard();
        }
        else if ( _player->IsInCombat() )
        {
            _player->GetHostileRefManager().deleteReferences();
            _player->BuildPlayerRepop();
            _player->KillPlayer();
            _player->RepopAtGraveyard();
        }
        else if ( _player->HasAuraType( SPELL_AURA_SPIRIT_OF_REDEMPTION ) )
        {
            // this will kill character by SPELL_AURA_SPIRIT_OF_REDEMPTION
            _player->RemoveAurasByType( SPELL_AURA_MOD_SHAPESHIFT );
            _player->KillPlayer();
            _player->BuildPlayerRepop();
            _player->RepopAtGraveyard();
        }
        else if ( _player->HasPendingBind() )
        {
            _player->RepopAtGraveyard();
            _player->SetPendingBind( 0, 0 );
        }

        //drop a flag if player is carrying it
        if ( Battleground* bg = _player->GetBattleground() )
            bg->EventPlayerLoggedOut( _player );

        ///- Teleport to home if the player is in an invalid instance
        if ( !_player->m_InstanceValid && !_player->IsGameMaster() )
            _player->TeleportTo( _player->m_homebindMapId, _player->m_homebindX, _player->m_homebindY, _player->m_homebindZ, _player->GetOrientation() );

        sOutdoorPvPMgr->HandlePlayerLeaveZone( _player, _player->GetZoneId() );

        for ( int i = 0; i < PLAYER_MAX_BATTLEGROUND_QUEUES; ++i )
        {
            if ( BattlegroundQueueTypeId bgQueueTypeId = _player->GetBattlegroundQueueTypeId( i ) )
            {
                _player->RemoveBattlegroundQueueId( bgQueueTypeId );

                if (bgQueueTypeId == BATTLEGROUND_QUEUE_RB)
                {
                    for (uint8 i = BATTLEGROUND_QUEUE_AV; i < BATTLEGROUND_QUEUE_RB; ++i)
                    {
                        bgQueueTypeId = BattlegroundQueueTypeId(i);

                        BattlegroundQueue& queue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
                        queue.RemovePlayer(_player->GetGUID(), true);
                    }
                }
                else
                {
                    BattlegroundQueue& queue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
                    queue.RemovePlayer(_player->GetGUID(), true);
                }
            }
        }

        // Repop at GraveYard or other player far teleport will prevent saving player because of not present map
        // Teleport player immediately for correct player save
        while ( _player->IsBeingTeleportedFar() )
            HandleMoveWorldportAckOpcode();

        ///- If the player is in a guild, update the guild roster and broadcast a logout message to other guild members
        if ( Guild* guild = sGuildMgr->GetGuildById( _player->GetGuildId() ) )
            guild->HandleMemberLogout( this );

        ///- Remove pet
        if (Pet* pet = _player->GetPet())
            _player->RemovePet(pet, PET_SLOT_LOG_OUT, true, pet->IsStampeded());
        else if (_player->GetClass() != CLASS_HUNTER)
            _player->RemovePet(NULL, PET_SLOT_LOG_OUT, true, true);

        ///- Clear whisper whitelist
        _player->ClearWhisperWhiteList();

        ///- empty buyback items and save the player in the database
        // some save parts only correctly work in case player present in map/player_lists (pets, etc)
        if ( save )
        {
            uint32 eslot;
            for ( int j = BUYBACK_SLOT_START; j < BUYBACK_SLOT_END; ++j )
            {
                eslot = j - BUYBACK_SLOT_START;
                _player->SetGuidValue( PLAYER_VENDORBUYBACK_SLOTS + ( eslot * 2 ), ObjectGuid::Empty );
                _player->SetUInt32Value( PLAYER_BUYBACK_PRICE + eslot, 0 );
                _player->SetUInt32Value( PLAYER_BUYBACK_TIMESTAMP + eslot, 0 );
            }
            _player->SaveToDB();
        }

        ///- Leave all channels before player delete...
        _player->CleanupChannels();

        // if player is leader of a group and is holding a ready check, complete it early
        _player->ReadyCheckComplete();

        ///- If the player is in a group (or invited), remove him. If the group if then only 1 person, disband the group.
        _player->UninviteFromGroup();

        // remove player from the group if he is:
        // a) in group; b) not in raid group; c) logging out normally (not being kicked or disconnected)
        if ( _player->GetGroup() && !_player->GetGroup()->IsRaidGroup() && m_Socket )
            _player->RemoveFromGroup();

        //! Send update to group and reset stored max enchanting level
        if ( _player->GetGroup() )
        {
            _player->GetGroup()->SendUpdate();
            _player->GetGroup()->ResetMaxEnchantingLevel();
        }

        //! Broadcast a logout message to the player's friends
        sSocialMgr->SendFriendStatus( _player, FRIEND_OFFLINE, _player->GetGUID(), true );
        sSocialMgr->RemovePlayerSocial( _player->GetGUID() );

        // remove from battle pet queues
        sPetBattleQueueMgr->RemovePlayerFromPetBattleQueue(_player->GetGUID());

        // forfeit any pet battles in progress
        sPetBattleSystem->ForfeitBattle(_player->GetGUID());

        //! Call script hook before deletion
        sScriptMgr->OnPlayerLogout( _player );

        // Save BattlePayShop
        if (_battlePayShop)
        {
            _battlePayShop->SaveToDB();
            _battlePayShop->ResetSaveVariables();
        }

        // Save RaF Data
        if (_rafData)
            _rafData->SaveToDB();

        //! Remove the player from the world
        // the player may not be in the world when logging out
        // e.g if he got disconnected during a transfer to another map
        // calls to GetMap in this case may cause crashes
        _player->CleanupsBeforeDelete();
        TC_LOG_INFO( "entities.player.character", "Account: %d (IP: %s) Logout Character:[%s] (GUID: %u) Level: %d",
                     GetAccountId(), GetRemoteAddress().c_str(), _player->GetName().c_str(), _player->GetGUID().GetCounter(), _player->GetLevel() );
        if ( Map* _map = _player->FindMap() )
            _map->RemovePlayerFromMap( _player, true );

        SetPlayer( NULL ); //! Pointer already deleted during RemovePlayerFromMap

        //! Send the 'logout complete' packet to the client
        //! Client will respond by sending 3x CMSG_CANCEL_TRADE, which we currently dont handle
        WorldPacket data( SMSG_LOGOUT_COMPLETE, 1 + 8 + 1 );
        ObjectGuid guid; // Autolog guid - 0 for logout

        data.WriteBit( 0 ); // Dafuck ? 1st bit twice read ??????

        data.WriteGuidMask( guid, 3, 2, 1, 4, 6, 7, 5, 0 );

        data.FlushBits();

        data.WriteGuidBytes( guid, 6, 4, 1, 2, 7, 3, 0, 5 );

        SendPacket( &data );

        TC_LOG_DEBUG( "network", "SESSION: Sent SMSG_LOGOUT_COMPLETE Message" );

        //! Since each account can only have one online character at any given time, ensure all characters for active account are marked as offline
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement( CHAR_UPD_ACCOUNT_ONLINE );
        stmt->setUInt32( 0, GetAccountId() );
        CharacterDatabase.Execute( stmt );
    }

    m_playerLogout = false;
    m_playerSave = false;
    m_playerRecentlyLogout = true;
    LogoutRequest( 0 );
}

/// Kick a player out of the World
void WorldSession::KickPlayer()
{
    if ( m_Socket )
    {
        m_Socket->CloseSocket();
        forceExit = true;
    }
}

void WorldSession::SendNotification( const char *format, ... )
{
    if ( format )
    {
        va_list ap;
        char szStr[ 1024 ];
        szStr[ 0 ] = '\0';
        va_start( ap, format );
        vsnprintf( szStr, 1024, format, ap );
        va_end( ap );

        size_t len = strlen( szStr );

        WorldPacket data( SMSG_NOTIFICATION, 2 + len );

        data.WriteBits( len, 12 );

        data.FlushBits();

        data.WriteString( szStr );

        SendPacket( &data );
    }
}

void WorldSession::SendNotification( uint32 string_id, ... )
{
    char const* format = GetTrinityString( string_id );
    if ( format )
    {
        va_list ap;
        char szStr[ 1024 ];
        szStr[ 0 ] = '\0';
        va_start( ap, string_id );
        vsnprintf( szStr, 1024, format, ap );
        va_end( ap );

        size_t len = strlen( szStr );

        WorldPacket data( SMSG_NOTIFICATION, 2 + len );

        data.WriteBits( len, 12 );

        data.FlushBits();

        data.WriteString(szStr);

        SendPacket( &data );
    }
}

char const* WorldSession::GetTrinityString( uint32 entry ) const
{
    return sObjectMgr->GetTrinityString( entry, GetSessionDbLocaleIndex() );
}

void WorldSession::Handle_NULL( WorldPacket& recvData )
{
    TC_LOG_ERROR( "network.opcode", "Received unhandled opcode %s from %s",
                  GetOpcodeNameForLogging( recvData.GetOpcode(), false ).c_str(), GetPlayerInfo().c_str() );
}

void WorldSession::Handle_EarlyProccess( WorldPacket& recvData )
{
    TC_LOG_ERROR( "network.opcode", "Received opcode %s that must be processed in WorldSocket::OnRead from %s",
                  GetOpcodeNameForLogging( recvData.GetOpcode(), false ).c_str(), GetPlayerInfo().c_str() );
}

void WorldSession::Handle_Deprecated( WorldPacket& recvData )
{
    TC_LOG_ERROR( "network.opcode", "Received deprecated opcode %s from %s",
                  GetOpcodeNameForLogging( recvData.GetOpcode(), false ).c_str(), GetPlayerInfo().c_str() );
}

void WorldSession::LoadGlobalAccountData()
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement( CHAR_SEL_ACCOUNT_DATA );
    stmt->setUInt32( 0, GetAccountId() );
    LoadAccountData( CharacterDatabase.Query( stmt ), GLOBAL_CACHE_MASK );
}

void WorldSession::LoadAccountData( PreparedQueryResult result, uint32 mask )
{
    for ( uint32 i = 0; i < NUM_ACCOUNT_DATA_TYPES; ++i )
        if ( mask & ( 1 << i ) )
            m_accountData[ i ] = AccountData();

    if ( !result )
        return;

    do
    {
        Field* fields = result->Fetch();
        uint32 type = fields[ 0 ].GetUInt8();
        if ( type >= NUM_ACCOUNT_DATA_TYPES )
        {
            TC_LOG_ERROR( "misc", "Table `%s` have invalid account data type (%u), ignore.",
                          mask == GLOBAL_CACHE_MASK ? "account_data" : "character_account_data", type );
            continue;
        }

        if ( ( mask & ( 1 << type ) ) == 0 )
        {
            TC_LOG_ERROR( "misc", "Table `%s` have non appropriate for table  account data type (%u), ignore.",
                          mask == GLOBAL_CACHE_MASK ? "account_data" : "character_account_data", type );
            continue;
        }

        m_accountData[ type ].Time = time_t( fields[ 1 ].GetUInt32() );
        m_accountData[ type ].Data = fields[ 2 ].GetString();
    }
    while ( result->NextRow() );
}

void WorldSession::SetAccountData( AccountDataType type, time_t tm, std::string const& data )
{
    uint32 id = 0;
    uint32 index = 0;
    if ( ( 1 << type ) & GLOBAL_CACHE_MASK )
    {
        id = GetAccountId();
        index = CHAR_REP_ACCOUNT_DATA;
    }
    else
    {
        // _player can be NULL and packet received after logout but m_GUID still store correct guid
        if ( !m_GUIDLow )
            return;

        id = m_GUIDLow;
        index = CHAR_REP_PLAYER_ACCOUNT_DATA;
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement( index );
    stmt->setUInt32( 0, id );
    stmt->setUInt8( 1, type );
    stmt->setUInt32( 2, uint32( tm ) );
    stmt->setString( 3, data );
    CharacterDatabase.Execute( stmt );

    m_accountData[ type ].Time = tm;
    m_accountData[ type ].Data = data;
}

void WorldSession::SendAccountDataTimes( uint32 mask )
{
    WorldPacket data( SMSG_ACCOUNT_DATA_TIMES, 1 + NUM_ACCOUNT_DATA_TYPES * 4 + 4 + 4 );

    data.WriteBit( 1 );

    data.FlushBits();

    for ( uint32 i = 0; i < NUM_ACCOUNT_DATA_TYPES; ++i )
        data << uint32( GetAccountData( AccountDataType( i ) )->Time ); // also unix time

    data << uint32( mask );
    data << uint32( GameTime::GetGameTime() );

    SendPacket( &data );
}

void WorldSession::LoadTutorialsData()
{
    memset( m_Tutorials, 0, sizeof( uint32 ) * MAX_ACCOUNT_TUTORIAL_VALUES );

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement( CHAR_SEL_TUTORIALS );
    stmt->setUInt32( 0, GetAccountId() );
    if ( PreparedQueryResult result = CharacterDatabase.Query( stmt ) )
    {
        for ( uint8 i = 0; i < MAX_ACCOUNT_TUTORIAL_VALUES; ++i )
            m_Tutorials[ i ] = ( *result )[ i ].GetUInt32();

        m_TutorialsChanged |= TUTORIALS_FLAG_LOADED_FROM_DB;
    }

    m_TutorialsChanged &= ~TUTORIALS_FLAG_CHANGED;
}

void WorldSession::SendTutorialsData()
{
    WorldPacket data( SMSG_TUTORIAL_FLAGS, 4 * MAX_ACCOUNT_TUTORIAL_VALUES );

    for ( uint8 i = 0; i < MAX_ACCOUNT_TUTORIAL_VALUES; ++i )
        data << m_Tutorials[ i ];

    SendPacket( &data );
}

void WorldSession::SaveTutorialsData( SQLTransaction &trans )
{
    if ( !( m_TutorialsChanged & TUTORIALS_FLAG_CHANGED ) )
        return;

    bool const hasTutorialsInDB = ( m_TutorialsChanged & TUTORIALS_FLAG_LOADED_FROM_DB ) != 0;
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement( hasTutorialsInDB ? CHAR_UPD_TUTORIALS : CHAR_INS_TUTORIALS );
    for ( uint8 i = 0; i < MAX_ACCOUNT_TUTORIAL_VALUES; ++i )
        stmt->setUInt32( i, m_Tutorials[ i ] );
    stmt->setUInt32( MAX_ACCOUNT_TUTORIAL_VALUES, GetAccountId() );
    trans->Append( stmt );

    // now has, set flag so next save uses update query
    if ( !hasTutorialsInDB )
        m_TutorialsChanged |= TUTORIALS_FLAG_LOADED_FROM_DB;

    m_TutorialsChanged &= ~TUTORIALS_FLAG_CHANGED;
}

void WorldSession::ReadAddonsInfo( WorldPacket &recvData )
{
    if ( recvData.rpos() + 4 > recvData.size() )
        return;

    uint32 size;
    recvData >> size;

    if ( !size )
        return;

    if ( size > 0xFFFFF )
    {
        TC_LOG_ERROR( "misc", "WorldSession::ReadAddonsInfo addon info too big, size %u", size );
        return;
    }

    uLongf uSize = size;

    uint32 pos = recvData.rpos();

    ByteBuffer data;
    data.resize( size );

    bool success = uncompress( data.contents(), &uSize, recvData.contents() + pos, recvData.size() - pos ) == Z_OK;
    if ( !success )
    {
        TC_LOG_ERROR( "misc", "Addon packet uncompress error!" );
        return;
    }

    uint32 addonsCount;
    data >> addonsCount;                         // addons count

    m_addonsList.resize( addonsCount );

    for ( uint32 idx = 0; idx < addonsCount; ++idx )
    {
        // check next addon data format correctness
        if ( data.rpos() + 1 > data.size() )
        {
            m_addonsList.resize( idx );
            return;
        }

        AddonInfo& addonInfo = m_addonsList[ idx ];
        addonInfo.Enabled = true;
        addonInfo.State = 2;

        data >> addonInfo.Name;

        uint8 usePublicKeyOrCrc;
        data >> usePublicKeyOrCrc;

        addonInfo.UsePublicKeyOrCRC = usePublicKeyOrCrc;

        data >> addonInfo.CRC;

        uint32 urlFile;
        data >> urlFile;

        TC_LOG_DEBUG( "misc", "ADDON: Name: %s, UsePubKey: 0x%x, CRC: 0x%x, UrlFile: %i", addonInfo.Name.c_str(), addonInfo.UsePublicKeyOrCRC, addonInfo.CRC, urlFile );

        auto knownAddon = sAddonMgr->GetKnownAddon( addonInfo.Name );
        if ( knownAddon.is_initialized() )
        {
            if ( addonInfo.CRC != knownAddon->CRC )
            {
                TC_LOG_ERROR( "misc", "ADDON: %s was known, but didn't match known CRC (0x%x)!", addonInfo.Name.c_str(), knownAddon->CRC );
            }
            else
            {
                TC_LOG_DEBUG( "misc", "ADDON: %s was known, CRC is correct (0x%x)", addonInfo.Name.c_str(), knownAddon->CRC );
            }
        }
        else
        {
            sAddonMgr->SaveAddon( addonInfo );

            TC_LOG_DEBUG( "misc", "ADDON: %s (0x%x) was not known, saving...", addonInfo.Name.c_str(), addonInfo.CRC );
        }
    }

    uint32 currentTime;
    data >> currentTime;

    TC_LOG_DEBUG( "network", "ADDON: CurrentTime: %u", currentTime );
}

void WorldSession::SendAddonsInfo()
{
    uint8 addonPublicKey[ 256 ] =
    {
        0xC3, 0x5B, 0x50, 0x84, 0xB9, 0x3E, 0x32, 0x42, 0x8C, 0xD0, 0xC7, 0x48, 0xFA, 0x0E, 0x5D, 0x54,
        0x5A, 0xA3, 0x0E, 0x14, 0xBA, 0x9E, 0x0D, 0xB9, 0x5D, 0x8B, 0xEE, 0xB6, 0x84, 0x93, 0x45, 0x75,
        0xFF, 0x31, 0xFE, 0x2F, 0x64, 0x3F, 0x3D, 0x6D, 0x07, 0xD9, 0x44, 0x9B, 0x40, 0x85, 0x59, 0x34,
        0x4E, 0x10, 0xE1, 0xE7, 0x43, 0x69, 0xEF, 0x7C, 0x16, 0xFC, 0xB4, 0xED, 0x1B, 0x95, 0x28, 0xA8,
        0x23, 0x76, 0x51, 0x31, 0x57, 0x30, 0x2B, 0x79, 0x08, 0x50, 0x10, 0x1C, 0x4A, 0x1A, 0x2C, 0xC8,
        0x8B, 0x8F, 0x05, 0x2D, 0x22, 0x3D, 0xDB, 0x5A, 0x24, 0x7A, 0x0F, 0x13, 0x50, 0x37, 0x8F, 0x5A,
        0xCC, 0x9E, 0x04, 0x44, 0x0E, 0x87, 0x01, 0xD4, 0xA3, 0x15, 0x94, 0x16, 0x34, 0xC6, 0xC2, 0xC3,
        0xFB, 0x49, 0xFE, 0xE1, 0xF9, 0xDA, 0x8C, 0x50, 0x3C, 0xBE, 0x2C, 0xBB, 0x57, 0xED, 0x46, 0xB9,
        0xAD, 0x8B, 0xC6, 0xDF, 0x0E, 0xD6, 0x0F, 0xBE, 0x80, 0xB3, 0x8B, 0x1E, 0x77, 0xCF, 0xAD, 0x22,
        0xCF, 0xB7, 0x4B, 0xCF, 0xFB, 0xF0, 0x6B, 0x11, 0x45, 0x2D, 0x7A, 0x81, 0x18, 0xF2, 0x92, 0x7E,
        0x98, 0x56, 0x5D, 0x5E, 0x69, 0x72, 0x0A, 0x0D, 0x03, 0x0A, 0x85, 0xA2, 0x85, 0x9C, 0xCB, 0xFB,
        0x56, 0x6E, 0x8F, 0x44, 0xBB, 0x8F, 0x02, 0x22, 0x68, 0x63, 0x97, 0xBC, 0x85, 0xBA, 0xA8, 0xF7,
        0xB5, 0x40, 0x68, 0x3C, 0x77, 0x86, 0x6F, 0x4B, 0xD7, 0x88, 0xCA, 0x8A, 0xD7, 0xCE, 0x36, 0xF0,
        0x45, 0x6E, 0xD5, 0x64, 0x79, 0x0F, 0x17, 0xFC, 0x64, 0xDD, 0x10, 0x6F, 0xF3, 0xF5, 0xE0, 0xA6,
        0xC3, 0xFB, 0x1B, 0x8C, 0x29, 0xEF, 0x8E, 0xE5, 0x34, 0xCB, 0xD1, 0x2A, 0xCE, 0x79, 0xC3, 0x9A,
        0x0D, 0x36, 0xEA, 0x01, 0xE0, 0xAA, 0x91, 0x20, 0x54, 0xF0, 0x72, 0xD8, 0x1E, 0xC7, 0x89, 0xD2
    };

    uint8 pubKeyOrder[ 256 ] =
    {
        0x05, 0xB0, 0x94, 0x2B, 0x1C, 0x87, 0x40, 0x08, 0xA0, 0x91, 0xE2, 0x77, 0xB5, 0xC0, 0xF0, 0x48,
        0xF3, 0xD4, 0xD1, 0xAC, 0x15, 0xED, 0x55, 0x0A, 0x4B, 0x75, 0xF4, 0x52, 0x18, 0x14, 0x12, 0x4C,
        0x43, 0x39, 0x9D, 0x3B, 0xC6, 0x5A, 0x16, 0x06, 0x31, 0x0C, 0x5F, 0xC1, 0x76, 0x5E, 0x28, 0x62,
        0xFF, 0xA9, 0xD6, 0x53, 0x80, 0xDB, 0x49, 0xF7, 0x84, 0xCA, 0xDA, 0x9A, 0x70, 0x83, 0xB1, 0x6F,
        0x90, 0x38, 0x27, 0x98, 0x30, 0x3F, 0x19, 0x72, 0x26, 0x54, 0x63, 0xA5, 0x7E, 0x22, 0x45, 0xB7,
        0xB9, 0x34, 0x67, 0x24, 0xE9, 0x03, 0x2F, 0x8D, 0xA2, 0xE8, 0xC2, 0xFD, 0x74, 0x1B, 0x50, 0x2E,
        0x59, 0x6B, 0xBD, 0x0E, 0xE1, 0xA7, 0x8C, 0xFA, 0xBC, 0x11, 0x1D, 0x89, 0x85, 0x4A, 0xB2, 0x3E,
        0xEC, 0x1F, 0x65, 0x09, 0xA4, 0xC8, 0x88, 0x9F, 0xC5, 0xD8, 0xF6, 0x86, 0x00, 0x61, 0xEA, 0xA6,
        0xCC, 0x41, 0x3C, 0xDF, 0x7A, 0x02, 0x04, 0xEF, 0xF9, 0x1E, 0xFC, 0xD3, 0x7C, 0x1A, 0x17, 0xA1,
        0x5C, 0x8A, 0x25, 0xE3, 0x78, 0x99, 0x73, 0x97, 0xFE, 0xAD, 0xAF, 0x6C, 0x82, 0xFB, 0xAA, 0x9E,
        0x0B, 0xF5, 0xBE, 0x68, 0xD9, 0x07, 0x4E, 0xE7, 0x9B, 0xAB, 0x37, 0x51, 0x8F, 0xCE, 0x46, 0x9C,
        0x58, 0x2D, 0xC9, 0xB6, 0xB4, 0x10, 0xD7, 0xE6, 0x32, 0x95, 0xCB, 0xA8, 0xDC, 0xBB, 0x29, 0x3D,
        0xEE, 0xD0, 0xE0, 0x6A, 0xCD, 0xDE, 0x2A, 0x44, 0x7F, 0xD2, 0x4D, 0x81, 0xD5, 0x0F, 0x66, 0x92,
        0x36, 0x23, 0x5B, 0x13, 0xC7, 0x20, 0x8B, 0x96, 0xC4, 0x7D, 0x35, 0x64, 0x71, 0x6E, 0x47, 0xBF,
        0x3A, 0xF2, 0xF8, 0x0D, 0xB8, 0xA3, 0x93, 0x4F, 0x5D, 0xE5, 0xE4, 0xBA, 0xCF, 0x01, 0x42, 0x21,
        0x79, 0x60, 0x7B, 0xB3, 0xEB, 0xF1, 0x6D, 0x8E, 0x2C, 0x56, 0xC3, 0xAE, 0x57, 0x69, 0x33, 0xDD,
    };

    WorldPacket data( SMSG_ADDON_INFO, 1000 );

    const AddonMgr::BannedAddonsList& bannedAddons = sAddonMgr->GetBannedAddons();

    data.WriteBits( ( uint32 )bannedAddons.size(), 18 );
    data.WriteBits( ( uint32 )m_addonsList.size(), 23 );

    for ( AddonsList::iterator itr = m_addonsList.begin(); itr != m_addonsList.end(); ++itr )
    {
        data.WriteBit( 0 ); // Has URL
        data.WriteBit( itr->Enabled );
        data.WriteBit( !itr->UsePublicKeyOrCRC ); // If client doesnt have it, send it
    }

    data.FlushBits();

    for ( AddonsList::iterator itr = m_addonsList.begin(); itr != m_addonsList.end(); ++itr )
    {
        if ( !itr->UsePublicKeyOrCRC )
        {
            size_t pos = data.wpos();
            for ( int i = 0; i < 256; i++ )
            {
                data << uint8( 0 );
            }

            for ( int i = 0; i < 256; i++ )
            {
                data.put( pos + pubKeyOrder[ i ], addonPublicKey[ i ] );
            }
        }

        if ( itr->Enabled )
        {
            data << uint8( itr->Enabled );
            data << uint32( 0 );
        }

        data << uint8( itr->State );
    }

    m_addonsList.clear();

    for ( const AddonMgr::BannedAddon & addon : bannedAddons )
    {
        data << uint32( addon.Id );
        data << uint32( 1 );        // IsBanned

        for ( uint8 i = 0; i < 8; i++ )
        {
            data << uint32( 0 );
        }

        // Those 3 might be in wrong order
        data << uint32( addon.Timestamp );
    }

    SendPacket( &data );
}

void WorldSession::SendTimezoneInformation()
{
    char timezoneString[ 256 ];

    // TIME_ZONE_INFORMATION timeZoneInfo;
    // GetTimeZoneInformation(&timeZoneInfo);
    // wcstombs(timezoneString, timeZoneInfo.StandardName, sizeof(timezoneString));

    sprintf( timezoneString, "Etc/UTC" ); // The method above cannot be used, because of non-english OS translations, so we send const data (possible strings are hardcoded in the client because of the same reason)

    size_t timezoneStringSize = strlen(timezoneString);

    WorldPacket data( SMSG_SET_TIMEZONE_INFORMATION, 2 + 2 * timezoneStringSize );

    data.WriteBits( timezoneStringSize, 7 );
    data.WriteBits( timezoneStringSize, 7 );

    data.FlushBits();

    data.WriteString( timezoneString );
    data.WriteString( timezoneString );

    SendPacket( &data );
}

void WorldSession::SendDisplayPromotion()
{
    WorldPacket data( SMSG_DISPLAY_PROMOTION, 4 );

    data << uint32( 0 );

    SendPacket( &data );
}

void WorldSession::SendGlueScreen()
{
    bool HasBattlePayShopStoreEnabled = sWorld->getBoolConfig(CONFIG_BATTLE_PAY_SHOP_ENABLED);
    bool HasBattlePayShopStoreAvailable = sWorld->getBoolConfig(CONFIG_BATTLE_PAY_SHOP_ENABLED);
    bool HasBattlePayShopStoreDisabledByParentalControl = !sWorld->getBoolConfig(CONFIG_BATTLE_PAY_SHOP_ENABLED);

    WorldPacket data( SMSG_GLUE_SCREEN_STATUS, 1 );

    data.WriteBit( HasBattlePayShopStoreEnabled );
    data.WriteBit( HasBattlePayShopStoreDisabledByParentalControl );
    data.WriteBit( HasBattlePayShopStoreAvailable );

    data.FlushBits();

    SendPacket( &data );
}

void WorldSession::SendDanceStudioCreate()
{
    // guessed
    WorldPacket data( SMSG_DANCESTUDIO_CREATE_STATUS, 4 + 4 + 4 + 4 + 1);

    data << uint32( 0x473216 );
    data << uint32( 0x0C24E5 );
    data << uint32( 0xA58757 );
    data << uint32( 0x108D59 );

    data.WriteBit(false);

    data.FlushBits();

    SendPacket( &data );
}

bool WorldSession::IsAddonRegistered( const std::string& prefix ) const
{
    if ( !_filterAddonMessages ) // if we have hit the softcap (64) nothing should be filtered
        return true;

    if ( _registeredAddonPrefixes.empty() )
        return false;

    std::vector<std::string>::const_iterator itr = std::find( _registeredAddonPrefixes.begin(), _registeredAddonPrefixes.end(), prefix );
    return itr != _registeredAddonPrefixes.end();
}

void WorldSession::HandleUnregisterAddonPrefixesOpcode( WorldPacket& /*recvData*/ )
{
    TC_LOG_DEBUG( "network", "WORLD: Received CMSG_UNREGISTER_ALL_ADDON_PREFIXES" );

    _registeredAddonPrefixes.clear();
}

void WorldSession::HandleAddonRegisteredPrefixesOpcode( WorldPacket& recvData )
{
    TC_LOG_DEBUG( "network", "WORLD: Received CMSG_ADDON_REGISTERED_PREFIXES" );

    // This is always sent after CMSG_UNREGISTER_ALL_ADDON_PREFIXES

    uint32 count = recvData.ReadBits( 24 );

    if ( count > REGISTERED_ADDON_PREFIX_SOFTCAP )
    {
        // if we have hit the softcap (64) nothing should be filtered
        _filterAddonMessages = false;
        recvData.rfinish();
        return;
    }

    std::vector<uint8> lengths( count );
    for ( uint32 i = 0; i < count; ++i )
        lengths[ i ] = recvData.ReadBits( 5 );

    recvData.FlushBits();

    for ( uint32 i = 0; i < count; ++i )
        _registeredAddonPrefixes.push_back( recvData.ReadString( lengths[ i ] ) );

    if ( _registeredAddonPrefixes.size() > REGISTERED_ADDON_PREFIX_SOFTCAP ) // shouldn't happen
    {
        _filterAddonMessages = false;
        recvData.rfinish();
        return;
    }

    _filterAddonMessages = true;
}

void WorldSession::SetPlayer( Player* player )
{
    _player = player;

    // set m_GUID that can be used while player loggined and later until m_playerRecentlyLogout not reset
    if ( _player )
        m_GUIDLow = _player->GetGUID().GetCounter();
}

void WorldSession::ProcessQueryCallbacks()
{
    _queryProcessor.ProcessReadyQueries();

    // InitializeSessionCallback
    if (_authSessionCallback.valid() && _authSessionCallback.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
        InitializeSessionCallback( reinterpret_cast<AuthSessionQueryHolder*>( _authSessionCallback.get() ) );

    //! HandlePlayerLoginOpcode
    if ( ( _charLoginCallback.valid() && _charLoginCallback.wait_for( std::chrono::seconds( 0 ) ) == std::future_status::ready )
        && ( _accountLoginCallback.valid() && _accountLoginCallback.wait_for( std::chrono::seconds( 0 ) ) == std::future_status::ready ))
        HandlePlayerLogin( reinterpret_cast<CharLoginQueryHolder*>( _charLoginCallback.get() ),  reinterpret_cast<AccountLoginQueryHolder*>( _accountLoginCallback.get() ) );

    // HandleLoadPet
    if ( _petLoginCallback.valid() && _petLoginCallback.wait_for( std::chrono::seconds( 0 ) ) == std::future_status::ready )
        HandleLoadPet( reinterpret_cast<PetLoginQueryHolder*>( _petLoginCallback.get() ) );
}

void WorldSession::InitWarden( BigNumber* k, std::string const& os )
{
    if ( os == "Win" )
    {
        _warden = new WardenWin();
        _warden->Init( this, k );
    }
    else if ( os == "OSX" )
    {
        // Disabled as it is causing the client to crash
        // _warden = new WardenMac();
        // _warden->Init(this, k);
    }
}

void WorldSession::LoadPermissions()
{
    uint32 id = GetAccountId();
    std::string name;
    AccountMgr::GetName( id, name );
    uint8 secLevel = GetSecurity();

    _RBACData = new rbac::RBACData( id, name, realmID, secLevel );
    _RBACData->LoadFromDB();

    TC_LOG_DEBUG( "rbac", "WorldSession::LoadPermissions [AccountId: %u, Name: %s, realmId: %d, secLevel: %u]",
                  id, name.c_str(), realmID, secLevel );
}

rbac::RBACData* WorldSession::GetRBACData()
{
    return _RBACData;
}

bool WorldSession::HasPermission( uint32 permission )
{
    if ( !_RBACData )
        LoadPermissions();

    bool hasPermission = _RBACData->HasPermission( permission );
    TC_LOG_DEBUG( "rbac", "WorldSession::HasPermission [AccountId: %u, Name: %s, realmId: %d]",
                  _RBACData->GetId(), _RBACData->GetName().c_str(), realmID );

    return hasPermission;
}

void WorldSession::InvalidateRBACData()
{
    TC_LOG_DEBUG( "rbac", "WorldSession::Invalidaterbac::RBACData [AccountId: %u, Name: %s, realmId: %d]",
                  _RBACData->GetId(), _RBACData->GetName().c_str(), realmID );
    delete _RBACData;
    _RBACData = NULL;
}

bool WorldSession::DosProtection::EvaluateOpcode( WorldPacket& p, time_t time ) const
{
    OpcodeTable& opcodeTable = clientOpcodeTable;

    uint32 maxPacketCounterAllowed = GetMaxPacketCounterAllowed( p.GetOpcode() );

    // Return true if there no limit for the opcode
    if ( !maxPacketCounterAllowed )
        return true;

    PacketCounter& packetCounter = _PacketThrottlingMap[ p.GetOpcode() ];
    if ( packetCounter.lastReceiveTime != time )
    {
        packetCounter.lastReceiveTime = time;
        packetCounter.amountCounter = 0;
    }

    // Check if player is flooding some packets
    if ( ++packetCounter.amountCounter <= maxPacketCounterAllowed )
        return true;

    TC_LOG_WARN( "network", "AntiDOS: Account %u, IP: %s, Ping: %u, Character: %s, flooding packet (opc: %s (0x%X), count: %u)",
                 Session->GetAccountId(), Session->GetRemoteAddress().c_str(), Session->GetLatency(), Session->GetPlayerName().c_str(),
                 opcodeTable[ p.GetOpcode() ]->Name, p.GetOpcode(), packetCounter.amountCounter );

    switch ( _policy )
    {
        case POLICY_LOG:
            return true;
        case POLICY_KICK:
        {
            TC_LOG_WARN( "network", "AntiDOS: Player kicked!" );
            Session->KickPlayer();
            return false;
        }
        case POLICY_BAN:
        {
            BanMode bm = ( BanMode )sWorld->getIntConfig( CONFIG_PACKET_SPOOF_BANMODE );
            uint32 duration = sWorld->getIntConfig( CONFIG_PACKET_SPOOF_BANDURATION ); // in seconds
            std::string nameOrIp = "";
            switch ( bm )
            {
                case BAN_CHARACTER: // not supported, ban account
                case BAN_ACCOUNT: ( void )sAccountMgr->GetName( Session->GetAccountId(), nameOrIp ); break;
                case BAN_IP: nameOrIp = Session->GetRemoteAddress(); break;
            }
            sWorld->BanAccount( bm, nameOrIp, duration, "DOS (Packet Flooding/Spoofing", "Server: AutoDOS" );
            TC_LOG_WARN( "network", "AntiDOS: Player automatically banned for %u seconds.", duration );
            Session->KickPlayer();
            return false;
        }
        default: // invalid policy
            return true;
    }
}


uint32 WorldSession::DosProtection::GetMaxPacketCounterAllowed( uint16 opcode ) const
{
    uint32 maxPacketCounterAllowed;
    switch ( opcode )
    {
        // CPU usage sending 2000 packets/second on a 3.70 GHz 4 cores on Win x64
        //                                              [% CPU mysqld]   [%CPU worldserver RelWithDebInfo]
        case CMSG_PLAYER_LOGIN:                         //   0               0.5
        case CMSG_NAME_QUERY:                           //   0               1
        case CMSG_PET_NAME_QUERY:                       //   0               1
        case CMSG_NPC_TEXT_QUERY:                       //   0               1
        case CMSG_ATTACK_STOP:                          //   0               1
        case CMSG_QUERY_TIME:                           //   0               1
        case CMSG_CORPSE_TRANSPORT_QUERY:               //   0               1
        case CMSG_MOVE_TIME_SKIPPED:                    //   0               1
        case CMSG_QUERY_NEXT_MAIL_TIME:                 //   0               1
        case CMSG_SET_SHEATHED:                         //   0               1
        case CMSG_RAID_TARGET_UPDATE:                   //   0               1
        case CMSG_LOGOUT_REQUEST:                       //   0               1
        case CMSG_PET_RENAME:                           //   0               1
        case CMSG_QUEST_GIVER_REQUEST_REWARD:           //   0               1
        case CMSG_COMPLETE_CINEMATIC:                   //   0               1
        case CMSG_COMPLETE_MOVIE:                       //   0               1
        case CMSG_BANKER_ACTIVATE:                      //   0               1
        case CMSG_BUY_BANK_SLOT:                        //   0               1
        case CMSG_OPT_OUT_OF_LOOT:                      //   0               1
        case CMSG_DUEL_PROPOSED:                        //   0               1
        case CMSG_DUEL_RESPONSE:                        //   0               1
        case CMSG_CALENDAR_COMPLAIN:                    //   0               1
        case CMSG_QUEST_QUERY:                          //   0               1.5
        case CMSG_ITEM_TEXT_QUERY:                      //   0               1.5
        case CMSG_GAMEOBJECT_QUERY:                     //   0               1.5
        case CMSG_CREATURE_QUERY:                       //   0               1.5
        case CMSG_QUEST_GIVER_STATUS_QUERY:             //   0               1.5
        case CMSG_GUILD_QUERY:                          //   0               1.5
        case CMSG_TAXI_NODE_STATUS_QUERY:               //   0               1.5
        case CMSG_TAXI_NODES_AVAILABLE_QUERY:           //   0               1.5
        case CMSG_QUEST_GIVER_QUERY_QUEST:              //   0               1.5
        case CMSG_PAGE_TEXT_QUERY:                      //   0               1.5
        case CMSG_GUILD_BANK_QUERY_TEXT:                //   0               1.5
        case CMSG_CORPSE_QUERY:                         //   0               1.5
        case CMSG_MOVE_SET_FACING:                      //   0               1.5
        case CMSG_REQUEST_PARTY_MEMBER_STATS:           //   0               1.5
        case CMSG_QUEST_GIVER_COMPLETE_QUEST:           //   0               1.5
        case CMSG_SET_ACTION_BUTTON:                    //   0               1.5
        case CMSG_RESET_INSTANCES:                      //   0               1.5
        case CMSG_HEARTH_AND_RESURRECT:                 //   0               1.5
        case CMSG_TOGGLE_PVP:                           //   0               1.5
        case CMSG_PET_ABANDON:                          //   0               1.5
        case CMSG_TAXI_ACTIVATE_EXPRESS:                //   0               1.5
        case CMSG_TAXI_ACTIVATE:                        //   0               1.5
        case CMSG_SELF_RES:                             //   0               1.5
        case CMSG_UNLEARN_SKILL:                        //   0               1.5
        case CMSG_EQUIPMENT_SET_SAVE:                   //   0               1.5
        case CMSG_EQUIPMENT_SET_DELETE:                 //   0               1.5
        case CMSG_DISMISS_CRITTER:                      //   0               1.5
        case CMSG_REPOP_REQUEST:                        //   0               1.5
        case CMSG_GROUP_INVITE:                         //   0               1.5
        case CMSG_GROUP_INVITE_RESPONSE:                //   0               1.5
        case CMSG_GROUP_UNINVITE:                       //   0               1.5
        case CMSG_GROUP_UNINVITE_GUID:                  //   0               1.5
        case CMSG_GROUP_DISBAND:                        //   0               1.5
        case CMSG_BATTLEMASTER_HELLO:                   //   0               1.5
        case CMSG_BATTLEMASTER_JOIN_ARENA:              //   0               1.5
        case CMSG_BATTLEGROUND_LEAVE:                   //   0               1.5
        case CMSG_GUILD_BANK_LOG_QUERY:                 //   0               2
        case CMSG_LOGOUT_CANCEL:                        //   0               2
        case CMSG_ALTER_APPEARANCE:                     //   0               2
        case CMSG_QUEST_CONFIRM_ACCEPT:                 //   0               2
        case CMSG_GUILD_EVENT_LOG_QUERY:                //   0               2.5
        case CMSG_READY_FOR_ACCOUNT_DATA_TIMES:         //   0               2.5
        case CMSG_QUEST_GIVER_STATUS_MULTIPLE_QUERY:    //   0               2.5
        case CMSG_BEGIN_TRADE:                          //   0               2.5
        case CMSG_INITIATE_TRADE:                       //   0               3
        case CMSG_INSPECT:                              //   0               3.5
        case CMSG_AREA_SPIRIT_HEALER_QUERY:             // not profiled
        case CMSG_STANDSTATECHANGE:                     // not profiled
        case CMSG_RANDOM_ROLL:                           // not profiled
        case CMSG_TIME_SYNC_RESP:                       // not profiled
        case CMSG_TRAINER_BUY_SPELL:                    // not profiled
        {
            // "0" is a magic number meaning there's no limit for the opcode.
            // All the opcodes above must cause little CPU usage and no sync/async database queries at all
            maxPacketCounterAllowed = 0;
            break;
        }

        case CMSG_QUEST_GIVER_ACCEPT_QUEST:             //   0               4
        case CMSG_QUEST_LOG_REMOVE_QUEST:               //   0               4
        case CMSG_QUEST_GIVER_CHOOSE_REWARD:            //   0               4
        case CMSG_CONTACT_LIST:                         //   0               5
        case CMSG_LEARN_TALENT:                         //   0               6
        case CMSG_AUTOBANK_ITEM:                        //   0               6
        case CMSG_AUTOSTORE_BANK_ITEM:                  //   0               6
        case CMSG_WHO:                                  //   0               7
        case CMSG_PLAYER_VEHICLE_ENTER:                 //   0               8
        case CMSG_LEARN_PET_SPECIALIZATION_GROUP:       // not profiled
        case CMSG_MOVE_HEARTBEAT:
        {
            maxPacketCounterAllowed = 200;
            break;
        }

        case CMSG_GUILD_SET_NOTE:                       //   1               2         1 async db query
        case CMSG_SET_CONTACT_NOTES:                    //   1               2.5       1 async db query
        case CMSG_CALENDAR_GET_CALENDAR:                //   0               1.5       medium upload bandwidth usage
        case CMSG_GUILD_BANK_QUERY_TAB:                 //   0               3.5       medium upload bandwidth usage
        case CMSG_QUERY_INSPECT_ACHIEVEMENTS:           //   0              13         high upload bandwidth usage
        case CMSG_GAMEOBJ_REPORT_USE:                   // not profiled
        case CMSG_GAMEOBJ_USE:                          // not profiled
        case CMSG_PETITION_DECLINE:                     // not profiled
        {
            maxPacketCounterAllowed = 50;
            break;
        }

        case CMSG_QUEST_POI_QUERY:                      //   0              25         very high upload bandwidth usage
        {
            maxPacketCounterAllowed = MAX_QUEST_LOG_SIZE;
            break;
        }

        case CMSG_SPELLCLICK:                           // not profiled
        case CMSG_DISMISS_CONTROLLED_VEHICLE:           // not profiled
        {
            maxPacketCounterAllowed = 20;
            break;
        }

        case CMSG_PETITION_SIGN:                        //   9               4         2 sync 1 async db queries
        case CMSG_TURN_IN_PETITION:                     //   8               5.5       2 sync db query
        case CMSG_GROUP_CHANGE_SUB_GROUP:               //   6               5         1 sync 1 async db queries
        case CMSG_PETITION_QUERY:                       //   4               3.5       1 sync db query
        case CMSG_CHAR_CUSTOMIZE:                       //   5               5         1 sync db query
        case CMSG_CHAR_FACTION_OR_RACE_CHANGE:          //   5               5         1 sync db query
        case CMSG_CHAR_DELETE:                          //   4               4         1 sync db query
        case CMSG_DEL_FRIEND:                           //   7               5         1 async db query
        case CMSG_ADD_FRIEND:                           //   6               4         1 async db query
        case CMSG_CHAR_RENAME:                          //   5               3         1 async db query
        case CMSG_GM_SURVEY_SUBMIT:                     //   2               3         1 async db query
        case CMSG_GROUP_SET_LEADER:                     //   1               2         1 async db query
        case CMSG_GROUP_RAID_CONVERT:                   //   1               5         1 async db query
        case CMSG_GROUP_ASSISTANT_LEADER:               //   1               2         1 async db query
        case CMSG_CALENDAR_ADD_EVENT:                   //  21              10         2 async db query
        case CMSG_PETITION_BUY:                         // not profiled                1 sync 1 async db queries
        case CMSG_CHANGE_SEATS_ON_CONTROLLED_VEHICLE:   // not profiled
        case CMSG_REQUEST_VEHICLE_PREV_SEAT:            // not profiled
        case CMSG_REQUEST_VEHICLE_NEXT_SEAT:            // not profiled
        case CMSG_REQUEST_VEHICLE_SWITCH_SEAT:          // not profiled
        case CMSG_REQUEST_VEHICLE_EXIT:                 // not profiled
        case CMSG_ITEM_REFUND:                          // not profiled
        case CMSG_SOCKET_GEMS:                          // not profiled
        case CMSG_WRAP_ITEM:                            // not profiled
        case CMSG_REPORT_PVP_AFK:                       // not profiled
        {
            maxPacketCounterAllowed = 10;
            break;
        }

        case CMSG_CHAR_CREATE:                          //   7               5         3 async db queries
        case CMSG_CHAR_ENUM:                            //  22               3         2 async db queries
        case CMSG_GM_TICKET_CREATE:                     //   1              25         1 async db query
        case CMSG_GM_TICKET_UPDATE_TEXT:                //   0              15         1 async db query
        case CMSG_GM_TICKET_DELETE_TICKET:              //   1              25         1 async db query
        case CMSG_GM_RESPONSE_RESOLVE:                  //   1              25         1 async db query
        case CMSG_SUPPORT_TICKET_SUBMIT_BUG:            // not profiled                1 async db query
        case CMSG_SUPPORT_TICKET_SUBMIT_SUGGESTION:     // not profiled                1 async db query
        case CMSG_SUPPORT_TICKET_SUBMIT_COMPLAIN:       // not profiled                1 async db query
        case CMSG_CALENDAR_UPDATE_EVENT:                // not profiled
        case CMSG_CALENDAR_REMOVE_EVENT:                // not profiled
        case CMSG_CALENDAR_COPY_EVENT:                  // not profiled
        case CMSG_CALENDAR_EVENT_INVITE:                // not profiled
        case CMSG_CALENDAR_EVENT_SIGNUP:                // not profiled
        case CMSG_CALENDAR_EVENT_RSVP:                  // not profiled
        case CMSG_CALENDAR_EVENT_REMOVE_INVITE:         // not profiled
        case CMSG_CALENDAR_EVENT_MODERATOR_STATUS:      // not profiled
        case CMSG_LOOT_METHOD:                          // not profiled
        case CMSG_GUILD_INVITE:                         // not profiled
        case CMSG_GUILD_ACCEPT:                         // not profiled
        case CMSG_GUILD_DECLINE:                        // not profiled
        case CMSG_GUILD_LEAVE:                          // not profiled
        case CMSG_GUILD_DISBAND:                        // not profiled
        case CMSG_GUILD_SET_GUILD_MASTER:               // not profiled
        case CMSG_GUILD_MOTD:                           // not profiled
        case CMSG_GUILD_ADD_RANK:                       // not profiled
        case CMSG_GUILD_DEL_RANK:                       // not profiled
        case CMSG_GUILD_INFO_TEXT:                      // not profiled
        case CMSG_GUILD_BANK_DEPOSIT_MONEY:             // not profiled
        case CMSG_GUILD_BANK_WITHDRAW_MONEY:            // not profiled
        case CMSG_GUILD_BANK_BUY_TAB:                   // not profiled
        case CMSG_GUILD_BANK_UPDATE_TAB:                // not profiled
        case CMSG_SET_GUILD_BANK_TEXT:                  // not profiled
        case CMSG_GUILD_EMBLEM_SAVE:                    // not profiled
        case CMSG_PETITION_RENAME:                      // not profiled
        case CMSG_SET_DUNGEON_DIFFICULTY:               // not profiled
        case CMSG_SET_RAID_DIFFICULTY:                  // not profiled
        case CMSG_RAID_READY_CHECK:                     // not profiled
        {
            maxPacketCounterAllowed = 3;
            break;
        }

        case CMSG_ITEM_REFUND_INFO:                     // not profiled
        {
            maxPacketCounterAllowed = PLAYER_SLOTS_COUNT;
            break;
        }

        default:
        {
            maxPacketCounterAllowed = 100;
            break;
        }
    }

    return maxPacketCounterAllowed;
}

void WorldSession::UpdateGuildRosterTimer(uint32 diff)
{
    if (m_guildRosterTimer <= 0)
        return;

    m_guildRosterTimer -= diff;
}

class AuthSessionQueryHolder final : public SQLQueryHolder
{
    uint32 m_accountId;

public:
    AuthSessionQueryHolder(uint32 accountId) : m_accountId(accountId) { }

    uint32 GetAccountId() const { return m_accountId; }
    bool Initialize();
};

bool AuthSessionQueryHolder::Initialize()
{
    SetSize(MAX_AUTH_SESSION_QUERY);

    bool res = true;

    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_BATTLE_PAY_SHOP);
    stmt->setUInt32(0, m_accountId);
    res &= SetPreparedQuery(AUTH_SESSION_QUERY_LOAD_BATTLE_PAY_SHOP, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_BATTLE_PAY_SHOP_PRODUCTS);
    stmt->setUInt32(0, m_accountId);
    res &= SetPreparedQuery(AUTH_SESSION_QUERY_LOAD_BATTLE_PAY_SHOP_PRODUCTS, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_RAF_DATA);
    stmt->setUInt32(0, m_accountId);
    res &= SetPreparedQuery(AUTH_SESSION_QUERY_LOAD_RAF_DATA, stmt);

    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_RAF_RECRUITS_COUNT);
    stmt->setUInt32(0, m_accountId);
    res &= SetPreparedQuery(AUTH_SESSION_QUERY_LOAD_RAF_RECRUITS_COUNT, stmt);

    return res;
}

void WorldSession::InitializeSession()
{
    AuthSessionQueryHolder* holder = new AuthSessionQueryHolder(GetAccountId());
    if (!holder->Initialize())
    {
        delete holder;
        SendAuthResponse(AUTH_REJECT, 0);
        return;
    }

    _authSessionCallback = LoginDatabase.DelayQueryHolder(holder);
}

void WorldSession::InitializeSessionCallback(AuthSessionQueryHolder* holder)
{
    if (_battlePayShop)
        _battlePayShop->LoadFromDB(holder->GetPreparedResult(AUTH_SESSION_QUERY_LOAD_BATTLE_PAY_SHOP), holder->GetPreparedResult(AUTH_SESSION_QUERY_LOAD_BATTLE_PAY_SHOP_PRODUCTS));

    if (_rafData)
        _rafData->LoadFromDB(holder->GetPreparedResult(AUTH_SESSION_QUERY_LOAD_RAF_DATA), holder->GetPreparedResult(AUTH_SESSION_QUERY_LOAD_RAF_RECRUITS_COUNT));

    if (!m_inQueue)
        SendAuthResponse(AUTH_OK, 0);
    else
        SendAuthWaitQueue(0);

    SetInQueue(false);
    ResetTimeOutTime(false);

    SendDanceStudioCreate();

    SendGlueScreen();

    SendAddonsInfo();

    SendClientCacheVersion(sWorld->getIntConfig(CONFIG_CLIENTCACHE_VERSION));

    SendTutorialsData();

    SendDisplayPromotion();

    if (_battlePayShop)
    {
        SendBattlePayShopDistributionList();
        SendBattlePayShopPurchaseList();
    }

    if (sWorld->getBoolConfig(CONFIG_RECRUIT_A_FRIEND_REWARDS_ENABLED) && _rafData && _rafData->GetRewardsCount() > 0)
        SendRaFRewards(true);

    delete holder;
}

void WorldSession::InitBattlePayShop(uint32 accountID, std::string const& accountName)
{
    _battlePayShop = new BattlePayShop(this, accountID, accountName);
}

void WorldSession::InitReferAFriendSystem(uint32 accountID)
{
    _rafData = new RaFData(accountID);
}
