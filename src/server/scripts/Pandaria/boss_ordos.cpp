/*
* Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
* Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* ScriptData
SDName: Boss_Ordos
SD%Complete: 55%
SDComment: Placeholder
SDCategory: Boss_Ordos
EndScriptData */

/* ContentData
EndContentData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"

enum Texts
{
    SAY_AGGRO         = 0, // Ordos yells: You will take my place on the eternal brazier.
    SAY_DEATH         = 1, // Ordos yells: The eternal fire will never be extinguished.
    SAY_SLAY          = 2, // Ordos yells: Up in smoke.
    SAY_ANCIENT_FLAME = 3, // Ordos yells: Your flesh will melt.
    SAY_ETERNAL_AGONY = 4, // Ordos yells: Your pain will be endless.
    SAY_POOL_OF_FIRE  = 5, // Ordos Yells: You will feel but a fraction of my agony.
    SAY_BURNING_SOUL  = 6  // Ordos Yells: Burn!
};

enum Spells
{
    SPELL_ANCIENT_FLAME     = 144695, // 40 SEC AFTER PULL
    SPELL_BURNING_SOUL      = 144689, // 20 SEC AFTER PULL // NEXT: 30 SEC LATER
    SPELL_ETERNAL_AGONY     = 144696, // ENRAGE SPELL AFTER 5 MINUTES
    SPELL_MAGMA_CRUSH       = 144688, // 10 SEC AFTER PULL // NEXT: 15 SEC LATER
    SPELL_POOL_OF_FIRE      = 144692  // 30 SEC AFTER PULL
};

enum Events
{
    EVENT_ANCIENT_FLAME    = 1,
    EVENT_BURNING_SOUL     = 2,
    EVENT_POOL_OF_FIRE     = 3,
    EVENT_MAGMA_CRUSH      = 4,
    EVENT_ETERNAL_AGONY    = 5
};

void AddSC_boss_ordos()
{
}