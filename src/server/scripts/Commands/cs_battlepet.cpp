/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePet.h"
#include "BattlePetMgr.h"
#include "Chat.h"
#include "Common.h"
#include "DB2Stores.h"
#include "Language.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "Unit.h"

class battlepet_commandscript : public CommandScript
{
public:
    battlepet_commandscript() : CommandScript("battlepet_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> battlePetLearnCommandTable =
        {
            { "all",            rbac::RBAC_PERM_COMMAND_BATTLE_PET_LEARN_ALL,   false, &HandleBattlePetLearnAll, "" },
        };

        static std::vector<ChatCommand> battlePetCommandTable =
        {
            { "level",          rbac::RBAC_PERM_COMMAND_BATTLE_PET_LEVEL,       false, &HandleBattlePetLevel,               "" },
            { "learn",          rbac::RBAC_PERM_COMMAND_BATTLE_PET_LEARN_ALL,   false, nullptr, "", battlePetLearnCommandTable },
        };

        static std::vector<ChatCommand> commandTable =
        {
            { "battlepet",      rbac::RBAC_PERM_COMMAND_BATTLE_PET,             false, nullptr, "", battlePetCommandTable },
        };

        return commandTable;
    }

    static bool HandleBattlePetLevel(ChatHandler* handler, char const* args)
    {
        if (!*args)
            return false;

        uint8 newLevel = atoi((char*)args);
        if (!newLevel)
            return false;

        // make sure a creature is selected
        Creature* creature = handler->GetSelectedCreature();
        if (!creature)
        {
            handler->PSendSysMessage(LANG_SELECT_CREATURE);
            handler->SetSentErrorMessage(true);

            return false;
        }

        // creature must be a battle pet that belongs to a player
        ObjectGuid battlePetGUID = creature->GetGuidValue(UNIT_BATTLE_PET_COMPANION_GUID);
        if (!battlePetGUID)
        {
            handler->PSendSysMessage(LANG_BATTLE_PET_NOT_PLAYER_OWNED);
            handler->SetSentErrorMessage(true);

            return false;
        }

        // if this fails there is a major problem elsewhere
        Player* player = ObjectAccessor::GetPlayer(*creature, creature->GetGuidValue(UNIT_CREATED_BY));
        ASSERT(player);

        BattlePetMgr* battlePetMgr = player->GetBattlePetMgr();
        if (!battlePetMgr)
            return false;

        // update battle pet level
        BattlePet* battlePet = battlePetMgr->GetBattlePet(battlePetGUID);
        if (!battlePet)
            return false;

        battlePet->SetLevel(newLevel);

        // alert client of change
        player->GetBattlePetMgr()->SendBattlePetUpdate(battlePet, false);

        handler->PSendSysMessage(LANG_BATTLE_PET_TARGET_LEVELED, newLevel);

        return true;
    }

    static bool HandleBattlePetLearnAll(ChatHandler* handler, char const* args)
    {
        Player* player = handler->GetSelectedPlayer();
        if (!player)
            player = handler->GetSession()->GetPlayer();

        BattlePetMgr* battlePetMgr = player->GetBattlePetMgr();
        if (!battlePetMgr)
            return false;

        for (BattlePetSpeciesEntry const* battlePetSpeciesEntry : sBattlePetSpeciesStore)
        {
            // make sure player doesn't already have a battle pet of this species
            uint16 species = battlePetSpeciesEntry->Id;
            if (battlePetMgr->GetBattlePetCount(species))
                continue;

            // add battle pet to journal
            battlePetMgr->Create(battlePetSpeciesEntry->Id, 25, sObjectMgr->GetBattlePetRandomBreedID(species), sObjectMgr->GetBattlePetRandomQuality(species), true);
        }

        return true;
    }
};

void AddSC_battlepet_commandscript()
{
    new battlepet_commandscript();
}
