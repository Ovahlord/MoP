/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 /**
 * @file main.cpp
 * @brief Authentication Server main program
 *
 * This file contains the main program for the
 * authentication server
 */

#include <ace/Dev_Poll_Reactor.h>
#include <ace/TP_Reactor.h>
#include <ace/ACE.h>
#include <openssl/opensslv.h>
#include <openssl/crypto.h>

#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>

#include "Common.h"
#include "Database/DatabaseEnv.h"
#include "Configuration/Config.hpp"
#include "Log.h"
#include "SystemConfig.h"
#include "Util.h"
#include "RealmList.h"
#include "RealmAcceptor.h"

#ifdef __linux__
#include <sched.h>
#include <sys/resource.h>
#endif
#include "AuthServer.hpp"

#ifndef _TRINITY_REALM_CONFIG
# define _TRINITY_REALM_CONFIG  "authserver.conf"
#endif

 /// Print out the usage string for this program on the console.
void usage( const char* prog )
{
    TC_LOG_INFO( "server.authserver", "Usage: \n %s [<options>]\n    -c config_file           use config_file as configuration file\n\r", prog );
}

/// Launch the auth server
extern int main( int argc, char** argv )
{
    // Command line parsing to get the configuration file name
    char const* configFile = _TRINITY_REALM_CONFIG;
    int count = 1;
    while ( count < argc )
    {
        if ( strcmp( argv[ count ], "-c" ) == 0 )
        {
            if ( ++count >= argc )
            {
                printf( "Runtime-Error: -c option requires an input argument\n" );
                usage( argv[ 0 ] );
                return 1;
            }
            else
                configFile = argv[ count ];
        }
        ++count;
    }

    if ( !Tod::GetConfig().Load( configFile, "authserver" ) )
    {
        printf( "Invalid or missing configuration file : %s\n", configFile );
        printf( "Verify that the file exists and has \'[authserver]\' written in the top of the file!\n" );
        return 1;
    }


    Tod::AuthServer server;
    server.Run();

    return 0;
}

