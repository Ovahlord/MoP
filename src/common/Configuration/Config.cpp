/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Config.hpp"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

namespace Tod
{
    bool Config::Load( const std::string& fileName, const std::string& sectionName )
    {
        m_values.clear();

        boost::property_tree::ptree root;

        try
        {
            boost::property_tree::ini_parser::read_ini( fileName, root );
        }
        catch ( ... )
        {
            return false;
        }

        auto section = root.get_child_optional( sectionName );
        if ( !section )
            return false;

        m_fileName = fileName;
        m_sectionName = sectionName;

        for ( auto & it : section.get() )
        {
            const std::string& key = it.first;
            std::string value = it.second.get_value< std::string >();

            if ( value.find_first_of( "\"" ) == 0 )
            {
                value = value.substr( 1, value.length() - 1 );
            }

            if ( value.find_last_of( "\"" ) == value.length() - 1 )
            {
                value = value.substr( 0, value.length() - 1 );
            }

            m_values[ key ] = value;
        }

        return true;
    }

    bool Config::Reload()
    {
        if ( m_fileName.empty() )
            return false;

        return Load( m_fileName, m_sectionName );
    }

    const std::string& Config::GetFilename() const
    {
        return m_fileName;
    }

    std::vector< std::string > Config::GetKeysWithPrefix( const std::string& prefix ) const
    {
        std::vector< std::string > result;

        result.reserve( m_values.size() );
        for ( auto & it : m_values )
        {
            const std::string& key = it.first;
            if ( key.find( prefix ) != 0 )
                continue;

            result.push_back( key );
        }

        return result;
    }


    Config& GetConfig()
    {
        static Config s_config;
        return s_config;
    }
}
