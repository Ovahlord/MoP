/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TOD_CONFIG_HPP_
#define _TOD_CONFIG_HPP_

#include <string>
#include <vector>
#include <unordered_map>

#include <boost/lexical_cast/try_lexical_convert.hpp>

namespace Tod
{
    class Config
    {
    public:
        bool                            Load( const std::string& fileName, const std::string& sectionName );
        bool                            Reload();

        const std::string&              GetFilename() const;
        std::vector< std::string >      GetKeysWithPrefix( const std::string& prefix ) const;

        template< typename T >
        const T                         Get( const std::string& key, const T & def )
        {
            auto it = m_values.find( key );
            if ( it == m_values.end() )
                return def;

            T result;
            if ( boost::conversion::try_lexical_convert< T >( it->second, result ) )
                return result;

            return def;
        }

    private:
        std::string                     m_fileName;
        std::string                     m_sectionName;

        typedef std::unordered_map< std::string, std::string > KeyValueMap;
        KeyValueMap                     m_values;
    };

    Config& GetConfig();
}

#endif /* _TOD_CONFIG_HPP_ */
